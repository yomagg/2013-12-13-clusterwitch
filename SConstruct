"""
	@file SConstruct
	Created on: @date Dec 18, 2013
	@author Magdalena Slawinska aka Magic Magg
	@email magg dot gatech at gmail.com
"""
import os
import socket
import sys
import getpass
import subprocess

# here are variables we use in our build system
# they are set in set_build_variables

# this should work for if evpath was compiled with gcc complier
# in the older version of evpath it was required to have gen_thread specified as a library
# apparently is not required any longer
EVPATH = { 
           'ROOT' : None,
           'lib' : ['evpath', 'cercs_env', 'atl', 'ffs', 'dill' ] # the name of the library (-l)           
         }


# glib20 headers are installed in a few directories,
# if all the headers are in the same directory just set this
# to the same directory, scons should remove the redundant directories
#GLIB20 = { 
#          'ROOT' : None,
#          'lib'         : 'glib-2.0' # the name of the library (-l)           
#          }

# this is for GCC, the C++ compiler
GCC = { 
        'CC' : 'g++',
        'CCFLAGS' :  ['-fPIC', '-Wall', '-Wextra', '-Wno-unused-parameter', '-std=c++11'],
#        'CCDEBUGFLAGS' : ['-ggdb', '-g', '-DDEBUG'],
        'CCDEBUGFLAGS' : ['-ggdb', '-g'],
        'CCOPTFLAGS' : ['-O3']    
      }

# the C compiler
MY_CC = { 
        'CC' : 'gcc',
        'CCFLAGS' :  ['-fPIC', '-Wall', '-Wextra', '-Wno-unused-parameter', '-std=gnu99'],
#        'CCDEBUGFLAGS' : ['-ggdb', '-g', '-DDEBUG'],
        'CCDEBUGFLAGS' : ['-ggdb', '-g'],
        'CCOPTFLAGS' : ['-O3']    
      }


NVML = {
	'lib'  : ['nvidia-ml'],
	'LIB'  : None,
	'INC'  : None,
	'ccflags' : '-DNVML_PRESENT',
	'yes_nvml': None
	}
# where is the installation directory (default installation directory)
INSTALL_DIR = os.getcwd() + '/build'

# where the cpp unit test library is located
GTEST = {
	'ROOT' : 'deps/gtest-1.7.0'
}

SQLITE3 = {
		'ROOT' : '../../devel/external/sqlite3',
		'lib'  : ['sqlite3']
}

'''
Help("""
   $ scons -Q   # builds quietly the program
   $ scons -c   # cleans the program

   # run with the debug compilation options enabled [default is disabled]
   $ scons --fdbg
   
   # to generate the extra documentation; it will be installed in the docs dir
   # if not present the documentation will be not installed
   $ scons --doc

   # no NVML
   $ scons --no-nvml
      
   # to specify the installation directory (other than a default one)
   scons --prefix=/tmp 
 
   # to build and install in a default directory (distro)
   # installation within a default directory and with debug enabled
   scons --fdbg install
 
   # installation within a specified directory and with debug enabled
   scons --fdbg --prefix=/tmp install
  
   NOTE1: if you forget to add 'install', there will be no installation

   NOTE2: to uninstall files, you need to provide the directory and install
   command to tell scons that it should use paths used during installation,
   e.g. if
   scons --fdbg --prefix=/tmp install
      then
   scons -c --prefix=/tmp install

   For defined configurations see function
          
   set_build_variables()
    """)
'''    
Help("""
   scons -Q   # builds quietly the program with default parameters
   scons -c   # cleans the program

   # to specify the root path to evpath run
   scons --evpath=/home/user/evpath
              
   # to specify the installation directory (other than a default one)
   scons --prefix=/tmp 
 
   # to build and install in a default directory (build)
   # installation within a default directory
   scons install
 
   # installation within a specified directory and with debug enabled
   scons --prefix=/tmp install
  
   # typical installation in the default dir with a specific evpath dir
   scons --evpath=/home/magg/evpath install
   
   # typical installation in the specified dir with a specific evpath dir
   scons --evpath=/home/magg/evpath  --prefix=/home/magg/cw install
   
   
   NOTE1: if you forget to add 'install', there will be no installation

   NOTE2: to uninstall files, you need to provide the directory and install
   command to tell scons that it should use paths used during installation,
   e.g. if
   scons --prefix=/tmp install
      then
   scons -c --prefix=/tmp install

   NOTE3: currently CW is build only with debug option enabled
    """)


# -------------------------------------------
# the actual build
# -------------------------------------------

#AddOption('--fdbg', action='store_true', dest='DEBUG',  
#          help='if present the debug compilation options will be enabled', default=False)
AddOption('--prefix', action='store', dest='install_dir', type='string', 
          help='if present it is the directory where the distro will be installed', default=INSTALL_DIR)
#AddOption('--doc', action='store_true', dest='opt_doc',
#		  help='if present the extra documentation will be generated', default=False)
AddOption('--no-nvml', action='store_true', dest='nvml_no',
	  help='if present the gpu support will NOT be compiled', default=False)
AddOption('--evpath', action='store', dest='evpath_root', type='string',
		help='if present it is the directory where the evpath installation root directory can be found', 
		default='/rock/opt/evpath')
AddOption('--nvml-inc-path', action='store', dest='nvml_INC',
	help='if present the nvml support will be compiled and it should contain the path to nvml.h',
	default='/export1/local/0/cuda/CUDAToolsSDK/NVML')
# for pastec /usr/lib64
AddOption('--nvml-lib-path', action='store', dest='nvml_LIB',
        help='indicates where is the LIB path to the nvml library',
	default='../../devel/external/libs')
	  
# specific settings
EVPATH['ROOT'] = GetOption('evpath_root')

NVML['INC'] = GetOption('nvml_INC')
NVML['LIB'] = GetOption('nvml_LIB')
NVML['yes_nvml'] = not GetOption('nvml_no')


# configure
BASE_ENV=Environment(
    CPPPATH = ['../include', '../../../lynx/lynx-1.0.0/lynx/instrumentation/interface'],
    CCFLAGS = GCC['CCFLAGS'],
    CXX = GCC['CC']
    )      
    
BASE_ENV.Append(CCFLAGS = GCC['CCDEBUGFLAGS'])
INSTALL_DIR = GetOption('install_dir') 

# to enable installing outside of the top-level directory; installing
# is still considered a type of file 'build.' The default behavior of
# Scons is to build files in or below the current directory
BASE_ENV.Alias('install', INSTALL_DIR)

# the base environment for the CC
# the base environment for compiling with gcc
BASE_ENV_CC = Environment(
    CPPPATH = ['../include', ],
    CCFLAGS = MY_CC['CCFLAGS'],
    CC = MY_CC['CC']
    )  

BASE_ENV_CC.Append(CCFLAGS = MY_CC['CCDEBUGFLAGS'])
BASE_ENV_CC.Alias('install', INSTALL_DIR)


# export variables to other scripts
Export(
       'EVPATH',
       'GCC',
       'INSTALL_DIR',
       'NVML',       
       'BASE_ENV',
       'BASE_ENV_CC', 
       'GTEST', 
       'SQLITE3')
       
SConscript([ 'src/body/SConstruct'
		])

    
