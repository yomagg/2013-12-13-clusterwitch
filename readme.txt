# @file readme.txt
# @date Created on Dec 13, 2013
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

DESCRIPTION
===========
This is the monitoring software, called ClusterWitch. 
The aim is to provide a flexible and fast monitoring 
infrastructure that allows for configurable and contrallable monitoring.

The messaging implementation is built on top of EVPATH, a messaging constructor
layer that has been developed at Georgia Tech for over 15 years.

The predecessor of ClusterWitch is ClusterWatch.

The architecture of CW is based on the concepts of atoms that are connected
in the data flow graph. The white atoms collect the events on nodes,
the gray atoms pass the events to other nodes with possible additional
processing, and the black atoms consume the events. It is advised that
there should be one collector for each monitoring event per physical node.

Currently (2014-09-15) there are two flavors: asynchronous spies and
synchronous spies. The synchronous spies are being actively developed, 
whereas asynchronous spies are getting abandoned. White atoms own
spies of different kinds which are capable of collection monitoring
events realted to cpu utilization, memory, basic gpu usage, and networking.
The white atoms periodically execute the routine to gather the monitoring
data and sent the data to the wired gray atoms that pass the data to
the black atoms. The monitoring data are in two flavors: raw
(i.e., data read directly from the data source with no data processing involved) 
or processed data (e.g., utilization per cpu core). The black atom upon
receipt of the data stores them in the relavant Sqlite3 db 
(one database per spy type, i.e., all
cpu data is persistently stored in the one, cpu.db file, all mem data
in one mem.db file, and so on.)

The persistent storage is supported via "readers" that periodically store 
the data into the SQlite3 database. The name "reader" is historical, as in
the past the code was used to read the data from the memory mapped file
and then writing the data to the database. 

Currently there are two versions of topologies: EVPath DFG (Data Flow Graph)
and non-DFG. Please take into account that the non-dfg topology 
is being abandoned, and currently only DFG topology is being developed.

In both topologies the communication substrate is realized via 
the EVPath transport. Here you can find the information about EVPath

http://www.cc.gatech.edu/systems/projects/EVPath/

http://www.cc.gatech.edu/systems/projects/EVPath/doxygen/

DFG TOPO
=====

This is based on the EVPath DFGs and it should support the multi-tier,
hierarchical, and scalable monitoring. It uses the EVPath DFG construct
so the DFG is deployed and the DFG is realized by joining appropriate
nodes by source stones and sink stones. This version is actively developed
(as of 2014-09-15). The DFG needs to be described somehow, and 
now it is described in a bunch of files that specify the names
of the DFG nodes, source stones and sink stones belonging to the nodes, 
and their wiring/linkage.


Non-dfg
=======
The collector process needs to know the contact information of 
the aggregator process (ranger).
At present that information is disseminated via a common file by taking advantage
of the shared file system. The aggregator writes its contact info to the
file that is read by the collectors. It is certainly possible to configure
each collector node separately by providing an individual configuration file.

In an overview, the aggregator creates EVPath stones per spy type 
upon which it gets all information related to a particular type of a spy. 
The collector process creates a single bridge stone per spy which is 
connected to the aggregator
(known through the well-known contact info specified in the ini file).

Each enabled spy first sends a message about resource capabilities such
as the number of cores, the amount of available memory and 
its unique identifier. Subsequent messages are actual monitoring data.

At present, CW implements cpu, mem, network (gpu and Lynx spies are coming).  
Monitoring of cpu, mem, network is based on the /proc file system whereas 
gpu will be realized via NVML (NVIDIA Management Library to manage gpu devices).
The Lynx spy will be built upon the modified version of the GPU Lynx software.

This topology is being slightly abandoned (as of 2014-08-12).
The topology has not been touched (as of 2014-09-15).


DEPENDENCIES
============
* Scons - the building system for the Dwarf module

   http://www.scons.org/ (general information)
   In particular, the version I am using is 
   http://sourceforge.net/projects/scons/files/scons/2.2.0/
 
   Installation:
   scons-2.2.0 $ python setup.py install --prefix=$HOME/opt/bin

* EVPath - this is the software available at Georgia Tech, the system for 
  creating the flexible messaging. Here you can find the information about 
  EVPath

  http://www.cc.gatech.edu/systems/projects/EVPath/

  In particular, if you want to know more about EVPath here is the source

  http://www.cc.gatech.edu/systems/projects/EVPath/doxygen/index.html
  
  Please see APPENDIX for some installation guidelines.

  More information technical information how to write a new spy can be
  found in Programming Guide in docs/prog_guide/pg.tex (you need to run pdflatex pg.tex to 
  get pg.pdf).

  NOTE (2014-09-15): EVPath with VERSION_TAG=development required (new DFG API) 
    
GETTING SOURCES
===============

Bitbucket
---------
You might be able to access the bitbucket repo (if I didn't make it private);
master branch is relatively stable or at least some complete features
should be implemented. There is also a devel branch that  I am using for active 
development. 

There are also 'cw-xxx' branches that at least have been tested somewhere.
The other option is a corresponding zip file in the download section.

# cloning the repository
$ git clone https://bitbucket.org/yomagg/2013-12-13-clusterwitch.git cw

Kaos SVN
--------
This is not very frequently updated. However, it is also possible to
use GeorgiaTech repository to obtain the latest (uploaded) release:

http://svn.research.cc.gatech.edu/kaos/keeneland/software/releases/clusterwatch/

user: anon
passwd: anon

Look for cw-blah-blah.zip


BUILD
=====

$ cd 2013-12-13-clusterwitch

# this builds and installs all dependencies for CW; run this
# only once (during the first build)
$ ./build_deps.sh 1

# I have scons installed under this location; make sure scons is visible
$ export PATH=/home/magg/opt/adbf/bin/:$PATH

# you may check what are the options
$ scons --help

# build and install in the default dir; provide location of evpath in your env
$ scons --evpath=/home/magg/opt/adbf
.....
# After it finishes see the content of build

$ ls build
bin include  lib  tests

$ ls build/tests
dfg_client  test_agg                    test_ctrl_worker       test_mon
dfg_master  test_c_api                  test-cw-splitters.txt  test_split_recv
links.txt   test_c_api_dfg              test_ev_master         test_split_send
logs        test_c_api_spy_agent_utils  test_ev_worker         stones.txt
nodes.txt   test_ctrl_master            test_main



TESTING BUILD
==============

There are three scenarios

1. Regression tests - checks if the internal functions of CW work as 
                      expected
2. Basic test -
    - dfg: run ./dfg_master, then ./dfg_client
     
    - non-dfg run aggregator on one node and the monitor 
            on the second node
3. C-API test - test if C-API works correctly
    - dfg: test_c_api_dfg, test_c_api_spy_agent_utils
    - non-dfg: test_c_api       

* REGRESSION TESTS
----------------

$ hostname
compute-4-00.local.255.10.in-addr.arpa

# this is where evpath is
$ export LD_LIBRARY_PATH=/home/magg/opt/adbf/lib:$LD_LIBRARY_PATH

# this is where cw library lives
$ export LD_LIBRARY_PATH=/home/magg/opt/adbf/src/2013-12-13-clusterwitch/build/lib:$LD_LIBRARY_PATH

# this is the basic tests
$ ./test_main
...
[----------] Global test environment tear-down
[==========] 54 tests from 21 test cases ran. (2146 ms total)
[  PASSED  ] 54 tests.
$

You might see Segmentation fault message at the end of the test report.
I have not figured out what is wrong, however, my suspicion is that
EVPath is not terminated correctly in my tests.

* BASIC TESTS 
--------------------

DFG
----
The dfg configuration files need to be in the same directory as executables.
The system can be configured through the file. You can use the tool

 build/bin/dfg-ini-create

to create a cw.ini file with default variables that you can
change appropriately.  If CW can't find cw.ini file
(it should be in the same directory as executables dfg_master and dfg_client), 
it takes default values, so existence of the cw.ini file is not a requirement.
I am using a very simple reading external library so be gentle to the ini file 
and do not expect too much from it.

BE WARNED: if you run ./test_main this file will be removed by tests so plan
your tests accordingly. You can always recreate the cw.ini file by running
build/bin/dfg-ini-create.

First run the dfg_master on the first node
$ ./dfg_master

Assuming your topology is a->b->c, you have to run 
the CW black atom (sink node) on the DFG node c 
$ ./dfg_client b c

The CW gray atom (sink and source node) on the DFG node b
$ ./dfg_client g b 

The CW white atom (source node) on the DFG node c
$ ./dfg_client w a

Currently dfg_client runs all of available spies, so remember about
this when you construct the description of the DFG. You can always change
the dfg_client.cpp source to get a desired behavior.

* C_API TESTS
-----------

DFG
-----
There are two tests that test the current C API

- test_c_api_spy_agent_utils - tests utility functions used in the test_c_api_dfg
  it requires the presence of the test-cw-splitters.txt file
$ ./test_c_api_spy_agent_utils 

- test_c_api_dfg - tests the current C API

  First start your CW dfg_master, dfg_client. The a->b->c DFG is
  assumed. Then run
  $ ./test_c_api_dfg
  It will read the splitter contact file, create output stones 
  on splitter hooks, and get the events cpu and mem events from node a,
  and net and nvml events from b. You can compare the events with
  the events stored in spy databases for a quick visual test.

NOTES
=====

* If debugging code related to EVPath, it might be useful to turn on to get
more verbose output from EVPath

CMDataVerbose=1

* If format server doesn't work, 

CMSelfFormats=1 might help

* sqlite3 mem-xxx.db

sqlite> .table
MEM_CAP_TAB  MEM_MON_TAB
sqlite> PRAGMA table_info(MEM_MON_TAB);
sqlite> select * from MEM_MON_TAB;


INTERNAL DEPENDENCIES
=====================
These are dependencies that are included. You don't need to
worry about them. They are internal to CW and you don't need
to do anything about them

* gtest (included), for C++ unit testing
* easylogging++ (included), for logging (http://easylogging.org/)
* INIxxx.h for parsing ini files (included) (http://code.google.com/p/feather-ini-parser/)
* sqlite3 (included)
* my-nvml.h (included) this is a dirty hack, since on PASTEC (the machine
  I develop with nvidia the header file is outdated; it should be
  nvml.h (original)

INTERNAL DEPENDENCIES BUILD
===========================
The build is now supported by the ./build_deps.sh script
and includes all below steps that build dependencies.

* build gtest (included)

gtest-1.7.0> 
g++ -isystem include -I. -pthread -c src/gtest-all.cc
ar -rv libgtest.a gtest-all.o
cp libgtest.a ../../devel/external/libs

* build sqlite3 (included)

> pwd
/rock/synchrobox/proj/w-ecl/2013-12-13-git-clusterwitch/deps/sqlite-autoconf-3080301
./configure --prefix=/rock/synchrobox/proj/w-ecl/2013-12-13-git-clusterwitch/devel/external/sqlite3
> make
> make install

CONTACT
=======

If you need some help or have some comments please contact:
Magdalena Slawinska at magg dot gatech at gmail.com

Enjoy!

APPENDIX
=========

HISTORY
========
2014-09-16
* Removed some info about non-DFG version from this file.
* Added the Super Spy that is the main spy running synchronously spies 
  (ie. with the same timestamp)

2014-08-12 
* Decided to abandon the non-dfg version. DFG is much more fun and
  opportunities.

INSTALLING SOME OF DEPENDENCIES
===============================
* Installing EVPATH (on my laptop) (2014-08-27)

$ svn co http://svn.research.cc.gatech.edu/kaos/chaos_base/trunk/ chaos_base
$ cd chaos_base
$ chmod 755 chaos_build.pl
$ cp build_config build_config.marulin

%   INSTALL_DIRECTORY 
%   this is variable substituted inside perl script.
%   possible values are:
%	$ARCH -- the result of `cercs_arch` or the -a flag,
%	$hostname  -- the host where the build is performed
%	$HOME  -- the $HOME environment variable value during the build
%	$project  --  the project name (like "ffs", "evpath", etc.)
%	$repository  --  like "kaos" or "github"
%INSTALL_DIRECTORY=$HOME
INSTALL_DIRECTORY=/opt/evpath-dev

%   VERSION_TAG  -  various values, including "development", "stable", etc.
%
%VERSION_TAG=stable
VERSION_TAG=development

%BUILDLIST
% project	repository
dill		kaos
cercs_env	kaos
atl		kaos
ffs		kaos
enet		eisengithub
nnti		eisengithub
evpath		kaos
%echo2		kaos
%comm_group	kaos
%cmrpc		kaos
%pds		kaos
%gs		kaos
%lgs		kaos
%libbench	kaos

%REPOSITORIES
%
% database of archive names, source code tool and repo spec values to use
%
% format for each line:  <archive-name>  <tool> <value for CVSROOT>
kaos svn http://svn.research.cc.gatech.edu/kaos/$project/trunk
kaosarch svn http://svn.research.cc.gatech.edu/kaos/$project/trunk
eisengithub git https://github.com/eisenhauer/$project.git

% special flags
DISABLE_TESTING
%ENABLE_CDASH_SUBMIT
%ENABLE_CDASH_VALGRIND

%  uncomment below to set results files dir (default /tmp)
% RESULTS_FILES_DIR=/tmp/build_results

%  build master directory specificiation
%BUILD_AREA=$HOME/nightly_build_area
BUILD_AREA=/opt/synchrobox/proj/w-ecl/evpath-dev/evpath-build

%CONFIGURE/CMAKE ARGUMENTS
%
% format for each line:  <archive-name>  cmake/configure <arguments>
%
%   configure arguments are variable substituted inside perl script.
%   possible values are: 
%	$ARCH -- the result of `cercs_arch` or the -a flag,
%	$hostname  -- the host where the build is performed
%	$HOME  -- the $HOME environment variable value during the build
%	$project  --  the project name (like "ffs", "evpath", etc.)
%	$repository  --  like "kaos" or "github"
kaos configure
kaos cmake
eisengithub
eisengithub
%

# this is probably not important any more (2014-09-15) to have mpi 
# on the path
$ module list
Currently Loaded Modules:
  1) openmpi/1.8.1

$ time perl chaos_build.pl -a marulin
......
real	2m29.245s
user	1m16.929s
sys	0m12.519s


$ ls /opt/evpath-dev/
bin/     include/ lib/

$ ls /opt/evpath-dev/bin/
atom_check     attr_dump      FFSdump        format_dump    format_test
atom_server    attr_test      FFSsort        format_info    
atom_test      FFScp          format_cmd     format_server

* Installing EVPATH (on GT PASTEC cluster)
  - you need to have a decent version of cmake (not too old, I tested with 2.8.4 and above; it complained for 2.8.3)
$ svn co http://svn.research.cc.gatech.edu/kaos/chaos_base/trunk/ chaos_base

$ cd chaos_base

# make sure this is executable
$ chmod 755 chaos_build.pl

$ cp build_config build_config.pastec
$ vi build_config.pastec
...
$ cat build_config.pastec

%BUILDLIST
% project       repository
dill            kaos
cercs_env       kaos
gen_thread      kaos
atl             kaos
ffs             kaos
%enet           eisengithub
%nnti           eisengithub
evpath          kaos
%soapstone      kaos
%echo2          kaos
%comm_group     kaos
%cmrpc          kaos
%pds            kaos
%gs             kaos
%lgs            kaos
%amaze          kaos
%libbench       kaos

%REPOSITORIES
%
% database of archive names, source code tool and repo spec values to use
%
% format for each line:  <archive-name>  <tool> <value for CVSROOT>
kaos svn http://svn.research.cc.gatech.edu/kaos/$project/trunk
kaosarch svn http://svn.research.cc.gatech.edu/kaos/$project/trunk
eisengithub git https://github.com/eisenhauer/$project.git

% special flags
DISABLE_TESTING
%ENABLE_CDASH_SUBMIT
%ENABLE_CDASH_VALGRIND

%  uncomment below to set results files dir (default /tmp)
% RESULTS_FILES_DIR=/tmp/build_results

%  build master directory specificiation
%BUILD_AREA=$HOME/nightly_build_area
BUILD_AREA=$HOME/opt/adbf/evpath-build

%   INSTALL_DIRECTORY 
%   this is variable substituted inside perl script.
%   possible values are:
%       $CERCS_ARCH -- the result of `cercs_arch`,
%       $hostname  -- the host where the build is performed
%       $HOME  -- the $HOME environment variable value during the build
%       $project  --  the project name (like "ffs", "evpath", etc.)
%       $repository  --  like "kaos" or "github"
%INSTALL_DIRECTORY=$HOME
INSTALL_DIRECTORY=$HOME/opt/adbf

%CONFIGURE/CMAKE ARGUMENTS
%
% format for each line:  <archive-name>  cmake/configure <arguments>
%
%   configure arguments are variable substituted inside perl script.
%   possible values are: 
%       $CERCS_ARCH -- the result of `cercs_arch`,
%       $hostname  -- the host where the build is performed
%       $HOME  -- the $HOME environment variable value during the build
%       $project  --  the project name (like "ffs", "evpath", etc.)
%       $repository  --  like "kaos" or "github"
kaos configure
kaos cmake
eisengithub
eisengithub
%
$ cmake --version
cmake version 2.8.4


# build
$ module list
Currently Loaded Modulefiles:
  1) modules            2) mag/opt-bin        3) mag/cmake/2.8.10

$ time perl chaos_build.pl -a pastec
real	1m57.841s
user	1m0.535s
sys	0m17.536s

# build is here
$ pwd
 /home/magg/opt/adbf

#
# if something went wrong look in /tmp for log files

# to test the installation run - 2014-02-26 I think this 
# is not installed currently on PASTEC
$ module list
Currently Loaded Modulefiles:
  1) modules            3) mag/cmake/2.8.10
  2) mag/opt-bin        4) mag/evpath

# run format server test

$ format_test
Format server identifier is 4c453645
format is 1f2012e0
Remote context ID is : <ID ver=2, unused 0, rep_len 148, hash1 632e5390, hash2 649a108d>
Hex is : 0x02, 0x00, 0x00, 0x37, 0x99, 0x46, 0x83, 0x144, 0x100, 0x154, 0x16, 0x141, 
Local context ID is :  <ID ver=2, unused 0, rep_len 148, hash1 632e5390, hash2 649a108d>


TROUBLESHOOTING
===============
2014-03-13 (TEMP_SOLVED:2014-03-13) Running on PASTEC: test_agg on c4-00, and test_mon on c4-02,
           I couldn't run test_mon on c4-02 because  
           
           I got the error:
c4-00> ./test_mon blah blah           
modprobe: ERROR: could not insert 'nvidia': No such device
NVIDIA: failed to load the NVIDIA kernel module.
ERROR cw::Diag cw::Nvml_Acquisitor::init_caps():src/body/spies/nvml_spy/nvml_spy.cpp:404 Driver Not Loaded

Temporary solution: run mon on c4-02, and agg on c4-00. 

# EOF
