#!/bin/bash
# @file build_deps.sh
# @date Created on Feb 26, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

VERSION="1.0"

# bad arguments
let E_BADARGS=65

if [ $# -eq 0 ]; then
	echo "USAGE: $(basename $0)  dummy-param"
	echo
	echo "DESCRIPTION: builds the internal dependencies for subsequent CW builds"
	echo "Should be executed during initial CW build"             
 	echo
	echo "dummy-param just to make sure that it is possible to see this help"
	echo "EXAMPLE: $(basename $0)  1"
	exit $E_BADARGS
fi

# determine what is the sourcebasedir for this installation
SRCDIR=$(pwd)


# prepare the necessary structure assumed by build
# -----------> creation of directories
# check if the dir exists and if not then create it
# $_ is a copy of the argument from the previous command
test -d devel/external || mkdir -p $_/libs

# -----------------------
# build gtest
# ----------------------
pushd deps > /dev/null


# -----------> creation of gtest-1.7.0 dir
test -d gtest-1.7.0 || unzip $_.zip

pushd gtest-1.7.0 > /dev/null   # deps/gtest

g++ -isystem include -I. -pthread -c src/gtest-all.cc
ar -rv libgtest.a gtest-all.o
cp libgtest.a ../../devel/external/libs

echo "INSTALL: gtest: SUCCESS"

popd > /dev/null # gtest

# -------------------
# build sqlite3
# ------------------
# I should be in deps

test -d sqlite-autoconf-3080301 || tar xfvz $_.tar.gz
pushd sqlite-autoconf-3080301 > /dev/null # deps/sqlite

./configure --prefix=$SRCDIR/devel/external/sqlite3
make 
make install

echo "INSTALL: sqlite3: I think: SUCCESS"
popd > /dev/null # sqlite

popd > /dev/null # deps
exit 0

# EOF



