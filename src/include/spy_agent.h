/**
 *  @file   spy_agent.h
 *
 *  @date   Created on: Feb 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_AGENT_H_
#define SPY_AGENT_H_

#include "evpath.h"

#include "debug.h"

/**
 * TODO this is redundancy with the C code
 * look in transport.cpp for Ev_Monitoring_Atom::MON_ATOM_CONTACT_FILE
 * and the corresponding monitoring atoms for each type of the resource
 * the value should be identical because this is the file where
 * the monitoring atom outputs the contact information about
 * the splitter stone available
 * @see EV_Monitoring_Atom::setup_split_ntw_infrastructure();
 * @see Ev_Cpu_Monitoring_Atom::Ev_Cpu_Monitoring_Atom
 * @see Ev_Mem_Monitoring_Atom::Ev_Mem_Monitoring_Atom ...
 *
 * @see Cfg::CFG_CPU_MON_ATOM_FILE in src/body/misc.cpp
 * @see Cfg::CFG_MEM_MON_ATOM_FILE in src/body/misc.cpp
 * ...
 *
 */
#define Ev_Cpu_Monitoring_Atom_MON_ATOM_CONTACT_FILE "cw-cpu-mon-contact.txt"
#define Ev_Mem_Monitoring_Atom_MON_ATOM_CONTACT_FILE "cw-mem-mon-contact.txt"
#define Ev_Net_Monitoring_Atom_MON_ATOM_CONTACT_FILE "cw-net-mon-contact.txt"
#define Ev_Nvml_Monitoring_Atom_MON_ATOM_CONTACT_FILE "cw-nvml-mon-contact.txt"
//#define Ev_Lynx_Monitoring_Atom_MON_ATOM_CONTACT_FILE "cw-lynx-mon-contact.txt"

/**
 * TODO redundancy with the C++ code. This is the default name for the contact
 * files. Please @see Cfg::CFG_SPLITTERS_FILE_DEFAULT_NAME. Ideally there
 * should be only one name; right now I have two of them. Partially this is the
 * fun of working with codes written in C and C++. Anyway it should be
 * done smarter. No excuse.
 */
#define DFG_MON_SPLITTER_CONTACT_FILE "cw-splitters.txt"
/**
 * These are the diagnostic codes
 */
typedef enum diag_ {
	DIAG_CW_OK = 0,   //!< DIAG_OK
	DIAG_CW_ERR = 1,//!< DIAG_CW_ERR
	DIAG_CW_EVPATH_ERR = 10,  //! indicates the error related to EVPath
	DIAG_CW_UNINITIALIZED = 20, //! CW has been not initialized
	DIAG_CW_RAGENT_COUNT_EXCEEDED = 21, //! The number registered ragents has been exceeded
	DIAG_CW_CONTACT_KEY_NOT_FOUND = 30  //! the key was not found
} diag_t;

/**
 * handler type for the EVPath events
 */
typedef int (*handler_t)(CManager cm,  void *vevent, void *client_data, attr_list attrs);

/**
 * Types of the supported monitoring events
 */
enum cw_MON_EVENT {
	cw_MON_EVENT_CPU,//!< cw_MON_EVENT_CPU
	cw_MON_EVENT_MEM,//!< cw_MON_EVENT_MEM
	cw_MON_EVENT_NET, //!< cw_MON_EVENT_NET
	cw_MON_EVENT_NVML //! the nvml monitoring event
	//,
	//cw_MON_EVENT_LYNX,
	//cw_MON_EVENT_ADBF
};

/**
 * Descriptor for the monitoring event
 */
struct cw_mon_event {
	//! the identifier of the event type
	enum cw_MON_EVENT id;

	//! the pointer to the function that will be handling
	//! the monitoring events incoming from CW
	handler_t handler;
	//! parameter for the EVPath handler
	void *client_data;
	//! parameter for EVPath handler
	attr_list attrs;

	//! the contact node name as specified in the dfg topology text files
	//! this field is ignored in the non-dfg application
	char contact_node[100];
	//! the contact node stone name on that node from which the monitoring
	//! events will be received through this api
	//! this field is ignored in the non-dfg application
	char contact_stone[100];
};

#ifdef __cplusplus
extern "C" {
#endif

extern CManager cw_cm;

extern diag_t cw_init();
extern diag_t cw_nondfg_get_mon_events(struct cw_mon_event *p_event);
extern diag_t cw_get_mon_events(struct cw_mon_event *p_event);
extern diag_t cw_monitor(int secs);
extern diag_t cw_close();

#ifdef __cplusplus
}
#endif

#include "spy_events.h"
#include "spy_events_desc.h"

#include "cpu_spy_events.h"
#include "cpu_spy_events_desc.h"

#include "mem_spy_events.h"
#include "mem_spy_events_desc.h"

#include "net_spy_events.h"
#include "net_spy_events_desc.h"

// it shouldn't be required to put it into NVML_PRESENT #ifdef
// as it only installed the handler and doesn't do anything with the
// NVML library - maybe this is required anyway
#ifdef NVML_PRESENT
#include "nvml_spy_events.h"
#include "nvml_spy_events_desc.h"
#endif

#include "spy_agent_utils_types.h"
#include "spy_agent_utils.h"

#endif /* SPY_AGENT_H_ */
