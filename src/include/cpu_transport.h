/**
 *  @file   cpu_transport.h
 *
 *  @date   Created on: Jan 17, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CPU_TRANSPORT_H_
#define CPU_TRANSPORT_H_

#include "transport.h"
#include "cpu_spy.h"
#include "sqlite3_reader.h"

namespace cw {

/**
 * The base class for monitoring cpu; it should have all functionality
 * for the local monitoring; it should not have anything related to the
 * evpath
 */
class Cpu_Mon_Atom {

public:
	Cpu_Mon_Atom() = default;
	virtual ~Cpu_Mon_Atom() = default;

	Cpu_Spy *get_spy();
	Cpu_Sqlite3_Reader * get_sqlite3_reader();

protected:
	//! provides the monitoring functionality
	Cpu_Spy spy;

	//! the sqlite3 reader
	Cpu_Sqlite3_Reader sqlite3_reader;
};


/**
 * This atom is responsible for organizing a CpuSpy to enable
 * sending data from the place where data are collected to the
 * other place, i.e., to the aggregator
 *
 * TODO make it inherit after Cpu_Mon_Atom
 *
 */
class Ev_Cpu_Monitoring_Atom : public Ev_Monitoring_Atom {
public:
	Ev_Cpu_Monitoring_Atom();
	virtual ~Ev_Cpu_Monitoring_Atom();

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Cpu_Spy *get_spy();

protected:
	//! my spy
	Cpu_Spy *p_spy;

	Diag send_cap();
	Diag send_mon();
};

/**
 * The Cpu Aggregator Atom
 */
class Ev_Cpu_Aggregator_Atom : public Ev_Aggregator_Atom{
public:
	Ev_Cpu_Aggregator_Atom() = default;
	virtual ~Ev_Cpu_Aggregator_Atom();

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Cpu_Sqlite3_Reader * get_sqlite3_reader();
	Cpu_Spy *get_spy();

protected:
	vector<ev_cpu_cap> caps;

	//! the sqlite3 reader
	Cpu_Sqlite3_Reader sqlite3_reader;

	//! the spy for the processing function
	Cpu_Spy spy;

};

// ------------------------
// for DFG topology
// ------------------------
/**
 * TODO There is redundancy with Ev_Cpu_Monitoring_Atom that should
 * be at some point removed but I am skipping this for now
 * this is source
 */
//class Ev_Dfg_Cpu_White_Atom : public Ev_Dfg_White_Atom, public Mon_Atom<Cpu_Spy, Cpu_Sqlite3_Reader> {
class Ev_Dfg_Cpu_White_Atom : public Ev_Dfg_White_Atom, public Cpu_Mon_Atom {
public:
	Ev_Dfg_Cpu_White_Atom()=default;
	virtual ~Ev_Dfg_Cpu_White_Atom()=default;
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag run();
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};
/**
 * This is the aggregator-terminator functions (sink)
 */
//class Ev_Dfg_Cpu_Black_Atom : public Ev_Dfg_Black_Atom, public Mon_Atom<Cpu_Spy, Cpu_Sqlite3_Reader> {
class Ev_Dfg_Cpu_Black_Atom : public Ev_Dfg_Black_Atom, public Cpu_Mon_Atom {
public:
	Ev_Dfg_Cpu_Black_Atom();
	virtual ~Ev_Dfg_Cpu_Black_Atom();
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag quit_topo();
};
/**
 * This is a Reader and a Writer (sink and source)
 */
//class Ev_Dfg_Cpu_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Mon_Atom<Cpu_Spy, Cpu_Sqlite3_Reader>{
class Ev_Dfg_Cpu_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Cpu_Mon_Atom {
public:
	Ev_Dfg_Cpu_Gray_Atom() ;
	virtual ~Ev_Dfg_Cpu_Gray_Atom() ;
	virtual Diag reg(string node_name, string sink_name, string src_name);
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};

}  // namespace cw



#endif /* CPU_TRANSPORT_H_ */
