/**
 *  @file   nvml_spy_events.h
 *
 *  @date   Created on: Feb 27, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NVML_SPY_EVENTS_H_
#define NVML_SPY_EVENTS_H_

#ifdef __cplusplus
#include "spy_events.h"
namespace cw {
#endif // __cplusplus

/**
 * describes the capabilities of the device
 */
struct ev_nvml_dev_cap {
	char *product_name;
	//! This number matches the serial number physically printed on each board.
	//! It  is  a  globally unique immutable alphanumeric value.
	char *serial_number;
	//! the compute mode
	int compute_mode;
	//! an id assigned to this device by us
	int id;
	//! memory total in bytes
	unsigned long long mem_total_bytes;
	//! power limit in milliwatts
	unsigned int power_limit_mW;
	//! max clocks in MHz
	unsigned int max_graphics_clock_MHz;
	//! max clocks in MHz
	unsigned int max_SM_clock_MHz;
	//! max clocks in MHz
	unsigned int max_memory_clock_MHz;
#ifdef __cplusplus
	ev_nvml_dev_cap();
	~ev_nvml_dev_cap();
	ev_nvml_dev_cap(const ev_nvml_dev_cap &other);
	ev_nvml_dev_cap & operator=(const ev_nvml_dev_cap & other);
	char* copy_str(const char *src);
	void free_str(char *dst);
protected:
	void my_copy(const ev_nvml_dev_cap & other);
#endif // __cplusplus
};

/**
 * These are capabilities of the NVIDIA device
 */
struct ev_nvml_cap {
	//! the identity of the node where the gpu devices are attached
	struct identity id;
	//! the timestamp when we took the data
	long long int ts;
	//! the driver version  80-characters
	char *driver_ver;
	//! how many gpus are attached to the node
	unsigned int	 gpu_count;
	//! the array of devices
	struct ev_nvml_dev_cap *devs;
#ifdef __cplusplus
	ev_nvml_cap();
	ev_nvml_cap(const ev_nvml_cap &other);
	ev_nvml_cap & operator=(const ev_nvml_cap & other);

	int my_copy(const ev_nvml_cap & other);
	void my_free();
#endif // __cplusplus
};

/**
 * The monitoring data on the device
 * perf stats
 */
struct ev_nvml_dev_mon {
	//! performance state of the GPU
	int performance_state;

	//! as reported by Memory Usage->Used; it is strange but the
	//! Used+Free < Total in Memory Usage
	unsigned long long mem_used_bytes;

	//! Percentage of time over the past second during which one or more kernels was executing on the GPU
	//! @see nvmlUtilization_t gpu
	unsigned int nvml_util_gpu;

	//! Percentage of time over the past second during which global (device) memory was being read or written.
	//! @see nvmlUtilization_t mem
	unsigned int nvml_util_mem;

	//! Utilization->Memory in percentage
	float util_mem;
	//! Power Draw / Power Limit (in percentage)
	float power_draw;
	//! Clock Graphics / Max Clock Graphics
	float graphics_clock;
	float sm_clock;
	float mem_clock;
#ifdef __cplusplus
	ev_nvml_dev_mon();
#endif // __cplusplus
};
/**
 * The structure that will be sent from a collector to an aggregator with monitoring
 * data
 *
 * the monitored data per gpu device; this corresponds to what I can get from
 * nvml
 *
 */
struct ev_nvml_mon {
	//! the id of the node where the measurement is going to be
	int id;

	//! the timestamp of the measurement
	long long int ts;
	//! the number of gpu devices attached to a node
	unsigned int gpu_count;

	//! the array of gpus where the gpu_count is the number
	//! devices
	struct ev_nvml_dev_mon * gpu_arr;

#ifdef __cplusplus
	ev_nvml_mon();
	~ev_nvml_mon();
	ev_nvml_mon(const ev_nvml_mon &other);
	ev_nvml_mon & operator=(const ev_nvml_mon & other);

	void my_copy(const ev_nvml_mon & other);
	void my_free();
#endif // __cplusplus
};


#ifdef __cplusplus
}   // namespace
#endif // __cplusplus


#endif /* NVML_SPY_EVENTS_H_ */
