/**
 *  @file   nvml_spy.h
 *
 *  @date   Created on: Feb 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NVML_SPY_H_
#define NVML_SPY_H_

#include "spy.h"
#include "transport.h"  // Ev_Msg
#include "nvml_spy_events.h"  //cap and monitoring

#include "my-nvml.h"

namespace cw {

extern std::ostream & operator<<(std::ostream & os, const ev_nvml_dev_cap& rec);
extern std::ostream & operator<<(std::ostream & os, const ev_nvml_cap& rec);
extern std::ostream & operator<<(std::ostream & os, const ev_nvml_dev_cap& rec);
extern std::ostream & operator<<(std::ostream & os, const ev_nvml_dev_mon& rec);
extern std::ostream & operator<<(std::ostream & os, const ev_nvml_mon& rec);


class Nvml_Acquisitor {
public:
	Nvml_Acquisitor() = default;
	~Nvml_Acquisitor();
	Diag get_caps(ev_nvml_cap & rec);
	Diag get_mon(ev_nvml_mon & rec);

	static const std::string UNSPPRTED_SERIAL_NO;
	static const unsigned int UNSPPRTED_POWER_LIMIT;
	static const unsigned int UNSPPRTED_MAX_CLOCK;

protected:
	//! the device capabilities
	ev_nvml_cap caps;

	//! the device monitoring data
	ev_nvml_mon mon;

	Diag init_caps();
	bool are_caps_initialized();
	bool is_mon_initialized();

};

/**
 * This is the nvml spy
 */
class Nvml_Spy : public Spy {
public:
	Nvml_Spy() = default;
	virtual ~Nvml_Spy() = default;

	virtual Diag setup_for_read();
	Diag read_mon();
	ev_nvml_cap * get_caps();
	ev_nvml_mon * get_mon();
	virtual Diag clean_after_read();

	virtual Diag setup_for_process();
	virtual Diag process(const ev_nvml_cap & rec);
	virtual Diag process(const ev_nvml_mon & rec);
	virtual Diag clean_after_process();

	virtual Diag set_caps_ts(long long int ts);
	virtual Diag set_mon_ts(long long int ts);

protected:
	Nvml_Acquisitor acq;
	ev_nvml_cap caps;
	ev_nvml_mon mon;
};

/**
 * The representation of the dev capabilities; apparently it is important
 * to register the format
 */
class Ev_Nvml_Dev_Cap_Msg : public Ev_Msg {
public:
	static Ev_Nvml_Dev_Cap_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Nvml_Dev_Cap_Msg *p_msg;

	Ev_Nvml_Dev_Cap_Msg() = default;
	~Ev_Nvml_Dev_Cap_Msg() = default;

	Ev_Nvml_Dev_Cap_Msg(const Ev_Nvml_Dev_Cap_Msg&) = delete;
	Ev_Nvml_Dev_Cap_Msg & operator=(const Ev_Nvml_Dev_Cap_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the cap message
 */
class Ev_Nvml_Cap_Msg :public Ev_Msg {
public:
	static Ev_Nvml_Cap_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Nvml_Cap_Msg *p_msg;

	Ev_Nvml_Cap_Msg() = default;
	~Ev_Nvml_Cap_Msg() = default;

	Ev_Nvml_Cap_Msg(const Ev_Nvml_Cap_Msg&) = delete;
	Ev_Nvml_Cap_Msg & operator=(const Ev_Nvml_Cap_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the mon message
 */
class Ev_Nvml_Mon_Dev_Msg :public Ev_Msg {
public:
	static Ev_Nvml_Mon_Dev_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Nvml_Mon_Dev_Msg *p_msg;

	Ev_Nvml_Mon_Dev_Msg() = default;
	~Ev_Nvml_Mon_Dev_Msg() = default;

	Ev_Nvml_Mon_Dev_Msg(const Ev_Nvml_Mon_Dev_Msg&) = delete;
	Ev_Nvml_Mon_Dev_Msg & operator=(const Ev_Nvml_Mon_Dev_Msg&) = delete;

	Diag reg();
};


/**
 * The representation of the mon message
 */
class Ev_Nvml_Mon_Msg :public Ev_Msg {
public:
	static Ev_Nvml_Mon_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Nvml_Mon_Msg *p_msg;

	Ev_Nvml_Mon_Msg() = default;
	~Ev_Nvml_Mon_Msg() = default;

	Ev_Nvml_Mon_Msg(const Ev_Nvml_Mon_Msg&) = delete;
	Ev_Nvml_Mon_Msg & operator=(const Ev_Nvml_Mon_Msg&) = delete;

	Diag reg();
};

} // end of cw


#endif /* NVML_SPY_H_ */
