/**
 * @file  super_transport.h
 * @date  Sep 4, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#ifndef SUPER_TRANSPORT_H_
#define SUPER_TRANSPORT_H_

#include "transport.h"
#include "super_spy.h"
#include "sqlite3_reader.h"

namespace cw {
// -----------------------------
// DFG topology
// -----------------------------

// -------------------------------------
// Ev_Dfg_Super_White_Atom
// -------------------------------------
/**
 * This class will send the events to DFG
 */
class Ev_Dfg_Super_White_Atom : public Ev_Dfg_White_Atom, public Mon_Atom<Super_Spy, Super_Sqlite3_Reader>{
public:
	Ev_Dfg_Super_White_Atom() = default;
	virtual ~Ev_Dfg_Super_White_Atom() = default;
	virtual Diag reg(string node_name, stone_names & names);
	virtual Diag run();
	Diag submit();

protected:

	template <typename EV_MON_MSG_TYPE>
	Diag reg_spy(string node_name, string stone_name);
};

/**
 * Registers a message format in EVPath and joins the DFG topology
 *
 * @param node_name name of the DFG node
 * @param stone_name name of the stone registering in that node
 * @return Diag::OK if everything fine
 *         != Diag::OK otherwise
 */
template <typename EV_MON_MSG_TYPE>
Diag Ev_Dfg_Super_White_Atom::reg_spy(string node_name, string stone_name) {

	// currently only sending monitoring information
	auto *p_msg = EV_MON_MSG_TYPE::get_instance();
	auto p_formats = p_msg->get_format_list();
	return join_dfg_node(node_name, stone_name, p_formats);
}
// --------------------------------------------------------------
// Ev_Dfg_Super_Black_Atom
// --------------------------------------------------------------
/**
 * Class responsible for absorbing events sent by the white atoms
 */
class Ev_Dfg_Super_Black_Atom : public Ev_Dfg_Black_Atom, public Mon_Atom<Super_Spy, Super_Sqlite3_Reader>{
public:
	Ev_Dfg_Super_Black_Atom() = default;
	virtual ~Ev_Dfg_Super_Black_Atom() = default;

	virtual Diag reg(string node_name, stone_names & names);

protected:
	// TODO ugly and redundant with Ev_Dfg_xxx_white_Atom::reg()
	// but here as it is
	template <typename EV_MON_MSG_TYPE>
	Diag reg_spy(string node_name, string stone_name, EVSimpleHandlerFunc handler);
};
/**
 * registers the message formats with EVPath and joins the DFG nodes
 *
 * @param node_name The name of the DFG node to join
 * @param stone_name The stone name that will be instantiated on the node node_name
 * @param handler The handler for incoming events
 * @return Diag::OK everything fine
 *         != Diag::OK otherwise
 */
template <typename EV_MON_MSG_TYPE>
Diag Ev_Dfg_Super_Black_Atom::reg_spy(string node_name, string stone_name, EVSimpleHandlerFunc handler) {
	auto *p_msg = EV_MON_MSG_TYPE::get_instance();
	auto p_formats = p_msg->get_format_list();
	return join_dfg_node(node_name, stone_name, p_formats, handler, this);
}
/**
 * The handler for incoming events templated with the message type, spy type
 * and sqlite3 reader type
 *
 * It processes the event and writes the event out to the relevant db
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return Diag::OK if everything went fine
 *         != Diag::OK otherwise
 */
template <typename EV_MON_MSG_TYPE, typename SPY_TYPE, typename SQLITE3_READER_TYPE>
Diag dfg_super_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	Diag diag = Diag::OK;
	EV_MON_MSG_TYPE *event = static_cast<EV_MON_MSG_TYPE*>(vevent);
	auto *atom = static_cast<Ev_Dfg_Super_Black_Atom*>(client_data);

	SPY_TYPE *spy = nullptr;
	int index = 0;

	// find the relevant spy
	spy = atom->get_spy()->get_spy<SPY_TYPE>(&index);

	if( spy ){
		if ( (diag = spy->process(*event)) != Diag::OK){
			LOG(ERROR) << "Got error from processing. Returning";
			return diag;
		}
		auto readers_vec = static_cast<Super_Sqlite3_Reader*>(atom->get_sqlite3_reader())->get_sqlite3_readers();
		auto *r = dynamic_cast<SQLITE3_READER_TYPE*>(readers_vec.at(index));
		r->insert_mon_rec(event);
	} else {
		LOG(WARNING) << "Can't find the appropriate spy for this task. Reporting and ignoring";
		diag = Diag::WARN;
	}

	return diag;
}
// ---------------------------------------------
// Ev_Dfg_Super_Gray_Atom
// ---------------------------------------------
/**
 * This is the class that passes the events from the white atoms to black atoms
 */
class Ev_Dfg_Super_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Mon_Atom<Super_Spy, Super_Sqlite3_Reader>{
public:
	Ev_Dfg_Super_Gray_Atom() = default;
	virtual ~Ev_Dfg_Super_Gray_Atom() = default;

	virtual Diag reg(string node_name, stone_names & src_names, stone_names & sink_names);
	virtual Diag quit_topo();
protected:
	template <typename EV_MON_MSG_TYPE>
	Diag reg_spy(string node_name, string sink_name, string src_name, EVSimpleHandlerFunc handler);

};
/**
 * The handler for the each spy that submits the events to the source
 * and splitter hook
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return Diag::OK everything fine
 *         != Diag::OK otherwise
 */
template <typename EV_MON_MSG_TYPE, typename SPY_TYPE>
Diag dfg_super_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	Diag diag = Diag::OK;

	auto *event = static_cast<EV_MON_MSG_TYPE*> (vevent);
	auto atom = static_cast<Ev_Dfg_Super_Gray_Atom*>(client_data);

	int index = 0;
	auto spy = atom->get_spy()->get_spy<SPY_TYPE>(&index);

	// write the event to the graph
	if (spy){
		auto src = atom->get_src().at(index);

		if( EVclient_source_active(src) ){
			EVsubmit(src, event, nullptr);
			// send the same event to the splitter hook in case
			// the external entity is interested in this event
			EVsubmit(atom->get_splitter_hook().at(index)->get_src(), event, nullptr);
		} else {
			LOG(ERROR) << "The client source stone not active";
			diag = Diag::ERR;
		}
	} else {
		LOG(ERROR) << "Can't get the correct spy";
		diag = Diag::ERR;
	}
    return diag;
}
/**
 * Registers the spy in DFG
 *
 * @param node_name
 * @param sink_name
 * @param src_name
 * @param handler
 * @return Diag::OK everything is fine
 *         != Diag::OK otherwise
 */
template <typename EV_MON_MSG_TYPE>
Diag Ev_Dfg_Super_Gray_Atom::reg_spy(string node_name, string sink_name, string src_name, EVSimpleHandlerFunc handler){
	Diag diag = Diag::OK;
	// currently only sending monitoring information
	auto *p_msg = EV_MON_MSG_TYPE::get_instance();
	auto p_formats = p_msg->get_format_list();

	// TODO apparently the below join_topo has been shadowed
	if( (diag = Ev_Dfg_Gray_Atom::join_dfg_node( node_name, src_name, sink_name, p_formats, handler, this)) != Diag::OK){
		LOG(DEBUG) << "Node: " << node_name << " Sink: " << sink_name << " Source: " << src_name << " registering: FAILURE";
		return diag;
	} else {
		LOG(DEBUG) << "Node: " << node_name << " Sink: " << sink_name << " Source: " << src_name << " registering: SUCCESS";
	}

	return diag;
}

} // cw namespace
#endif /* SUPER_TRANSPORT_H_ */
