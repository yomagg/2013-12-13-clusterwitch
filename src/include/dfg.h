/**
 *  @file   dfg.h
 *
 *  @date   Created on: Mar 27, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 *
 *  Reused Aditi's file
 */
#ifndef DFG_H_
#define DFG_H_

#include <map>

#include "evpath.h"
#include "ev_dfg.h"

using namespace std;

struct join_info{
	char* node_name;
	char* stone_name;
	FMStructDescList simple_format_list;

	EVdfg node_dfg;
	EVmaster master;
	EVclient client;
};
typedef struct join_info join_info;

struct sink_join_info : join_info{
	EVSimpleHandlerFunc handler_func;
	void * client_data;
	//! needed for EVclient_assoc(_local), can be null
	EVclient_sinks sink_caps;
};
typedef struct sink_join_info sink_join_info;

struct source_join_info : join_info{
	EVsource src;
	//! needed for EVclient_assoc(_local), can be null
	EVclient_sources source_caps;
};
typedef struct source_join_info source_join_info;

struct node_info
{
	char* name;
	int num_src;
	int num_sink;
	std::map<string,EVdfg_stone> source_list;
	std::map<string,EVdfg_stone> sink_list;

};
typedef struct node_info node_info;

struct stone_info
{
	EVdfg_stone stone_id;
	int src_count;
};
typedef struct stone_info stone_info;

struct DFG
{
	//! holds the handle for the managing the virtual stones
	EVdfg dfg;
	//! master of the dfg
	EVmaster master;
	//! dfg client
	EVclient client;

	char **nodes;
	//! the contact to the master dfg; the general contact point
	//! for this DFG
	char* dfg_contact;
	const char* master_node;
	std::map<string,node_info> name_node_map;
	std::map<string,stone_info> all_stone_map;
	std::map<string,string>stone_to_node_map;
};
typedef struct DFG DFG;

//! the name of the node file
#define NODES_FNAME "nodes.txt"
//! the name of the stones file
#define STONES_FNAME "stones.txt"
//! the name of the links file
#define LINKS_FNAME "links.txt"


//! diagnostic information
#define DIAG_OK  0
#define DIAG_ERR 1

//! the name of the master contact file
#define MASTER_CONTACT "master-contact.txt"


int dfg_reg_source(CManager cm,source_join_info *src_info);
int dfg_reg_sink(CManager cm,sink_join_info *sink_info);

int dfg_join(CManager cm, join_info * info, EVclient_sources src_caps,
		EVclient_sinks sink_caps);
DFG* dfg_create(CManager cm);
int dfg_shutdown(EVclient client);


#endif /* DFG_H_ */
