/**
 *  @file   cpu_spy_events_desc.h
 *
 *  @date   Created on: Feb 18, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CPU_SPY_EVENTS_DESC_H_
#define CPU_SPY_EVENTS_DESC_H_

#ifdef __cplusplus
using namespace cw;
#endif

#include "spy_events_desc.h"  // include identity description
#include "cpu_spy_events.h"

// ------------------------------
// the cap structures
// ------------------------------
static FMField cpu_cap_field_list[] = {
		{"ts", "integer", sizeof(long long int), FMOffset(struct ev_cpu_cap *, ts)},
		{"id", "identity", sizeof(struct identity), FMOffset(struct ev_cpu_cap *, id)},
		{"core_count", "integer", sizeof(int), FMOffset(struct ev_cpu_cap *, core_count)},
		{NULL, NULL, 0, 0}
};

static FMStructDescRec cpu_cap_format_list[] = {
		{"ev_cpu_cap", cpu_cap_field_list, sizeof(struct ev_cpu_cap), NULL},
		{"identity", id_field_list, sizeof(struct identity), NULL},
		{NULL, NULL, 0, NULL}
};

// ------------------------------
// the cpu monitoring structures
// ------------------------------
static FMField cpu_mon_field_list[] = {
	{"id", "integer", sizeof(int), FMOffset(struct ev_cpu_mon *, id)},
	{"ts", "integer", sizeof(long long int), FMOffset(struct ev_cpu_mon *, ts)},
	{"core_plus_one_count", "integer", sizeof(int), FMOffset(struct ev_cpu_mon *, core_plus_one_count)},
	{"cpu_and_core_usage", "float[core_plus_one_count]", sizeof(float), FMOffset(struct ev_cpu_mon *, cpu_and_core_usage)},
	{NULL, NULL, 0, 0}
};

static FMStructDescRec cpu_mon_format_list[] = {
		{"ev_cpu_mon", cpu_mon_field_list, sizeof(struct ev_cpu_mon), NULL},
		{NULL, NULL, 0, NULL}
};



#endif /* CPU_SPY_EVENTS_DESC_H_ */
