/**
 *  @file   mem_spy.h
 *
 *  @date   Created on: Feb 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef MEM_SPY_H_
#define MEM_SPY_H_

#include "spy.h"
#include "transport.h"  // Ev_Msg
#include "mem_spy_events.h"  //ev_mem_cap and ev_mem_mon

namespace cw {

// ----------------- internal structure

extern std::ostream& operator<<(std::ostream& os, const ev_mem_cap& caps);
extern std::ostream& operator<<(std::ostream& os, const ev_mem_mon& caps);


/**
 * This is the source acquisitor of the monitoring and cap data
 */
class Mem_Acquisitor : public Proc_Acquisitor {
public:
	Mem_Acquisitor();
	~Mem_Acquisitor() = default;
	Diag get_caps(ev_mem_cap & caps);
	Diag get_mon(ev_mem_mon & rec);
protected:
	/**
	 *
	 * I want to get MemTotal and SwapTotal from this:
	 *
	 * [smagg@kidlogin2 ~]$ cat /proc/meminfo
	MemTotal:     24676588 kB
	MemFree:      14231836 kB
	Buffers:        347832 kB
	Cached:        6896608 kB
	SwapCached:          0 kB
	Active:        3027484 kB
	Inactive:      5733040 kB
	HighTotal:           0 kB
	HighFree:            0 kB
	LowTotal:     24676588 kB
	LowFree:      14231836 kB
	SwapTotal:     2031608 kB
	SwapFree:      2031412 kB
	Dirty:             616 kB
	Writeback:           0 kB
	AnonPages:     1515648 kB
	Mapped:         116600 kB
	Slab:          1322840 kB
	PageTables:      69572 kB
	NFS_Unstable:        0 kB
	Bounce:              0 kB
	CommitLimit:  14369900 kB
	Committed_AS:  3866964 kB
	VmallocTotal: 34359738367 kB
	VmallocUsed:    336896 kB
	VmallocChunk: 34359401183 kB
	HugePages_Total:     0
	HugePages_Free:      0
	HugePages_Rsvd:      0
	Hugepagesize:     2048 kB
	*/

	//! the total memory in KB
	long mem_total;
	//! the total swap memory in KB
	long swap_total;
};


/**
 * This is the memory spy
 */
class Mem_Spy : public Spy {
public:
	Mem_Spy() = default;
	virtual ~Mem_Spy() = default;

	virtual Diag setup_for_read();

	Diag read_mon();
	ev_mem_cap * get_caps();
	ev_mem_mon * get_mon();

	virtual Diag clean_after_read();

	virtual Diag setup_for_process();
	virtual Diag process(const ev_mem_cap & rec);
	virtual Diag process(const ev_mem_mon & rec);
	virtual Diag clean_after_process();

	virtual Diag set_caps_ts(long long int ts);
	virtual Diag set_mon_ts(long long int ts);

protected:
	//! this will provide all necessary data
	Mem_Acquisitor acq;
	//! this will store the mem capabilities
	ev_mem_cap caps;
	//! this is a monitoring record
	ev_mem_mon mon;

	Diag init_caps();
};

/**
 * The representation of the cap message
 */
class Ev_Mem_Cap_Msg :public Ev_Msg {
public:
	static Ev_Mem_Cap_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Mem_Cap_Msg *p_msg;

	Ev_Mem_Cap_Msg() = default;
	~Ev_Mem_Cap_Msg() = default;

	Ev_Mem_Cap_Msg(const Ev_Mem_Cap_Msg&) = delete;
	Ev_Mem_Cap_Msg & operator=(const Ev_Mem_Cap_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the mon message
 */
class Ev_Mem_Mon_Msg :public Ev_Msg {
public:
	static Ev_Mem_Mon_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Mem_Mon_Msg *p_msg;

	Ev_Mem_Mon_Msg() = default;
	~Ev_Mem_Mon_Msg() = default;

	Ev_Mem_Mon_Msg(const Ev_Mem_Mon_Msg&) = delete;
	Ev_Mem_Mon_Msg & operator=(const Ev_Mem_Mon_Msg&) = delete;

	Diag reg();
};

} // namespace cw
#endif /* MEM_SPY_H_ */
