/**
 *  @file   cw_runtime.h
 *
 *  @date   Created on: Dec 30, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CW_RUNTIME_H_
#define CW_RUNTIME_H_

#include <iostream>

#include "evpath.h"

#include "cpu_transport.h"
#include "mem_transport.h"
#include "net_transport.h"
#ifdef NVML_PRESENT
#include "nvml_transport.h"
#endif

#include "dfg.h"

using namespace std;
namespace cw {

/**
 * The runtime operations
 */
class CW_Runtime {
public:
	// some public constants
	//! the value of that will be inserted if NAN value is detected; should be a float
	const static float IN_CASE_NAN;

	static CW_Runtime *get_instance();
	static bool exists();

protected:
	//! this will store my singleton instance
	static CW_Runtime *p_cw_rt;


	CW_Runtime() = default;
	virtual ~CW_Runtime() = default;

	CW_Runtime(const CW_Runtime&) = delete;
	CW_Runtime & operator=(const CW_Runtime&) = delete;
};


/**
 * The runtime operations
 *
 * TODO not sure what is the connection with the CW_Runtime
 */
class Ev_CW_Mon_Runtime {
public:

	static Ev_CW_Mon_Runtime *get_instance();
	static bool exists();

	Diag read_aggregator_contact_list(const string & cfg_file_name);

	Diag send_agg_stone_request(Atom_Type type);

	Diag add_atom(Ev_Cpu_Monitoring_Atom &atom);
	Diag add_atom(Ev_Mem_Monitoring_Atom &atom);
	Diag add_atom(Ev_Net_Monitoring_Atom &atom);
#ifdef NVML_PRESENT
	Diag add_atom(Ev_Nvml_Monitoring_Atom &atom);
#endif

	Diag set_agg_stone_for_cpu_atom(EVstone agg_stone);
	Diag set_agg_stone_contact_for_cpu_atom(const string contact);

	Diag set_agg_stone_for_mem_atom(EVstone agg_stone);
	Diag set_agg_stone_contact_for_mem_atom(const string & contact);

	Diag set_agg_stone_for_net_atom(EVstone agg_stone);
	Diag set_agg_stone_contact_for_net_atom(const string & contact);

#ifdef NVML_PRESENT
	Diag set_agg_stone_for_nvml_atom(EVstone agg_stone);
	Diag set_agg_stone_contact_for_nvml_atom(const string & contact);
#endif

protected:
	//! this will store my singleton instance
	static Ev_CW_Mon_Runtime *p_cw_rt;

	//! the stone to enable receiving the  contact information from the aggregator
	EVstone contact_stone;

	//! necessary for sending the initial connection
	CMConnection conn;

	//! stores the aggregator contact list to be able to send the alive message
	string aggreg_contact_list;

	//! Those pointers are required for setting the aggregator
	//! stone contact when the appropriate message arrives
	//! not very clever but I don't know how to deal with this at the moment
	Ev_Cpu_Monitoring_Atom *p_cpu_mon_atom;

	Ev_Mem_Monitoring_Atom *p_mem_mon_atom;

	Ev_Net_Monitoring_Atom *p_net_mon_atom;

#ifdef NVML_PRESENT
	Ev_Nvml_Monitoring_Atom *p_nvml_mon_atom;
#endif

	Ev_CW_Mon_Runtime();
	virtual ~Ev_CW_Mon_Runtime() = default;

	Ev_CW_Mon_Runtime(const Ev_CW_Mon_Runtime&) = delete;
	Ev_CW_Mon_Runtime & operator=(const Ev_CW_Mon_Runtime&) = delete;

	Diag init();
};
class Ev_Cpu_Aggregator_Atom;
#ifdef NVML_PRESENT
class Ev_Nvml_Aggregator_Atom;
#endif

/**
 * The runtime operations
 *
 * TODO not sure what is the connection with the CW_Runtime
 */
class Ev_CW_Agg_Runtime {
public:

	static Ev_CW_Agg_Runtime *get_instance();
	static bool exists();

	EVstone get_cpu_aggreg_stone();
	EVstone get_mem_aggreg_stone();
	EVstone get_net_aggreg_stone();
#ifdef NVML_PRESENT
	EVstone get_nvml_aggreg_stone();
#endif

protected:
	//! this will store my singleton instance
	static Ev_CW_Agg_Runtime *p_cw_rt;

	//! there are will be created when needed
	Ev_Cpu_Aggregator_Atom *p_cpu_atom;
	Ev_Mem_Aggregator_Atom *p_mem_atom;
	Ev_Net_Aggregator_Atom *p_net_atom;
#ifdef NVML_PRESENT
	Ev_Nvml_Aggregator_Atom *p_nvml_atom;
#endif

	Ev_CW_Agg_Runtime();
	virtual ~Ev_CW_Agg_Runtime() = default;

	Ev_CW_Agg_Runtime(const Ev_CW_Agg_Runtime&) = delete;
	Ev_CW_Agg_Runtime & operator=(const Ev_CW_Agg_Runtime&) = delete;

	Diag init();
};
// ------------------------------------------------
// Ev_Dfg_CW_Master_Runtime
// ------------------------------------------------
/**
 * The class for creating the logical structure of the DFG
 */
class Ev_Dfg_CW_Master_Runtime {
public:
	static Ev_Dfg_CW_Master_Runtime *get_instance();
	static bool exists();

	Diag create_dfg();

protected:
	//! this will store my singleton instance
	static Ev_Dfg_CW_Master_Runtime *p_cw_rt;

	Ev_Dfg_CW_Master_Runtime();
	virtual ~Ev_Dfg_CW_Master_Runtime() = default;

	Ev_Dfg_CW_Master_Runtime(const Ev_Dfg_CW_Master_Runtime&) = delete;
	Ev_Dfg_CW_Master_Runtime & operator=(const Ev_Dfg_CW_Master_Runtime&) = delete;


	//! the master of the dfg
	EVmaster master;

	//! the constructor of the physical DFG infrastructure
	EVdfg dfg_mgr;
};

// -------------------------------------------------------
// Ev_Dfg_CW_Client_Runtime
// -------------------------------------------------------
/**
 * This is responsible for sending out the events, i.e.,
 * writing.
 * TODO I am not sure if I need separate runtimes for the Reader
 * Writer, and ReaderWriter part; so far it seems I don't it
 */
class Ev_Dfg_CW_Client_Runtime  {
public:
	static Ev_Dfg_CW_Client_Runtime *get_instance();
	static bool exists();

	Diag run_dfg(const string & node_name);
	EVclient get_client();
	Diag shutdown_dfg();


protected:
	//! this will store my singleton instance
	static Ev_Dfg_CW_Client_Runtime *p_cw_rt;

	Ev_Dfg_CW_Client_Runtime();
	virtual ~Ev_Dfg_CW_Client_Runtime() = default;

	Ev_Dfg_CW_Client_Runtime(const Ev_Dfg_CW_Client_Runtime&) = delete;
	Ev_Dfg_CW_Client_Runtime & operator=(const Ev_Dfg_CW_Client_Runtime&) = delete;

	//! the dfg client
	EVclient client;
};

// --------------------------------------------------------
// Ev_Dfg_Synchro_Master_RT
// -------------------------------------------------------
/**
 * The class for creating the logical structure of the DFG
 */
class Ev_Dfg_Synchro_Master_RT {
public:
	static Ev_Dfg_Synchro_Master_RT *get_instance();
	static bool exists();

	Diag create_dfg();

protected:
	//! this will store my singleton instance
	static Ev_Dfg_Synchro_Master_RT *p_cw_rt;

	Ev_Dfg_Synchro_Master_RT();
	virtual ~Ev_Dfg_Synchro_Master_RT() = default;

	Ev_Dfg_Synchro_Master_RT(const Ev_Dfg_Synchro_Master_RT&) = delete;
	Ev_Dfg_Synchro_Master_RT & operator=(const Ev_Dfg_Synchro_Master_RT&) = delete;


	//! the master of the dfg
	EVmaster master;

	//! the constructor of the physical DFG infrastructure
	EVdfg dfg_mgr;
};

// -------------------------------------------------------
// Ev_Dfg_Synchro_Client_RT
// -------------------------------------------------------
/**
 * This is responsible for sending out the events, i.e.,
 * writing.
 * TODO I am not sure if I need separate runtimes for the Reader
 * Writer, and ReaderWriter part; so far it seems I don't it
 */
class Ev_Dfg_Synchro_Client_RT  {
public:
	static Ev_Dfg_Synchro_Client_RT *get_instance();
	static bool exists();

	Diag run_dfg(const string & node_name);
	EVclient get_client();
	Diag shutdown_dfg();


protected:
	//! this will store my singleton instance
	static Ev_Dfg_Synchro_Client_RT *p_cw_rt;

	Ev_Dfg_Synchro_Client_RT();
	virtual ~Ev_Dfg_Synchro_Client_RT() = default;

	Ev_Dfg_Synchro_Client_RT(const Ev_Dfg_Synchro_Client_RT&) = delete;
	Ev_Dfg_Synchro_Client_RT & operator=(const Ev_Dfg_Synchro_Client_RT&) = delete;

	//! the dfg client
	EVclient client;
};


} /* namespace cw */

#endif /* CW_RUNTIME_H_ */
