/**
 * @file  super_spy.h
 * @date  Sep 4, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#ifndef SUPER_SPY_H_
#define SUPER_SPY_H_

#include <vector>

#include "spy.h"

namespace cw {
/**
 * A spy that synchronously run all available spies
 */
class Super_Spy : public Spy {
public:
	Super_Spy();
	virtual ~Super_Spy();

	virtual Diag setup_for_read();
	Diag read_mon();
	virtual Diag clean_after_read();

	virtual Diag setup_for_process();
	virtual Diag clean_after_process();

	virtual Diag set_caps_ts(long long int ts);
	virtual Diag set_mon_ts(long long int ts);

	std::vector<Spy*> & get_spies_vec();

	template<class SPY_TYPE>
	SPY_TYPE* get_spy(int *index);

protected:
	//! this will have all spies
	std::vector<Spy*> spies_vec;

};

/**
 * gets the particular spy out the spies_vec
 *
 * @param index The index in vector of this particular spy
 * @return the spy or nullptr if there is no such a spy
 */
template<class SPY_TYPE>
SPY_TYPE* Super_Spy::get_spy(int *index){
	int i = 0;
	SPY_TYPE * spy = nullptr;

	for( auto & el : spies_vec){
		if( dynamic_cast<SPY_TYPE*>(el) ){
			// TODO there might be smarter way with find()
			spy = dynamic_cast<SPY_TYPE*>(el);
			break;
		}
		++i;
	}
	if( spy ){
		*index = i;
	}
	return spy;
}

} // cw namespace
#endif /* SUPER_SPY_H_ */
