/**
 *  @file   misc.h
 *
 *  @date   Created on: Dec 19, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef MISC_H_
#define MISC_H_
#include <string>

#include "evpath.h"

namespace cw {

enum class Diag : int {
	OK = 0, //! everything is ok
	ERR = 1, //! there were some errors, general error
	WARN = 2, //! a warning error, something went wrong but not necessary sth to worry about
	NAN_ERR = 10, //! the NAN detected

	EV_ERR = 30,    //! general error with the EVPath call
	EV_CM_ERR = 31, //! EVPath Connection Manager error

	INI_ERR = 40,   //! issues related to ini file
	INI_NO_FILE_ERR = 41, //! a file does not exists

	COL_ERR = 50, //! issues related to inserting, getting from, etc containers
	COL_ITEM_EXISTS_ERR = 51, //! an item exists in a collection

	AGGREG_WARN = 60,
	TIMEOUT_ERR = 70, //! the timeout elapsed

	SQL_ERR = 80, //! error related to the sql reader

	NVML_ERR = 90, //! error related to the NVML
	NVML_UNSUPPORTED = 100, //! if the operation was unsupported

	EV_DFG_ERR = 110 //! problem with the EVpath dfg
};

/**
 * is able to get a timestamp
 */
class Time_Stamp {
public:
	Time_Stamp();
	virtual ~Time_Stamp();
	static long long int get_ts();
};
/**
 * The settings for the key-value configuration file
 */
class Cfg {
public:
	Cfg() = default;
	virtual ~Cfg() = default;

	//! the default name for the configuration file
	static const std::string CFG_DEFAULT_NAME;

	//! the name of the section for the ev_master configuration
	static const std::string CFG_EV_MASTER_SEC;
	//! the name of the contact
	static const std::string CFG_EV_MASTER_SEC_CONTACT_KEY;

	//! the monitoring period in seconds
	static const int CFG_EV_MONITOR_PERIOD;
	//! the monitoring period in microseconds
	static const int CFG_EV_MONITOR_PERIOD_USEC;

	//! TODO it might be in the cfg file later, for now
	//! here this is how long the monitoring atom will wait
	//! for aggregator stone from aggregator
	static const int WAIT_FOR_AGGREG_TIMEOUT_SEC;


	//! the directory where the output will be done
	static const std::string CFG_SQLITE3_READER_OUTPUT_DIR;

	//! files for local contact information for Monitoring Atoms
	static const std::string CFG_CPU_MON_ATOM_FILE;
	static const std::string CFG_MEM_MON_ATOM_FILE;
	static const std::string CFG_NET_MON_ATOM_FILE;
	static const std::string CFG_NVML_MON_ATOM_FILE;
	//static const std::string CFG_LYNX_MON_ATOM_FILE;


	//! variables for the splitter purpose file
	//! the default splitter contact file name
	static const std::string CFG_SPLITTERS_FILE_DEFAULT_NAME;
	//! the default name of the splitters section
	static const std::string CFG_SPLITTERS_SEC;
	//! the default splitters key
	static const std::string CFG_SPLITTERS_DEFAULT_KEY;

	static Diag wrt_val(const std::string & file_name, const std::string & value,
		const std::string & section, const std::string & key);

	static Diag wrt_val(const std::string & file_name, int value,
		const std::string & section, const std::string & key);

	static Diag read_val(const std::string & file_name, std::string & value,
		const std::string & section, const std::string & key);

	static Diag read_val(const std::string & file_name, int * value,
			const std::string & section, const std::string & key);

	static Diag rm_cfg(const std::string & file_name);

	static int get_wait_for_aggreg_timeout();
	static int get_monitor_period();

	static bool file_exists(const std::string& file);
};

/**
 * it is dedicated for creating a configuration for DFGs
 */
class Cfg_Dfg : public Cfg {
public:

	static const std::string CFG_SQLITE3_SEC;
	static const std::string CFG_SQLITE3_OUTPUT_DIR_KEY;

	static const std::string CFG_EV_MONITORING_SEC;
	static const std::string CFG_EV_MON_SAMPLING_RATE_KEY_SEC;
	static const std::string CFG_EV_MON_SAMPLING_RATE_KEY_USEC;

	static const std::string CFG_EV_DFG_MASTER_RUNNING_KEY;
	static const int CFG_EV_DFG_MASTER_RUNNING_SEC;

	static const std::string CFG_EV_DFG_CLIENT_RUNNING_KEY;
	static const int CFG_EV_DFG_CLIENT_RUNNING_SEC;

	static Diag create_cfg_file();
	static std::string get_sqlite3_reader_output_dir();
	static int get_monitoring_sampling_sec_rate();
	static int get_monitoring_sampling_usec_rate();
	static int get_dfg_master_running_time_sec();
	static int get_dfg_client_running_time_sec();

protected:
	//static Diag read_sqlite3_reader_output_dir(std::string & output_dir);
	//static Diag read_monitoring_sampling_rate(const std::string & key, int * rate);
	static Diag get(const std::string & sec, const std::string & key, int * val,  int default_val);
	static Diag get(const std::string & sec, const std::string & key, std::string & val, const std::string & default_val);
};
/**
 * This is the contact information that the monitoring worker will
 * output for the external api
 */
struct Ev_Contact {
	//! the contact information
	std::string contact;
	//! the id of the split stone
	EVstone split_stone;
	//! the id of the split_action
	EVaction split_action;
};
/**
 * This file is intended to be shared
 */
class Contact_File {
public:
	Contact_File();
	virtual ~Contact_File();
	static Diag wrt_to(const std::string & file_name, const struct Ev_Contact & content);
	static Diag read_from(const std::string & file_name, struct Ev_Contact & contact);
};
/**
 * Encompassing some utility functions
 */
class Ev_Utils {
public:
	static Diag get_my_contact(std::string & contact);
	static Diag get_master_contact_list(const std::string &cfg_file_name, std::string & contact);
};
/**
 * Encompassing some communication idioms
 */
//namespace Ev_Comm_Pattern {

/* TODO this is an unfinished business with template fights

 template <class T1, class T2>
	 Diag send_alive_pattern(const std::string & contact, CMConnection & conn,
			const T1 *p_msg, const T2 *p_dummy);


*/
Diag send_alive_pattern(const std::string & contact, CMConnection & conn,
		const struct ev_alive_msg *p_msg);
Diag send_alive_pattern(const std::string & contact, CMConnection & conn,
		const struct ev_alive_mon_msg *p_msg);

//} // ev_comm_patterns

/**
 * This is covenience structure for registering stone names
 * for the Super_Atom
 */
struct stone_names {
	std::string cpu_stone_name;
	std::string mem_stone_name;
	std::string net_stone_name;
	std::string nvml_stone_name;
};

/**
 * Basic Processing functions
 */
class Processing_Functions {
public:
	static float percentage(float total, float x);

};
/**
 * 	Characteristics such as utilization
 */
class Characteristics : public Processing_Functions {
public:
	static float utilization(float total, float x);
};
} /* namespace cw */


#endif /* MISC_H_ */
