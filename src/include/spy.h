/**
 *  @file   spy.h
 *
 *  @date   Created on: Dec 18, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_H_
#define SPY_H_

#include <fstream>

#include "misc.h"

#include "spy_events.h"

namespace cw {


/**
 * Describe identity of the entity such as a unique id, etc
 */
class Identity {
public:
	Identity();
	virtual ~Identity() = default;

	Diag determine();
	operator identity();

	int get_id();
	pid_t get_pid();
	const std::string & get_hostname();

private:
	int 	id;				    //! for storing unique id
	std::string  hostname;		//! for storing the name of the host
	pid_t	pid;			    //! the pid of the process
};

/**
 * Used get monitoring data
 */
/*class Data {
public:
	Data();
	virtual ~Data() = default;

	virtual Diag update();

	long long int get_ts();

protected:
	//! the timestamp for when the data were updated
	long long int ts;
};
*/

/**
 * This the common base for source capabilities
 */
/*class Source_Caps : public Data {
public:
	Source_Caps() = default;
	virtual ~Source_Caps() = default;

protected:
	virtual bool is_initialized() = 0;
	Diag determine_id(identity & id);
}; */


/**
 * Describe the source and its capabilities
 */
/*class Data_Source {
public:
	Data_Source();
	virtual ~Data_Source();
};*/

/**
 * Base class for the spy
 */
class Spy {

public:
	Spy();
	virtual ~Spy()=default;

	bool is_enabled() const;

	// this will need to be implemented by the particular spies
	virtual Diag enable();
	virtual Diag disable();

	virtual Diag setup_for_read();
	virtual Diag clean_after_read()=0;

	virtual Diag read_mon()=0;

	virtual Diag setup_for_process();
	virtual Diag clean_after_process() = 0;

	Identity get_identity();
	Diag set_identity(const Identity & new_id);

	virtual Diag set_mon_ts(long long int ts) = 0;
	virtual Diag set_caps_ts(long long int ts) = 0;

protected:
	//! indicates if the spy is enabled (true) or not (false)
	bool enabled;

	//! the identity of the spy
	Identity id;

	virtual Diag determine_identity();
};

/**
 * The base class for acquiring data from the proc file system
 */
class Proc_Acquisitor {
public:
	Proc_Acquisitor() = default;
	virtual ~Proc_Acquisitor();
protected:
	//! this will hold the handler to the input stream
	std::ifstream proc_stream;
	//! this will hold the name of the stream the object will operate on
	std::string stream_name;

	virtual Diag init();
	virtual bool is_initialized();
};

} /* namespace cw */


#endif /* SPY_H_ */
