/**
 *  @file   cpu_spy.h
 *
 *  @date   Created on: Dec 18, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CPU_SPY_H_
#define CPU_SPY_H_

#include <vector>
#include <fstream>

#include "spy.h"
#include "transport.h"
#include "cpu_spy_events.h"

namespace cw {

// ----------------- internal structure

extern std::ostream& operator<<(std::ostream& os, const ev_cpu_cap& caps);
extern std::ostream& operator<<(std::ostream& os, const ev_cpu_mon& caps);

class Cpu_Acquisitor : public Proc_Acquisitor {
public:
	Cpu_Acquisitor();
	~Cpu_Acquisitor();
	Diag get(ev_cpu_cap & rec);
	Diag get(ev_cpu_mon & rec);

	/**
	 * these are the fields we will read from /proc/stat; read man proc
	 * for more information
	 * This is the excerpt from 'man proc'
	 *
	 * /proc/stat
	 *             kernel/system  statistics.   Varies  with   architecture.
	 *             Common entries include:
	 *
	 *             cpu  3357 0 4313 1362393
	 *                    The  amount  of time, measured in units of USER_HZ
	 *                    (1/100ths of a second on most architectures), that
	 *                    the  system spent in user mode, user mode with low
	 *                    priority (nice), system mode, and the  idle  task,
	 *                    respectively.   The  last  value should be USER_HZ
	 *                   times the second entry in the uptime  pseudo-file.
	 *
	 *                    In  Linux  2.6 this line includes three additional
	 *                    columns: iowait - time waiting for I/O to complete
	 *                    (since  2.5.41);  irq  - time servicing interrupts
	 *                    (since  2.6.0-test4);  softirq  -  time  servicing
	 *                    softirqs (since 2.6.0-test4).
	 *
	 * the example /proc/stat from keeneland compute node
	 * cpu [user] [nice] [system] [idle] [user-hz] [iowait] [irq] [softirq] [steal] [guest]
	 *
	 * user: how much time the system spent on normal processes executing in user mode
	 * nice: how much time the system spent on niced processes executing in user mode
	 * system: processes executing in kernel mode
	 * idle: time twiddling thumbs; doing nothing
	 * iowait: waiting for I/O to complete
	 * irq: servicing interrupts
	 * softirq: servicing softirqs
	 * steal and guest - related to virtualization
	 *
	 * We will use: user, nice, system, idle, iowait, irq, softirq, steal;
	 * We will ignore: user-hz
	 * We will not take into account: guest
	 *
	 * cpu  180978792 93 17522592 817920438 959981 6090 16774 0
	 * cpu0 17828779 8 2090399 64663121 198259 2306 1344 0
	 * cpu1 17870460 4 1704097 65127976 81240 167 89 0
	 * cpu2 17477701 20 1547904 65614394 138429 1024 4438 0
	 * cpu3 12131185 3 783253 71839946 29289 4 79 0
	 * cpu4 12125164 18 736464 71606424 309094 1315 5265 0
	 * cpu5 12830291 0 1141017 70784368 27761 21 110 0
	 * cpu6 22211763 3 4080023 58386967 101459 650 2793 0
	 * cpu7 13974231 0 1189737 69600593 18876 3 71 0
	 * cpu8 13485953 12 781184 70501333 13706 321 1150 0
	 * cpu9 14251182 0 928914 69595009 8245 11 104 0
	 * cpu10 13327686 20 903528 70529850 21185 246 1190 0
	 * cpu11 13464392 1 1636065 69670452 12432 16 135 0
	 * intr 880881258 847917465 3 0 1 25 2 0 0 1 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 24155804 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1457648 0 0 0 0 0 0 0 976807 0 0 0 0 0 0 0 722326 0 0 0 0 0 0 0 874677 0 0 0 0 0 0 0 687565 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1574107 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 30 0 0 0 0 0 0 0 0 0 390837 0 0 0 0 0 1349243 0 391600 0 0 0 0 0 0 0 383114 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
	 * ctxt 4151757936
	 * btime 1304728728
	 * processes 513523
	 * procs_running 1
	 * procs_blocked 0
	 */
	struct procfs_cpu{
		// members are public
		long user;
		long nice;
		long system;
		long idle;
		long iowait;
		long irq;
		long softirq;
		long steal;
	} proc_fs;


protected:

	std::vector<struct procfs_cpu> curr;
	std::vector<struct procfs_cpu> prev;

	//! the usage of the cpu, it should contain as many fields as
	//! the number of cores plus one for the aggregate cpu_usage
	//! since it is possible to read from procfs
	std::vector<float> usage;


	//! the timestamp for the current
	long long int ts_curr;
	//! the timestamp for the previous one
	long long int ts_prev;

	//! the network capabilities
	ev_cpu_cap caps;

	//! the network monitoring
	ev_cpu_mon mon;

	Diag init_caps();

	int get_core_count(void);

	bool are_caps_initialized();
	bool is_mon_initialized();

	Diag compute_usage();

	Diag sample_procfs();
};
/**
 * It implements the spy
 */
class Cpu_Spy : public Spy {
public:
	Cpu_Spy() = default;
	virtual ~Cpu_Spy();

	virtual Diag setup_for_read();
	Diag read_mon();
	ev_cpu_cap *get_caps();
	ev_cpu_mon *get_mon();
	virtual Diag clean_after_read();

	virtual Diag setup_for_process();
	virtual Diag process(const ev_cpu_cap & rec);
	virtual Diag process(const ev_cpu_mon & rec);
	virtual Diag clean_after_process() ;

	virtual Diag set_caps_ts(long long int ts);
	virtual Diag set_mon_ts(long long int ts);

protected:
	Cpu_Acquisitor acq;
	ev_cpu_cap caps;
	ev_cpu_mon mon;
};

/**
 * The representation of the cap message
 */
class Ev_Cpu_Cap_Msg :public Ev_Msg {
public:
	static Ev_Cpu_Cap_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Cpu_Cap_Msg *p_msg;

	Ev_Cpu_Cap_Msg() = default;
	~Ev_Cpu_Cap_Msg() = default;

	Ev_Cpu_Cap_Msg(const Ev_Cpu_Cap_Msg&) = delete;
	Ev_Cpu_Cap_Msg & operator=(const Ev_Cpu_Cap_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the mon message
 */
class Ev_Cpu_Mon_Msg :public Ev_Msg {
public:
	static Ev_Cpu_Mon_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Cpu_Mon_Msg *p_msg;

	Ev_Cpu_Mon_Msg() = default;
	~Ev_Cpu_Mon_Msg() = default;

	Ev_Cpu_Mon_Msg(const Ev_Cpu_Mon_Msg&) = delete;
	Ev_Cpu_Mon_Msg & operator=(const Ev_Cpu_Mon_Msg&) = delete;

	Diag reg();
};

} /* namespace cw */

#endif /* CPU_SPY_H_ */
