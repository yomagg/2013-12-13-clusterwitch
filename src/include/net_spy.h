/**
 *  @file   net_spy.h
 *
 *  @date   Created on: Feb 20, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NET_SPY_H_
#define NET_SPY_H_

#include <vector>

#include "spy.h"
#include "transport.h"  // Ev_Msg
#include "net_spy_events.h"  //cap and monitoring

namespace cw {

// ----------------- internal structure

extern std::ostream& operator<<(std::ostream& os, const ev_net_cap& caps);
extern std::ostream& operator<<(std::ostream& os, const ev_net_mon& caps);


/**
 * This is the source acquisitor of the monitoring and cap data
 */
class Net_Acquisitor: public Proc_Acquisitor {
public:
	Net_Acquisitor();
	~Net_Acquisitor() = default;
	Diag get_caps(ev_net_cap & rec);
	Diag get_mon(ev_net_mon & rec);


	//! fields found in /proc/net/dev; we will use it later for the transmit
	//! and receive part (they are almost the same)
	struct procfs_net_fields_common {
		unsigned long bytes;
		unsigned long packets;
		unsigned long errs;
		unsigned long drop;
		unsigned long fifo;
		unsigned long compressed;
		procfs_net_fields_common();
	};

	//! this will hold the readings from /proc/net/dev
	struct procfs_net {
		procfs_net_fields_common receive;
		procfs_net_fields_common transmit;
		//! the timestamp of the measurement
		long long int ts;

		procfs_net();
	};

	//! this will store the information about the particular network
	//! interfaces; it is the same as with cpu; computing utilization
	//! requires knowing the current and previous readings
	struct net_usage {
		procfs_net curr;
		procfs_net prev;
		string nic_name;
	};
protected:

	//! this will hold the network usage for each network interface
	vector<net_usage> usage;

	//! the network capabilities
	ev_net_cap caps;

	//! the network monitoring
	ev_net_mon mon;

	Diag init_caps();

	bool are_caps_initialized();
	bool is_mon_initialized();

	Diag compute_usage();
	Diag clean_usage();

	Diag sample_procfs();
};

/**
 * This is the net spy
 */
class Net_Spy : public Spy {
public:
	Net_Spy() = default;
	virtual ~Net_Spy();

	virtual Diag setup_for_read();
	Diag read_mon();
	ev_net_cap *get_caps();
	ev_net_mon *get_mon();

	virtual Diag clean_after_read();

	virtual Diag setup_for_process();
	virtual Diag process(const ev_net_cap & rec);
	virtual Diag process(const ev_net_mon & rec);
	virtual Diag clean_after_process();

	virtual Diag set_caps_ts(long long int ts);
	virtual Diag set_mon_ts(long long int ts);

protected:
	Net_Acquisitor acq;
	ev_net_cap caps;
	ev_net_mon mon;
};

/**
 * The representation of the cap message
 */
class Ev_Net_Cap_Msg :public Ev_Msg {
public:
	static Ev_Net_Cap_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Net_Cap_Msg *p_msg;

	Ev_Net_Cap_Msg() = default;
	~Ev_Net_Cap_Msg() = default;

	Ev_Net_Cap_Msg(const Ev_Net_Cap_Msg&) = delete;
	Ev_Net_Cap_Msg & operator=(const Ev_Net_Cap_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the mon message
 */
class Ev_Net_Mon_Msg :public Ev_Msg {
public:
	static Ev_Net_Mon_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Net_Mon_Msg *p_msg;

	Ev_Net_Mon_Msg() = default;
	~Ev_Net_Mon_Msg() = default;

	Ev_Net_Mon_Msg(const Ev_Net_Mon_Msg&) = delete;
	Ev_Net_Mon_Msg & operator=(const Ev_Net_Mon_Msg&) = delete;

	Diag reg();
};


} // namespace


#endif /* NET_SPY_H_ */
