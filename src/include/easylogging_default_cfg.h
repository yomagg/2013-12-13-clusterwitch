/**
 *  @file   easylogging_default_cfg.h
 *
 *  @date   Created on: Jan 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

el::Configurations defaultConf;
defaultConf.setToDefault();
defaultConf.setGlobally(el::ConfigurationType::Format, "%level %func:%loc %msg");
el::Loggers::reconfigureLogger("default", defaultConf);
