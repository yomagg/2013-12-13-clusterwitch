/**
 *  @file   mem_spy_events.h
 *
 *  @date   Created on: Feb 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef MEM_SPY_EVENTS_H_
#define MEM_SPY_EVENTS_H_


#ifdef __cplusplus
#include "spy_events.h"
namespace cw {
#endif // __cplusplus

/**
 * the memory capabilities as what we can get from /proc/meminfo
 */
struct ev_mem_cap {
	//! the identity of the the spy
	struct identity id;

	//! the timestamp indicating the start of measurument
	long long int ts;

	// the mem capabilities

	//! total amount of physical RAM in kilobytes
	long mem_total_kB;
	//! total amount of swap available in kilobytes
	long swap_total_kB;

#ifdef __cplusplus
	ev_mem_cap();
	ev_mem_cap(const ev_mem_cap & other);
	ev_mem_cap & operator=(const ev_mem_cap & other);
#endif // __cplusplus
};


/**
 * This is what will be reported by a spy to a secret service.
 *
 * I used /proc/meminfo for that so please refer to its documentation
 * e.g. http://www.centos.org/docs/5/html/5.2/Deployment_Guide/s2-proc-meminfo.html
 */
struct ev_mem_mon {
	//! the id of the sending entity
	int id;

	//! the timestamp of measurement occurrence
	long long int ts;


	//! total amount of physical RAM in kilobytes - this is from the cap
	//! since now we do not send the first cap record; now we send
//	long mem_total_kB;
	//! total amount of swap available in kilobytes - this is from the cap
	//! we do not send the first cap record; now we send
//	long swap_total_kB;

	//! current mem utilization in percentage (mem_total_kB-mem_free)/mem_total_kB
	float mem_util_perc;

	//! The amount of physical RAM, used for file buffers.
	//! percentage of mem_total_kB
	float mem_buffers_util_perc;
	//! The amount of physical RAM, used as cache memory. Percentage of mem_total_kB
	float mem_cached_util_perc;
	//! The total amount of buffer or page cache memory, w.r.t mem_total_kB,
	//! that is in active use. This is memory that has been recently used and is
	//! usually not reclaimed for other purposes.
	float mem_active_util_perc;
	//! The total amount of buffer or page cache memory, w.r.t. mem_total_kB,
	//! that are free and available. This is memory that has not been recently
	//! used and can be reclaimed for other purposes.
	float mem_inactive_util_perc;

	//! The total amount of memory, w.r.t. mem_total_kB, used by the kernel to cache
	//! data structures for its own use.
	float slab_util_perc;
	//! The total amount of memory, in % w.r.t. mem_total_kB, which have been used to
	//! map devices, files, or libraries using the mmap command
	float mapped_perc;

	//! The amount of swap, in % w.r.t. swap_total_kB, used as cache memory.
	float swap_cached_perc;
	//! The total amount of swap used, in % w.r.t swap_total_kB.
	float swap_util_perc;
};

#ifdef __cplusplus
}   // namespace
#endif // __cplusplus



#endif /* MEM_SPY_EVENTS_H_ */
