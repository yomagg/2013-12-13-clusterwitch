/**
 * @file debug.h
 *
 * @date Jul 7, 2011
 * @author Magda Slawinska, aka Magic Magg, magg dot gatech at gmail dot edu.
 * based on remote_gpu/common/gvim_debug.h (GViM stuff), however, substantially
 * extended
 *
 * The debugging utility macros
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <stdlib.h>

#define DEBUG

//! Debug printing verbosity
#define DBG_LEVEL   DBG_DEBUG

// New debug messaging state. There is no sense of a "level" for debugging. Each of these define the
// purpose of the messages and is enabled/disabled per file

//! system cannot continue, e.g. malloc
#define DBG_ERROR   0
//!
#define DBG_CRITICAL 1
//! some serious problems
#define DBG_WARNING 2
#define DBG_MESSAGE 3
//! messages about state or configuration; high-level flow
#define DBG_INFO    4
//!  func args, variable values, etc; full flow, may slow system down
#define DBG_DEBUG   5
//! indicator that something should be done here
#define DBG_TODO	-1

#define DBG_ERROR_STR 		"ERROR\t"
#define DBG_CRITICAL_STR 	"CRITICAL\t"
#define DBG_WARNING_STR 	"WARNING\t"
#define DBG_MESSAGE_STR 	"MESSAGE\t"
#define DBG_INFO_STR		"INFO\t"
#define DBG_DEBUG_STR		"DEBUG\t"
#define DBG_TODO_STR		"TODO\t"


#define p_todo(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_TODO_STR, (DBG_TODO), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
    } while(0)

#define p_not_impl()	\
	do {                                                  \
         printf("%s(%d) %s:%s:%d: Not implemented!\n", DBG_TODO_STR, (DBG_TODO), __FILE__, __FUNCTION__, __LINE__);  \
         fflush(stdout);  											\
    } while(0)



#define p_exit(fmt, args...)                             \
    do {                                                  \
         printf("%s(%d) %s:%s:%d: ", DBG_ERROR_STR, (DBG_ERROR), __FILE__, __FUNCTION__, __LINE__);  \
         printf(fmt, ##args);                                       \
         fflush(stdout);  											\
         exit(-1);													\
    } while(0)


//! @todo do something like that but smarter without unnecessary copying
#define p_error(fmt, args...)                             				\
    do {                                                                \
        if((DBG_ERROR) <= DBG_LEVEL) {                                  \
            printf("%s(%d) %s:%s:%d: ", DBG_ERROR_STR, (DBG_ERROR), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);  											\
        }                                                               \
    } while(0)


#define p_critical(fmt, args...) \
	do {                                                                \
        if((DBG_CRITICAL) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_CRITICAL_STR, (DBG_CRITICAL), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_warn(fmt, args...) \
	do {                                                                \
        if((DBG_WARNING) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_WARNING_STR, (DBG_WARNING), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_mesg(fmt, args...) \
	do {                                                                \
        if((DBG_MESSAGE) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_MESSAGE_STR, (DBG_MESSAGE), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_info(fmt, args...) \
	do {                                                                \
        if((DBG_INFO) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_INFO_STR, (DBG_INFO), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_debug(fmt, args...) \
	do {                                                                \
        if((DBG_DEBUG) <= DBG_LEVEL) {                                      \
            printf("%s(%d) %s:%s:%d: ", DBG_DEBUG_STR, (DBG_DEBUG), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);												\
        }                                                               \
    } while(0)

#define p_nvml_error(fmt, args...)                             				\
    do {                                                                \
        if((DBG_ERROR) <= DBG_LEVEL) {                                  \
            printf("%s(%d) %s:%s:%d: NVML: ", DBG_ERROR_STR, (DBG_ERROR), __FILE__, __FUNCTION__, __LINE__);   \
            printf(fmt, ##args);                                        \
            fflush(stdout);  											\
        }                                                               \
    } while(0)

#endif /* DEBUG_H_ */
