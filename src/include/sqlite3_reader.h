/**
 *  @file   sqlite3_reader.h
 *
 *  @date   Created on: Feb 12, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SQLITE3_READER_H_
#define SQLITE3_READER_H_

#include <vector>

#include <sqlite3.h>


#include "misc.h"
#include "cpu_spy_events.h"
#include "mem_spy_events.h"
#include "net_spy_events.h"
// The prefix #define NVML_PRESENT is not necessary because
// the sqlite3 reader is pretty independent
#include "nvml_spy_events.h"

namespace cw {

/**
 * This is the base class for sqlite3 readers
 */
class Sqlite3_Reader{
public:
	Sqlite3_Reader();
	virtual ~Sqlite3_Reader();
	virtual Diag open_db();
	virtual Diag close_db();

protected:
	//! the handler to the database
	sqlite3 *db;
	//! the name of the capability tab
	std::string CAP_TAB;
	//! the name of teh monitoring tab
	std::string MON_TAB;
	//! the prefix for the database
	std::string DB_PREFIX;

	virtual Diag create_cap_tab() = 0;
	virtual Diag create_mon_tab() = 0;

	Diag gen_db_file_name(std::string & file_name);
	Diag exec_sql(const std::string & sql);
};

/**
 * This is the Cpu reader
 */
class Cpu_Sqlite3_Reader : public Sqlite3_Reader {
public:
	Cpu_Sqlite3_Reader();
	virtual ~Cpu_Sqlite3_Reader() = default;
	virtual Diag insert_cap_rec(const ev_cpu_cap *p_rec);
	virtual Diag insert_mon_rec(const ev_cpu_mon *p_rec);
protected:
	virtual Diag create_cap_tab();
	virtual Diag create_mon_tab();
};

/**
 * The mem reader
 */
class Mem_Sqlite3_Reader : public Sqlite3_Reader {
public:
	Mem_Sqlite3_Reader();
	virtual ~Mem_Sqlite3_Reader() = default;
	virtual Diag insert_cap_rec(const ev_mem_cap *p_rec);
	virtual Diag insert_mon_rec(const ev_mem_mon *p_rec);
protected:
	virtual Diag create_cap_tab();
	virtual Diag create_mon_tab();
};

/**
 * The net reader
 */
class Net_Sqlite3_Reader : public Sqlite3_Reader {
public:
	Net_Sqlite3_Reader();
	virtual ~Net_Sqlite3_Reader() = default;
	virtual Diag insert_cap_rec(const ev_net_cap *p_rec);
	virtual Diag insert_mon_rec(const ev_net_mon *p_rec);
protected:
	virtual Diag create_cap_tab();
	virtual Diag create_mon_tab();
};

/**
 * The nvml reader
 */
class Nvml_Sqlite3_Reader : public Sqlite3_Reader {
public:
	Nvml_Sqlite3_Reader();
	virtual ~Nvml_Sqlite3_Reader() = default;
	virtual Diag insert_cap_rec(const ev_nvml_cap *p_rec);
	virtual Diag insert_mon_rec(const ev_nvml_mon *p_rec);
protected:
	virtual Diag create_cap_tab();
	virtual Diag create_mon_tab();

	//! the name of the capability tab
	std::string DEV_CAP_TAB;
};

/**
 * This reader is responsible for the managing a cohort of readers
 * actually all available sqlite3 readers. The operations are
 * defined in terms of a set of readers rather than a single reader.
 */
class Super_Sqlite3_Reader : public Sqlite3_Reader {
public:
	Super_Sqlite3_Reader() = default;
	virtual ~Super_Sqlite3_Reader() = default;

	virtual Diag open_db();
	virtual Diag close_db();

	std::vector<Sqlite3_Reader*> & get_sqlite3_readers();

protected:
	//! this will hold all readers I am interested in
	std::vector<Sqlite3_Reader*> readers_vec;

	virtual Diag create_cap_tab();
	virtual Diag create_mon_tab();
};
} // cw namespace
#endif /* SQLITE3_READER_H_ */
