/**
 *  @file   mem_spy_events_desc.h
 *
 *  @date   Created on: Feb 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef MEM_SPY_EVENTS_DESC_H_
#define MEM_SPY_EVENTS_DESC_H_


#ifdef __cplusplus
using namespace cw;
#endif

#include "spy_events_desc.h"  // include identity description

// --------------------------------
// capabilities
// --------------------------------
static FMField mem_cap_field_list[] = {
  {"id", "identity", sizeof(struct identity), FMOffset(struct ev_mem_cap *, id)},
  {"ts", "integer", sizeof(long long int), FMOffset(struct ev_mem_cap *, ts)},
  {"mem_total_kB", "integer", sizeof(long), FMOffset(struct ev_mem_cap *, mem_total_kB)},
  {"swap_total_kB", "integer", sizeof(long), FMOffset(struct ev_mem_cap *, swap_total_kB)},
  {NULL, NULL, 0, 0}
};


//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
static FMStructDescRec mem_cap_format_list[] = {
		{ "ev_mem_cap", mem_cap_field_list, sizeof(struct ev_mem_cap), NULL },
		{ "identity", id_field_list, sizeof(struct identity), NULL },
		{ NULL, NULL, 0, NULL}
};

// ------------------------------------------------
// monitoring events
// ------------------------------------------------
static FMField mem_mon_field_list[] = {
	{"id", "integer", sizeof(int), FMOffset(struct ev_mem_mon *, id)},
	{"ts", "integer", sizeof(long long int), FMOffset(struct ev_mem_mon *, ts) },
  //{"mem_total_kB", "integer", sizeof(long), FMOffset(struct mem_mon *, mem_total_kB)},
  //{"swap_total_kB", "integer", sizeof(long), FMOffset(struct mem_mon *, swap_total_kB)},
  {"mem_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mem_util_perc)},
  {"mem_buffers_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mem_buffers_util_perc)},
  {"mem_cached_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mem_cached_util_perc)},
  {"mem_active_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mem_active_util_perc)},
  {"mem_inactive_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mem_inactive_util_perc)},
  {"slab_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, slab_util_perc)},
  {"mapped_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, mapped_perc)},
  {"swap_cached_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, swap_cached_perc)},
  {"swap_util_perc", "float", sizeof(float), FMOffset(struct ev_mem_mon *, swap_util_perc)},
  { NULL, NULL, 0, 0}
};

static FMStructDescRec mem_mon_format_list[] = {
		{"ev_mem_mon", mem_mon_field_list, sizeof(struct ev_mem_mon), NULL },
		{ NULL, NULL, 0, NULL}
};



#endif /* MEM_SPY_EVENTS_DESC_H_ */
