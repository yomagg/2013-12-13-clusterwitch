/**
 *  @file   mem_transport.h
 *
 *  @date   Created on: Feb 19, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef MEM_TRANSPORT_H_
#define MEM_TRANSPORT_H_

#include "transport.h"
#include "mem_spy.h"
#include "sqlite3_reader.h"

namespace cw {

/**
 * The base class for monitoring mem; it should have all functionality
 * for the local monitoring; it should not have anything related to the
 * evpath
 */
class Mem_Mon_Atom {

public:
	Mem_Mon_Atom() = default;
	virtual ~Mem_Mon_Atom() = default;

	Mem_Spy *get_spy();
	Mem_Sqlite3_Reader * get_sqlite3_reader();

protected:
	//! provides the monitoring functionality
	Mem_Spy spy;

	//! the sqlite3 reader
	Mem_Sqlite3_Reader sqlite3_reader;
};

// ------------------------------------------------------
// This is star topology
// ------------------------------------------------------

/**
 * the monitoring component for the mem_spy
 */
class Ev_Mem_Monitoring_Atom : public Ev_Monitoring_Atom {
public:
	Ev_Mem_Monitoring_Atom();
	virtual ~Ev_Mem_Monitoring_Atom() = default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Mem_Spy *get_spy();
protected:
	Mem_Spy spy;

	Diag send_cap();
	Diag send_mon();
};

/**
 * Aggregator Atom
 */
class Ev_Mem_Aggregator_Atom : public Ev_Aggregator_Atom {
public:
	Ev_Mem_Aggregator_Atom() = default;
	virtual ~Ev_Mem_Aggregator_Atom()=default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Mem_Sqlite3_Reader *get_sqlite3_reader();
	Mem_Spy *get_spy();

protected:
	//! the sqlite3 reader
	Mem_Sqlite3_Reader sqlite3_reader;
	//! the spy for the processing function
	Mem_Spy spy;
};

// ---------------------------------------------
// for DFG topology
// ---------------------------------------------
/**
 * TODO There is redundancy with Ev_Mem_Monitoring_Atom that should
 * be at some point removed but I am skipping this for now
 * this is source
 */
class Ev_Dfg_Mem_White_Atom : public Ev_Dfg_White_Atom, public Mem_Mon_Atom {
public:
	Ev_Dfg_Mem_White_Atom()=default;
	virtual ~Ev_Dfg_Mem_White_Atom()=default;
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag run();
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};
/**
 * This is the aggregator-terminator functions (sink)
 */
class Ev_Dfg_Mem_Black_Atom : public Ev_Dfg_Black_Atom, public Mem_Mon_Atom {
public:
	Ev_Dfg_Mem_Black_Atom();
	virtual ~Ev_Dfg_Mem_Black_Atom();
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag quit_topo();

};
/**
 * This is a Reader and a Writer (sink and source)
 */
class Ev_Dfg_Mem_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Mem_Mon_Atom{
public:
	Ev_Dfg_Mem_Gray_Atom() ;
	virtual ~Ev_Dfg_Mem_Gray_Atom() ;
	virtual Diag reg(string node_name, string sink_name, string src_name);
	virtual Diag quit_topo();
protected:
	Diag send_mon();  // TODO seems unnecessary
};

}


#endif /* MEM_TRANSPORT_H_ */
