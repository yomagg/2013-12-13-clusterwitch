/**
 *  @file   net_spy_events.h
 *
 *  @date   Created on: Feb 20, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NET_SPY_EVENTS_H_
#define NET_SPY_EVENTS_H_

#ifdef __cplusplus
#include "spy_events.h"
namespace cw {
#endif // __cplusplus

/**
 * The structure that is needed to send capability data
 * from a spy
 */
struct ev_net_cap {
	//! the identity of the net scout
	struct identity id;
	//! the timestamp when the measure took place
	long long int ts;

	//! how many NICS were detected
	int nics_count;
	//! the names of the corresponding names
	char **nics_names;

#ifdef __cplusplus
	ev_net_cap();
	~ev_net_cap();
	ev_net_cap(const ev_net_cap & other);
	ev_net_cap & operator=(const ev_net_cap & other);
protected:
	void free_alloc();
	void copy_nics(const ev_net_cap & other);
#endif // __cplusplus
};

// --------------------------------------
// for monitoring
// --------------------------------------

struct ev_nic_usage {
	//! the name of the NIC
	char *name;
	//! the combined usage of the interface (transmitted + received)
	//! this is the data transfer rate including bytes transmitted and recv
	//! [kB/s] kilo= 1000 kilobytes per second
	float combined_usage;
	//! this is the data transfer rate including bytes received [kB/s] (recv component)
	float received;
	//! this is the data transfer rate including bytes transmitted [kB/s] (trans component)
	float transmitted;
#ifdef __cplusplus
	ev_nic_usage();
	~ev_nic_usage();
	ev_nic_usage(const ev_nic_usage & other);
	ev_nic_usage & operator=(const ev_nic_usage & other);
protected:
	void free_name();
	void copy_name(const ev_nic_usage & other);
#endif // __cplusplus
};

/**
 * The structure that will be sent from a scout to a trooper with monitoring
 * data
 */
struct ev_net_mon {
	//! the id of the node where the measurement is going to be
	int id;
	//! the timestamp of the measurement
	long long int ts;

	//! number of interfaces
	int nics_count;

	//! the array of usage
	struct ev_nic_usage * nics_usage_arr;
#ifdef __cplusplus
	ev_net_mon();
	~ev_net_mon();
	ev_net_mon(const ev_net_mon & other);
	ev_net_mon & operator=(const ev_net_mon & other);
protected:
	void copy_usage_arr(const ev_net_mon & other);
	void free_usage_arr();

#endif // __cplusplus
};


#ifdef __cplusplus
}   // namespace
#endif // __cplusplus


#endif /* NET_SPY_EVENTS_H_ */
