/**
 *  @file   spy_events_desc.h
 *
 *  @date   Created on: Feb 18, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_EVENTS_DESC_H_
#define SPY_EVENTS_DESC_H_

#include "spy_events.h"

#ifdef __cplusplus
using namespace cw;
#endif

static FMField id_field_list[] = {
		{"id", "integer", sizeof(int), FMOffset(struct identity *, id)},
		{"hostname", "string", sizeof(char*), FMOffset(struct identity *, hostname)},
		{"pid", "integer", sizeof(pid_t), FMOffset(struct identity *, pid)},
		{ NULL, NULL, 0, 0}
};

#endif /* SPY_EVENTS_DESC_H_ */
