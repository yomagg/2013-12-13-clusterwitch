/**
 *  @file   net_spy_events_desc.h
 *
 *  @date   Created on: Feb 20, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NET_SPY_EVENTS_DESC_H_
#define NET_SPY_EVENTS_DESC_H_

#ifdef __cplusplus
using namespace cw;
#endif

#include "spy_events_desc.h"  // include identity description
// --------------------------------
// capabilities
// --------------------------------

static FMField net_cap_field_list[] = {
  {"id", "identity", sizeof(struct identity), FMOffset(struct ev_net_cap *, id)},
  {"ts", "integer", sizeof(long long int), FMOffset(struct ev_net_cap *, ts)},
  {"nics_count", "integer", sizeof(int), FMOffset(struct ev_net_cap *, nics_count)},
  {"nics_names", "string[nics_count]", sizeof(char*), FMOffset(struct ev_net_cap*, nics_names)},
  {NULL, NULL, 0, 0}
};

//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
static FMStructDescRec net_cap_format_list[] = {
		{ "ev_net_cap", net_cap_field_list, sizeof(struct ev_net_cap), NULL },
		{ "identity", id_field_list, sizeof(struct identity), NULL },
		{ NULL, NULL, 0, NULL}
};

// ------------------------------------------------
// monitoring events
// ------------------------------------------------

static FMField ev_nic_usage_field_list[] = {
	{"name", "string", sizeof(char*), FMOffset(struct ev_nic_usage *, name)},
	{"combined_usage", "float", sizeof(float), FMOffset(struct ev_nic_usage *, combined_usage)},
	{"received", "float", sizeof(float), FMOffset(struct ev_nic_usage *, received)},
	{"transmitted", "float", sizeof(float), FMOffset(struct ev_nic_usage *, transmitted)},
	{NULL, NULL, 0, 0}
};

static FMStructDescRec ev_nic_usage_format_list[] = {
	{ "ev_nic_usage", ev_nic_usage_field_list, sizeof(struct ev_nic_usage), NULL },
	{ NULL, NULL, 0, NULL}
};


static FMField net_mon_field_list[] = {
  {"id", "integer", sizeof(int), FMOffset( struct ev_net_mon *, id)},
  {"ts", "integer", sizeof(long long int), FMOffset( struct ev_net_mon *, ts) },
  {"nics_count", "integer", sizeof(int), FMOffset(struct ev_net_mon *, nics_count)},
  {"nics_usage_arr", "ev_nics_usage[nics_count]",sizeof(struct ev_nic_usage),FMOffset(struct ev_net_mon *, nics_usage_arr) },
  {NULL, NULL, 0, 0}
};

static FMStructDescRec net_mon_format_list[] = {
  { "ev_net_mon", net_mon_field_list, sizeof(struct ev_net_mon), NULL },
  { "ev_nics_usage", ev_nic_usage_field_list, sizeof(struct ev_nic_usage), NULL },
  { NULL, NULL, 0, NULL}
};



#endif /* NET_SPY_EVENTS_DESC_H_ */
