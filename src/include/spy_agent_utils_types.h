/**
 *  @file   spy_agent_utils_types.h
 *
 *  @date   Created on: Feb 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_AGENT_UTILS_TYPES_H_
#define SPY_AGENT_UTILS_TYPES_H_

#include "evpath.h"
#include "spy_agent.h"

/**
 * This is the contact information that the monitoring worker will
 * output for the external api; @see struct Ev_Contact in misc.h
 */
struct ev_contact {
	//! the contact information
	char contact[200];
	//! the id of the split stone
	EVstone split_stone;
	//! the id of the split_action
	EVaction split_action;
};

/**
 * This combines the information about the important entities that
 * are created in the spy remotely by the agent
 */
struct ragent_info {
	//! the contact manager
	CManager cm;
	//! the connection
	CMConnection conn;
	//! the contact information
	char contact[200];
	//! the id of the split stone
	EVstone rsplit_stone;
	//! the id of the split_action
	EVaction rsplit_action;
	//! the bridge (output) stone created remotely and added to the splitter
	EVstone rbridge_stone;
	//! the local terminal stone id with the handler
	EVstone lterminal_stone;
	//! key to find an appropriate contact in the splitter contact file
	//! should follow the convention of the writing when a DFG atom (source or
	//! sink) joins the DFG; this is ignored in the non-DFG approach
	char key_contact[200];
};

/**
 * This structure stores the information related to handling
 * events the user wants to get from CW
 */
struct ev_handler_info {
	//! pointer to the fields for the event handler
	//FMField *p_fields; // TODO it seems that this is not needed
	//! pointer to the formats for the event handler
	FMStructDescRec *p_formats;
	//! client data
	void *p_client_data;
	//! pointer to the handler handling the specific event
	handler_t p_handler;

};
#endif /* SPY_AGENT_UTILS_TYPES_H_ */
