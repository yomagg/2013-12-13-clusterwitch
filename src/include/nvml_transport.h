/**
 *  @file   nvml_transport.h
 *
 *  @date   Created on: Feb 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NVML_TRANSPORT_H_
#define NVML_TRANSPORT_H_

#include "transport.h"
#include "nvml_spy.h"
#include "sqlite3_reader.h"

namespace cw {

/**
 * The base class for monitoring nvml; it should have all functionality
 * for the local monitoring; it should not have anything related to the
 * evpath
 *
 * @todo This should be a template class
 */
class Nvml_Mon_Atom {

public:
	Nvml_Mon_Atom() = default;
	virtual ~Nvml_Mon_Atom() = default;

	Nvml_Spy *get_spy();
	Nvml_Sqlite3_Reader * get_sqlite3_reader();

protected:
	//! provides the monitoring functionality
	Nvml_Spy spy;

	//! the sqlite3 reader
	Nvml_Sqlite3_Reader sqlite3_reader;
};

// ------------------------------------------------------
// This is star topology
// ------------------------------------------------------
/**
 * the monitoring component for the nvml_spy
 */
class Ev_Nvml_Monitoring_Atom : public Ev_Monitoring_Atom {
public:
	Ev_Nvml_Monitoring_Atom();
	virtual ~Ev_Nvml_Monitoring_Atom() = default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Nvml_Spy *get_spy();
protected:
	Nvml_Spy spy;

	Diag send_cap();
	Diag send_mon();
};

/**
 * Aggregator Atom
 */
class Ev_Nvml_Aggregator_Atom : public Ev_Aggregator_Atom {
public:
	Ev_Nvml_Aggregator_Atom() = default;
	virtual ~Ev_Nvml_Aggregator_Atom()=default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Nvml_Sqlite3_Reader *get_sqlite3_reader();
	Nvml_Spy *get_spy();

protected:
	//! the sqlite3 reader
	Nvml_Sqlite3_Reader sqlite3_reader;
	//! the spy for the processing function
	Nvml_Spy spy;
};

// --------------------------------------
// for DFG topology
// --------------------------------------
/**
 * This is source
 */
class Ev_Dfg_Nvml_White_Atom : public Ev_Dfg_White_Atom, public Nvml_Mon_Atom {
public:
	Ev_Dfg_Nvml_White_Atom()=default;
	virtual ~Ev_Dfg_Nvml_White_Atom()=default;
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag run();
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};

/**
 * This is the aggregator-terminator functions (sink)
 */
class Ev_Dfg_Nvml_Black_Atom : public Ev_Dfg_Black_Atom, public Nvml_Mon_Atom {
public:
	Ev_Dfg_Nvml_Black_Atom();
	virtual ~Ev_Dfg_Nvml_Black_Atom();
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag quit_topo();
};
/**
 * This is a Reader and a Writer (sink and source)
 */
class Ev_Dfg_Nvml_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Nvml_Mon_Atom{
public:
	Ev_Dfg_Nvml_Gray_Atom() ;
	virtual ~Ev_Dfg_Nvml_Gray_Atom() ;
	virtual Diag reg(string node_name, string sink_name, string src_name);
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};

}


#endif /* NVML_TRANSPORT_H_ */
