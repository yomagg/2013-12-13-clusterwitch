/**
 *  @file   test_main.h
 *
 *  @date   Created on: Dec 19, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef TEST_MAIN_H_
#define TEST_MAIN_H_


#include "gtest/gtest.h"

namespace cw {

/**
 *
 */
class Test_Main : public ::testing::Test {

protected:
	Test_Main();
	virtual ~Test_Main();

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:

	virtual void SetUp();

	virtual void TearDown();

	// Objects declared here can be used by all tests in the test case for Foo.

};

} /* namespace cw */

#endif /* TEST_MAIN_H_ */
