/**
 *  @file   spy_events.h
 *
 *  @date   Created on: Feb 18, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_EVENTS_H_
#define SPY_EVENTS_H_

#include <sys/types.h>

#ifdef __cplusplus
#include "misc.h" // for Diag
namespace cw {
#endif

/**
 * the structure that helps describe identity of the
 * such as a unique id, etc
 */
struct identity {
	int 		id;			      //! for storing unique id
	char* 		hostname;    	  //! for storing the name of the host
	pid_t		pid;			  //! the pid of the process

#ifdef __cplusplus
	identity();
	identity(int an_id, const char* p_hostname, pid_t a_pid);
	identity(const identity &other);
	identity & operator=(const identity & other);
	~identity();
protected:
	Diag copy_identity(const identity & other);
#endif // __cplusplus
};


#ifdef __cplusplus
} // namespace cw
#endif // __cplusplus

#endif /* SPY_EVENTS_H_ */
