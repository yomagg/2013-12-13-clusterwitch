/**
 *  @file   cpu_spy_events.h
 *
 *  @date   Created on: Feb 18, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CPU_SPY_EVENTS_H_
#define CPU_SPY_EVENTS_H_

#include "spy_events.h"

#ifdef __cplusplus
namespace cw {
#endif // __cplusplus

// --------------------------------
// structures that are used by evpath
// --------------------------------
/**
 * The structure that describes the cpu capabilities
 * This structure will be sent only once
 */
struct ev_cpu_cap {
	//! the identity of the cpu_spy
	struct identity id;
	//! the timestamp when we started measurement
	long long int ts;
	//! the cpu capabilities
	int 	core_count;
#ifdef __cplusplus
	ev_cpu_cap();
	ev_cpu_cap(const ev_cpu_cap &other);
	ev_cpu_cap & operator=(const ev_cpu_cap & other);
#endif // __cplusplus

};

/**
 * the record that contains monitoring data sent from scouts to troopers
 */
struct ev_cpu_mon {
	//! the id of the sending entity
	int	id;
	//! the timestamp when the measuremnt has occurred
	long long int ts;

	//! the number of cores plus 1 (for the total cpu usage)
	int core_plus_one_count;
	//! an array of size core_plus_one_count, that contains cpu usage 0..1,
	//! the size includes the total cpu usage as reported by /proc/stat
	//! the first element will contain the total usage as reported by
	//! /proc/stat; this is a kind of a weird thing since I often got
	//! 100%, whereas the entire system is almost all idle, this requires
	//! more in-depth investigation
	float * cpu_and_core_usage;
#ifdef __cplusplus
	ev_cpu_mon();
	ev_cpu_mon(const ev_cpu_mon &other);
	ev_cpu_mon & operator=(const ev_cpu_mon & other);
protected:
	void copy_usage_arr(const ev_cpu_mon & other);
#endif // __cplusplus
};


#ifdef __cplusplus
}
#endif // __cplusplus


#endif /* CPU_SPY_EVENTS_H_ */
