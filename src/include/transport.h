/**
 *  @file   transport.h
 *
 *  @date   Created on: Dec 31, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef TRANSPORT_H_
#define TRANSPORT_H_

#include <vector>
#include <string>
#include <set>

#include "evpath.h"
#include "ev_dfg.h"
#include "misc.h"

#include "dfg.h"

using namespace std;

namespace cw {

/**
 * EVPath alive message; this message will be sent in topo0
 * to establish the contact between the master and the worker
 */
struct ev_alive_msg {
	//! EVPath contact information e.g. AAIAAJTJ8o2yZQAAATkCmEoBqMA=
	char *contact;

	ev_alive_msg();
};
/**
 * Types of the monitoring or aggregation atoms corresponding to apporpriate
 * spies.
 */
enum class Atom_Type : int {
	UNKNOWN = -1,
	CPU = 0, //!< CPU
	MEM, //!< MEM
	NET, //!< NET
	NVML,//!< NVML
	LYNX //!< LYNX
};
/**
 * This is because the alive message is send by CMwrite, but CMwrite
 * does just sends the format and any handler that is listening for
 * a particular format can get that message, not necessary the handler
 * I wanted to use it for this purpose. I want to minimize the number
 * of information that needs to be shared via a file. So I would like
 * to have it more dynamic. To overcome this issue, I need to have a different
 * message so if CMwrite() sends a format it will be paired with the
 * unique alive_message_handler
 *
 * This message is designed to initialize the connection between the
 * monitoring entity and the aggregator entity.
 *
 * You need to modify accordingly the Ev_Alive_Mon_Msg wrapper
 * if you add/remove/modify fields in this structure
 */
struct ev_alive_mon_msg {
	//! the contact information
	char *contact;
	//! this will store all stone ids you want to store
	EVstone *stone_ids;
	//! how many stone ids I want to send
	int stone_ids_count;
	//! the type of the atom
	Atom_Type atom_type;


	ev_alive_mon_msg();
	ev_alive_mon_msg(int stones_count);

	~ev_alive_mon_msg();
};
/**
 * EVPath control message; this will be sent from the master to workers
 * to do some commands; the command should be parsed
 */
struct ev_ctrl_msg {
	//! this is the command to be sent
	char *cmd;
};

/**
 * This is basic EVPath message
 */
class Ev_Msg {
public:
	Ev_Msg();
	virtual ~Ev_Msg();

	FMStructDescRec * get_format_list();
	CMFormat get_format();
	FMField * get_field_list();

protected:
	//! this is for a simple field list
	FMField *field_list;

	//! this is for the format list
	FMStructDescRec *format_list;

	//! the format after registering
	CMFormat format;

	//! registers the format with evpath and does all necessary stuff
	virtual Diag reg()=0;

	Diag add_field_list_elem(int i, const char * name = nullptr, const char * type = nullptr,
			int size=0, int offset=0);
	//Diag add_field_list_elem(int i, const char * name, const char * type,
	//	int size, int offset);
	//Diag add_field_list_end_elem(int i);

	Diag add_format_list_elem(int i, const char * name = nullptr,
			FMFieldList p_fields = nullptr,
			int size = 0, FMOptInfo * p_info = nullptr);
};
/**
 * The representation of the control message
 */
class Ev_Ctrl_Msg :public Ev_Msg {
public:
	static Ev_Ctrl_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Ctrl_Msg *p_msg;

	Ev_Ctrl_Msg() = default;
	~Ev_Ctrl_Msg() = default;

	Ev_Ctrl_Msg(const Ev_Ctrl_Msg&) = delete;
	Ev_Ctrl_Msg & operator=(const Ev_Ctrl_Msg&) = delete;

	Diag reg();
};

/**
 * The representation of the alive message
 */
class Ev_Alive_Msg : public Ev_Msg {
public:
	static Ev_Alive_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my topology instance
	static Ev_Alive_Msg *p_msg;

	Ev_Alive_Msg() = default;
	~Ev_Alive_Msg() = default;

	Ev_Alive_Msg(const Ev_Alive_Msg&) = delete;
	Ev_Alive_Msg & operator=(const Ev_Alive_Msg&) = delete;

	Diag reg();
};
/**
 * The representation of the alive message for monitoring-aggregator
 * TODO you might want to make this and Ev_Alive_Msg a templated class
 * somehow
 */
class Ev_Alive_Mon_Msg : public Ev_Msg {
public:
	static Ev_Alive_Mon_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my singleton instance
	static Ev_Alive_Mon_Msg *p_msg;

	Ev_Alive_Mon_Msg() = default;
	~Ev_Alive_Mon_Msg() = default;

	Ev_Alive_Mon_Msg(const Ev_Alive_Mon_Msg&) = delete;
	Ev_Alive_Mon_Msg & operator=(const Ev_Alive_Mon_Msg&) = delete;

	Diag reg();
};
/**
 * The representation of the identity message
 */
class Ev_Identity_Msg : public Ev_Msg {
public:
	static Ev_Identity_Msg *get_instance();
	static bool exists();

protected:
	//! this will store my singleton instance
	static Ev_Identity_Msg *p_msg;

	Ev_Identity_Msg() = default;
	~Ev_Identity_Msg() = default;

	Ev_Identity_Msg(const Ev_Identity_Msg&) = delete;
	Ev_Identity_Msg & operator=(const Ev_Identity_Msg&) = delete;

	Diag reg();
};


/**
 * The general class that stores information
 */
class Info {
public:
	Info()=default;
	virtual ~Info() = default;
};

/**
 * The helper class for storing information about the worker
 */
class Worker_Info : public Info {
public:
	Worker_Info();
	virtual ~Worker_Info();
protected:
	string contact;
	string hostname;
};

/**
 * Transport topology
 */
class Topo {
public:
	Topo()=default;
	virtual ~Topo() = default;
};

/**
 * EVPath topology; this should be a singleton object
 */
class Ev_Topo : public Topo {
public:

	static Ev_Topo* get_instance();
	static CManager get_CM_now();
	static bool exists();
	static Diag run(int seconds);

	CManager get_CM();

protected:
	//! the connection manager
	CManager cm;

	//! this will store my topology instance
	static Ev_Topo *p_topo;

	Ev_Topo();
	virtual ~Ev_Topo();

	Ev_Topo(const Ev_Topo &) = delete;
	Ev_Topo & operator=(const Ev_Topo &) = delete;
};


/**
 * Stores: the connection output_id -> remote_id over the contact_to_remote
 * contact
 */
struct Ev_Comm_Link {
	EVstone output_id;			//! the output stone id
	string contact_to_remote;   //! the contact to the remote id
	EVstone remote_id;			//! the remote stone id
};

/**
 * Atom - the communication entity
 */
class Atom {
public:
	Atom()=default;
	virtual ~Atom() = default;
	virtual Diag join_topo()=0;
	virtual Diag quit_topo()=0;
};

/**
 * The EVPath communication entity for control messaging
 */
class Ev_Atom : public Atom {

public:
	Ev_Atom();
	virtual ~Ev_Atom();

	Diag get_my_contact(string & contact);
	Diag wrt_my_contact(const string & file_name, const string &contact);

	//! indicates the invalid stone identifer, e.g., uninitialized
	static const EVstone INVALID_STONE_ID;
	//! indicates the invalid action id, e.g. uninitialized
	static const EVaction INVALID_ACTION_ID;

protected:

	//! my topology
	Ev_Topo *p_topo;
};
/**
 * The Ev Master Atom in the EVPath world. The pair master-worker
 * is responsible for setting up the control communication channel
 * so it is possible to issue commands to individual workers.
 */
class Ev_Master_Atom : public Ev_Atom {
public:
	Ev_Master_Atom();
	virtual ~Ev_Master_Atom();

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Diag add_worker_contact(const char * contact);

	EVstone get_cmd_split_stone() const ;
	EVaction get_cmd_split_action() const;
	Diag add_cmd_link(const Ev_Comm_Link & link);

	Diag send_cmd(const string cmd);

	Diag run(const string ini_file_name);

protected:

	//! the stone that collects data from workers; registers the
	//! stones that join the topology
	EVstone alive_stone;

	//! the source of commands
	EVsource cmd_source;
	//! the splite stone for dispatching the command to all
	EVstone cmd_split_stone;
	//! the identifier of the split action for the command
	EVaction cmd_split_action;
	//! this stores the links to the
	vector<Ev_Comm_Link> cmd_links;

	//! stores contacts that were handled by the alive handler;
	set<string> worker_contacts;
};


/**
 * The Ev Worker Atom in the EVPath world
 */
class Ev_Worker_Atom : public Ev_Atom {
public:
	Ev_Worker_Atom(const string my_cfg_file_name=Cfg::CFG_DEFAULT_NAME);
	virtual ~Ev_Worker_Atom();

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Diag read_master_contact(const string & file_name);
	string get_master_contact();

	static const string CMD_MASTER_HANDLER_NAME;

	Diag run();

protected:

	//! this will store the name of the master contact
	string master_contact;

	//! the id of the master stone
	EVstone master_stone;

	//! for storing the connection to the master
	CMConnection conn;

	//! the name of the configuration file
	string cfg_file_name;
};

extern "C" int
master_cmd_handler(CManager cm, void *vevent, void *client_data, attr_list attrs);

/**
 * Base class for EVPath based monitoring atoms. This should be run
 * on the node where the actual monitoring data are gathered.
 */
class Ev_Monitoring_Atom : public Ev_Atom{
public:
	Ev_Monitoring_Atom();
	virtual ~Ev_Monitoring_Atom();
	virtual Diag quit_topo()=0;
	virtual Diag join_topo()=0;

	Diag read_aggregator_contact_list(const string & cfg_file_name);
	Diag set_aggreg_stone(EVstone an_aggreg_stone);
	Diag set_aggreg_contact_list(const string an_aggreg_contact_list);

	EVsource get_mon_source();

protected:

	//! the id of the split stone
	EVstone split_stone;
	//! the id of the split action
	EVaction split_action;

	//! the monitoring data will be sent over this stone
	EVstone output_stone;

	//! the source stone for submitting capabilities events
	EVsource cap_source;
	//! the source stone for submitting monitoring events
	EVsource mon_source;

	//! The id of the stone that is remote, that I got from
	//! the alive message
	EVstone aggreg_stone;

	//! stores the aggregator contact list to be able to send the alive message
	string aggreg_contact_list;

	//! TODO the name of the contact file for the monitoring atom, the monitoring
	//! atom needs to expose some contact information to enable API
	//! to contact it and add or remove communication infrastructure
	//! currently it is assume that each monitoring atom creates its
	//! own file with the contact information; it should be set by the
	//! derived class; I know this is a lamer approach
	string MON_ATOM_CONTACT_FILE;


	Diag setup_split_ntw_infrastructure();

	Diag wait_for_aggreg();

	Diag lets_join_topo(const Atom_Type atom_type);

};
/**
 * The base class for aggregators that aggregate data coming from
 * the Monitoring Atoms.
 */
class Ev_Aggregator_Atom : public Ev_Atom {
public:
	Ev_Aggregator_Atom();
	virtual ~Ev_Aggregator_Atom();

	virtual Diag join_topo()=0;
	virtual Diag quit_topo()=0;

	EVstone get_aggreg_stone() const;

protected:

	//! this will aggregate messages coming from the monitoring stones
	//! with this stone the terminal action should be associated
	//! that is respective to the format list
	//! this is a splitter stone but other actions can be
	//! associated as necessary
	EVstone aggreg_stone;
	EVaction split_action;

	Diag setup_ntw_basic_infrastructure();
};
// ---------------------------------------------------
// Splitter hook
// ---------------------------------------------------
/**
 * TODO not sure if this is the best approach
 * The intent of this class is to provide the mechanisms to obtain
 * the monitoring data in addition to the what is flowing via DFG
 * In the non-DFG topology it was done by setup_ntw_basic_infrastructure()
 * on the aggregator and monitoring atom. Although this is not in
 * the spirit of DFG, it is a workaround to get data locally. Given
 * that the proper infrastructure is setup, the external application
 * that is a process in a different process space can use that information
 * to interact with CW. Once DFGs have more flexibility and dynamicity
 * then the existence and purpose of Ev_Splitter_Hook should be terminated.
 *
 * Basically the class allows to instantiate an EVPATH splitter stone so the
 * API can create an output stone on the splitter if there is a need
 * and obtain the monitoring data.
 *
 * The idea behind this class is to have an independent src stone
 * that is connected to the splitter. Through the REVPATH the CW API
 * can create an output stone and thus get all events that the src submits.
 * To get the monitoring data, the src just needs to submit the data.
 * The contact info to splitter is dumped to the file.
 *
 * The splitter contact file is not removed and it is assumed the user
 * responsibility to remove the file if necessary.
 *
 * The default name of the file is Cfg::CFG_SPLITTERS_FILE_DEFAULT_NAME
 */
class Ev_Splitter_Hook {
public:
	Ev_Splitter_Hook(string contact_file_name=Cfg::CFG_SPLITTERS_FILE_DEFAULT_NAME);
	~Ev_Splitter_Hook() = default;

	Diag create_splitter_hook(FMStructDescRec * p_formats, const string key=Cfg::CFG_SPLITTERS_DEFAULT_KEY);
	string get_contact_file_name();

	EVstone get_split_stone();
	EVaction get_split_action();
	EVsource get_src();
	bool is_hook_created();

protected:
	//! the id of the split stone
	EVstone split_stone;
	//! the id of the split action
	EVaction split_action;
	//! this is the source that will be submitting
	//! events to the splitter stone
	EVsource src;

	//! TODO the name of the contact file for the monitoring atom, the monitoring
	//! atom needs to expose some contact information to enable API
	//! to contact it and add or remove communication infrastructure
	//! currently it is assume that each monitoring atom creates its
	//! own file with the contact information;
	//! the name is set in the splitter constructor
	//! I know this is a lamer approach
	string contact_file;
};


// ---------------------------------------------------
// For DFG topology
// ---------------------------------------------------

// -------------------------------------------
// Mon_Atom template
// -------------------------------------------

/**
 * The template for monitoring classes
 * TODO eliminate all xxx_Mon_Atom classes and change it to this class
 *
 * The base class for monitoring cpu; it should have all functionality
 * for the local monitoring; it should not have anything related to the
 * evpath
 *
 * To get rid of compilation issues you should put the implementation
 * of the template in the same file. The compiler does not know
 * your template and you need to help it find where the implementation
 * is.
 */
template <class Spy_Type, class Sqlite3_Reader_Type> class Mon_Atom {
public:
	Mon_Atom() = default;
	virtual ~Mon_Atom() = default;

	Spy_Type *get_spy();
	Sqlite3_Reader_Type * get_sqlite3_reader();

protected:
	//! this is our spy
	Spy_Type spy;
	//! this is the sqlite3 reader
	Sqlite3_Reader_Type sqlite3_reader;
};

/**
 * Getter for the spy
 */
template <class Spy_Type, class Sqlite3_Reader_Type>
Spy_Type* Mon_Atom<Spy_Type, Sqlite3_Reader_Type>::get_spy(){
	return &spy;
}
/**
 * getter for the reader
 */
template <class Spy_Type, class Sqlite3_Reader_Type>
Sqlite3_Reader_Type* Mon_Atom<Spy_Type, Sqlite3_Reader_Type>::get_sqlite3_reader(){
	return &sqlite3_reader;
}

// -------------------------------------------
// Ev_Dfg_Atom
// -------------------------------------------

/**
 * This is the base class for the DFG topology
 */
class Ev_Dfg_Atom : public Ev_Atom {
public:
	Ev_Dfg_Atom() = default;
	// probably hooks should be deallocated and shutdown somehow
	virtual ~Ev_Dfg_Atom()=default;
	virtual Diag join_topo();
	virtual Diag quit_topo();

	vector<Ev_Splitter_Hook*> & get_splitter_hook();
protected:

	Diag load_join_info(join_info *p_info, string node_name, string stone_name, FMStructDescRec * p_formats);

	//! the splitter hook
	vector<Ev_Splitter_Hook*> splitter_hook_vec;
};
/**
 * This class is responsible for providing basic functionality for the source.
 */
class Ev_Dfg_White_Atom : public virtual Ev_Dfg_Atom {
public:
	Ev_Dfg_White_Atom()=default;
	virtual ~Ev_Dfg_White_Atom()=default;
	virtual Diag join_dfg_node(string node_name, string stone_name, FMStructDescRec * p_formats);

	vector<EVsource> & get_src();
protected:

	Diag load_source(source_join_info *p_src, string node_name, string stone_name, FMStructDescRec * p_formats);

	//! EVPath handler for the source
	vector<EVsource> src_vec;
};

/**
 * This class provides basic functionality fo the sink.
 */
class Ev_Dfg_Black_Atom : public virtual Ev_Dfg_Atom {
public:
	Ev_Dfg_Black_Atom() = default;
	virtual ~Ev_Dfg_Black_Atom()=default;
	virtual Diag join_dfg_node(string node_name, string stone_name, FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void * client_data);

protected:
	Diag load_sink(sink_join_info *p_info, string node_name, string stone_name,
			FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void* client_data);

};

/**
 * This is the class that combines the functionality of the sink and the source.
 */
class Ev_Dfg_Gray_Atom : public virtual Ev_Dfg_White_Atom, public virtual Ev_Dfg_Black_Atom{
public:
	Ev_Dfg_Gray_Atom() = default;
	virtual ~Ev_Dfg_Gray_Atom() = default;

	virtual Diag join_dfg_node(string node_name,
			string white_stone_name, string black_stone_name,
			FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void * client_data);
};
} /* namespace cw */

#endif /* TRANSPORT_H_ */
