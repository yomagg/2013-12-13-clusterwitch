/**
 *  @file   spy_agent_utils.h
 *
 *  @date   Created on: Feb 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef SPY_AGENT_UTILS_H_
#define SPY_AGENT_UTILS_H_

#include "evpath.h"
#include "spy_agent.h"
#include "spy_agent_utils_types.h"


extern diag_t read_splitter_contact(const  char * file_name,
		struct ragent_info *p_ragent_info);
extern diag_t dfg_read_splitter_contact(const  char * file_name,
		struct ragent_info *p_ragent_info);

extern diag_t create_output_stone(struct ragent_info *p_ragent_info,
		const struct ev_handler_info *p_handler_info);
extern diag_t destroy_output_stone(struct ragent_info *p_ragent_info);
extern diag_t register_ragent(struct ragent_info * arr, int arr_len, int * p_arr_slot,
		struct ragent_info *p_ragent_info);


#endif /* SPY_AGENT_UTILS_H_ */
