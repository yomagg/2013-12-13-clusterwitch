/**
 *  @file   nvml_spy_events_desc.h
 *
 *  @date   Created on: Feb 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NVML_SPY_EVENTS_DESC_H_
#define NVML_SPY_EVENTS_DESC_H_

#ifdef __cplusplus
using namespace cw;
#endif

#include "spy_events_desc.h" // identity desc

// -------------------------------------------
// capabilities
// -------------------------------------------
static FMField nvml_dev_cap_field_list[] = {
	{"product_name", "string", sizeof(char*), FMOffset(struct ev_nvml_dev_cap *, product_name)},
	{"serial_number", "string", sizeof(char*), FMOffset(struct ev_nvml_dev_cap *, serial_number)},
	{"compute_mode", "integer", sizeof(int), FMOffset(struct ev_nvml_dev_cap*, compute_mode)},
	{"id", "integer", sizeof(int), FMOffset(struct ev_nvml_dev_cap *, id)},
	{"mem_total_bytes", "integer", sizeof(unsigned long long), FMOffset(struct ev_nvml_dev_cap *, mem_total_bytes)},
	{"power_limit_mW", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_cap *, power_limit_mW)},
	{"max_graphics_clock_MHz", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_cap *, max_graphics_clock_MHz)},
	{"max_SM_clock_MHz", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_cap *, max_SM_clock_MHz)},
	{"max_memory_clock_MHz", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_cap *, max_memory_clock_MHz)},
	{NULL, NULL, 0, 0}
};

//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
static FMStructDescRec nvml_dev_cap_format_list[] = {
		{ "ev_nvml_dev_cap", nvml_dev_cap_field_list, sizeof(struct ev_nvml_dev_cap), NULL},
		{NULL, NULL, 0, NULL}
};

static FMField nvml_cap_field_list[] = {
	{"id", "identity", sizeof(struct identity), FMOffset(struct ev_nvml_cap *, id)},
	{"ts", "integer", sizeof(long long int), FMOffset(struct ev_nvml_cap *, ts)},
	{"driver_ver", "string", sizeof(char*), FMOffset(struct ev_nvml_cap *, driver_ver)},
	{"gpu_count", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_cap *, gpu_count)},
	{"devs", "ev_nvml_dev_cap[gpu_count]", sizeof(struct ev_nvml_dev_cap), FMOffset(struct ev_nvml_cap *, devs)},
	{NULL, NULL, 0, 0}
};

//! the order of struct is important, first the main, then the elements that
//! are defined in the structure first; first introduce then explain
static FMStructDescRec nvml_cap_format_list[] = {
		{ "ev_nvml_cap", nvml_cap_field_list, sizeof(struct ev_nvml_cap), NULL },
		{ "identity", id_field_list, sizeof(struct identity), NULL },
		{ "ev_nvml_dev_cap", nvml_dev_cap_field_list, sizeof(struct ev_nvml_dev_cap), NULL},
		{NULL, NULL, 0, NULL}
};

// ---------------------------------------------
// monitoring
// ---------------------------------------------
static FMField nvml_mon_dev_field_list[] = {
	{"performance_state", "integer", sizeof(int), FMOffset(struct ev_nvml_dev_mon *, performance_state)},
	{"mem_used_bytes", "integer", sizeof(unsigned long long), FMOffset(struct ev_nvml_dev_mon *, mem_used_bytes)},
	{"nvml_util_gpu", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_mon *, nvml_util_gpu)},
	{"nvml_util_mem", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_dev_mon *, nvml_util_mem)},
	{"util_mem", "float", sizeof(float), FMOffset(struct ev_nvml_dev_mon *, util_mem)},
	{"power_draw", "float", sizeof(float), FMOffset(struct ev_nvml_dev_mon *, power_draw)},
	{"graphics_clock", "float", sizeof(float), FMOffset(struct ev_nvml_dev_mon *, graphics_clock)},
	{"sm_clock", "float", sizeof(float), FMOffset(struct ev_nvml_dev_mon *, sm_clock)},
	{"mem_clock", "float", sizeof(float), FMOffset(struct ev_nvml_dev_mon *, mem_clock)},
	{NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
static FMStructDescRec nvml_mon_dev_format_list[] = {
  { "ev_nvml_dev_mon", nvml_mon_dev_field_list, sizeof(struct ev_nvml_dev_mon), NULL},
  {NULL, NULL, 0, NULL}
};


static FMField nvml_mon_field_list[] = {
  {"id", "integer", sizeof(int), FMOffset(struct ev_nvml_mon *, id)},
  {"ts", "integer", sizeof(long long int), FMOffset(struct ev_nvml_mon *, ts)},
  {"gpu_count", "integer", sizeof(unsigned int), FMOffset(struct ev_nvml_mon *, gpu_count)},
  {"gpu_arr", "ev_nvml_dev_mon[gpu_count]", sizeof(struct ev_nvml_dev_mon), FMOffset(struct ev_nvml_mon *, gpu_arr)},
  {NULL, NULL, 0, 0}
};

//! start with the main structure, then describe the substructures
static FMStructDescRec nvml_mon_format_list[] = {
  { "ev_nvml_mon", nvml_mon_field_list, sizeof(struct ev_nvml_mon), NULL },
  { "ev_nvml_dev_mon", nvml_mon_dev_field_list, sizeof(struct ev_nvml_dev_mon), NULL},
  {NULL, NULL, 0, NULL}
};


#endif /* NVML_SPY_EVENTS_DESC_H_ */
