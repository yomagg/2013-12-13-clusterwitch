/**
 *  @file   net_transport.h
 *
 *  @date   Created on: Feb 25, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef NET_TRANSPORT_H_
#define NET_TRANSPORT_H_

#include "transport.h"
#include "net_spy.h"
#include "sqlite3_reader.h"

namespace cw {

/**
 * The base class for monitoring net; it should have all functionality
 * for the local monitoring; it should not have anything related to the
 * evpath
 */
class Net_Mon_Atom {

public:
	Net_Mon_Atom() = default;
	virtual ~Net_Mon_Atom() = default;

	Net_Spy *get_spy();
	Net_Sqlite3_Reader * get_sqlite3_reader();

protected:
	//! provides the monitoring functionality
	Net_Spy spy;

	//! the sqlite3 reader
	Net_Sqlite3_Reader sqlite3_reader;
};

// -----------------------------------------------------
// This is star topology
// -----------------------------------------------------
/**
 * the monitoring component for the mem_spy
 */
class Ev_Net_Monitoring_Atom : public Ev_Monitoring_Atom {
public:
	Ev_Net_Monitoring_Atom();
	virtual ~Ev_Net_Monitoring_Atom() = default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Net_Spy *get_spy();
protected:
	Net_Spy spy;

	Diag send_cap();
	Diag send_mon();
};

/**
 * Aggregator Atom
 */
class Ev_Net_Aggregator_Atom : public Ev_Aggregator_Atom {
public:
	Ev_Net_Aggregator_Atom() = default;
	virtual ~Ev_Net_Aggregator_Atom()=default;

	virtual Diag join_topo();
	virtual Diag quit_topo();

	Net_Sqlite3_Reader *get_sqlite3_reader();
	Net_Spy *get_spy();

protected:
	//! the sqlite3 reader
	Net_Sqlite3_Reader sqlite3_reader;
	//! the spy for the processing function
	Net_Spy spy;
};

// ------------------------
// for DFG topology
// ------------------------
/**
 * this is source
 */
class Ev_Dfg_Net_White_Atom : public Ev_Dfg_White_Atom, public Net_Mon_Atom {
public:
	Ev_Dfg_Net_White_Atom()=default;
	virtual ~Ev_Dfg_Net_White_Atom()=default;
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag run();
	virtual Diag quit_topo();
protected:
	Diag send_mon();
};
/**
 * This is the aggregator-terminator functions (sink)
 */
class Ev_Dfg_Net_Black_Atom : public Ev_Dfg_Black_Atom, public Net_Mon_Atom {
public:
	Ev_Dfg_Net_Black_Atom();
	virtual ~Ev_Dfg_Net_Black_Atom();
	virtual Diag reg(string node_name, string stone_name);
	virtual Diag quit_topo();
};
/**
 * This is a Reader and a Writer (sink and source)
 */
class Ev_Dfg_Net_Gray_Atom : public virtual Ev_Dfg_Gray_Atom, public Net_Mon_Atom{
public:
	Ev_Dfg_Net_Gray_Atom() ;
	virtual ~Ev_Dfg_Net_Gray_Atom() ;
	virtual Diag reg(string node_name, string sink_name, string src_name);
	virtual Diag quit_topo();
protected:
	Diag send_mon(); // TODO seems unnecessary
};

}



#endif /* NET_TRANSPORT_H_ */
