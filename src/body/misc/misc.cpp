/**
 *  @file   misc.cpp
 *
 *  @date   Created on: Dec 19, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h> // for stat()

#include <cassert>
#include <iostream>
#include <fstream>

#include "easylogging++.h"
using namespace std;

#include "INI.h"

#include "misc.h"
#include "transport.h"


namespace cw {

// --------------------------------------------------
// Time_Stamp
// --------------------------------------------------
Time_Stamp::Time_Stamp() {

}

Time_Stamp::~Time_Stamp() {
}
/**
 * Returns the timestamp as an integer
 *
 * @return the timestamp converted to long long int
 */
long long int Time_Stamp::get_ts() {
	struct timeval tv;
	gettimeofday(&tv, nullptr);

	return (long long int)(tv.tv_sec * 1000000 + tv.tv_usec) ;
}

// ----------------------------------------
// Cfg
// ----------------------------------------

const string Cfg::CFG_DEFAULT_NAME = "cw.ini";
const string Cfg::CFG_EV_MASTER_SEC = "ev_master";
const string Cfg::CFG_EV_MASTER_SEC_CONTACT_KEY = "contact";

// in seconds
const int Cfg::CFG_EV_MONITOR_PERIOD = 1;
// additional microsecs
const int Cfg::CFG_EV_MONITOR_PERIOD_USEC = 0;

// in seconds
const int Cfg::WAIT_FOR_AGGREG_TIMEOUT_SEC = 30;


const string Cfg::CFG_SQLITE3_READER_OUTPUT_DIR = "/tmp";

const string Cfg::CFG_CPU_MON_ATOM_FILE="cw-cpu-mon-contact.txt";
const string Cfg::CFG_MEM_MON_ATOM_FILE="cw-mem-mon-contact.txt";
const string Cfg::CFG_NET_MON_ATOM_FILE="cw-net-mon-contact.txt";
const string Cfg::CFG_NVML_MON_ATOM_FILE="cw-nvml-mon-contact.txt";
//const string Cfg::CFG_LYNX_MON_ATOM_FILE="cw-lynx-mon-contact.txt";

const string Cfg::CFG_SPLITTERS_FILE_DEFAULT_NAME = "cw-splitters.txt";
const string Cfg::CFG_SPLITTERS_SEC = "splitters";
const string Cfg::CFG_SPLITTERS_DEFAULT_KEY = "splitter_key";



/**
 * Writes a string to the config file. Checks if input parameter are zero
 *
 * @param file_name The name of the file to be read
 * @param value The value to be written to the file
 * @param section Under what section to write the string
 * @param key Under what key write this value
 * @return Diag::OK always
 */
Diag Cfg::wrt_val(const string & file_name, const string & value,
		const string & section, const string & key){
	assert(file_name.size() > 0);
	assert(value.size() > 0);
	assert(section.size() > 0);
	assert(key.size() > 0);

	Diag diag = Diag::OK;
	INI <string, string, string> ini(file_name, true); // <Section, Key, Value>

	// create (if not exists) and select as a current section
	ini.create(section);
	ini.set(key, value);
	ini.save();

	return diag;
}

/**
 * Writes an  integer to the config file. Checks if input parameter are zero
 *
 * @param file_name The name of the file to be read
 * @param value The value to be written to the file
 * @param section Under what section to write the string
 * @param key Under what key write this value
 * @return Diag::OK always
 */
Diag Cfg::wrt_val(const string & file_name, int value,
		const string & section, const string & key){
	assert(file_name.size() > 0);
	assert(section.size() > 0);
	assert(key.size() > 0);

	Diag diag = Diag::OK;
	INI <string, string, int> ini(file_name, true); // <Section, Key, Value>

	// create (if not exists) and select as a current section
	ini.create(section);
	ini.set(key, value);
	ini.save();

	return diag;
}

/**
 * Read the string value from the cfg file. Not sure what happens if the
 * not existing key or section is provided as a parameter.
 *
 * @param file_name (IN)
 * @param value (OUT) The read value will be returned here
 * @param section (IN) from what section
 * @param key (IN) from what key
 * @return
 */
Diag Cfg::read_val(const string & file_name, std::string & value,
		const string & section, const std::string & key){
	assert(file_name.size() > 0);
	assert(section.size() > 0);
	assert(key.size() > 0);

	Diag diag = Diag::OK;

	// currently a hack because INI.h does not provide checking
	// if file does not exists
	if( !file_exists(file_name.c_str()) ){
		return Diag::ERR;
	}

	INI< string, string, string> ini(file_name, true); // <section, key, value>

	ini.select(section);
	value = ini.get(key);

	return diag;
}

/**
 * Read the string value from the cfg file. Not sure what happens if the
 * not existing key or section is provided as a parameter.
 *
 * @param file_name (IN)
 * @param value (OUT) The read value will be returned here
 * @param section (IN) from what section
 * @param key (IN) from what key
 * @return
 */
Diag Cfg::read_val(const string & file_name, int * value,
		const string & section, const std::string & key){
	assert(file_name.size() > 0);
	assert(section.size() > 0);
	assert(key.size() > 0);

	Diag diag = Diag::OK;

	// currently a hack because INI.h does not provide checking
	// if file does not exists
	if( !file_exists(file_name.c_str()) ){
		return Diag::ERR;
	}

	INI< string, string, string> ini(file_name, true); // <section, key, value>

	ini.select(section);
	*value = ini.get<int>(key);

	return diag;
}

/**
 * Removes the config file. Checks if the file name is not empty
 *
 * @param file_name what file to remove
 * @return Diag::OK always
 */
Diag Cfg::rm_cfg(const std::string & file_name){
	Diag diag = Diag::OK;
	assert(file_name.size()>0);
	if ( remove(file_name.c_str()) != 0 ){
		LOG(WARNING) << "Can't remove the monitoring contact file: "
					<< file_name;
		diag = Diag::WARN;
	}

	return diag;
}
/**
 *
 * @return returns the aggregator timeout in seconds
 */
int Cfg::get_wait_for_aggreg_timeout(){
	return WAIT_FOR_AGGREG_TIMEOUT_SEC;
}

int Cfg::get_monitor_period(){
	return CFG_EV_MONITOR_PERIOD;
}


/**
 * Check if a file exists. Required because INI.h does not support
 * finding this.
 *
 * @return true if and only if the file exists, false else
 */
bool Cfg::file_exists(const std::string& file) {
	struct stat buf;
	return (stat(file.c_str(), &buf) == 0);
}
// ---------------------------------------------
// Cfg_Dfg
// ----------------------------------------------
//! the name of the section in the CW ini file for sqlite3 configuration
const string Cfg_Dfg::CFG_SQLITE3_SEC = "sqlite3";
//! the key for the output dir for the sqlite3
const string Cfg_Dfg::CFG_SQLITE3_OUTPUT_DIR_KEY = "output_dir";

//! the name of the section in the CW ini file related to initial monitoring parameters
const string Cfg_Dfg::CFG_EV_MONITORING_SEC = "monitoring";
//! the key for sampling in seconds
const string Cfg_Dfg::CFG_EV_MON_SAMPLING_RATE_KEY_SEC = "sampling_every_sec";
//! the key for additional sampling in microsecs (see @CMadd_periodic_task)
const string Cfg_Dfg::CFG_EV_MON_SAMPLING_RATE_KEY_USEC = "sampling_every_usec";

//! the running time for the dfg master in sec
const string Cfg_Dfg::CFG_EV_DFG_MASTER_RUNNING_KEY = "dfg_master_running_time_sec";
//! the default value for the dfg master; if -1 then interpreted as
//! infinity; otherwise should be specified in seconds
const int Cfg_Dfg::CFG_EV_DFG_MASTER_RUNNING_SEC = -1;
//! the running time for the dfg client in sec
const string Cfg_Dfg::CFG_EV_DFG_CLIENT_RUNNING_KEY = "dfg_client_running_time_sec";
//! the default value for the execution time for the dfg client
//! -1 interpreted as infinity; otherwise should be positive and specified
//! in seconds
const int Cfg_Dfg::CFG_EV_DFG_CLIENT_RUNNING_SEC = -1;

/**
 * Creates the default config file for the DFG topology.
 * the name of the file is defined by Cfg::CFG_DEFAULT_NAME
 * @return Diag::OK everything went fine
 *         Diag::ERR something went wrong
 */
Diag Cfg_Dfg::create_cfg_file(){
	Diag diag = Diag::OK;
	string file_name = CFG_DEFAULT_NAME;

	assert(file_name.size() > 0);

	INI <string, string, string> ini(file_name, true); // <Section, Key, Value>

	// create (if not exists) and select as a current section
	// the creation order: the last one will first appear in the cfg file

	ini.create(CFG_SQLITE3_SEC);
	ini.set(CFG_SQLITE3_OUTPUT_DIR_KEY, CFG_SQLITE3_READER_OUTPUT_DIR);

	ini.create(CFG_EV_MONITORING_SEC);
	ini.set(CFG_EV_MON_SAMPLING_RATE_KEY_USEC, CFG_EV_MONITOR_PERIOD_USEC);
	ini.set(CFG_EV_MON_SAMPLING_RATE_KEY_SEC, CFG_EV_MONITOR_PERIOD);

	ini.set(CFG_EV_DFG_MASTER_RUNNING_KEY, CFG_EV_DFG_MASTER_RUNNING_SEC);
	ini.set(CFG_EV_DFG_CLIENT_RUNNING_KEY, CFG_EV_DFG_CLIENT_RUNNING_SEC);
	ini.save();


	return diag;
}

/**
 * Gets the output directory for storing the sqlite3 databases; first
 * it attempts to get that value from the configuration file; if it is
 * unsuccessful for some reason; it returns the default value
 *
 * @return The output directory for storing sqlite3 db files
 */
string Cfg_Dfg::get_sqlite3_reader_output_dir(){
	string output_dir;

	get(CFG_SQLITE3_SEC, CFG_SQLITE3_OUTPUT_DIR_KEY, output_dir, CFG_SQLITE3_READER_OUTPUT_DIR);

	return output_dir;
}
/**
 * Gets the secs monitoring sampling rate; first it tries to get that value
 * from the configuration file; if unsuccessful it returns the default value
 * @return
 */
int Cfg_Dfg::get_monitoring_sampling_sec_rate(){
	int sampling_rate;

	get(CFG_EV_MONITORING_SEC,
			CFG_EV_MON_SAMPLING_RATE_KEY_SEC, &sampling_rate, CFG_EV_MONITOR_PERIOD);

	return sampling_rate;
}
/**
 * Gets the microsecs monitoring sampling rate; first it tries to get that value
 * from the configuration file; if unsuccessful it returns the default value
 * @return
 */
int Cfg_Dfg::get_monitoring_sampling_usec_rate(){
	int sampling_rate;

	get(CFG_EV_MONITORING_SEC,
				CFG_EV_MON_SAMPLING_RATE_KEY_USEC, &sampling_rate, CFG_EV_MONITOR_PERIOD_USEC);

	return sampling_rate;
}
/**
 * Gets the time for running the dfg master in sec
 *
 * @return -1 if the time should be infinity (the default value)
 *          >0 if the time is finite
 */
int Cfg_Dfg::get_dfg_master_running_time_sec() {
	int exec_time;

	get(CFG_EV_MONITORING_SEC,
			CFG_EV_DFG_MASTER_RUNNING_KEY, &exec_time, CFG_EV_DFG_MASTER_RUNNING_SEC);

	if (exec_time < 0){
		exec_time = CFG_EV_DFG_MASTER_RUNNING_SEC;
	}

	return exec_time;
}
/**
 * Gets the time for running the dfg client in sec
 *
 * @return -1 if the time should be infinity (the default value)
 *          >0 if the time is finite
 */
int Cfg_Dfg::get_dfg_client_running_time_sec() {
	int exec_time;

	get(CFG_EV_MONITORING_SEC,
			CFG_EV_DFG_CLIENT_RUNNING_KEY, &exec_time, CFG_EV_DFG_CLIENT_RUNNING_SEC);

	if (exec_time < 0){
		exec_time = CFG_EV_DFG_CLIENT_RUNNING_SEC;
	}

	return exec_time;
}

/**
 * Gets the string from the CFG_DEFAULT_NAME
 * @param sec section to look for
 * @param key the key to look for
 * @param val where the result will be returned
 * @param default_val the default value if the value can't be read from the
 *        file
 * @return Diag::OK if the value was read from the file
 *         != Diag::OK if the value is default ie. there was an error when
 *                     reading the value
 */
Diag Cfg_Dfg::get(const string & sec, const std::string& key, int* val, int default_val) {
	Diag diag = Diag::OK;

	if( (diag = read_val(CFG_DEFAULT_NAME, val, sec, key)) != Diag::OK){
		LOG(WARNING) << "Can't get the value from the configuration file. " <<
				"Setting a default value: " << sec << ":" << key << "=" <<
				default_val;
		*val = default_val;
		return diag;
	}

	return diag;
}

/**
 * Gets the string from the CFG_DEFAULT_NAME
 * @param sec section to look for
 * @param key the key to look for
 * @param val where the result will be returned
 * @param default_val the default value if the value can't be read from the
 *        file
 * @return Diag::OK if the value was read from the file
 *         != Diag::OK if the value is default ie. there was an error when
 *                     reading the value
 */
Diag Cfg_Dfg::get(const string & sec, const string& key, string& val,
		const string& default_val) {
	Diag diag = Diag::OK;

	if( (diag = read_val(CFG_DEFAULT_NAME, val,
		sec, key)) != Diag::OK ){
		LOG(WARNING) << "Can't get the value from the configuration file. " <<
				"Setting a default value: " << sec << ":" << key << "=" <<
				default_val;
		val = default_val;
		return Diag::ERR;
	}

	return diag;
}

// ------------------------------------------------
// Shared_File
// -----------------------------------------------

Contact_File::Contact_File(){

}
Contact_File::~Contact_File(){

}
/**
 * Writes a content to the file. There is assertion on the name of the file
 *
 * @param file_name (IN)
 * @param contact (IN) what will be written
 * @return Always Diag::OK
 */
Diag Contact_File::wrt_to(const string & file_name, const struct Ev_Contact & contact){
	Diag diag = Diag::OK;

	assert(file_name.size() > 0);
	ofstream myfile;
	myfile.open(file_name.c_str(), ios::out | ios::trunc);
	myfile <<  contact.contact << " " << contact.split_stone << " " << contact.split_action;
	myfile.close();

	return diag;
}
/**
 * Reads the data from the file and outputs the data to the contact. If it can't
 * open the file it doesn't change the contact value
 *
 * @param file_name (IN)
 * @param contact (OUT) Where we write the data that we read from the file_name
 * @return Diag::OK if everything went fine
 *         Diag::ERR if anything went wrong (e.g. can't open a file)
 */
Diag Contact_File::read_from(const std::string& file_name,
		struct Ev_Contact& contact) {
	Diag diag = Diag::OK;

	assert(file_name.size() > 0);
	ifstream myfile(file_name);

	if( !myfile){
		LOG(ERROR) << "Can't open a file: " << file_name;
		return Diag::ERR;
	}

	myfile >> contact.contact >> contact.split_stone >> contact.split_action;
	myfile.close();

	return diag;
}

/**
 * This gets the information how to contact me by the entity in the
 * EVPath world
 *
 * @param contact (OUT) This will contain the contact information of this
 *                      entity
 * @return Diag::OK if everything went ok
 *         != Diag::OK if anything went wrong
 */
Diag Ev_Utils::get_my_contact(string& contact) {

	Diag diag = Diag::OK;

	CManager cm = Ev_Topo::get_CM_now();

	// this allocates memory, I need to free it if not needed
	attr_list contact_list = CMget_contact_list(cm);
	if(!contact_list){
		LOG(ERROR) << "CMget_contact_list returned error. Returning ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	}
	contact = attr_list_to_string(contact_list);

	// I don't need the attr_list any more
	free_attr_list(contact_list);

	return diag;

}

/**
 * Reads the contact list of the master given the configuration file name
 *
 * @param cfg_file_name (IN) The name of the configuration file
 * @param contact (OUT) The contact list of the Cfg::CFG_EV_MASTER_SEC/Cfg::CFG_EV_MASTER_SEC_CONTACT_KEY
 * @return Diag::OK if everything went great
 *         != Diag::OK, e.g. Diag::INI_NO_FILE_ERR if anything went wrong
 */
Diag Ev_Utils::get_master_contact_list(const std::string &cfg_file_name, std::string & contact){
	Diag diag = Diag::OK;

	assert(cfg_file_name.size() > 0);

	INI <std::string, string, string> ini(cfg_file_name, true);  // parse by default
	if (!ini.select(Cfg::CFG_EV_MASTER_SEC)){
		diag = Diag::INI_NO_FILE_ERR;
		LOG(ERROR) << "The config file does not exist. Returning ...";
		return diag;
	}

	contact = ini.get(Cfg::CFG_EV_MASTER_SEC_CONTACT_KEY);

	return diag;
}

// --------------------------------
// Ev_Comm_Patterns
// -------------------------------
/**
 * This pattern allows to send an alive message from a worker to a master,
 * given that the contact to the master is passed
 *
 * It checks if the contact is of size greater than 0.
 *
 * It is supposed to be called with T1 - struct ev_alive_xxx_msg and the relevant
 * Ev_Alive_xxx message wrapper class for T2.
 *
 * @param contact (IN) Something like this AAIAAJTJ8o2TZQAAATkCmDkxPYA=
 * @param conn (OUT) This will hold a new initialized connection
 * @param p_msg (IN) The message to be sent
 * @param p_dummy (IN) a dummy parameter to pass the type of the class, ignored
 * @return
 */
/*
 Unfinished business with template fight (see transport.cpp, misc.h)
template <class T1, class T2>
Diag Ev_Comm_Pattern::send_alive_pattern(const string & contact, CMConnection & conn,
		const T1 *p_msg, const T2 *p_dummy){
	Diag diag = Diag::OK;

	assert(contact.size() > 0);

	CManager cm = Ev_Topo::get_CM_now();

	attr_list  contact_list = attr_list_from_string(contact.c_str());

	if ((conn = CMinitiate_conn(cm, contact_list)) == nullptr){
		LOG(ERROR) << "Can't initiate a connection. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	} else {
		LOG(DEBUG) << "The connection established.";
	}

	// now send a message
	T2 *msg = T2::get_instance();

	if (1 != CMwrite(conn, msg->get_format(), const_cast<T1*>(p_msg))){
		LOG(ERROR) << "Issues with writing to the master. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	}

	return diag;
}
*/
/**
 * This pattern allows to send an alive message from a worker to a master,
 * given that the contact to the master is passed
 *
 * It checks if the contact is of size greater than 0.
 *
 * It is supposed to be called with T1 - struct ev_alive_xxx_msg and the relevant
 * Ev_Alive_xxx message wrapper class for T2.
 *
 * @param contact (IN) Something like this AAIAAJTJ8o2TZQAAATkCmDkxPYA=
 * @param conn (OUT) This will hold a new initialized connection
 * @param p_msg (IN) The message to be sent
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong
 */

cw::Diag send_alive_pattern(const string & contact, CMConnection & conn,
		const ev_alive_msg *p_msg){
	Diag diag = Diag::OK;

	assert(contact.size() > 0);

	CManager cm = Ev_Topo::get_CM_now();

	attr_list  contact_list = attr_list_from_string(contact.c_str());

	if ((conn = CMinitiate_conn(cm, contact_list)) == nullptr){
		LOG(ERROR) << "Can't initiate a connection. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	} else {
		LOG(DEBUG) << "The connection established.";
	}

	// now send a message
	Ev_Alive_Msg *msg = Ev_Alive_Msg::get_instance();

	if (1 != CMwrite(conn, msg->get_format(), const_cast<struct ev_alive_msg*>(p_msg))){
		LOG(ERROR) << "Issues with writing to the master. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	}

	return diag;
}
/**
 * This pattern allows to send an alive message from a worker to a master,
 * given that the contact to the master is passed
 *
 * It checks if the contact is of size greater than 0.
 *
 * It is supposed to be called with T1 - struct ev_alive_xxx_msg and the relevant
 * Ev_Alive_xxx message wrapper class for T2.
 *
 * @param contact (IN) Something like this AAIAAJTJ8o2TZQAAATkCmDkxPYA=
 * @param conn (OUT) This will hold a new initialized connection
 * @param p_msg (IN) The message to be sent
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong
 */

Diag send_alive_pattern(const string & contact, CMConnection & conn,
		const struct ev_alive_mon_msg *p_msg){
	Diag diag = Diag::OK;

	assert(contact.size() > 0);

	CManager cm = Ev_Topo::get_CM_now();

	attr_list  contact_list = attr_list_from_string(contact.c_str());

	if ((conn = CMinitiate_conn(cm, contact_list)) == nullptr){
		LOG(ERROR) << "Can't initiate a connection. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	} else {
		LOG(DEBUG) << "The connection established.";
	}

	// now send a message
	Ev_Alive_Mon_Msg *msg = Ev_Alive_Mon_Msg::get_instance();

	if (1 != CMwrite(conn, msg->get_format(), const_cast<struct ev_alive_mon_msg*>(p_msg))){
		LOG(ERROR) << "Issues with writing to the master. ATS ...";
		diag = Diag::EV_CM_ERR;
		return diag;
	}

	return diag;
}
// -----------------------------------------
// Processing functions
// -----------------------------------------
/**
 * calculates the percentage; it doesn't check if total
 * and x are positive
 * @param total
 * @param x
 * @return  x/total*100.0
 */
float Processing_Functions::percentage(float total, float x){
	return static_cast<float>(x/total*100.0);
}

// --------------------------------------------
// Characteristics
// -------------------------------------------
/**
 * Calculates the utilization characteristic (based on a simple percentage)
 *
 * @param total
 * @param x
 * @return x/total*100.0 if total > 0.0, otherwise 0.0
 */
float Characteristics::utilization(float total, float x){
	return ( (total > 0.0 && x > 0.0) ? static_cast<float>(x/total*100.0) : 0.0);
}

} /* namespace cw */

