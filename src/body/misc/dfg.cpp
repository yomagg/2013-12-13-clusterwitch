#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <assert.h>
#include <unistd.h>

#include "dfg.h"


using namespace std;

/**
 * gets the master contact from the MASTER_CONTACT file
 *
 * @param buf where to store the master contact string, must be big enough
 *            and preallocated
 * @return DIAG_OK everything went fine
 *         DIAG_ERR some errors
 */
static int get_master_contact( char * buf){
	FILE *fp = fopen(MASTER_CONTACT,"r");

    if(fp) {
    	fscanf(fp,"%s", buf);
    	fclose(fp);
    } else {
    	printf("\nERROR: Master contact file not found. Returning an error code\n");
    	return DIAG_ERR;
    }
    return DIAG_OK;
}

/**
 *
 * @param filename The name of the filename
 * @return >=0  the number of lines
 *         -1 if error
 */
static int get_num_file_lines( char *filename)
{
	FILE *fp=fopen(filename,"r");
	if(!fp){
		cout << "ERROR: file: " << filename << " NOT FOUND\n";
		return -1;
	}
	int count=0;
	int c;
	while((c=fgetc(fp))!=EOF)
	{
		if(c=='\n')
		count++;

	}
		
	//printf("\n count%d\n",count);
	return count;
}


/**
 * reads the names of the nodes from the file
 * @param dfg
 * @param filename
 */
static void dfg_read_node_names(DFG *dfg,char* filename)
{
	assert(dfg);
	assert(filename);
	int i=0;
	
	FILE *fp=fopen(filename,"r");
	char buf[200];
	if(fp == NULL){
		printf("\n File to assign Node names not found");
		exit(1);
	}

	while(fscanf(fp,"%s",buf)!=EOF){
		dfg->nodes[i]=strdup(buf);
		node_info node_info_struct;
		node_info_struct.name=strdup(dfg->nodes[i]);
		char* node_name=strdup(dfg->nodes[i]);
		printf("DEBUG: inserting %s\n", dfg->nodes[i]);
		string str(node_name);
		dfg->name_node_map[str]=node_info_struct;
		
		i++;
	}
	
	dfg->nodes[i]=NULL;
	fclose(fp);
}

/**
 * Create source and sink stones
 *
 * @param dfg_1
 * @param file
 */
static void dfg_create_stones(DFG *dfg_1, char * file) {
	FILE *fp;
	char node_name[200];
	char stone_name[200];
	char buf[20];
	fp = fopen(file, "r");
	if (fp == NULL) {
		printf("File to create stones not found \n");
		exit(1);
	}

	while (fscanf(fp, "%s", buf) != EOF) {
		// create virtual source stones and assign them to a relevant node
		if (strcmp(buf, "*") == 0) {
			fscanf(fp, "%s", node_name);
			std::string node_name_str(node_name);
			fscanf(fp, "%s", stone_name);
			while (strcmp(stone_name, "src_end") != 0) {
				std::string stone_name_str(stone_name);
				printf("Creating source stone %s \n", stone_name);
				EVdfg_stone src = EVdfg_create_source_stone(dfg_1->dfg, stone_name);
				printf("Assigning stone %s to node %s  \n", stone_name, node_name);
				EVdfg_assign_node(src, node_name);
				stone_info stone;
				stone.stone_id = src;
				stone.src_count = 0;
				dfg_1->all_stone_map[stone_name_str] = stone;
				fscanf(fp, "%s", stone_name);
			}

			fscanf(fp, "%s", stone_name);
			while (strcmp(stone_name, "sink_end") != 0) {
				std::string stone_name_str(stone_name);
				printf("Creating sink stone %s \n", stone_name);
				EVdfg_stone sink = EVdfg_create_sink_stone(dfg_1->dfg, stone_name);
				printf("Assigning stone %s to node %s  \n", stone_name, node_name);
				EVdfg_assign_node(sink, node_name);
				stone_info stone;
				stone.stone_id = sink;
				stone.src_count = 0;
				dfg_1->all_stone_map[stone_name_str] = stone;
				fscanf(fp, "%s", stone_name);
			}
		}
	}
}


static void dfg_link_stones(DFG *dfg, char *file) {
	DFG *dfg_1 = dfg;
	FILE *fp = nullptr;
	char buf[200];
	string sink;
	EVdfg_stone sink_id;

	if ( (fp = fopen(file, "r")) == NULL) {
		printf("\n File to link stones not found \n");
		exit(1);
	}

	while (fscanf(fp, "%s", buf) != EOF) {
		if (strcmp(buf, "*") == 0) {
			fscanf(fp, "%s", buf);
			sink = strdup(buf);
			sink_id = dfg_1->all_stone_map[sink].stone_id;
		} else {
			// all these are sources to the previous sink
			std::string source_str(buf);
			EVdfg_stone src_id = dfg_1->all_stone_map[source_str].stone_id;

			assert(src_id);
			EVdfg_link_dest(src_id, sink_id);

			//int out_degree=dfg_1->all_stone_map[source_str].src_count;
			//EVdfg_link_port(src_id, out_degree, sink_id);
			//dfg_1->all_stone_map[source_str].src_count++;
		}
	}
	// look at file,find each stone and make the links
}

/**
 * Creates the master and a E
 * @param cm
 * @return nullptr if some errors
 *         a handle for DFG
 */
DFG* dfg_create(CManager cm)
{
	DFG *dfg_1 = new DFG();
	FILE *fp;
	
	dfg_1->master = EVmaster_create(cm);
	dfg_1->dfg_contact = EVmaster_get_contact_list(dfg_1->master);

	// write DFG contact string to the file
	cout << "Master contact " << dfg_1->dfg_contact << "\n";
	fp = fopen(MASTER_CONTACT,"w+");
	fprintf(fp,"%s",dfg_1->dfg_contact);
	fclose(fp);

	int num_participating_nodes = get_num_file_lines(const_cast<char*>(NODES_FNAME));
	if (num_participating_nodes == -1){
		cout << "ERROR: some issues with get_num_file_lines(). Returning error\n";
		return nullptr;
	}
	
	dfg_1->nodes = (char**) malloc(sizeof(char*)*num_participating_nodes);
	
	dfg_read_node_names(dfg_1, const_cast<char*>(NODES_FNAME));

	EVmaster_register_node_list(dfg_1->master, dfg_1->nodes);
	
	dfg_1->dfg = EVdfg_create(dfg_1->master);
	
	// create and assign stones to nodes
	dfg_create_stones(dfg_1, const_cast<char*>(STONES_FNAME));

	// do the linking
	dfg_link_stones(dfg_1, const_cast<char*>(LINKS_FNAME));


	EVdfg_realize(dfg_1->dfg);

   return dfg_1;
   
}

/**
 * Pure join the dfg, no registration of handlers.
 *
 * The node joins the DFG
 *
 * @author Magdalena Slawinska
 * @param cm
 * @param info.node_name the name of the node that joins the dfg
 * @param info.master != nullptr if this is local association (within master process)
 *        false otherwise, i.e., non-master client
 * @param src_caps The source caps if any (can be nullptr)
 * @param sink_caps The sink caps if any (can be nullptr)
 * @return DIAG_OK If everything went fine
 *         DIAG_ERR Some errors, e.g. the master file contact has been not found
 *         info.client has a pointer to the client structure
 *
 */
int dfg_join(CManager cm, join_info * info,  EVclient_sources src_caps,
		EVclient_sinks sink_caps){
	assert(cm);
	assert(info);
	assert(info->node_name);

	cout << "DEBUG: joining master: ... ";
	if(info->master){
		info->client = EVclient_assoc_local(cm, info->node_name, info->master, src_caps, sink_caps);
		cout << "(locally) SUCCESS\n";
    } else {
    	char mastercontact[200];

    	if( get_master_contact(mastercontact) != DIAG_OK) {
    		cout << "ERROR: got error from get_master_contact(). Returning error\n";
    		return DIAG_ERR;
    	}
    	info->client = EVclient_assoc(cm, info->node_name, mastercontact, src_caps, sink_caps);
    	cout << "with mastercontact: " << mastercontact << " ... SUCCESS\n";
    }

	cout << "DEBUG: waiting for DFG instantiation ...\n";
	if (EVclient_ready_wait(info->client) != 1 ){
		cout << "ERROR: problems with EVclient_ready_wait(). Returning an error\n";
		return DIAG_ERR;
	}

	return DIAG_OK;
}
/**
 * Register sink
 *
 * @param cm
 * @param sink_info (INOUT) sink_info->sink_caps after registering sink handler
 * @return DIAG_OK
 */
int dfg_reg_sink(CManager cm, sink_join_info *sink_info){
	assert(cm);
	assert(sink_info);
	sink_info->sink_caps = EVclient_register_sink_handler(cm,sink_info->stone_name,sink_info->simple_format_list,sink_info->handler_func, sink_info->client_data);

	return DIAG_OK;
}

/**
 * Create a handle and register data for this handle
 * @param cm
 * @param src_info (INOUT) src_info->source_caps the current caps
 *                         src_info->src the source
 * @return DIAG_OK if everything went ok
 *         DIAG_ERR otherwise
 */
int dfg_reg_source(CManager cm, source_join_info *src_info)
{
	assert(cm);
	assert(src_info);
	EVsource src = EVcreate_submit_handle(cm, DFG_SOURCE, (src_info->simple_format_list));
	if(src == NULL){
		return DIAG_ERR;
	}
	src_info->src = src;
	// I can ignore source_caps because EVdfg keeps track of them
	// and later I can just call assoc function with a NULL parameter for source
	// capabilities
	EVclient_sources source_caps = EVclient_register_source(src_info->stone_name, src_info->src );
	src_info->source_caps = source_caps;

 	return DIAG_OK;
}


/**
 * shutdown the client: signal to the master that we are ready to
 * shutdown and wait for it
 *
 * @param client The client that will be reporting readiness for the shutdown
 * @return DIAG_OK everything went fine
 *         DIAG_ERR issues witht the EVclient_wait_for_shutdown()
 */
int dfg_shutdown(EVclient client){
	assert(client);
	EVclient_ready_for_shutdown(client);
	if( EVclient_wait_for_shutdown(client) != DFG_STATUS_SUCCESS){
		return DIAG_ERR;
	}
	return DIAG_OK;
}
