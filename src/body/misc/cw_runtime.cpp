/**
 *  @file   cw_runtime.cpp
 *
 *  @date   Created on: Dec 30, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>
#include <cstring>

#include "cw_runtime.h"
#include "transport.h"
#include "dfg.h"

#include "easylogging++.h"

namespace cw {


const float CW_Runtime::IN_CASE_NAN = 0.0;


// ----------------------------------
// CW_Runtime
// ----------------------------------
//! global initialization to facilitate a singleton design pattern
CW_Runtime * CW_Runtime::p_cw_rt = nullptr;

/**
 * Get the instance of the clusterwitch runtime; instantiate if not instantiated
 * @return
 */
CW_Runtime *CW_Runtime::get_instance(){
	if (!exists()){
		p_cw_rt = new CW_Runtime();
		assert(nullptr != p_cw_rt);

	}
	return p_cw_rt;
}
/**
 * checks if the instance exists or not
 *
 * @return true exists
 *         false otherwsie
 */
bool CW_Runtime::exists(){
	return (nullptr != p_cw_rt);
}

// --------------------------------
// Ev_CW_Mon_Runtime
// --------------------------------
/**
 * The handler that is supposed to be executed on the monitoring side to get and
 * take appropriate actions.
 *
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return
 */
extern "C" int alive_monitoring_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_alive_mon_msg * event = static_cast<struct ev_alive_mon_msg*> (vevent);
	Ev_CW_Mon_Runtime * rt = static_cast<Ev_CW_Mon_Runtime *>(client_data);
	for(int i = 0; i < event->stone_ids_count; ++i){
		LOG(INFO) << "Got from aggregator stone:type " <<
				event->stone_ids[i] << ":" << static_cast<int>(event->atom_type) << "\n";
	}
	switch(event->atom_type){
	case Atom_Type::CPU:
		rt->set_agg_stone_for_cpu_atom(event->stone_ids[0]);
		rt->set_agg_stone_contact_for_cpu_atom(string(event->contact));
		break;
	case Atom_Type::MEM:
		rt->set_agg_stone_for_mem_atom(event->stone_ids[0]);
		rt->set_agg_stone_contact_for_mem_atom(string(event->contact));
		break;
	case Atom_Type::NET:
		rt->set_agg_stone_for_net_atom(event->stone_ids[0]);
		rt->set_agg_stone_contact_for_net_atom(string(event->contact));
		break;
#ifdef NVML_PRESENT
	case Atom_Type::NVML:
		rt->set_agg_stone_for_nvml_atom(event->stone_ids[0]);
		rt->set_agg_stone_contact_for_nvml_atom(string(event->contact));
		break;
#endif // NVML_PRESENT
	default:
		LOG(ERROR) << "Unknown atom type: " << static_cast<int>(event->atom_type);
		break;
	}

    return 1;
}

//! global initialization to facilitate a singleton design pattern
Ev_CW_Mon_Runtime * Ev_CW_Mon_Runtime::p_cw_rt = nullptr;

/**
 * Get the instance of the clusterwitch runtime; instantiate if not instantiated
 * @return
 */
Ev_CW_Mon_Runtime *Ev_CW_Mon_Runtime::get_instance(){
	if (!exists()){
		p_cw_rt = new Ev_CW_Mon_Runtime();
		assert(nullptr != p_cw_rt);
		if( p_cw_rt->init() != Diag::OK){
			LOG(ERROR) << "Issues with initialization.";
		}
	}
	return p_cw_rt;
}

Ev_CW_Mon_Runtime::Ev_CW_Mon_Runtime() : contact_stone(Ev_Atom::INVALID_STONE_ID),
		conn(nullptr), p_cpu_mon_atom(nullptr), p_mem_mon_atom(nullptr),
		p_net_mon_atom(nullptr)
#ifdef NVML_PRESENT
	, p_nvml_mon_atom(nullptr)
#endif
{
}
/**
 * checks if the instance exists or not
 *
 * @return true exists
 *         false otherwise
 */
bool Ev_CW_Mon_Runtime::exists(){
	return (nullptr != p_cw_rt);
}

/**
 * reads the aggregator contact list from the file name. This should
 * be called before calling any join_topo because it reads the contact
 * information needed to send an alive message to the aggregator to get
 * back some information from it about the remote stones etc.
 *
 * @param cfg_file_name The name of the configuration file
 *
 * @return Diag::OK if everything went ok
 *         != Diag::OK if any noticed errors
 */
Diag Ev_CW_Mon_Runtime::read_aggregator_contact_list(const string & cfg_file_name){
	return Ev_Utils::get_master_contact_list(cfg_file_name, aggreg_contact_list);
}

/**
 * sends the "aggregator stone" request to the aggregator
 *
 * @param type The type of atom to be sent
 * @return Diag::OK if everything went great
 *         != Diag::OK if any error
 */
Diag Ev_CW_Mon_Runtime::send_agg_stone_request(Atom_Type type){
	Diag diag = Diag::OK;

	// be sure that the aggregator contact is read
	if (aggreg_contact_list.size() == 0){
		LOG(ERROR) << "It seems that the aggregator contact list is not set appropriately";
		return Diag::ERR;
	}

	// get my contact
	string contact;
	if ( (diag = Ev_Utils::get_my_contact(contact)) != Diag::OK ){
		LOG(ERROR) << "Can't get my contact ec=" << static_cast<int>(diag);
		return diag;
	}

	// prepare the alive message
	struct ev_alive_mon_msg rec(1);
	assert(1 == rec.stone_ids_count);
	// I care only about the contact information
	rec.contact = const_cast<char*>(contact.c_str());
	assert(contact_stone >= 0);
	assert(Ev_Atom::INVALID_STONE_ID != contact_stone);
	rec.stone_ids[0] = contact_stone;
	rec.atom_type = type;

	if( (diag = send_alive_pattern(aggreg_contact_list, conn, &rec)) != Diag::OK){
		LOG(ERROR) << "Issues with the sending alive message from CPU monitoring atom ec=" <<
				static_cast<int>(diag);
		return diag;
	} else {
		LOG(DEBUG) << "Sent my contact:" << contact;
	}

	return diag;
}
/**
 * Adds the pointer to the atom
 * @param atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::add_atom(Ev_Cpu_Monitoring_Atom &atom){
	Diag diag = Diag::OK;
	p_cpu_mon_atom = &atom;
	return diag;
}
/**
 * Adds the pointer to the atom
 * @param atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::add_atom(Ev_Mem_Monitoring_Atom &atom){
	Diag diag = Diag::OK;
	p_mem_mon_atom = &atom;
	return diag;
}
/**
 * Adds the pointer to the atom
 * @param atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::add_atom(Ev_Net_Monitoring_Atom &atom){
	Diag diag = Diag::OK;
	p_net_mon_atom = &atom;
	return diag;
}

#ifdef NVML_PRESENT
/**
 * Adds the pointer to the atom
 * @param atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::add_atom(Ev_Nvml_Monitoring_Atom &atom){
	Diag diag = Diag::OK;
	p_nvml_mon_atom = &atom;
	return diag;
}
#endif // NVML_PRESENT

/**
 * Sets the aggreg stone for cpu atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_for_cpu_atom(EVstone agg_stone){
	Diag diag = Diag::OK;

	assert(p_cpu_mon_atom != nullptr);
	p_cpu_mon_atom->set_aggreg_stone(agg_stone);

	return diag;
}
/**
 * Sets the aggreg stone for cpu atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_contact_for_cpu_atom(const string contact){
	Diag diag = Diag::OK;

	assert(p_cpu_mon_atom != nullptr);
	p_cpu_mon_atom->set_aggreg_contact_list(contact);

	return diag;
}

/**
 * Sets the aggreg stone for mem atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_for_mem_atom(EVstone agg_stone){
	Diag diag = Diag::OK;

	assert(p_mem_mon_atom != nullptr);
	p_mem_mon_atom->set_aggreg_stone(agg_stone);

	return diag;
}
/**
 * Sets the aggreg stone for mem atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_contact_for_mem_atom(const string & contact){
	Diag diag = Diag::OK;

	assert(p_mem_mon_atom != nullptr);
	p_mem_mon_atom->set_aggreg_contact_list(contact);

	return diag;
}

/**
 * Sets the aggreg stone for net atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_for_net_atom(EVstone agg_stone){
	Diag diag = Diag::OK;

	assert(p_net_mon_atom != nullptr);
	p_net_mon_atom->set_aggreg_stone(agg_stone);

	return diag;
}
/**
 * Sets the aggreg stone for mem atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_contact_for_net_atom(const string & contact){
	Diag diag = Diag::OK;

	assert(p_net_mon_atom != nullptr);
	p_net_mon_atom->set_aggreg_contact_list(contact);

	return diag;
}

#ifdef NVML_PRESENT
/**
 * Sets the aggreg stone for net atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_for_nvml_atom(EVstone agg_stone){
	Diag diag = Diag::OK;

	assert(p_nvml_mon_atom != nullptr);
	p_nvml_mon_atom->set_aggreg_stone(agg_stone);

	return diag;
}
/**
 * Sets the aggreg stone for mem atom
 * @return Diag::OK always
 */
Diag Ev_CW_Mon_Runtime::set_agg_stone_contact_for_nvml_atom(const string & contact){
	Diag diag = Diag::OK;

	assert(p_nvml_mon_atom != nullptr);
	p_nvml_mon_atom->set_aggreg_contact_list(contact);

	return diag;
}
#endif

/**
 * Initialize the runtime
 * @return
 */
Diag Ev_CW_Mon_Runtime::init(){
	Diag diag = Diag::OK;

	CManager cm = Ev_Topo::get_CM_now();

	// create a stone that is able to receive the alive message from the
	// aggregator
	Ev_Alive_Mon_Msg *msg = Ev_Alive_Mon_Msg::get_instance();
	assert(nullptr != msg);
	contact_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, contact_stone, msg->get_format_list(), alive_monitoring_handler, this);

	LOG(DEBUG) << "Contact stone for the aggregator's alive message created";

	return diag;
}
// --------------------------------
// Ev_CW_Agg_Runtime
// --------------------------------
/**
 * This is getting the information from the monitoring
 * atoms and sending back the contact information to the aggregator
 * atoms
 *
 *
 * @param cm
 * @param vevent struct ev_alive_mon_msg
 * @param client_data Ev_Aggregator_Atom
 * @param attrs
 * @return
 */
extern "C" void alive_aggregator_handler(CManager cm, CMConnection conn, void *vevent,
			void *client_data, attr_list attrs){

	ev_alive_mon_msg *event =  static_cast<ev_alive_mon_msg*> (vevent);
	Ev_CW_Agg_Runtime *rt = static_cast<Ev_CW_Agg_Runtime *>(client_data);

	// expected the id of the remote stone
	assert(1 == event->stone_ids_count);
	LOG(DEBUG) << "Got contact:stone:atom_type " << event->contact << ":" <<
			event->stone_ids[0] << ":" << static_cast<int>(event->atom_type);

	EVstone output_stone = EValloc_stone(cm);
	attr_list mon_atom_contact_list = attr_list_from_string(event->contact);

	EVassoc_bridge_action(cm, output_stone, mon_atom_contact_list, event->stone_ids[0]);
	free_attr_list(mon_atom_contact_list);

	Ev_Alive_Mon_Msg * msg = Ev_Alive_Mon_Msg::get_instance();
	EVsource source = EVcreate_submit_handle(cm, output_stone, msg->get_format_list() );

	ev_alive_mon_msg rec(1);

	string my_contact;
	if ( Ev_Utils::get_my_contact(my_contact) != Diag::OK ){
		LOG(ERROR) << "Can't get my contact";
		return;
	}
	rec.contact = const_cast<char *>(my_contact.c_str());

	switch(event->atom_type){
	case Atom_Type::CPU :
		rec.stone_ids[0] = rt->get_cpu_aggreg_stone();
		rec.atom_type = event->atom_type;
		break;
	case Atom_Type::MEM :
		rec.stone_ids[0] = rt->get_mem_aggreg_stone();
		rec.atom_type = event->atom_type;
		break;
	case Atom_Type::NET:
		rec.stone_ids[0] = rt->get_net_aggreg_stone();
		rec.atom_type = event->atom_type;
		break;
#ifdef NVML_PRESENT
	case Atom_Type::NVML:
		rec.stone_ids[0] = rt->get_nvml_aggreg_stone();
		rec.atom_type = event->atom_type;
		break;
#endif
	default:
		LOG(WARNING) << "Unknown aggregator atom request ec="
			<< static_cast<int>(event->atom_type);
		break;
	}

	EVsubmit(source, &rec, nullptr);

	// clean the stones that I don't need
	if( 1 == EVdestroy_stone(cm, output_stone)){
		LOG(DEBUG) << "Output_stone: " << output_stone << " DESTROYED\n";
	} else  {
		LOG(ERROR) << "ERROR: issues with destroying a stone. Continuing ...\n";
	}

	EVfree_source(source);

    return;
}

//! global initialization to facilitate a singleton design pattern
Ev_CW_Agg_Runtime * Ev_CW_Agg_Runtime::p_cw_rt = nullptr;

/**
 * Get the instance of the clusterwitch runtime; instantiate if not instantiated
 * @return
 */
Ev_CW_Agg_Runtime *Ev_CW_Agg_Runtime::get_instance(){
	if (!exists()){
		p_cw_rt = new Ev_CW_Agg_Runtime();
		assert(nullptr != p_cw_rt);
		if( p_cw_rt->init() != Diag::OK){
			LOG(ERROR) << "Issues with initialization.";
		}
	}
	return p_cw_rt;
}

Ev_CW_Agg_Runtime::Ev_CW_Agg_Runtime() :p_cpu_atom(nullptr),
		p_mem_atom(nullptr), p_net_atom(nullptr)
#ifdef NVML_PRESENT
	, p_nvml_atom(nullptr)
#endif
{
}
/**
 * checks if the instance exists or not
 *
 * @return true exists
 *         false otherwise
 */
bool Ev_CW_Agg_Runtime::exists(){
	return (nullptr != p_cw_rt);
}

/**
 * Gets the aggregator stone; if necessary creates the
 * cpu aggregator atom
 *
 * @return The cpu aggregator stone id
 *         Ev_Atom::INVALID_STONE_ID if any errors
 */
EVstone Ev_CW_Agg_Runtime::get_cpu_aggreg_stone(){
	if( nullptr == p_cpu_atom ){
		EVstone aggreg_stone = Ev_Atom::INVALID_STONE_ID;
		p_cpu_atom = new Ev_Cpu_Aggregator_Atom();
		assert(nullptr != p_cpu_atom);
		if( p_cpu_atom->join_topo() != Diag::OK){
			LOG(ERROR) << "Issues with joining the topology by the CPU aggreg";
			return aggreg_stone;
		} else {
			LOG(DEBUG) << "CPU aggreg joined successfully aggreg_stone=" <<
					p_cpu_atom->get_aggreg_stone();
		}
	}
	assert(nullptr != p_cpu_atom);

	return p_cpu_atom->get_aggreg_stone();
}
/**
 * Gets the aggregator stone; if necessary creates the
 * mem aggregator atom
 *
 * @return The mem aggregator stone id
 *         Ev_Atom::INVALID_STONE_ID if any errors
 */
EVstone Ev_CW_Agg_Runtime::get_mem_aggreg_stone(){
	if( nullptr == p_mem_atom ){
		EVstone aggreg_stone = Ev_Atom::INVALID_STONE_ID;
		p_mem_atom = new Ev_Mem_Aggregator_Atom();
		assert(nullptr != p_mem_atom);
		if( p_mem_atom->join_topo() != Diag::OK){
			LOG(ERROR) << "Issues with joining the topology by the MEM aggreg";
			return aggreg_stone;
		} else {
			LOG(DEBUG) << "MEM aggreg joined successfully aggreg_stone=" <<
					p_mem_atom->get_aggreg_stone();
		}
	}
	assert(nullptr != p_mem_atom);

	return p_mem_atom->get_aggreg_stone();
}
/**
 * Gets the aggregator stone; if necessary creates the
 * net aggregator atom
 *
 * @return The net aggregator stone id
 *         Ev_Atom::INVALID_STONE_ID if any errors
 */
EVstone Ev_CW_Agg_Runtime::get_net_aggreg_stone(){
	if( nullptr == p_net_atom ){
		EVstone aggreg_stone = Ev_Atom::INVALID_STONE_ID;
		p_net_atom = new Ev_Net_Aggregator_Atom();
		assert(nullptr != p_net_atom);
		if( p_net_atom->join_topo() != Diag::OK){
			LOG(ERROR) << "Issues with joining the topology by the NET aggreg";
			return aggreg_stone;
		} else {
			LOG(DEBUG) << "NET aggreg joined successfully aggreg_stone=" <<
					p_net_atom->get_aggreg_stone();
		}
	}
	assert(nullptr != p_net_atom);

	return p_net_atom->get_aggreg_stone();
}

#ifdef NVML_PRESENT
/**
 * Gets the aggregator stone; if necessary creates the
 * net aggregator atom
 *
 * @return The nvml aggregator stone id
 *         Ev_Atom::INVALID_STONE_ID if any errors
 */
EVstone Ev_CW_Agg_Runtime::get_nvml_aggreg_stone(){
	if( nullptr == p_nvml_atom ){
		EVstone aggreg_stone = Ev_Atom::INVALID_STONE_ID;
		p_nvml_atom = new Ev_Nvml_Aggregator_Atom();
		assert(nullptr != p_nvml_atom);
		if( p_nvml_atom->join_topo() != Diag::OK){
			LOG(ERROR) << "Issues with joining the topology by the NVML aggreg";
			return aggreg_stone;
		} else {
			LOG(DEBUG) << "NVML aggreg joined successfully aggreg_stone=" <<
					p_nvml_atom->get_aggreg_stone();
		}
	}
	assert(nullptr != p_nvml_atom);

	return p_nvml_atom->get_aggreg_stone();
}
#endif  // NVML_PRESENT

/**
 * Initialize the runtime, basically register the handler
 * for incoming alive monitoring messages
 * @return
 */
Diag Ev_CW_Agg_Runtime::init(){
	Diag diag = Diag::OK;

	// first prepare the infrastructure for getting the alive message
	Ev_Alive_Mon_Msg * msg = Ev_Alive_Mon_Msg::get_instance();
	CMregister_handler(msg->get_format(), alive_aggregator_handler, this);
	LOG(DEBUG) << "Registered alive_aggregator_handler for Ev_Alive_Mon_Msg";

	// now I am ready for incoming alive messages

	return diag;
}

// ---------------------------------------------------
// Ev_Dfg_CW_Master_Runtime
// ---------------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Dfg_CW_Master_Runtime * Ev_Dfg_CW_Master_Runtime::p_cw_rt = nullptr;


/**
 * Get the instance of the clusterwitch runtime; instantiate if not instantiated
 * @return
 */
Ev_Dfg_CW_Master_Runtime *Ev_Dfg_CW_Master_Runtime::get_instance(){
	if (!exists()){
		p_cw_rt = new Ev_Dfg_CW_Master_Runtime();
		assert(nullptr != p_cw_rt);
	}
	return p_cw_rt;
}
/**
 * Creates the dfg; should be called before any operations on the
 * dfgs. The dfg_info should be before initialzed
 *
 * @return Diag::OK if everything is fine
 *         Diag::WARN if it is detected that the dfg has been already created
 *         != Diag::OK something went wrong
 */
Diag Ev_Dfg_CW_Master_Runtime::create_dfg(){

	if( master ){
		LOG(WARNING) << "The dfg has been already created. RAI (Report and Ignore)";
		return Diag::WARN;
	}

	CManager cm = Ev_Topo::get_CM_now();
	// TODO I know this allocates some memory but I don't know where this is
	// released

	DFG * dfg = dfg_create(cm);
	if( !dfg ){
		LOG(ERROR) << "Got an error from dfg_create(). Returning an arror";
		return Diag::EV_DFG_ERR;
	}
	// since I don't know how to check if this returned with the error or not
	assert(dfg);
	assert(dfg->master);

	master = dfg->master;
	dfg_mgr = dfg->dfg;

	return Diag::OK;
}


Ev_Dfg_CW_Master_Runtime::Ev_Dfg_CW_Master_Runtime() : master(nullptr), dfg_mgr(nullptr){
}
/**
 * checks if the instance exists or not
 *
 * @return true exists
 *         false otherwise
 */
bool Ev_Dfg_CW_Master_Runtime::exists(){
	return (nullptr != p_cw_rt);
}

// -----------------------------------------------
// Ev_Dfg_CW_Client_Runtime
// -----------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Dfg_CW_Client_Runtime * Ev_Dfg_CW_Client_Runtime::p_cw_rt = nullptr;


Ev_Dfg_CW_Client_Runtime::Ev_Dfg_CW_Client_Runtime() : client(nullptr){
}

/**
 * Get the instance of the clusterwitch runtime; instantiate if not instantiated
 * @return
 */
Ev_Dfg_CW_Client_Runtime *Ev_Dfg_CW_Client_Runtime::get_instance(){
	if (!exists()){
		p_cw_rt = new Ev_Dfg_CW_Client_Runtime();
		assert(nullptr != p_cw_rt);
	}
	return p_cw_rt;
}

/**
 * checks if the instance exists or not
 *
 * @return true exists
 *         false otherwise
 */
bool Ev_Dfg_CW_Client_Runtime::exists(){
	return (nullptr != p_cw_rt);
}

/**
 * Associates the client with master process and waits
 * for further actions
 * @return Diag::OK if everything went great
 *         Diag::ERR if there were some issues with joining the master process
 */
Diag Ev_Dfg_CW_Client_Runtime::run_dfg(const string & node_name){
	Diag diag = Diag::OK;

	if( client ){
		LOG(WARNING) << "The client already running. Returning ...";
		return Diag::OK;
	}

	join_info info;
	info.node_name = const_cast<char*>(node_name.c_str());
	info.master = nullptr;

	if ( dfg_join(Ev_Topo::get_CM_now(), &info, nullptr, nullptr) != DIAG_OK){
		LOG(ERROR) << "Got an error from dfg_join_graph(). Returning error";
		return Diag::ERR;
	}
	client = info.client;

	return diag;
}

/**
 * @return EVclient
 */
EVclient Ev_Dfg_CW_Client_Runtime::get_client(){
	return client;
}


/**
 * Shuts down the dfg. If client == nullptr then, the method returns.
 *
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong
 */
Diag Ev_Dfg_CW_Client_Runtime::shutdown_dfg(){
	if(client){
		if ( dfg_shutdown(client) != DIAG_OK ){
			LOG(ERROR) << "issues with shutting down dfg. Returning an error";
			return Diag::ERR;
		}
	}
	return Diag::OK;
}

// -----------------------------------------------
// Ev_Dfg_Synchro_Master_RT
// -----------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Dfg_Synchro_Master_RT * Ev_Dfg_Synchro_Master_RT::p_cw_rt = nullptr;


Ev_Dfg_Synchro_Master_RT::Ev_Dfg_Synchro_Master_RT() : master(nullptr),
		dfg_mgr(nullptr){

}


Ev_Dfg_Synchro_Master_RT* Ev_Dfg_Synchro_Master_RT::get_instance() {
	if (!exists()){
		p_cw_rt = new Ev_Dfg_Synchro_Master_RT();
		assert(nullptr != p_cw_rt);
	}
	return p_cw_rt;
}

bool Ev_Dfg_Synchro_Master_RT::exists() {
	return (nullptr != p_cw_rt);
}

/**
 *
 * @return Diag::OK if everything is ok
 *         Diag::EV_DFG_ERR if some errors during the creation of the DFG
 */
Diag Ev_Dfg_Synchro_Master_RT::create_dfg() {
	if( master ){
		LOG(WARNING) << "The dfg has been already created. RAI (Report and Ignore)";
		return Diag::WARN;
	}

	CManager cm = Ev_Topo::get_CM_now();
	// TODO I know this allocates some memory but I don't know where this is
	// released
	DFG * dfg = dfg_create(cm);
	if( !dfg ){
		return Diag::EV_DFG_ERR;
	}

	// since I don't know how to check if this returned with the error or not
	assert(dfg);
	assert(dfg->master);

	master = dfg->master;
	dfg_mgr = dfg->dfg;

	return Diag::OK;
}

// ------------------------------------------------
// Ev_Dfg_Synchro_Client_RT
// ------------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Dfg_Synchro_Client_RT * Ev_Dfg_Synchro_Client_RT::p_cw_rt = nullptr;


Ev_Dfg_Synchro_Client_RT::Ev_Dfg_Synchro_Client_RT() : client(nullptr) {
}


Ev_Dfg_Synchro_Client_RT* Ev_Dfg_Synchro_Client_RT::get_instance() {
	if (!exists()){
		p_cw_rt = new Ev_Dfg_Synchro_Client_RT();
		assert(nullptr != p_cw_rt);
	}
	return p_cw_rt;
}

bool Ev_Dfg_Synchro_Client_RT::exists() {
	return (nullptr != p_cw_rt);
}

Diag Ev_Dfg_Synchro_Client_RT::run_dfg(const string& node_name) {
	Diag diag = Diag::OK;

	if( client ){
		LOG(WARNING) << "The client already running. Returning ...";
		return Diag::OK;
	}

	join_info info;
	info.node_name = const_cast<char*>(node_name.c_str());
	info.master = nullptr;

	if ( dfg_join(Ev_Topo::get_CM_now(), &info, nullptr, nullptr) != DIAG_OK){
		LOG(ERROR) << "Got an error from dfg_join_graph(). Returning error";
		return Diag::ERR;
	}
	client = info.client;

	return diag;
}

EVclient Ev_Dfg_Synchro_Client_RT::get_client() {
	return client;
}

Diag Ev_Dfg_Synchro_Client_RT::shutdown_dfg() {
	if(client){
		if ( dfg_shutdown(client) != DIAG_OK ){
			LOG(ERROR) << "issues with shutting down dfg. Returning an error";
			return Diag::ERR;
		}
	}
	return Diag::OK;
}


} /* namespace cw */

