/**
 *  @file   transport.cpp
 *
 *  @date   Created on: Dec 31, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>
#include <cstdio>
#include <cstdlib>

#include <unistd.h>

#include "transport.h"
#include "cw_runtime.h"
#include "spy.h"
#include "misc.h"

#include "easylogging++.h"


#include "revpath.h"   // evpath.h must be included before

using namespace std;

namespace cw {

/* TODO this is some a fight that needs continuation with using the template function
extern template Diag Ev_Comm_Pattern::send_alive_pattern<struct ev_alive_msg, Ev_Alive_Msg>(
const std::string & contact, CMConnection & conn,
			const struct ev_alive_msg *p_msg, const Ev_Alive_Msg *p_dummy);

extern template Diag Ev_Comm_Pattern::send_alive_pattern<struct ev_alive_mon_msg, Ev_Alive_Mon_Msg>(
const std::string & contact, CMConnection & conn,
			const struct ev_alive_mon_msg *p_msg, const Ev_Alive_Mon_Msg *p_dummy);
*/
// -------------------------
// struct ev_alive_msg
// -------------------------
ev_alive_msg::ev_alive_msg() : contact(nullptr){

}
// --------------------------
// struct ev_alive_mon_msg
// -------------------------
ev_alive_mon_msg::ev_alive_mon_msg() : contact(nullptr), stone_ids(nullptr), stone_ids_count(0),
		atom_type(Atom_Type::UNKNOWN){

}
/**
 * Allocates the array for storing the stone_ids
 *
 * @param stones_count The number of stones
 */
ev_alive_mon_msg::ev_alive_mon_msg(int stones_count) : contact(nullptr), stone_ids_count(stones_count),
		atom_type(Atom_Type::UNKNOWN){
	assert(stones_count > 0);
	stone_ids = new EVstone[stone_ids_count];
}
/**
 * Deallocates the memory if the memory for storing ids were allocated
 */
ev_alive_mon_msg::~ev_alive_mon_msg(){
	if( stone_ids ){
		delete [] stone_ids;
		stone_ids = nullptr;
	}
}

// ----------------------------------------------
// Ev_Msg
// ----------------------------------------------
Ev_Msg::Ev_Msg() : field_list(nullptr),
		format_list(nullptr), format(nullptr){

}
Ev_Msg::~Ev_Msg(){

	// field_list and format_list should be ended
	// with an empty record
	if( field_list ){
		int i = 0;
		while(field_list[i].field_name){
			delete field_list[i].field_name;
			delete field_list[i].field_type;
			++i;
		}
		delete [] field_list;
	}

	// do same for the format list
	if( format_list ){
		int i = 0;
		while(format_list[i].format_name){
			free(format_list[i].format_name);
			++i;
		}
		delete [] format_list;
	}
}
/**
 * @return getter for the format list
 */
FMStructDescRec * Ev_Msg::get_format_list(){
	return format_list;
}
/**
 * @return getter the format
 */
CMFormat Ev_Msg::get_format(){
	return format;
}
/**
 * @return getter for the field list
 */
FMField* Ev_Msg::get_field_list(){
	return field_list;
}

/**
 * adds an element to the list
 *
 * For description of the fields refer to FMField structure
 * @param i where to put this in field_list
 * @param field_name
 * @param field_type
 * @param field_size
 * @param field_offset
 * @return
 */
Diag Ev_Msg::add_field_list_elem(int i, const char * name, const char * type,
	int size, int offset){
	Diag diag = Diag::OK;

	if(name){
		field_list[i].field_name = strdup(name);
		field_list[i].field_type = strdup(type);
	} else {
		field_list[i].field_name = nullptr;
		field_list[i].field_type = nullptr;
	}
	field_list[i].field_size = size;
	field_list[i].field_offset = offset;

	return diag;
}


/**
 * Add the element to the format list
 * @param i
 * @param format_name
 * @param a_field_list
 * @param struct_size
 * @param p_info
 * @return
 */
Diag Ev_Msg::add_format_list_elem(int i, const char * name,
		FMFieldList p_fields,
		int size, FMOptInfo * p_info){
	Diag diag = Diag::OK;

	// not sure what strdup does if name is null
	// that's why for safety
	if( name ){
		format_list[i].format_name = strdup(name);
	} else {
		format_list[i].format_name = nullptr;
	}
	format_list[i].field_list = p_fields;
	format_list[i].struct_size = size;
	format_list[i].opt_info = p_info;

	return diag;
}

// -----------------------------------------------
// Ev_Ctrl_Msg
// -----------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Ctrl_Msg * Ev_Ctrl_Msg::p_msg = nullptr;


/**
 * Registers all necessary stuff related to creating appropriate data
 * for ev_ctrl_msg
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Ctrl_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = new FMField[2];
	add_field_list_elem(0, "cmd", "string", sizeof(char*), FMOffset(ev_ctrl_msg *, cmd));
	add_field_list_elem(1);

	format_list = new FMStructDescRec[2];
	add_format_list_elem(0, "ev_ctrl_msg", field_list, sizeof(ev_ctrl_msg), nullptr );
	add_format_list_elem(1);

/*	FMField tmp_field_list[] = {
		{ "cmd", "string", sizeof(char*), FMOffset(struct ev_ctrl_msg *, cmd) },
		{ nullptr, nullptr, 0, 0 }
	};

	for (auto el : tmp_field_list) {
		field_list_old.emplace_back(el);
	}

	FMStructDescRec tmp_format_list[] = {
		{ "struct-ev_ctrl_msg", &field_list_old[0], sizeof(struct ev_ctrl_msg), nullptr },
		{ nullptr, nullptr, 0, nullptr }
	};

	for (auto el : tmp_format_list) {
		format_list_old.emplace_back(el);
	} */

	CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	format = CMregister_format(cm, format_list);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Ctrl_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Ctrl_Msg *Ev_Ctrl_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Ctrl_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

// -----------------------------
// Ev_Alive_Msg
// -----------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Alive_Msg * Ev_Alive_Msg::p_msg = nullptr;


/**
 * Registers all necessary stuff related to creating appropriate data
 * for ev_alive_msg
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Alive_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = new FMField[2];

	add_field_list_elem(0, "contact", "string", sizeof(char*), FMOffset(ev_alive_msg *, contact));
	add_field_list_elem(1);

	format_list = new FMStructDescRec[2];

	add_format_list_elem(0, "alive", field_list, sizeof(ev_alive_msg), nullptr);
	add_format_list_elem(1);

	CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	format = CMregister_format(cm, format_list);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Alive_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Alive_Msg *Ev_Alive_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Alive_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
// -----------------------------
// Ev_Alive_Mon_Msg
// -----------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Alive_Mon_Msg * Ev_Alive_Mon_Msg::p_msg = nullptr;

/**
 * Registers all necessary stuff related to creating appropriate data
 * for ev_alive_msg
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Alive_Mon_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = new FMField[5];

	add_field_list_elem(0, "contact", "string", sizeof(char*), FMOffset(ev_alive_mon_msg *, contact));
	add_field_list_elem(1, "stone_ids", "integer[stone_ids_count]", sizeof(EVstone), FMOffset(struct ev_alive_mon_msg *, stone_ids));
	add_field_list_elem(2, "stone_ids_count", "integer", sizeof(int), FMOffset(struct ev_alive_mon_msg *, stone_ids_count));
	add_field_list_elem(3, "atom_type", "integer", sizeof(int), FMOffset(ev_alive_mon_msg *, atom_type));
	add_field_list_elem(4);

	format_list = new FMStructDescRec[2];

	add_format_list_elem(0, "ev_alive_mon_msg", field_list, sizeof(struct ev_alive_mon_msg), nullptr);
	add_format_list_elem(1);

	CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	format = CMregister_format(cm, format_list);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Alive_Mon_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Alive_Mon_Msg *Ev_Alive_Mon_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Alive_Mon_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
// -----------------------------
// Ev_Identity_Msg
// -----------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Identity_Msg * Ev_Identity_Msg::p_msg = nullptr;


/**
 * Registers all necessary stuff related to creating appropriate data
 * for identity
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Identity_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = new FMField[4];
	add_field_list_elem(0, "id", "integer", sizeof(int), FMOffset(identity *, id));
	add_field_list_elem(1, "hostname", "string", sizeof(char*), FMOffset(identity *, hostname));
	add_field_list_elem(2, "pid", "integer", sizeof(pid_t), FMOffset(identity *, pid));
	add_field_list_elem(3);

	format_list = new  FMStructDescRec[2];
	add_format_list_elem(0, "identity", field_list, sizeof(identity), nullptr);
	add_format_list_elem(1);

	//CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	// I don't think this is required for the submission of EVPath events
	//format = CMregister_format(cm, &format_list_old[0]);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Identity_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Identity_Msg *Ev_Identity_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Identity_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

// -------------------------------------------
// Ev_Topo
// -------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Topo * Ev_Topo::p_topo = nullptr;

/**
 * initializes the Connection Manager
 */
Ev_Topo::Ev_Topo() {
	cm = CManager_create();

	if(nullptr == cm){
		LOG(ERROR) << "Can't create a connection manager. Quitting ...";
		return;
	}

	CMlisten(cm);
}

/**
 * Closes the connection manager
 */
Ev_Topo::~Ev_Topo() {
	CManager_close(cm);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Topo *Ev_Topo::get_instance(){
	if(!p_topo){
		p_topo = new Ev_Topo();
	}
	assert(nullptr != p_topo);
	return p_topo;
}
/**
 * a convenient function to get the connection manager. It checks
 * if the pointers are not null;
 *
 * @return The Connection Manager or fails on assertions.
 */
CManager Ev_Topo::get_CM_now(){
	Ev_Topo *topo = Ev_Topo::get_instance();
	assert(nullptr != topo);

	// register formats
	CManager my_cm = topo->get_CM();
	assert(nullptr != my_cm);

	return my_cm;
}
/**
 * CMsleeps the network, i.e., handles the network until seconds elapsed. If
 * necessary it initializes the network, so no need to do any preparatory steps
 *
 * TODO Not sure how it behaves when called from different threads
 *      (see the description of CMsleep())
 *
 * @param seconds For how many seconds handle the network if positive
 *                if negative it just runs forever
 * @return Diag::OK (at the moment always)
 */

Diag Ev_Topo::run(int seconds){
	CManager cm = Ev_Topo::get_CM_now();

	if(seconds >= 0){
		CMfork_comm_thread(cm);
		CMsleep(cm, seconds);
	} else {
		CMrun_network(cm);
	}

	return Diag::OK;
}
/**
 * checks if the object already exists
 * @return
 */
bool Ev_Topo::exists(){
	return (nullptr != p_topo);
}

/**
 * Returns the Connection manager
 * @return connection manager
 */
CManager Ev_Topo::get_CM(){
	return cm;
}

// ------------------------------------------
// Ev_Atom
// ------------------------------------------
const EVstone Ev_Atom::INVALID_STONE_ID = -1;
const EVaction Ev_Atom::INVALID_ACTION_ID = -1;

/**
 * Sets up appropriately the evpath field lists and format lists
 */
Ev_Atom::Ev_Atom() : p_topo(nullptr){
}

Ev_Atom::~Ev_Atom(){

}

/**
 * This gets the information how to contact me by the entity in the
 * EVPath world
 *
 * @param contact (OUT) This will contain the contact information of this
 *                      entity
 * @return Diag::OK if everything went ok
 *         != Diag::OK if anything went wrong
 */
Diag Ev_Atom::get_my_contact(string & contact){
	return Ev_Utils::get_my_contact(contact);
}

/**
 * write the contact to the file. The file_name and contact can't be empty
 *
 * @param file_name the file name where to write the contact
 * @param contact what to write
 * @return Diag::OK
 */
Diag Ev_Atom::wrt_my_contact(const string & file_name, const string & contact){
	return Cfg::wrt_val(file_name, contact,Cfg::CFG_EV_MASTER_SEC, Cfg::CFG_EV_MASTER_SEC_CONTACT_KEY);
}


// -----------------------------------------
// Ev_Master_Atom
// -----------------------------------------
/**
 * TODO I couldn't make it protected or private method of Ev_Master_Atom
 *      since I got some nasty compilation errors I couldn't fixed. So
 *      that's why this is static.
 *
 * This handler is for setting up the worker and collecting the contact
 * information from the workers
 *
 * @param cm
 * @param conn
 * @param alive_v
 * @param client_data pointer to Ev_Master_Atom
 * @param attrs
 */
extern "C" void alive_handler(CManager cm, CMConnection conn, void *alive_v,
			void *client_data, attr_list attrs){
	struct ev_alive_msg * event = static_cast<struct ev_alive_msg *>(alive_v);
	Ev_Master_Atom *p_atom = static_cast<Ev_Master_Atom *>(client_data);

	// TODO: not sure if this will be necessary
	if (p_atom->add_worker_contact(event->contact) != Diag::OK){
		LOG(ERROR) << "Got an error from add_worker_contact" << static_cast<int>(Diag::OK);
		return;
	}

	Ev_Ctrl_Msg *p_msg = Ev_Ctrl_Msg::get_instance();
	assert(nullptr != p_msg);
	auto p_formats = p_msg->get_format_list();
	assert(nullptr != p_formats);

	string cmd_holder = Ev_Worker_Atom::CMD_MASTER_HANDLER_NAME;

	assert(cmd_holder.size() > 0);

	// basically create a link: local output stone, remote terminal stone,
	// and add output stone to the splitter target

	Ev_Comm_Link link;
	// create an output stone
	link.output_id = EValloc_stone(cm);
	// Create a remote stone
	link.remote_id = REValloc_stone(conn);
	REVassoc_terminal_action(conn, link.remote_id, p_formats, const_cast<char*>(cmd_holder.c_str()));
	link.contact_to_remote = event->contact;

	// now make an output stone an stone
	attr_list contact_list = attr_list_from_string(event->contact);
	EVassoc_bridge_action(cm, link.output_id, contact_list, link.remote_id);

	// add the local output as a target for the splitter
	EVaction_add_split_target(cm, p_atom->get_cmd_split_stone(), p_atom->get_cmd_split_action(),
			link.output_id);

	// remember the link
	p_atom->add_cmd_link(link);
	LOG(DEBUG) << "New Worker joined me (local-output,contact,remote-terminal): "
			<< link.output_id << "," << event->contact <<  "," << link.remote_id ;

}

Ev_Master_Atom::Ev_Master_Atom() : alive_stone(Ev_Atom::INVALID_STONE_ID),
		cmd_source(nullptr),
		cmd_split_stone(Ev_Atom::INVALID_STONE_ID),
		cmd_split_action(Ev_Atom::INVALID_ACTION_ID){
}

Ev_Master_Atom::~Ev_Master_Atom(){

}
/**
 * Should be called after init() was called
 * @return
 */
Diag Ev_Master_Atom::join_topo(){
	if ( (Ev_Atom::INVALID_STONE_ID != alive_stone) ||
			(Ev_Atom::INVALID_STONE_ID != cmd_split_stone)){
		LOG(WARNING) << "in or cmd stones have been already initialized. Ignoring ...";
	}

	Diag diag = Diag::OK;
	CManager cm = Ev_Topo::get_CM_now();

	// this is a handler for collecting the info who joins the network
	Ev_Alive_Msg * msg = Ev_Alive_Msg::get_instance();
	CMregister_handler(msg->get_format(), alive_handler, this);

	// first I need to create a split stone (i.e., create a stone
	// and associate a split action with the stone to make it a split stone),
	// since it is used during the creation of the source as well as the output
	// stones
	cmd_split_stone = EValloc_stone(cm);
	cmd_split_action = EVassoc_split_action(cm, cmd_split_stone, nullptr);
	Ev_Ctrl_Msg * ctrl = Ev_Ctrl_Msg::get_instance();
	assert(nullptr != ctrl);
	cmd_source = EVcreate_submit_handle(cm, cmd_split_stone, ctrl->get_format_list());

	// now I am ready for the alive messages to come

	return diag;
}

Diag Ev_Master_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "NOT IMPLEMENTED";
	return diag;
}

/**
 * Adds a worker contact to master's registry
 * @param contact
 * @return
 */
Diag Ev_Master_Atom::add_worker_contact(const char * contact){
	Diag diag = Diag::OK;

	// check if I don't have such a contact
	auto ret = worker_contacts.emplace(contact);
	if(!ret.second){
		LOG(ERROR) << "I have already had a contact: " << contact;
		return Diag::COL_ITEM_EXISTS_ERR;
	}

	return diag;
}

/**
 * getter for the command split stone
 * @return a split stone
 */
EVstone Ev_Master_Atom::get_cmd_split_stone() const {
	return cmd_split_stone;
}
/**
 * getter fro the command split action
 * @return
 */
EVaction Ev_Master_Atom::get_cmd_split_action() const {
	return cmd_split_action;
}

/**
 * add the link to the cmd_links in the control network
 * @param link what will be remembered
 * @return always Diag::OK
 */
Diag Ev_Master_Atom::add_cmd_link(const Ev_Comm_Link& link) {
	Diag diag = Diag::OK;
	cmd_links.emplace_back(link);
	return diag;
}
/**
 * Sends the command to the control network of workers
 * @param cmd
 * @return Diag::OK always
 */
Diag Ev_Master_Atom::send_cmd(const string cmd) {
	Diag diag = Diag::OK;

	struct ev_ctrl_msg ctrl;
	ctrl.cmd = const_cast<char*>(cmd.c_str());
	EVsubmit(cmd_source, &ctrl, nullptr);

	return diag;
}

/**
 * A convenient method for joining the control network. Should be called
 * once after the master object was instantiated.
 *
 * @param cfg_file_name The name of the config file; it is checked if the
 *                      file name length is greater than 0
 *
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong
 */
Diag Ev_Master_Atom::run(const string cfg_file_name){
	Diag diag = Diag::OK;

	if( cfg_file_name.size() <= 0){
		diag = Diag::ERR;
		LOG(ERROR) << "The file name is empty()";
		return diag;
	}

	if( (diag = join_topo()) != Diag::OK){
		LOG(ERROR) << "Errors with joining the control topology ec=" << static_cast<int>(diag);
		return diag;
	}

	string my_contact;
	if( (diag = get_my_contact(my_contact)) != Diag::OK){
		LOG(ERROR) << "Errors with get_my_contact ec=" << static_cast<int>(diag);
		return diag;
	}

	if( (diag = wrt_my_contact(cfg_file_name, my_contact)) != Diag::OK){
		LOG(ERROR) << "Cant write my contact to a file: " << cfg_file_name <<
				" ec=" << static_cast<int>(diag);
		return diag;
	}
	return diag;
}
// ------------------------------------------------
// Ev_Worker_Atom
// ------------------------------------------------

/**
 * The handler that is supposed to be executed on the worker to get and
 * take appropriate actions.
 *
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return
 */
extern "C" int master_cmd_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_ctrl_msg * event = static_cast<struct ev_ctrl_msg*> (vevent);
	cout << "Cmd from master:" << event->cmd << "\n";
    return 1;
}
/**
 * The REVpath sends the name of the remote handler, the name is resolved
 * and loaded by dlopen()
 */
const string Ev_Worker_Atom::CMD_MASTER_HANDLER_NAME="master_cmd_handler";

Ev_Worker_Atom::Ev_Worker_Atom(const string my_cfg_file_name) :
		master_stone(Ev_Atom::INVALID_STONE_ID),
		conn(nullptr),
		cfg_file_name(my_cfg_file_name) {

}
Ev_Worker_Atom::~Ev_Worker_Atom(){

}

/**
 * This joins the control topology. Requires that init() is called before
 * calling that function.
 *
 * @return
 */
Diag Ev_Worker_Atom::join_topo(){

	Diag diag = Diag::OK;

	if (nullptr != conn){
		// assuming that it is already initialized
		return Diag::OK;
	}

	assert(cfg_file_name.size() > 0);

	// this should get the value of the master contact
	if( (diag = read_master_contact(cfg_file_name)) != Diag::OK){
		LOG(ERROR) << "Issues with read_master_contact. Returning ...";
		return diag;
	}

	// prepare for sending my contact info to the master
	string contact;

	if( (diag=get_my_contact(contact)) != Diag::OK){
		LOG(ERROR) << "Issues with getting the contact(). ATS ...";
		return diag;
	}

	// I don't care about the rest of the fields
	ev_alive_msg alive;
    alive.contact = const_cast<char*>(contact.c_str());

	if( (diag = send_alive_pattern(master_contact, conn, &alive)) != Diag::OK){
		LOG(ERROR) << "Can't send the alive message, ec=" << static_cast<int>(diag);
	}

	return diag;
}

Diag Ev_Worker_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "NOT IMPLEMENTED";
	return diag;
}

/**
 * Reads the master contact from the file
 *
 * @param file_name The name of the file that I want to read from
 * @return Diag::OK if everything ok
 *         != Diag::OK if errors detected
 */
Diag Ev_Worker_Atom::read_master_contact(const string & file_name){
	return Ev_Utils::get_master_contact_list(file_name, master_contact);
}
/**
 * Returns the master contact stored in the object; if the size is
 * zero you probably need to invoke the read_master_contact
 *
 * @return The value stored in master_contact
 */
string Ev_Worker_Atom::get_master_contact(){
	return master_contact;
}

/**
 * Convenient method to execute all commands necessary to initialize
 * and run the control network on the worker side.
 *
 * Should be called once, otherwise I don't what happens. It should be
 * called after the atom was constructed e.g.
 *
 * Ev_Worker_Atom atom(ini);
 * atom.run();
 *
 * @return Diag::OK if everything went fine
 *         != Diag::OK if something went wrong
 */
Diag Ev_Worker_Atom::run(){
	Diag diag = Diag::OK;

	if( (diag = join_topo()) != Diag::OK){
		LOG(ERROR) << "Can't join the topo ec=" << static_cast<int>(diag);
		return diag;
	}

	LOG(DEBUG) << "Joining the master topo: SUCCESS";

	return diag;
}


// ----------------------------------------------------
// Ev_Monitoring_Atom
// ----------------------------------------------------


Ev_Monitoring_Atom::Ev_Monitoring_Atom() : split_stone(Ev_Atom::INVALID_STONE_ID),
		split_action(Ev_Atom::INVALID_ACTION_ID),
		output_stone(Ev_Atom::INVALID_STONE_ID),
		cap_source(nullptr),
		mon_source(nullptr),
		aggreg_stone(Ev_Atom::INVALID_STONE_ID){
	MON_ATOM_CONTACT_FILE = "cw-mon-contact.txt";
}

/**
 *
 */
Ev_Monitoring_Atom::~Ev_Monitoring_Atom() {
	Cfg::rm_cfg(MON_ATOM_CONTACT_FILE);
}

/**
 * reads the aggregator contact list from the file name. This should
 * be called before calling any join_topo because it reads the contact
 * information needed to send an alive message to the aggregator to get
 * back some information from it about the remote stones etc.
 *
 * @param cfg_file_name The name of the configuration file
 *
 * @return Diag::OK if everything went ok
 *         != Diag::OK if any noticed errors
 */
Diag Ev_Monitoring_Atom::read_aggregator_contact_list(const string & cfg_file_name){
	return Ev_Utils::get_master_contact_list(cfg_file_name, aggreg_contact_list);
}
/**
 * Setter for an aggreg_stone
 *
 * @param an_aggreg_stone
 * @return Diag::OK always
 */
Diag Ev_Monitoring_Atom::set_aggreg_stone(EVstone an_aggreg_stone){
	Diag diag = Diag::OK;
	aggreg_stone = an_aggreg_stone;

	return diag;
}
/**
 * Setter for the aggreg_contact_list
 *
 * @param an_aggreg_contact_list
 * @return
 */
Diag Ev_Monitoring_Atom::set_aggreg_contact_list(const string an_aggreg_contact_list) {
	Diag diag = Diag::OK;

	aggreg_contact_list = an_aggreg_contact_list;
	return diag;
}
/**
 * Getter for the monitoring source
 * @return
 */
EVsource Ev_Monitoring_Atom::get_mon_source(){
	return mon_source;
}
/**
 * Setups the basic splitter messaging infrastructure; create splitters
 * and dump the contact to the file. It also creates an evpath-based infrastructure
 * to receive the contact information from the aggregator
 *
 * @return Diag::OK if everything went great
 *          != Diag::OK if anything went wrong
 */
Diag Ev_Monitoring_Atom::setup_split_ntw_infrastructure() {
	Diag diag = Diag::OK;

	// now setup the split infrastructure
	assert( aggreg_stone != Ev_Atom::INVALID_STONE_ID);
	assert( aggreg_contact_list.size() >0);


	CManager cm = Ev_Topo::get_CM_now();

	split_stone = EValloc_stone(cm);
	split_action = EVassoc_split_action(cm, split_stone, nullptr);

	attr_list remote_contact_list = attr_list_from_string(aggreg_contact_list.c_str());

	output_stone = EValloc_stone(cm);
	EVassoc_bridge_action(cm, output_stone, remote_contact_list, aggreg_stone);

	EVaction_add_split_target(cm, split_stone, split_action, output_stone);

	free_attr_list(remote_contact_list);

	// now output
	LOG(DEBUG) << "Output stone created. ";

	struct Ev_Contact contact;
	contact.split_action = split_action;
	contact.split_stone = split_stone;
	if ( (diag = Ev_Utils::get_my_contact(contact.contact)) != Diag::OK ){
		LOG(ERROR) << "Can't get my contact ec=" << static_cast<int>(diag);
		return diag;
	}
	if( (diag = Contact_File::wrt_to(MON_ATOM_CONTACT_FILE, contact)) != Diag::OK){
		LOG(ERROR) << "Can't write to a file ec=" << static_cast<int>(diag);
		return diag;
	}

	return diag;
}

/**
 * Waits until the timeout elapses or the aggregator stone
 * is != from Ev_Atom::INVALID_STONE_ID
 * @return Diag::OK The aggregator stone changed
 *         Diag::TIMEOUT_ERR the time elapsed
 */
Diag Ev_Monitoring_Atom::wait_for_aggreg(){
	Diag diag = Diag::TIMEOUT_ERR;
	for(int i = 0; i < Cfg::get_wait_for_aggreg_timeout(); ++i){
		if( Ev_Atom::INVALID_STONE_ID != aggreg_stone){
			diag = Diag::OK;
			break;
		}
		sleep(1);
	}

	return diag;
}
/**
 * Joins the star topology. It supposed to send the alive message to the
 * aggregator side and cause that the relevant aggregator will be
 * created on the aggregator side if it doesn't exists. Also
 * the aggreg_stone and aggreg_stone_contact_list should be set as a
 * result of this procedure
 *
 * @param atom_type The type of the atom that wants to join
 * @return Diag::OK if everything went great
 *         != Diag::OK if something went wrong
 */

Diag Ev_Monitoring_Atom::lets_join_topo(const Atom_Type atom_type){
	Diag diag = Diag::OK;

	Ev_CW_Mon_Runtime *rt = Ev_CW_Mon_Runtime::get_instance();
	assert(rt != nullptr);

	if ( (diag = rt->send_agg_stone_request(atom_type)) != Diag::OK){
			LOG(ERROR) << "Errors during the sending a request for the aggregator stone ec="
					<< static_cast<int>(diag);
	}

	// wait until we have the aggregator stone
	if( (diag=wait_for_aggreg()) == Diag::OK ){
		LOG(DEBUG) << "Got aggregator stone: " << aggreg_stone;
	} else {
		LOG(ERROR) << "Timeout error. Didn't get the aggreg stone aggreg_stone=" << aggreg_stone << " ATS...";
		return diag;
	}

	if ( (diag = setup_split_ntw_infrastructure()) != Diag::OK){
		LOG(ERROR) << "Can't setup the basic infrastructure ec=" << static_cast<int>(diag) << " ATS ...";
		return diag;
	}

	return diag;
}
// --------------------------------------------------
// Ev_Aggregator_Atom
// --------------------------------------------------

Ev_Aggregator_Atom::Ev_Aggregator_Atom() :
		aggreg_stone(Ev_Atom::INVALID_STONE_ID),
		split_action(Ev_Atom::INVALID_ACTION_ID){
}

Ev_Aggregator_Atom::~Ev_Aggregator_Atom() {
}
/**
 * @return getter fro aggreg_stone
 */
EVstone Ev_Aggregator_Atom::get_aggreg_stone() const{
	return aggreg_stone;
}
/**
 * setup aggregator stone - allocate memory for that stone
 * @return Diag::OK always
 */
Diag Ev_Aggregator_Atom::setup_ntw_basic_infrastructure(){
	Diag diag = Diag::OK;

	// now prepare an aggregator stone that will be a splitter stone
	// for getting data
	CManager cm = Ev_Topo::get_CM_now();

	aggreg_stone = EValloc_stone(cm);
	split_action = EVassoc_split_action(cm, aggreg_stone, nullptr);

	return diag;
}
// --------------------------------------------
// Ev_Splitter_Hook
// -------------------------------------------
/**
 * Sets the name of the contact file name
 *
 * @param contact_file_name The name of the file where the
 *                          contact to the the splitter stone
 *                          will be stored
 * It is not checked if the contact_file_name is an empty
 * string.
 */
Ev_Splitter_Hook::Ev_Splitter_Hook(string contact_file_name) :
	split_stone(Ev_Atom::INVALID_STONE_ID),
	split_action(Ev_Atom::INVALID_ACTION_ID),
	src(nullptr){
	contact_file = contact_file_name;
}

/**
 *
 * Creates the splitter stone and splitter action and
 * outputs the contact information to the file, and creates
 * the sources and connects the source to the splitter.
 *
 * The file is a key-value pair file with the default section
 * Cfg::CFG_SPLITTERS_SEC.
 *
 * The key is whatever is provided, and the value
 * is a string " contact split_stone split_action". Pay attention
 * to spaces.
 *
 * key= AAIAAJTJ8o3YZQAAATkCmGcAqMA= 0 0
 *
 * Checks if the parameter is nullptr and return an error
 * if this is the case
 *
 * @param p_formats The format description for the the source
 * @param key The key where the contact will be written; the contact
 *            will be the read
 * @return Diag::OK if the splitter stone and splitter action
 *                  has been created
 *         Diag::WARN if it was detected that the splitter stone
 *                    has been already created and then no
 *                    action is taken apart from the returning the
 *                    warning
 *         Diag::ERR if the list of EVPath format descriptions is nullptr
 *         != Diag::OK if there are some issues with writing to the
 *                   file or something else
 *
 */
Diag Ev_Splitter_Hook::create_splitter_hook(FMStructDescRec * p_formats, string key){
	Diag diag = Diag::OK;

	if( !p_formats ){
		LOG(ERROR) << "The EVPath event descriptions are nullptr. Returning an error";
		return Diag::ERR;
	}
	if( Ev_Atom::INVALID_STONE_ID != split_stone ){
		LOG(WARNING) << "It seems that the splitter hook has been already created. Returning.";
		return Diag::WARN;
	}

	CManager cm = Ev_Topo::get_CM_now();

	split_stone = EValloc_stone(cm);
	split_action = EVassoc_split_action(cm, split_stone, nullptr);

	string contact;
	if ( (diag = Ev_Utils::get_my_contact(contact)) != Diag::OK ){
		LOG(ERROR) << "Can't get my contact ec=" << static_cast<int>(diag);
		return diag;
	}

	string my_contact;
	//my_contact = " " + contact + " " + to_string(split_stone) + " " + to_string(split_action);
	my_contact = contact + ":" + to_string(split_stone) + ":" + to_string(split_action);
	if ( (diag = Cfg::wrt_val(contact_file, my_contact, Cfg::CFG_SPLITTERS_SEC, key )) != Diag::OK) {
		LOG(ERROR) << "Can't write to a file ec=" << static_cast<int>(diag);
		return diag;
	}

	src = EVcreate_submit_handle(cm, split_stone, p_formats);

	return diag;
}

/**
 * Getter for the contact file name of this splitter.
 *
 * @return
 */
string Ev_Splitter_Hook::get_contact_file_name(){
	return contact_file;
}

/**
 * @return getter for split_stone
 */
EVstone Ev_Splitter_Hook::get_split_stone(){
	return split_stone;
}
/**
 * @return getter for split action
 */
EVaction Ev_Splitter_Hook::get_split_action(){
	return split_action;
}

EVsource Ev_Splitter_Hook::get_src(){
	return src;
}
/**
 * Checks if the hook has been created
 * @return true The hook has been created
 *         false The hook has not been created
 */
bool Ev_Splitter_Hook::is_hook_created(){
	return Ev_Atom::INVALID_STONE_ID != split_stone;
}
// --------------------------------------------
// DFG topology
// --------------------------------------------


//---------------------------------------------
// Ev_Dfg_Atom
// ------------------------------------------------
/**
 * TODO this is probably a design flaw
 * This is because I inherit from Atom and join_topo() is virtual. The function
 * of this method member has been played by join_topo with parameters
 *
 * @return Diag::ERR always
 */
Diag Ev_Dfg_Atom::join_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "Not implemented";
	diag = Diag::ERR;

	return diag;
}

/**
 *
 * @return
 */
Diag Ev_Dfg_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "Not implemented";
	diag = Diag::ERR;

	return diag;
}
/**
 * getter for the splitter hook
 * @return
 */
vector<Ev_Splitter_Hook*> & Ev_Dfg_Atom::get_splitter_hook(){
	return splitter_hook_vec;
}

/**
 * Loads the source join info structure. The p_info->node_name and
 * p_info->stone_name have allocated memory. This should be freed
 * if not needed with free()
 *
 * @param p_info (out): This will be created as a result. Only
 *                      fields p_info->node_name, stone_name and simple_format_list
 *                      are set.
 * @param node_name (in): the name of the node in the dfg structure
 * @param stone_name (in): the name of the stone
 * @param p_formats (in): the formats
 * @return Diag::OK always
 */
Diag Ev_Dfg_Atom::load_join_info(join_info *p_info, string node_name, string stone_name, FMStructDescRec * p_formats){
	Diag diag = Diag::OK;

	p_info->node_name = strdup(const_cast<char*>(node_name.c_str()));
	p_info->stone_name = strdup(const_cast<char*>(stone_name.c_str()));
	p_info->simple_format_list = p_formats;

	return diag;
}

// ----------------------------------------
// Ev_Dfg_White_Atom (source)
// ----------------------------------------

/**
 * Registering a source with DFG. It sets the src with the value
 * obtained from EVdfg_register_source().
 *
 * The splitter_hook is created and the entry in the Cfg::CFG_SPLITTERS_DEFAULT_NAME
 * is created in appropriate section. The key and value is in the format
 * key = node_name + "_" + stone_name
 * value = " splitter_contact_string split_stone_id split_action_id"
 *
 * It expects that the src will be nullptr.
 *
 * Because of not clear internals
 * in DFG library it might be that there is memory leak, as some
 * allocated memory is not freed (see the content of this method)
 * The method expects that the Ev_Dfg_White_Atom::src will be null
 *
 * @param node_name The name of the node that I am joining
 * @param stone_name the name of the stone that I am joining
 * @param fmt_list The format of accepted messages
 * @return Diag::OK always
 */
Diag Ev_Dfg_White_Atom::join_dfg_node(string node_name, string stone_name, FMStructDescRec * p_formats){
	Diag diag = Diag::OK;

	source_join_info  info;

	if( load_source(&info, node_name, stone_name, p_formats) != Diag::OK){
		return Diag::ERR;
	}

	CManager cm = Ev_Topo::get_CM_now();

	if( dfg_reg_source(cm, &info) != DIAG_OK ){
		LOG(ERROR) << "dfg_reg_source() returned error message. Freeing memory and returning error";
		free(info.node_name);
		free(info.stone_name);
		return Diag::ERR;
	}

	src_vec.emplace_back(info.src);

	// TODO I am not sure if I can free(info.node_name) and free(info.stone_name)
	// as it might be used by EVdfg internal structures. so here it might
	// be a memory leak

	// initialize a splitter hook
	string key = node_name + "_" + stone_name;

	splitter_hook_vec.emplace_back(new Ev_Splitter_Hook());

	if ( (diag = splitter_hook_vec.back()->create_splitter_hook(p_formats, key)) != Diag::OK ){
		LOG(ERROR) << "Issues with creating a splitter hook";
		return diag;
	}

	assert(get_splitter_hook().back()->get_src());

	return diag;
}

/**
 * Getter for src_vec
 * @return
 */
vector<EVsource> & Ev_Dfg_White_Atom::get_src(){
	return src_vec;
}

/**
 * Loads the source join info structure. The p_info->node_name and
 * p_info->stone_name have allocated memory. This should be freed
 * if not needed with free()
 *
 * @param p_info (out): This will be created as a source
 * @param node_name (in): the name of the node in the dfg structure
 * @param stone_name (in): the name of the stone
 * @param p_formats (in): the formats
 * @return Diag::OK if everything is fine
 *         Diag::ERR if the src_vec range is out of bound
 */
Diag Ev_Dfg_White_Atom::load_source(source_join_info *p_info, string node_name,
		string stone_name, FMStructDescRec * p_formats){
	Diag diag = Diag::OK;

	if(  load_join_info(p_info, node_name, stone_name, p_formats) != Diag::OK) {
		LOG(ERROR) << "node_name:stone_name " << node_name << ":" << stone_name
				<< " loading error. Returning error.";
		return diag;
	}

	return diag;
}

// ----------------------------------------
// Ev_Dfg_Black_Atom
// ----------------------------------------
/**
 * Registers DFG sink handler
 * @param node_name
 * @param stone_name
 * @param p_formats
 * @param handler
 * @param client_data
 * @return Diag::OK if everything went fine
 *         Diag::ERR if loading the data to sink experienced some errors
 *                   or registering sink handler in DFG
 */
Diag Ev_Dfg_Black_Atom::join_dfg_node(string node_name, string stone_name, FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void * client_data){
	Diag diag = Diag::OK;

	sink_join_info info;

	if( load_sink(&info, node_name, stone_name, p_formats, handler, client_data) != Diag::OK){
		LOG(ERROR) << "Got error from load_sink(). Returning error.";
		return Diag::ERR;
	}

	CManager cm = Ev_Topo::get_CM_now();
	if (dfg_reg_sink(cm, &info) != DIAG_OK){
		LOG(ERROR) << "dfg_reg_sink() returned an error. Returning an error";
		free(info.node_name);
		free(info.stone_name);
		return Diag::ERR;
	}

	return diag;
}


/**
 * fills the structure sink_join_info with provided data. The
 * p_info->node_name and p_info->stone_name is allocated and should
 * be freed with free() when it is not needed at some point.
 *
 * @param p_info (out) This structure is filled with provided parameters
 * @param node_name (in) The provided information
 * @param stone_name (in) The name of the stone
 * @param p_formats (in) the incoming event formats (refer to EVPath)
 * @param handler   (in) the handler to the function that will be handling the formats
 * @param client_data (in) the client_data for EVPath
 * @return Diag::OK if everything went fine
 *         Diag::ERR if some errors e.g. from load_join_info()
 */
Diag Ev_Dfg_Black_Atom::load_sink(sink_join_info *p_info, string node_name, string stone_name, FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void *client_data){
	Diag diag = Diag::OK;

	if( load_join_info(p_info, node_name, stone_name, p_formats)
			!= Diag::OK){
		LOG(ERROR) << "Got an error from load_join_info(). Returning error";
		return Diag::ERR;
	}

	p_info->handler_func = handler;
	p_info->client_data = client_data;

	return diag;
}

// ------------------------------------------------
// Ev_Dfg_Gray_Atom: the sink and the source
// ------------------------------------------------
/**
 * registers handlers for sinks and sources for a provided node
 *
 * @param node_name
 * @param white_stone_name
 * @param black_stone_name
 * @param p_formats
 * @param handler
 * @param client_data
 * @return Diag::OK if everything went great
 *         Diag::ERR if there were some errors
 */
Diag Ev_Dfg_Gray_Atom::join_dfg_node(string node_name,
		string white_stone_name, string black_stone_name,
		FMStructDescRec * p_formats, EVSimpleHandlerFunc handler, void * client_data){

	Diag diag = Diag::OK;

	auto src_vec_size = src_vec.size();
	auto splitter_hook_vec_size = splitter_hook_vec.size();

	// for some reason first sink; but I think it matters
	if( (diag = Ev_Dfg_Black_Atom::join_dfg_node(node_name, black_stone_name, p_formats, handler, client_data)) != Diag::OK){
		LOG(ERROR) << "Got error from an attempt to join DFG by a sink. ATS";
		return diag;
	}

	// next source
	if ( (diag = Ev_Dfg_White_Atom::join_dfg_node(node_name, white_stone_name, p_formats)) != Diag::OK){
		LOG(ERROR) << "Propagated error from the source attempting to join DFG. ATS";
		return diag;
	}

	assert(src_vec.size() == src_vec_size + 1);
	assert(splitter_hook_vec.size() == splitter_hook_vec_size + 1);

	return diag;
}

} /* namespace cw */


