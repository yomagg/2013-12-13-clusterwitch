/**
 *  @file   net_transport.cpp
 *
 *  @date   Created on: Feb 25, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>

#include "easylogging++.h"

#include "net_transport.h"
#include "cw_runtime.h"

namespace cw {

//! TODO ugly but this is because EVdfg_register_sink_handler does not provide
//! passing the client_data as e.g. EVassociate_terminal_action does
//! EVdfg_register_sink_handler(cm,sink_info->stone_name,(sink_info->simple_format_list),sink_info->handler_func);
//! This is a temporary hack and should be removed if that thing is fixed

static Ev_Dfg_Net_Black_Atom * global_dfg_black_atom = nullptr;
static Ev_Dfg_Net_Gray_Atom * global_dfg_gray_atom = nullptr;

// -----------------------------------------------------
// Net_Mon_Atom
// -----------------------------------------------------
/**
 * Returns the pointer to the cpu_spy
 */
Net_Spy * Net_Mon_Atom::get_spy(){
	return &spy;
}
/**
 * Getter for cpu sqlite3 reader
 * @return
 */
Net_Sqlite3_Reader * Net_Mon_Atom::get_sqlite3_reader(){
	return &sqlite3_reader;
}

// -------------------------------------
// Monitoring atom
// -------------------------------------

/**
 * This is handler for periodic task in CM
 * @param cm
 * @param client_data
 */
extern "C" void net_mon_reader(CManager cm, void *client_data){
	Ev_Net_Monitoring_Atom *p_atom = static_cast<Ev_Net_Monitoring_Atom *>(client_data);

	Net_Spy *p_spy = p_atom->get_spy();
	ev_net_mon * rec = p_spy->get_mon();

	if( p_spy->read_mon() != Diag::OK){
		LOG(ERROR) << "Some issues with reading mon data";
		return;
	}

	EVsubmit(p_atom->get_mon_source(), rec, nullptr);

	return;
}

Ev_Net_Monitoring_Atom::Ev_Net_Monitoring_Atom(){
	MON_ATOM_CONTACT_FILE = Cfg::CFG_NET_MON_ATOM_FILE;
}

Diag Ev_Net_Monitoring_Atom::join_topo(){
	Diag diag = Diag::OK;

	Ev_CW_Mon_Runtime *rt = Ev_CW_Mon_Runtime::get_instance();
	assert(nullptr != rt);
	rt->add_atom(*this);

	if( (diag = lets_join_topo(Atom_Type::NET)) != Diag::OK ){
		LOG(ERROR) << "Got some error ec=" << static_cast<int>(diag);
		return diag;
	}

	// setting up the spy
	spy.setup_for_read();

	// send the capability record
	if( (diag = send_cap()) != Diag::OK) {
		LOG(ERROR) << "Can't send capabilities ec=" << static_cast<int>(diag);
		return diag;
	}

	// TODO setup the periodic events
	if( (diag = send_mon()) != Diag::OK) {
		LOG(ERROR) << "Can't send capabilities ec=" << static_cast<int>(diag);
		return diag;
	}

	return diag;
}

/**
 * quits the topo
 * @return
 */
Diag Ev_Net_Monitoring_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "NOT IMPLEMENTED";
	return diag;
}

/**
 * @return getter for the spy
 */
Net_Spy * Ev_Net_Monitoring_Atom::get_spy(){
	return &spy;
}

/**
 * sends the capabilities. should be called when
 * the infrastructure is already set up.
 *
 * @return Diag::OK always
 */
Diag Ev_Net_Monitoring_Atom::send_cap(){
	Diag diag = Diag::OK;

	assert(nullptr == cap_source);
	CManager cm = Ev_Topo::get_CM_now();
	Ev_Net_Cap_Msg *p_msg = Ev_Net_Cap_Msg::get_instance();
	auto p_formats = p_msg->get_format_list();
	cap_source = EVcreate_submit_handle(cm, split_stone, p_formats);

	ev_net_cap *rec = spy.get_caps();

	LOG(DEBUG) << *rec;

	EVsubmit(cap_source, rec, nullptr);

	return diag;
}

/**
 * starts sending the monitoring data
 *
 * @return Diag::OK always
 */
Diag Ev_Net_Monitoring_Atom::send_mon(){
	Diag diag = Diag::OK;

	assert(nullptr == mon_source);
	CManager cm = Ev_Topo::get_CM_now();
	Ev_Net_Mon_Msg *p_msg = Ev_Net_Mon_Msg::get_instance();
	auto p_formats = p_msg->get_format_list();
	mon_source = EVcreate_submit_handle(cm, split_stone, p_formats);

	CMadd_periodic_task(cm, Cfg::get_monitor_period(), 0, net_mon_reader, this);

	return diag;
}
// ------------------------------------------
// Ev_Net_Aggregator_Atom
// ------------------------------------------
/**
 * The EVPath handler for handling the caps
 *
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return 1
 */
extern "C" int net_cap_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	ev_net_cap *event = static_cast<ev_net_cap*> (vevent);
	Ev_Net_Aggregator_Atom *atom = static_cast<Ev_Net_Aggregator_Atom *>(client_data);

	if( atom->get_sqlite3_reader()->insert_cap_rec(event) != Diag::OK){
		LOG(ERROR) << "Got an error from insert_cap_rec()";
		return 1;
	}
	if( atom->get_spy()->process(*event) != Diag::OK ){
		LOG(ERROR) << "Got an error from process()";
		return 1;
	}

    return 1;
}

/**
 * The evpath handler for handling the mon events
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return
 */
extern "C" int net_mon_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	ev_net_mon *event = static_cast<ev_net_mon*> (vevent);
	Ev_Net_Aggregator_Atom *atom = static_cast<Ev_Net_Aggregator_Atom *>(client_data);


	if( atom->get_sqlite3_reader()->insert_mon_rec(event) != Diag::OK){
		LOG(ERROR) << "Got error from insert_mon_rec()";
		return 1;
	}

	if( atom->get_spy()->process(*event) != Diag::OK){
		LOG(ERROR) << "Got error from processing by spy";
		return 1;
	}

    return 1;
}

Diag Ev_Net_Aggregator_Atom::join_topo() {

	Diag diag = Diag::OK;

	// initiate the readers (setup the database before setup of the
	// handler of events
	if ( (diag = sqlite3_reader.open_db()) != Diag::OK){
		LOG(ERROR) << "Can't open the Sqlite3 ec=" << static_cast<int>(diag);
		return diag;
	}

	// do the setup
	if ( (diag = setup_ntw_basic_infrastructure()) != Diag::OK){
		LOG(ERROR) << "Got from setup_ntw_alive_infrastructure ec=" << static_cast<int>(diag);
		return diag;
	}

	// add the receiver for capabilities
	CManager cm = Ev_Topo::get_CM_now();
	Ev_Net_Cap_Msg * p_cap_msg = Ev_Net_Cap_Msg::get_instance();
	auto p_cap_formats = p_cap_msg->get_format_list();

	EVassoc_terminal_action(cm, aggreg_stone, p_cap_formats, net_cap_handler, this);

	// add the receiver for monitoring data
	Ev_Net_Mon_Msg * p_mon_msg = Ev_Net_Mon_Msg::get_instance();
	auto p_mon_formats = p_mon_msg->get_format_list();

	EVassoc_terminal_action(cm, aggreg_stone, p_mon_formats, net_mon_handler, this);

	return diag;
}

Diag Ev_Net_Aggregator_Atom::quit_topo() {
	Diag diag = Diag::OK;
	LOG(DEBUG) << "NOT IMPLEMENTED";
	return diag;
}

Net_Sqlite3_Reader * Ev_Net_Aggregator_Atom::get_sqlite3_reader(){
	return &sqlite3_reader;
}

Net_Spy *Ev_Net_Aggregator_Atom::get_spy(){
	return &spy;
}

// --------------------------------------------------
// Ev_Dfg_Net_Writer_Atom (monitoring, i.e., the one that reads the data
// and writes to DFG for others
// --------------------------------------------------
/**
 * This is handler for periodic task in CM
 * @param cm
 * @param client_data
 */
extern "C" void dfg_net_white_reader(CManager cm, void *client_data){
	Ev_Dfg_Net_White_Atom *p_atom = static_cast<Ev_Dfg_Net_White_Atom *>(client_data);

	assert(p_atom);

	Net_Spy *p_spy = p_atom->get_spy();

	ev_net_mon * rec = p_spy->get_mon();

	if( p_spy->read_mon() != Diag::OK){
		LOG(ERROR) << "Some issues with reading mon data";
		return;
	}

	if( EVclient_source_active(p_atom->get_src().back()) ){
		EVsubmit(p_atom->get_src().back(), rec, nullptr);
		// send the same event to the splitter hook in case
		// the external entity is interested in this event
		EVsubmit(p_atom->get_splitter_hook().back()->get_src(), rec, nullptr);
	}

	return;
}
/**
 * Register in the dfg topology
 *
 * @param node_name The name of the node to which I am joining
 * @param stone_name The name of the stone that joins
 * @return
 */
Diag Ev_Dfg_Net_White_Atom::reg(string node_name, string stone_name){
	Diag diag = Diag::OK;

	// setting up the spy
	spy.setup_for_read();

	// currently I will send only monitoring information
	Ev_Net_Mon_Msg *p_msg = Ev_Net_Mon_Msg::get_instance();
	auto p_formats = p_msg->get_format_list();


	if( (diag = Ev_Dfg_White_Atom::join_dfg_node(node_name, stone_name, p_formats) ) != Diag::OK){
		LOG(ERROR) << "Node: " << node_name << " Stone: " << stone_name <<
				" can't register in DFG. Returning an error";
		return diag;
	} else {
		LOG(DEBUG) << "Node: " << node_name << " Source: " << stone_name << " registering in DFG: SUCCESS.";
	}

	return diag;
}

/**
 * start sending the monitoring data
 */
Diag Ev_Dfg_Net_White_Atom::run(){
	Diag diag = Diag::OK;

	if( (diag = send_mon()) != Diag::OK ){
		LOG(ERROR) << "Can't send monitoring events ec=" << static_cast<int>(diag)
				<< "Returning error";
		return diag;
	}

	return diag;
}

Diag Ev_Dfg_Net_White_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << "Not implemented";
	return diag;
}

/**
 * This starts sending the monitoring events
 *
 * @return
 */
Diag Ev_Dfg_Net_White_Atom::send_mon(){
	Diag diag = Diag::OK;

	CManager cm = Ev_Topo::get_CM_now();

	CMadd_periodic_task(cm, Cfg_Dfg::get_monitoring_sampling_sec_rate(), Cfg_Dfg::get_monitoring_sampling_usec_rate(), dfg_net_white_reader , this);

	return diag;
}

// --------------------------------------------------
// Ev_Dfg_Net_Reader_Atom, aggregator, absorbs data from writers but do not write
// --------------------------------------------------
/**
 * The evpath handler for handling the mon events
 * This uses the global_dfg_black_atom pointer.
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return
 */
extern "C" int dfg_net_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	ev_net_mon *event = static_cast<ev_net_mon*> (vevent);
	auto *atom = global_dfg_black_atom;

	if (atom->get_spy()->process(*event) != Diag::OK){
		LOG(ERROR) << "Got error from processing by spy";
		return 1;
	}

	if( atom->get_sqlite3_reader()->insert_mon_rec(event) != Diag::OK){
		LOG(ERROR) << "Got error from insert_mon_rec()";
		return 1;
	}

    return 1;
}

//! TODO remove if global_dfg_black_atom is not required
Ev_Dfg_Net_Black_Atom::Ev_Dfg_Net_Black_Atom() {
	global_dfg_black_atom = this;

}
//! TODO remove if global_dfg_black_atom is not required
Ev_Dfg_Net_Black_Atom::~Ev_Dfg_Net_Black_Atom() {
	global_dfg_black_atom = nullptr;
}

/**
 * Reg the dfg topology as a sink
 * @param node_name The name of the node to which I am joining
 * @param stone_name The name of the stone to which I am joining
 * @return Diag::OK if everything went ok
 *         != Diag::OK if something went wrong
 */
Diag Ev_Dfg_Net_Black_Atom::reg(string node_name, string stone_name){
	Diag diag = Diag::OK;

	// initiate the readers (setup the database before setup of the
	// handler of events
	if ( (diag = sqlite3_reader.open_db()) != Diag::OK){
		LOG(ERROR) << "Can't open the Sqlite3 ec=" << static_cast<int>(diag);
		return diag;
	}

	// currently only sending monitoring information
	Ev_Net_Mon_Msg *p_msg = Ev_Net_Mon_Msg::get_instance();
	auto p_formats = p_msg->get_format_list();

	if( (diag = Ev_Dfg_Black_Atom::join_dfg_node(node_name, stone_name, p_formats, dfg_net_black_handler, this)) != Diag::OK){
		LOG(ERROR) << "Node: " << node_name << " Stone: " << stone_name
				<< "DFG related registering error. ATS";
		return diag;
	} else {
		LOG(DEBUG) << "Node: " << node_name << " Sink: " << stone_name << " registering: SUCCESS.";
	}

	return diag;
}

/**
 * Quit the topology
 * @return
 */
Diag Ev_Dfg_Net_Black_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << " Not implemented";

	return diag;
}

// --------------------------------------------------
// Ev_Dfg_Net_Gray_Atom, aggregator, absorbs data from writers but do not write
// --------------------------------------------------

/**
 * The evpath handler for handling the mon events. The handler uses
 * global variable global_dfg_gray_atom
 *
 * @param cm
 * @param vevent
 * @param client_data
 * @param attrs
 * @return
 */
extern "C" int dfg_net_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	ev_net_mon *event = static_cast<ev_net_mon*> (vevent);

	//Ev_Dfg_Net_Gray_Atom *atom = static_cast<Ev_Dfg_Net_Gray_Atom*>(client_data);
	auto atom = global_dfg_gray_atom;

	// write the event to the graph
	if( EVclient_source_active(atom->get_src().back()) ){
		EVsubmit(atom->get_src().back(), event, nullptr);
		// send the same event to the splitter hook in case
		// the external entity is interested in this event
		EVsubmit(atom->get_splitter_hook().back()->get_src(), event, nullptr);
	}
    return 1;
}
//! TODO remove if global_dfg_gray_atom is not required
Ev_Dfg_Net_Gray_Atom::Ev_Dfg_Net_Gray_Atom() {
	global_dfg_gray_atom = this;
}
//! TODO remove if global_dfg_gray_atom is not required
Ev_Dfg_Net_Gray_Atom::~Ev_Dfg_Net_Gray_Atom() {
	global_dfg_gray_atom = nullptr;
}
/**
 * reg the dfg topology
 * @param node_name The name of the node to which I am joining
 * @param sink_name The name of the sink I am instantiating
 * @param src_name The name of the source I am instantiating
 * @return
 */
Diag Ev_Dfg_Net_Gray_Atom::reg(string node_name, string sink_name, string src_name){
	Diag diag = Diag::OK;

	// currently only sending monitoring information
	Ev_Net_Mon_Msg *p_msg = Ev_Net_Mon_Msg::get_instance();
	auto p_formats = p_msg->get_format_list();

	// TODO apparently the below join_topo has been shadowed
	if( (diag = Ev_Dfg_Gray_Atom::join_dfg_node( node_name, src_name, sink_name, p_formats, dfg_net_gray_handler, this)) != Diag::OK){
		LOG(DEBUG) << "Node: " << node_name << " Sink: " << sink_name << " Source: " << src_name << " registering: FAILURE";
		return diag;
	} else {
		LOG(DEBUG) << "Node: " << node_name << " Sink: " << sink_name << " Source: " << src_name << " registering: SUCCESS";
	}

	return diag;
}
/**
 * Quit the topology
 * @return
 */
Diag Ev_Dfg_Net_Gray_Atom::quit_topo(){
	Diag diag = Diag::OK;
	LOG(WARNING) << " Not implemented";

	return diag;
}


} // namespace cw
