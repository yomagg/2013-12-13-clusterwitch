/**
 *  @file   net_spy.cpp
 *
 *  @date   Created on: Feb 20, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <cassert>
#include <cstdlib>
#include <string>
#include <sstream>
#include <iomanip> // for changing precision
#include <cmath>

#include "easylogging++.h"

#include "net_spy.h"
#include "spy.h"
#include "net_spy_events_desc.h"
#include "misc.h"
#include "cw_runtime.h"
namespace cw {

using namespace std;
// -------------------------
// ev_net_cap
// -------------------------
std::ostream & operator<<(std::ostream & os, const ev_net_cap& rec){
	os << "TS:ID:HOST:PID:NICS_COUNT:NICS_NAMES " << rec.ts << ":"
	   << rec.id.id << ":" << rec.id.hostname << ":" << rec.id.pid << ":"
	   << rec.nics_count ;

	for(int i = 0 ; i < rec.nics_count; ++i){
		os << ":" << rec.nics_names[i];
	}

	return os;
}

ev_net_cap::ev_net_cap(): id(identity()), ts(0), nics_count(0), nics_names(nullptr){
}

ev_net_cap::ev_net_cap(const ev_net_cap & other) : id(other.id), ts(other.ts),
		nics_count(other.nics_count){
	copy_nics(other);
}

ev_net_cap::~ev_net_cap(){
	free_alloc();
}

ev_net_cap & ev_net_cap::operator=(const ev_net_cap & other){
	ts = other.ts;
	id = other.id;

	free_alloc();
	assert(nullptr == nics_names);
	assert(0 == nics_count);

	copy_nics(other);

	return *this;
}

/**
 * Frees the resources allocated for nics_names()
 * and sets nics_count to zero
 */
void ev_net_cap::free_alloc(){
	for(int i = 0; i < nics_count; ++i){
		// they were allocated with malloc (e.g., strdup)
		free(nics_names[i]);
		nics_names[i] = nullptr;
	}
	free(nics_names);
	nics_names = nullptr;
	nics_count = 0;
}

void ev_net_cap::copy_nics(const ev_net_cap & other){
	assert(nullptr == nics_names);
	nics_names = new char* [other.nics_count];
	for(int i = 0; i < other.nics_count; ++i){
		nics_names[i] = strdup(other.nics_names[i]);
		assert(nullptr != nics_names[i]);
	}
	nics_count = other.nics_count;
}

// ---------------------------------
// ev_nic_usage
// --------------------------------
ev_nic_usage::ev_nic_usage() : name(nullptr), combined_usage(0.0), received(0.0), transmitted(0.0) {
}
ev_nic_usage::~ev_nic_usage(){
	free_name();
}
ev_nic_usage::ev_nic_usage(const ev_nic_usage & other) : combined_usage(other.combined_usage), received(other.received), transmitted(other.transmitted){
	copy_name(other);
}
ev_nic_usage & ev_nic_usage::operator=(const ev_nic_usage & other){
	if( this == &other ){
		return *this;
	}
	combined_usage = other.combined_usage;
	received = other.received;
	transmitted = other.transmitted;
	copy_name(other);

	return *this;
}
void ev_nic_usage::free_name(){
	free(name);
	name = nullptr;
}
void ev_nic_usage::copy_name(const ev_nic_usage & other){
	free_name();
	if( other.name ){
		name = strdup(other.name);
	}
}


// -------------------------
// ev_net_mon
// -------------------------
std::ostream & operator<<(std::ostream & os, const ev_net_mon& rec){
	// save flags
	std::ios_base::fmtflags oldflags = os.flags();
	std::streamsize oldprecision = os.precision();

	os << std::fixed << std::setprecision(2);

	for(int i = 0; i < rec.nics_count; ++i ){
		os << "TS:ID:" << rec.nics_usage_arr[i].name
		<< "COMBINED:TSMITTED:RECV:"
		<< rec.ts << ":" << rec.id << ":"
		<< rec.nics_usage_arr[i].combined_usage << ":"
		<< rec.nics_usage_arr[i].transmitted << ":"
		<< rec.nics_usage_arr[i].received << "\n";
	}

	// restore flags
	os.flags (oldflags);
	os.precision (oldprecision);

	return os;
}

ev_net_mon::ev_net_mon(): id(0), ts(0), nics_count(0), nics_usage_arr(nullptr)  {
}
ev_net_mon::~ev_net_mon(){
	free_usage_arr();
}

ev_net_mon::ev_net_mon(const ev_net_mon & other) : id(other.id), ts(other.ts){
	if( nullptr != other.nics_usage_arr ){
		assert(other.nics_count > 0);
		nics_usage_arr = new ev_nic_usage[other.nics_count];
		nics_count = other.nics_count;
		copy_usage_arr(other);
	} else {
		nics_usage_arr = nullptr;
		nics_count = 0;
	}
}

ev_net_mon & ev_net_mon::operator=(const ev_net_mon & other){
	if( this == &other){
		return *this;
	}

	id = other.id;
	ts = other.ts;

	if( other.nics_count > 0){
		copy_usage_arr(other);
	}
	// otherwise it should be set to nullptr and nics_count

	return *this;
}

void ev_net_mon::copy_usage_arr(const ev_net_mon & other){
	assert(nullptr != other.nics_usage_arr);
	assert(other.nics_count == nics_count);

	nics_count = other.nics_count;
	for(int i = 0; i < other.nics_count; ++i){
		nics_usage_arr[i] = other.nics_usage_arr[i];
	}
}

void ev_net_mon::free_usage_arr(){
	delete [] nics_usage_arr;
	nics_usage_arr = nullptr;
	nics_count = 0;
}
// -----------------------------------------------
// procfs_net_fields_common
// -----------------------------------------------
Net_Acquisitor::procfs_net_fields_common::procfs_net_fields_common():
	bytes(0), packets(0), errs(0), drop(0), fifo(0), compressed(0){

}
// ----------------------------------
// procfs_net
// ----------------------------------
Net_Acquisitor::procfs_net::procfs_net(): ts(0){
}
// -----------------------------------
// Net_Acquisitor
// -----------------------------------


Net_Acquisitor::Net_Acquisitor() {
	stream_name = "/proc/net/dev";
}
/**
 * Gets the cap. The caps.id is not set in this member function.
 * @param rec (OUT) what will be set.
 * @return Diag::OK everything went fine
 *         != Diag::OK some errors
 */
Diag Net_Acquisitor::get_caps(ev_net_cap & rec){
	Diag diag = Diag::OK;

	if ( (diag = init_caps()) != Diag::OK ){
		LOG(ERROR) << "Errors with initialization";
		return diag;
	}

	// update our caps
	rec = caps;

	return diag;
}
/**
 * gets the monitoring data. The rec.id is NOT set
 *
 * @param rec (INOUT) it is assumed that rec has appropriately allocated memory
 *                    for the number of elements
 * @return
 */
Diag Net_Acquisitor::get_mon(ev_net_mon & rec){
	Diag diag = Diag::OK;
	if ( !is_mon_initialized()){
		if ( (diag = init_caps()) != Diag::OK){
			LOG(ERROR) << "Errors with initialization";
			return diag;
		}
	}

	// now I am assuming that the monitoring and caps data are
	// accordingly initialized
	if ( (diag = sample_procfs()) != Diag::OK ){
		LOG(ERROR) << "Errors with sampling the procfs";
		return diag;
	}

	if ( (diag = compute_usage()) != Diag::OK ){
		LOG(ERROR) << "The usage computation";
		return diag;
	}

	rec = mon;

	return diag;
}
/**
 * Parses the /proc to find the interfaces itemized.
 * It sets the this.caps structure (apart from identity).
 * > cat /proc/net/dev
Inter-|   Receive                                                |  Transmit
 face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
  eth0:       0       0    0    0    0     0          0         0        0       0    0    0    0     0       0          0
    lo:  581433    5697    0    0    0     0          0         0   581433    5697    0    0    0     0       0          0
 wlan0: 35947678   63622    0    0    0     0          0         0 13179801   41130    0    0    0     0       0          0
 *
 *
 * @return Diag::OK everything went fine
 *         != Diag::OK some errors
 */
Diag Net_Acquisitor::init_caps(){
	Diag diag = Diag::OK;

	if (are_caps_initialized()){
		return diag;
	}

	// we need to initialize the caps
	caps.nics_count ^= caps.nics_count; // i.e., caps.nics_count = 0

	// TODO Seems that is is safer to assume that open/close stream
	// works for /proc/net/dev to refresh the content;
	// proc_stream.seekg(0) doesn't seem to work especially when
	// trying to read the monitoring data; so I am opening and
	// closing the stream
	proc_stream.open(stream_name.c_str(), std::ifstream::in);

	// get the names of the interfaces and its number
	string line;
	while(std::getline(proc_stream, line)){
		istringstream iss(line);
		string name;
		iss >> name;

		if ( ( name.find("Inter-|") != std::string::npos) ||
			 ( name.find("face") != std::string::npos) ){
			// these are header lines; skip them
			continue;
		} else {
			// this is the interface
			net_usage iface;
			iface.nic_name = name;
			usage.emplace_back(iface);
		}
	}

	caps.ts = Time_Stamp::get_ts();
	caps.nics_count = usage.size();
	caps.nics_names = new char * [caps.nics_count];
	// now remember the names
	int i = 0;
	for( auto el : usage){
		caps.nics_names[i++] = strdup(el.nic_name.c_str());
	}

	// init monitoring
	mon.nics_count = caps.nics_count;
	mon.nics_usage_arr = new ev_nic_usage [mon.nics_count];
	i = 0;
	for( auto el :usage){
		mon.nics_usage_arr[i++].name = strdup(el.nic_name.c_str());
	}

	// TODO not sure but for some reason I need to close
	// and open the stream later for reading the monitoring data
	proc_stream.close();
	return diag;
}
/**
 * tests if the caps have been initialized or not
 * @return true yes
 *         false no
 */
bool Net_Acquisitor::are_caps_initialized(){
	return (usage.size() > 0);
}
/**
 * says if the monitoring has been initialized
 * @return true yes
 *         false no
 */
bool Net_Acquisitor::is_mon_initialized(){
	return are_caps_initialized();
}
/**
 * Samples procs to get the usage of all interfaces
 * @return
 */
Diag Net_Acquisitor::sample_procfs() {
	Diag diag = Diag::OK;

	// to see what current nic index we are operating on
	int nics_idx = 0;

	// TODO refresh our stream -- seems that it doesn't work for multiple calls
	// to this particular stream; that's why I am opening/closing
	//proc_stream.seekg(0);
	proc_stream.open(stream_name.c_str(), std::ifstream::in);

	long long int ts = Time_Stamp::get_ts();

	string line;
	while (std::getline(proc_stream, line)){
		istringstream iss(line);
		string name;
		unsigned long trans_bytes = 0;
		unsigned long recv_bytes = 0;
		unsigned long nothing = 0;

		iss >> name;

		if ((name.find("Inter-|") != std::string::npos)
				|| (name.find("face") != std::string::npos)) {
			// these are header lines; skip them
			continue;
		} else {
			// this is the interface; I assume that the
			// order is the same as during the first scan
			// eth0:1341413412341; I need the number after eth0
			// so we need to have the line without the name of eth0:
			// the bytes field for both the received and transmitted
			iss >> recv_bytes >> nothing >> nothing >> nothing >> nothing
			>> nothing >> nothing >> nothing >> trans_bytes >> nothing >> nothing
			>> nothing >> nothing >> nothing >> nothing >> nothing;
			usage[nics_idx].curr.receive.bytes = recv_bytes;
			usage[nics_idx].curr.transmit.bytes = trans_bytes;
			usage[nics_idx].curr.ts = ts;
			++nics_idx;
		}
	}

	proc_stream.close();

	return diag;
}
/**
 * computes the usage
 *
 * @return Diag::OK everything went fine
 * 		   != Diag::OK something went wrong
 */
Diag Net_Acquisitor::compute_usage(){
	Diag diag = Diag::OK;

	assert(usage.size() > 0);
	assert(mon.nics_count > 0);
	assert(nullptr != mon.nics_usage_arr);

	if( (diag = clean_usage()) != Diag::OK){
		LOG(ERROR) << "Issues with cleaning the data";
	}

	int i = 0;
	for(auto & el : usage){
		// bytes transferred
		double bytes_trsfr = 0.0;
		double bytes_recv = 0.0;
		double bytes_trans = 0.0;
		// @todo when run the very first time, I got the incorrect reading
		// like: NET 770927954 1319222932-899956 7197.17432
		//       NET 770927954 1319222933-899901 0.00179
		// right now, the just ignore the first reading in the log

		bytes_trsfr = el.curr.receive.bytes + el.curr.transmit.bytes -
				el.prev.receive.bytes - el.prev.transmit.bytes;
		bytes_recv = el.curr.receive.bytes - el.prev.receive.bytes;
		bytes_trans = el.curr.transmit.bytes - el.prev.transmit.bytes;
		long long int elapsed_time = el.curr.ts - el.prev.ts;

		if( elapsed_time < 0){
			LOG(WARNING) << "Time current is lower the the previous reading. Ignoring";
		} else {
			if (0 == elapsed_time){
				mon.nics_usage_arr[i].combined_usage = 0.0;
				mon.nics_usage_arr[i].received = 0.0;
				mon.nics_usage_arr[i].transmitted = 0.0;
			} else {
				// x = bytes_per_sec / elapsed_time * 1000000 [bytes/s]
				// x = x / 1000 [kB/s]
				// so the final formula to get the [kB/s], you should
				// x = bytes_per_sec / (curr_usec - prev_usec) * 1000 [kB/s]
				mon.nics_usage_arr[i].combined_usage = bytes_trsfr / elapsed_time *1000;
				mon.nics_usage_arr[i].received = bytes_recv / elapsed_time *1000;
				mon.nics_usage_arr[i].transmitted = bytes_trans / elapsed_time *1000;
			}
		}
		// now the current becomes the past
		el.prev = el.curr;

		++i;
	}

	// I assume that there is at least one element
	mon.ts = usage[0].curr.ts;

	return diag;
}
/**
 * cleans the usage data
 * @return Diag::OK Always
 */
Diag Net_Acquisitor::clean_usage(){
	// clean the data
	for(int i = 0; i < mon.nics_count; ++i){
		mon.nics_usage_arr[i].combined_usage = 0.0;
		mon.nics_usage_arr[i].received = 0.0;
		mon.nics_usage_arr[i].transmitted = 0.0;
	}
	return Diag::OK;
}
// ----------------------------------------
// Net_Spy
// ----------------------------------------
Net_Spy::~Net_Spy(){
	delete []  mon.nics_usage_arr;
	mon.nics_usage_arr = nullptr;
}
/**
 * Does nothing special apart from getting the identity that is stored in
 * Spy::id
 * @return
 */
Diag Net_Spy::setup_for_read(){
	Diag diag = Diag::OK;

	diag = Spy::setup_for_read();
	if( Diag::OK != diag ){
		return diag;
	}

	if( (diag = acq.get_caps(caps)) != Diag::OK){
		LOG(ERROR) << "Can't read the capabilities";
		return diag;
	}

	caps.id = id;

	// should be deallocated in destructor
	mon.nics_count = caps.nics_count;
	mon.nics_usage_arr = new ev_nic_usage[mon.nics_count];

	return diag;
}
/**
 * Before calling this function setup_for_read() should be called
 * @param rec
 * @return
 */
Diag Net_Spy::read_mon(){
	Diag diag = Diag::OK;


	if ( ( diag = acq.get_mon(mon)) != Diag::OK){
		LOG(ERROR) << "Can't read the monitoring";
		return diag;
	}
	// the id is not set by acq::get_mon()
	mon.id = caps.id.id;

	return diag;
}
/**
 * getter for caps
 * @return
 */
ev_net_cap * Net_Spy::get_caps(){
	return &caps;
}
/**
 * getter for the monitoring record
 * @return
 */
ev_net_mon * Net_Spy::get_mon(){
	return &mon;
}

Diag Net_Spy::clean_after_read() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Net_Spy::setup_for_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Net_Spy::process(const ev_net_cap& rec) {
	Diag diag = Diag::OK;

	cout << "NET:" << rec << "\n";

	return diag;
}

Diag Net_Spy::process(const ev_net_mon& rec) {
	Diag diag = Diag::OK;

	cout << "NET:" << rec << "\n";

	return diag;
}

Diag Net_Spy::clean_after_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Net_Spy::set_caps_ts(long long int ts) {
	caps.ts = ts;
	return Diag::OK;
}

Diag Net_Spy::set_mon_ts(long long int ts) {
	mon.ts = ts;
	return Diag::OK;
}


// -----------------------------------
// Ev_Net_Cap_Msg
// -----------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Net_Cap_Msg * Ev_Net_Cap_Msg::p_msg = nullptr;

/**
 * singleton
 *
 * @return
 */
Ev_Net_Cap_Msg* Ev_Net_Cap_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Net_Cap_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Net_Cap_Msg::exists() {
	return (nullptr != p_msg);
}

/**
 * Registers formats
 *
 * @return
 */
Diag Ev_Net_Cap_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = net_cap_field_list;
	format_list = net_cap_format_list;

	return diag;
}

// --------------------------------
// Ev_Net_Mon_Msg
// --------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Net_Mon_Msg * Ev_Net_Mon_Msg::p_msg = nullptr;

/**
 * singleton
 * @return
 */
Ev_Net_Mon_Msg* Ev_Net_Mon_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Net_Mon_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Net_Mon_Msg::exists() {
	return (nullptr != p_msg);
}

Diag Ev_Net_Mon_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = net_mon_field_list;
	format_list = net_mon_format_list;

	return diag;
}

} // namespace
