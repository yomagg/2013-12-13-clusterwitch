/**
 *  @file   nvml_spy.cpp
 *
 *  @date   Created on: Feb 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iomanip> // for changing precision
#include <cmath>

// TODO
// #include <nvml.h>
// a hack for the old nvml.h header; I assume that it should
// be compatible with available libnvidia-ml
#include "my-nvml.h"

#include "easylogging++.h"

#include "nvml_spy.h"
#include "spy.h"
#include "nvml_spy_events_desc.h"
#include "misc.h"
#include "cw_runtime.h"


namespace cw {
using namespace std;

// TODO this probably should be an inline function or something more i
// in the spirit of avoiding macros, anyway life is life
#define CHECK_IF_NVML_SUPPORTED LOG(WARNING) << nvmlErrorString(nvml_diag); \
			if(NVML_ERROR_NOT_SUPPORTED == nvml_diag){ \
				diag = Diag::NVML_UNSUPPORTED; \
			} else { \
				return Diag::NVML_ERR;}


// ----------------------------------------
// ev_nvml_dev_cap
// ----------------------------------------
std::ostream & operator<<(std::ostream & os, const ev_nvml_dev_cap& rec){
	os << "ID:NAME:SERIAL:COMP_MODE:MEM_TOT_BYTES:POWR_LMIT_W:MAX_GRAPHICS_CL_MHZ:MAX_SM_CL_MHZ:MAX_MEM_CL_MHZ " << rec.id << ":"
	   << rec.product_name << ":" << rec.serial_number << ":" << rec.compute_mode
	   << ":" << rec.mem_total_bytes << ":" << rec.power_limit_mW
	   << ":" << rec.max_graphics_clock_MHz << ":" << rec.max_SM_clock_MHz
	   << ":" << rec.max_memory_clock_MHz ;

	return os;
}
ev_nvml_dev_cap::ev_nvml_dev_cap() : product_name(nullptr), serial_number(nullptr),
		compute_mode(-1), id(0), mem_total_bytes(0), power_limit_mW(0),
		max_graphics_clock_MHz(0), max_SM_clock_MHz(0), max_memory_clock_MHz(0){
}

ev_nvml_dev_cap::~ev_nvml_dev_cap(){
}

ev_nvml_dev_cap::ev_nvml_dev_cap(const ev_nvml_dev_cap & other) : ev_nvml_dev_cap() {
	my_copy(other);
}

ev_nvml_dev_cap & ev_nvml_dev_cap::operator=(const ev_nvml_dev_cap & other){
	my_copy(other);
	return *this;
}

void ev_nvml_dev_cap::my_copy(const ev_nvml_dev_cap & other){
	product_name = copy_str(other.product_name);
	serial_number = copy_str(other.serial_number);
	compute_mode = other.compute_mode;
	id = other.id;
	mem_total_bytes = other.mem_total_bytes;
	power_limit_mW = other.power_limit_mW;
	max_graphics_clock_MHz = other.max_graphics_clock_MHz;
	max_SM_clock_MHz = other.max_SM_clock_MHz;
	max_memory_clock_MHz = other.max_memory_clock_MHz;
}
/**
 * Since this is a structure with public field members it is difficult to
 * control how strings are allocated. The user is advised to use
 * this method to assign string to a character field in this structure
 * and the corresponding free_str() to free the string. Otherwise
 * the user is on his own.
 * @param src The string that should be copied
 * @return the pointer to the newly allocated place or nullptr if
 *         src is nullptr
 */
char * ev_nvml_dev_cap::copy_str(const char *src){
	char * dst = nullptr;
	if(src){
		dst = new char [strlen(src) + 1];
		strcpy(dst, src);
	}
	return dst;
}
/**
 * this corresponds to copy_str()
 * @param dst which member of the structure should be freed
 */
void ev_nvml_dev_cap::free_str(char *dst){
	delete [] dst;
	dst = nullptr;
}
// ----------------------------------------
// ev_nvml_cap
// ----------------------------------------
std::ostream & operator<<(std::ostream & os, const ev_nvml_cap& rec){

	for(unsigned int i = 0 ; i < rec.gpu_count; ++i){
		os << "TS:ID:HOST:PID:DRIVER_VER:GPU_COUNT " << rec.ts << ":"
			   << rec.id.id << ":" << rec.id.hostname << ":" << rec.id.pid << ":"
			   << rec.driver_ver << ":" << rec.gpu_count << ":"
			   << rec.devs[i] << "\n";
	}

	return os;
}
ev_nvml_cap::ev_nvml_cap() : id(identity()), ts(0),
		driver_ver(nullptr), gpu_count(0), devs(nullptr){

}

ev_nvml_cap::ev_nvml_cap(const ev_nvml_cap &other) : ev_nvml_cap(){
	my_copy(other);
}

ev_nvml_cap & ev_nvml_cap::operator=(const ev_nvml_cap & other){
	my_copy(other);
	return *this;
}
/**
 * It is assumed that driver_ver and devs are set to nullptr
 * If not the my_copy only will do the partial copy of field members
 * that are simply such as int; the pointers will be not allocated
 * @return 0 full copy done
 *         1 some errors, i.e., partial copy
 */
int ev_nvml_cap::my_copy(const ev_nvml_cap & other){
	int res = 0;

	id = other.id;
	ts = other.ts;
	gpu_count = other.gpu_count;

	if( driver_ver ){
		LOG(WARNING) << "driver_ver NOT COPIED. Should be nullptr to enable copying";
		res = 1;
	} else {
		if(other.driver_ver){
			driver_ver = new char[strlen(other.driver_ver) + 1];
			strcpy(driver_ver, other.driver_ver);
		}
	}
	if (devs){
		LOG(WARNING) << "devs first should be released.";
		res = 1;
	} else {
		if(other.gpu_count > 0 && other.devs ){
			devs = new ev_nvml_dev_cap[other.gpu_count];
			for(unsigned int i = 0; i < gpu_count; ++i){
				devs[i] = other.devs[i];
			}
		}else {
			LOG(WARNING) << "other.devs or other.gpu_count nullptr or not positive";
			res = 1;
		}
	}
	return res;
}
/**
 * Assumes taht the driver_ver was allocated with new char [length]
 * and devs where allocated with new ev_nvml_dev_cap[length]
 */
void ev_nvml_cap::my_free(){
	delete [] driver_ver;
	driver_ver = nullptr;

	delete [] devs;
	devs = nullptr;
}
// ----------------------------------------
// ev_nvml_dev_mon
// ----------------------------------------
std::ostream & operator<<(std::ostream & os, const ev_nvml_dev_mon& rec){
	os << "PERF_STATE:MEM_USED_BYTES:NVML_UTIL_GPU_PERC:NVML_UTIL_MEM_PERC:MEM_UTIL:POWR_DRAW:GRAPHICS_CL:SM_CL:MEM_CL "
		<< rec.performance_state << ":" << rec.mem_used_bytes << ":"
		<< rec.nvml_util_gpu << ":" << rec.nvml_util_mem << ":" << rec.util_mem << ":" << rec.power_draw
		<< ":" << rec.graphics_clock << ":" << rec.sm_clock
		<< ":" << rec.mem_clock;

	return os;
}

ev_nvml_dev_mon::ev_nvml_dev_mon() : performance_state(0),
		mem_used_bytes(0), nvml_util_gpu(0), nvml_util_mem(0), util_mem(0.0), power_draw(0.0), graphics_clock(0.0),
		sm_clock(0.0), mem_clock(0.0){
}
// ----------------------------------------
// ev_nvml_mon
// ----------------------------------------
std::ostream & operator<<(std::ostream & os, const ev_nvml_mon& rec){

	for(unsigned int i = 0 ; i < rec.gpu_count; ++i){
		os << "TS:ID:GPU_COUNT:" << rec.ts << ":" << i << ":"
			   << rec.gpu_count << ":" << rec.gpu_arr[i] << "\n";
	}

	return os;
}

ev_nvml_mon::ev_nvml_mon() : id(0), ts(0), gpu_count(0), gpu_arr(nullptr){
}

ev_nvml_mon::ev_nvml_mon(const ev_nvml_mon & other){
	my_copy(other);
}

ev_nvml_mon::~ev_nvml_mon(){
	my_free();
}

ev_nvml_mon & ev_nvml_mon::operator=(const ev_nvml_mon & other){
	my_copy(other);

	return *this;
}

void ev_nvml_mon::my_copy(const ev_nvml_mon & other){
	if( gpu_count != other.gpu_count){
		my_free();
		gpu_count = other.gpu_count;
		gpu_arr = new ev_nvml_dev_mon[gpu_count];
	}
	id = other.id;
	ts = other.ts;

	assert(other.gpu_count == gpu_count);
	for(unsigned int i = 0; i < gpu_count; ++i){
		gpu_arr[i] = other.gpu_arr[i];
	}
}

void ev_nvml_mon::my_free(){
	delete [] gpu_arr;
	gpu_arr = nullptr;
}
// ----------------------------------------
// Nvml_Acquisitor
// ---------------------------------------
const std::string Nvml_Acquisitor::UNSPPRTED_SERIAL_NO = "000000";
const unsigned int Nvml_Acquisitor::UNSPPRTED_POWER_LIMIT = 1;
const unsigned int Nvml_Acquisitor::UNSPPRTED_MAX_CLOCK = 1;


Nvml_Acquisitor::~Nvml_Acquisitor(){
	delete [] mon.gpu_arr;
	mon.gpu_arr = nullptr;
}

Diag Nvml_Acquisitor::get_caps(ev_nvml_cap &rec){
	Diag diag = Diag::OK;

	if ( ( diag = init_caps() )!= Diag::OK){
		LOG(ERROR) << "Errors with initialization";
		return diag;
	}

	// return the record
	rec = caps;

	return diag;
}

Diag Nvml_Acquisitor::get_mon(ev_nvml_mon & rec){
	Diag diag = Diag::OK;

	if ( !is_mon_initialized()){
		if( (diag = init_caps()) != Diag::OK){
			LOG(ERROR) << "Errors with initialization";
			return diag;
		}
	}

	assert(mon.gpu_count > 0 );
	assert(nullptr != mon.gpu_arr);
	assert(nullptr != caps.devs);
	assert(caps.gpu_count == mon.gpu_count);
	// init this with nvmlInit() and
	// 	// shutdown with nvmlShutdown() at some point
	nvmlReturn_t nvml_diag = NVML_SUCCESS;

	if ((nvml_diag = nvmlInit()) != NVML_SUCCESS ) {
		LOG(ERROR) << nvmlErrorString(nvml_diag);
		return Diag::ERR;
    }


	mon.ts = Time_Stamp::get_ts();

	for( unsigned int i = 0; i < mon.gpu_count; ++i){
		nvmlDevice_t p_device = nullptr;
		nvmlReturn_t nvml_diag = NVML_SUCCESS;

		if ( (nvml_diag = nvmlDeviceGetHandleByIndex(i, &p_device)) != NVML_SUCCESS){
			LOG(ERROR) << nvmlErrorString(nvml_diag);
			return Diag::NVML_ERR;
		}

		ev_nvml_dev_mon dev_mon;

		nvmlPstates_t pstate = NVML_PSTATE_UNKNOWN;
		if ( (nvml_diag = nvmlDeviceGetPerformanceState(p_device, &pstate)) != NVML_SUCCESS){
            CHECK_IF_NVML_SUPPORTED;
            pstate = NVML_PSTATE_UNKNOWN;
		}
		dev_mon.performance_state = pstate;


		nvmlMemory_t mem = {0, 0, 0};
		if ( (nvml_diag = nvmlDeviceGetMemoryInfo(p_device, &mem)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			mem.used = 1;
			mem.total =1;
			mem.free = 0;
		}
		dev_mon.mem_used_bytes = mem.used;
		assert(mem.total);
		dev_mon.util_mem = mem.used / mem.total * 100.0;


		nvmlUtilization_t util_rates = {0, 0};
		if ( (nvml_diag = nvmlDeviceGetUtilizationRates(p_device, &util_rates)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			util_rates.gpu = 0;
			util_rates.memory = 0;
		}
		// Percentage of time over the past second during which one or more kernels was executing on the GPU
		dev_mon.nvml_util_gpu = util_rates.gpu;
		// you might want to use util_rate.memory which is a
		// percent of time over the past second during which global (device) memory was being read or written.
		dev_mon.nvml_util_mem = util_rates.memory;


		// On Fermi and Kepler GPUs the reading is accurate to within +/- 5% of current power draw.
		// Retrieve power usage for this GPU in milliwatts and its associated circuitry (e.g. memory)
		unsigned int power_mW;
		if ( (nvml_diag = nvmlDeviceGetPowerUsage(p_device, &power_mW)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			dev_mon.power_draw = 0.0;
		} else {
			dev_mon.power_draw = power_mW  / caps.devs[i].power_limit_mW * 100.0;
		}

		unsigned int clock_MHz;
		if ( (nvml_diag = nvmlDeviceGetClockInfo(p_device, NVML_CLOCK_GRAPHICS , &clock_MHz)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			dev_mon.graphics_clock = UNSPPRTED_MAX_CLOCK;
		} else {
			dev_mon.graphics_clock = clock_MHz / caps.devs[i].max_graphics_clock_MHz * 100.0;
		}
		if ( (nvml_diag = nvmlDeviceGetClockInfo(p_device, NVML_CLOCK_SM , &clock_MHz)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			dev_mon.sm_clock = UNSPPRTED_MAX_CLOCK;
		} else {
			dev_mon.sm_clock = clock_MHz / caps.devs[i].max_SM_clock_MHz * 100.0;
		}
		if ( (nvml_diag = nvmlDeviceGetClockInfo(p_device, NVML_CLOCK_MEM , &clock_MHz)) != NVML_SUCCESS){
			CHECK_IF_NVML_SUPPORTED;
			dev_mon.mem_clock = UNSPPRTED_MAX_CLOCK;
		} else {
			dev_mon.mem_clock = clock_MHz / caps.devs[i].max_memory_clock_MHz * 100.0;
		}

		// copy the result to the array for a single device
		mon.gpu_arr[i] = dev_mon;
	}

	// udpate the entire record
	rec = mon;

	return diag;
}


/**
 * Initializes the capabilities; checks if they have been already initialized
 *
 * TODO it should be cleaned with the handling of the errors, as the
 *      implicit assumption is that all nvml calls end with success
 *      so no need to call nvmlShutdown() unless at the very end. But in
 *      general this assumption is wrong.
 *
 * @return Diag::OK if everything went ok
 *         != Diag::OK if something went wrong
 */
Diag Nvml_Acquisitor::init_caps(){
	Diag diag = Diag::OK;

	if (are_caps_initialized()){
		return diag;
	}

	// init this with nvmlInit() and
	// shutdown with nvmlShutdown() at some point
	nvmlReturn_t nvml_diag = NVML_SUCCESS;

	if ((nvml_diag = nvmlInit()) != NVML_SUCCESS ) {
		LOG(ERROR) << nvmlErrorString(nvml_diag);
		return Diag::ERR;
	}

	// get timestamp
	caps.ts = Time_Stamp::get_ts();

	// get the capabitlities
	if ( (nvml_diag = nvmlDeviceGetCount(&caps.gpu_count)) != NVML_SUCCESS){
		LOG(ERROR) << nvmlErrorString(nvml_diag);
		return Diag::ERR;
	}
	if( caps.gpu_count <= 0) {
		LOG(ERROR) << "The number of gpu_count is =< 0.";
		return Diag::ERR;
	}

	char str[NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE];

	if ( (nvml_diag = nvmlSystemGetDriverVersion(str,  NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE)) != NVML_SUCCESS){
		LOG(ERROR) << nvmlErrorString(nvml_diag);
		return Diag::ERR;
	} else {
		caps.driver_ver = strdup(str);
	}

	caps.devs = new ev_nvml_dev_cap[caps.gpu_count];

	// now read the device capabilities
	for(unsigned int i = 0; i < caps.gpu_count; ++i){
		nvmlDevice_t p_device = nullptr;
		ev_nvml_dev_cap cap;

		if ( (nvml_diag = nvmlDeviceGetHandleByIndex(i, &p_device)) != NVML_SUCCESS){
			LOG(ERROR) << nvmlErrorString(nvml_diag);
			return Diag::ERR;
		}

		cap.id = i;
		char str [NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE];

		if ( (nvml_diag = nvmlDeviceGetName( p_device, str, NVML_SYSTEM_DRIVER_VERSION_BUFFER_SIZE )) != NVML_SUCCESS){
			LOG(ERROR) << nvmlErrorString(nvml_diag);
			return Diag::ERR;
		}
		// FIXME a quick fix for spaces in product_name; if there are spaces
		// then the reader reads it as separates fields as it treats spaces
		// as delimeters; so change them to underscores
		int k = 0;
		while( str[k] != '\0'){
			if (' ' == str[k])
				str[k] = '_';
			k++;
		}

		cap.product_name = strdup(str);

		char str2[NVML_DEVICE_SERIAL_BUFFER_SIZE];
		if ( (nvml_diag = nvmlDeviceGetSerial( p_device, str2,  NVML_DEVICE_SERIAL_BUFFER_SIZE )) != NVML_SUCCESS){
			if (NVML_ERROR_NOT_SUPPORTED == nvml_diag){
				cap.serial_number = strdup(UNSPPRTED_SERIAL_NO.c_str());
				LOG(WARNING) << "Assigning dummy value to SERIAL_NUMBER: " << UNSPPRTED_SERIAL_NO;
			} else {
				LOG(ERROR) << nvmlErrorString(nvml_diag);
				return Diag::ERR;
			}
		} else {
			cap.serial_number = strdup(str2);
		}

		nvmlComputeMode_t comp_mode;
		if ( (nvml_diag = nvmlDeviceGetComputeMode( p_device, &comp_mode )) != NVML_SUCCESS){
			LOG(ERROR) << nvmlErrorString(nvml_diag);
			return Diag::ERR;
		}
		cap.compute_mode = comp_mode;

		nvmlMemory_t mem;
		if ( (nvml_diag = nvmlDeviceGetMemoryInfo( p_device, &mem )) != NVML_SUCCESS){
			LOG(ERROR) << nvmlErrorString(nvml_diag);
			return Diag::ERR;
		} else {
			cap.mem_total_bytes = mem.total;
		}

		if ( (nvml_diag = nvmlDeviceGetPowerManagementLimit( p_device, &cap.power_limit_mW)) != NVML_SUCCESS){
			// TODO a temporary hack to workaround not supported features
			// it would be nice to address some other nvml calls if the feature
			// is not supported
			if( NVML_ERROR_NOT_SUPPORTED == nvml_diag){
				cap.power_limit_mW = UNSPPRTED_POWER_LIMIT;
				LOG(WARNING) << "Assigning dummy value to DEVICE POWER MGMT LIMIT: " << cap.power_limit_mW;
			} else {
				LOG(ERROR) << nvmlErrorString(nvml_diag);
				return Diag::ERR;
			}
		}

		// clocks returned in MHz
		if ( (nvml_diag = nvmlDeviceGetMaxClockInfo( p_device, NVML_CLOCK_GRAPHICS, &cap.max_graphics_clock_MHz )) != NVML_SUCCESS){
			if (NVML_ERROR_NOT_SUPPORTED == nvml_diag){
				cap.max_graphics_clock_MHz = UNSPPRTED_MAX_CLOCK;
				LOG(WARNING) << "Assigning dummy value to MAX_GRAPHICS_CLOCK: " << cap.max_graphics_clock_MHz;
			} else {
				LOG(ERROR) << nvmlErrorString(nvml_diag);
				return Diag::ERR;
			}
		}

		// clocks returned in MHz
		if ( (nvml_diag = nvmlDeviceGetMaxClockInfo( p_device, NVML_CLOCK_GRAPHICS, &cap.max_SM_clock_MHz )) != NVML_SUCCESS){
			if (NVML_ERROR_NOT_SUPPORTED == nvml_diag){
				cap.max_SM_clock_MHz = UNSPPRTED_MAX_CLOCK;
				LOG(WARNING) << "Assigning dummy value to MAX_SM_CLOCK: " << cap.max_SM_clock_MHz;
			} else {
				LOG(ERROR) << nvmlErrorString(nvml_diag);
				return Diag::ERR;
			}
		}

		// clocks returned in MHz
		if ( (nvml_diag = nvmlDeviceGetMaxClockInfo( p_device, NVML_CLOCK_GRAPHICS, &cap.max_memory_clock_MHz )) != NVML_SUCCESS){
			if (NVML_ERROR_NOT_SUPPORTED == nvml_diag){
				cap.max_memory_clock_MHz = UNSPPRTED_MAX_CLOCK;
				LOG(WARNING) << "Assigning dummy value to MAX_SM_CLOCK: " << cap.max_memory_clock_MHz;
			} else {
				LOG(ERROR) << nvmlErrorString(nvml_diag);
				return Diag::ERR;
			}
		}

		// copy the cap
		caps.devs[i] = cap;
	}

	if ((nvml_diag = nvmlShutdown()) != NVML_SUCCESS ) {
		LOG(ERROR) <<  nvmlErrorString(nvml_diag);
		return Diag::ERR;
	}

	// init monitoring
	mon.gpu_count = caps.gpu_count;
	mon.gpu_arr = new ev_nvml_dev_mon[mon.gpu_count];


	return diag;
}
/**
 * tests if the caps have been initialized or not
 * @return true yes
 *         false no
 */
bool Nvml_Acquisitor::are_caps_initialized(){
	return (nullptr != caps.devs);
}
/**
 * checks if the monitoring is
 * @return
 */
bool Nvml_Acquisitor::is_mon_initialized(){
	return (nullptr != mon.gpu_arr);
}


// ----------------------------------------
// Nvml_Spy
// ----------------------------------------
/**
 * Does nothing special apart from getting the identity that is stored in
 * Spy::id
 * @return
 */
Diag Nvml_Spy::setup_for_read(){
	Diag diag = Diag::OK;

	diag = Spy::setup_for_read();
	if( Diag::OK != diag ){
		return diag;
	}

	if( (diag = acq.get_caps(caps)) != Diag::OK){
		LOG(ERROR) << "Can't read the capabilities";
		return diag;
	}

	caps.id = id;

	return diag;
}
/**
 * Before calling this function setup_for_read() should be called
 * @param rec
 * @return
 */
Diag Nvml_Spy::read_mon(){
	Diag diag = Diag::OK;

	if ( ( diag = acq.get_mon(mon)) != Diag::OK){
		if( Diag::NVML_UNSUPPORTED == diag){
			LOG(WARNING) << "Got NVML unsupported error. Some values will be default. Ignoring.";
		} else {
			LOG(ERROR) << "Can't read the monitoring record";
			return diag;
		}
	}

	// the id is not set by acq::get_mon(); actually this should set
	// once since this is invariant
	mon.id = caps.id.id;

	return diag;
}

/**
 * Getter for caps
 * @return
 */
ev_nvml_cap * Nvml_Spy::get_caps(){
	return &caps;
}

/**
 * getter for mon record
 * @return
 */
ev_nvml_mon * Nvml_Spy::get_mon(){
	return &mon;
}

Diag Nvml_Spy::clean_after_read() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Nvml_Spy::setup_for_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Nvml_Spy::process(const ev_nvml_cap& rec) {
	Diag diag = Diag::OK;

	cout << "NVML:" << rec << "\n";

	return diag;
}

Diag Nvml_Spy::process(const ev_nvml_mon& rec) {
	Diag diag = Diag::OK;

	cout << "NVML:" << rec << "\n";

	return diag;
}

Diag Nvml_Spy::clean_after_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Nvml_Spy::set_caps_ts(long long int ts) {
	caps.ts = ts;
	return Diag::OK;
}

Diag Nvml_Spy::set_mon_ts(long long int ts) {
	mon.ts = ts;
	return Diag::OK;
}
// -----------------------------------
// Ev_Nvml_Dev_Cap_Msg
// -----------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Nvml_Dev_Cap_Msg * Ev_Nvml_Dev_Cap_Msg::p_msg = nullptr;

/**
 * singleton
 *
 * @return
 */
Ev_Nvml_Dev_Cap_Msg* Ev_Nvml_Dev_Cap_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Nvml_Dev_Cap_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Nvml_Dev_Cap_Msg::exists() {
	return (nullptr != p_msg);
}

/**
 * Registers formats
 *
 * @return
 */
Diag Ev_Nvml_Dev_Cap_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = nvml_dev_cap_field_list;
	format_list = nvml_dev_cap_format_list;

	return diag;
}


// -----------------------------------
// Ev_Nvml_Cap_Msg
// -----------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Nvml_Cap_Msg * Ev_Nvml_Cap_Msg::p_msg = nullptr;

/**
 * singleton
 *
 * @return
 */
Ev_Nvml_Cap_Msg* Ev_Nvml_Cap_Msg::get_instance() {

	if (!exists()){
		// be sure that the device capabilities format is registered
		Ev_Nvml_Dev_Cap_Msg *p_dev_cap_msg = Ev_Nvml_Dev_Cap_Msg::get_instance();
		assert(p_dev_cap_msg);

		p_msg = new Ev_Nvml_Cap_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Nvml_Cap_Msg::exists() {
	return (nullptr != p_msg);
}

/**
 * Registers formats
 *
 * @return
 */
Diag Ev_Nvml_Cap_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = nvml_cap_field_list;
	format_list = nvml_cap_format_list;

	return diag;
}
// --------------------------------
// Ev_Nvml_Mon_Dev_Msg
// --------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Nvml_Mon_Dev_Msg * Ev_Nvml_Mon_Dev_Msg::p_msg = nullptr;

/**
 * singleton
 * @return
 */
Ev_Nvml_Mon_Dev_Msg* Ev_Nvml_Mon_Dev_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Nvml_Mon_Dev_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Nvml_Mon_Dev_Msg::exists() {
	return (nullptr != p_msg);
}

Diag Ev_Nvml_Mon_Dev_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = nvml_mon_dev_field_list;
	format_list = nvml_mon_dev_format_list;

	return diag;
}


// --------------------------------
// Ev_Nvml_Mon_Msg
// --------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Nvml_Mon_Msg * Ev_Nvml_Mon_Msg::p_msg = nullptr;

/**
 * singleton
 * @return
 */
Ev_Nvml_Mon_Msg* Ev_Nvml_Mon_Msg::get_instance() {
	if (!exists()){
		// be sure that the device capabilities format is registered
		Ev_Nvml_Mon_Dev_Msg *p_mon_dev_msg = Ev_Nvml_Mon_Dev_Msg::get_instance();
		assert(p_mon_dev_msg);

		p_msg = new Ev_Nvml_Mon_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Nvml_Mon_Msg::exists() {
	return (nullptr != p_msg);
}

Diag Ev_Nvml_Mon_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = nvml_mon_field_list;
	format_list = nvml_mon_format_list;

	return diag;
}

} // end cw

