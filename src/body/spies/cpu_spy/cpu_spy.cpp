/**
 *  @file   cpu_spy.cpp
 *
 *  @date   Created on: Dec 18, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cmath>
#include <unistd.h>

#include "cpu_spy.h"
#include "easylogging++.h"

#include "misc.h"
#include "cw_runtime.h"
#include "cpu_spy_events_desc.h"

namespace cw {

using namespace std;

// -----------------------
// ev_cpu_cap
// -----------------------
std::ostream& operator<<(std::ostream& os, const ev_cpu_cap& caps){
	// write obj to stream
	os << "TS:ID:HOST:PID:CORE_COUNT " << caps.ts << ":" << caps.id.id << ":" <<
			caps.id.hostname << ":" << caps.id.pid << ":"
			<< caps.core_count;
	return os;
}
ev_cpu_cap::ev_cpu_cap():id(identity()), ts(0), core_count(0) {
}

ev_cpu_cap::ev_cpu_cap(const ev_cpu_cap & other) :
		 id(other.id), ts(other.ts), core_count(other.ts){
}

ev_cpu_cap & ev_cpu_cap::operator=(const ev_cpu_cap & other){
	ts = other.ts;
	id = other.id;
	core_count = other.core_count;

	return *this;
}
// -------------------------
// ev_cpu_mon
// ------------------------
std::ostream& operator<<(std::ostream& os, const ev_cpu_mon& mon){

	os << "TS:ID:CORE_COUNT:UTIL_AGG:UTIL_0:UTIL_1 " <<
			mon.ts << ":" << mon.id << ":" <<
			mon.core_plus_one_count - 1 ;

	for(int i = 0; i < mon.core_plus_one_count; ++i){
		os << ":" << mon.cpu_and_core_usage[i];
	}

	return os;
}
ev_cpu_mon::ev_cpu_mon():id(0), ts(0), core_plus_one_count(0), cpu_and_core_usage(nullptr) {
}

ev_cpu_mon::ev_cpu_mon(const ev_cpu_mon & other) :
		 id(other.id), ts(other.ts), core_plus_one_count(other.core_plus_one_count){
	if (other.cpu_and_core_usage){
		cpu_and_core_usage = new float[core_plus_one_count];
		copy_usage_arr(other);
	} else {
		cpu_and_core_usage = nullptr;
		core_plus_one_count = 0;
	}
}

ev_cpu_mon & ev_cpu_mon::operator=(const ev_cpu_mon & other){
	ts = other.ts;
	id = other.id;
	core_plus_one_count = other.core_plus_one_count;
	if (other.cpu_and_core_usage){
		copy_usage_arr(other);
	} else {
		cpu_and_core_usage = nullptr;
		core_plus_one_count = 0;
	}
	return *this;
}

void ev_cpu_mon::copy_usage_arr(const ev_cpu_mon & other){
	assert(other.cpu_and_core_usage);
	assert(cpu_and_core_usage);
	assert(other.core_plus_one_count == core_plus_one_count);

	for(int i = 0; i < other.core_plus_one_count; ++i){
		cpu_and_core_usage[i] = other.cpu_and_core_usage[i];
	}
}
// ------------------------------------
// Acquisitor
// -----------------------------------
Cpu_Acquisitor::Cpu_Acquisitor() :  ts_curr(0), ts_prev(0){
	stream_name = "/proc/stat";
}

Cpu_Acquisitor::~Cpu_Acquisitor(){
	delete [] mon.cpu_and_core_usage;
	mon.cpu_and_core_usage = nullptr;
}

/**
 * Gets the cap. The id is not set
 *
 * @param rec
 * @return Diag::OK everything went fine
 *         != Diag::OK some errors
 */
Diag Cpu_Acquisitor::get(ev_cpu_cap & rec){
	Diag diag = Diag::OK;

	// get timestamp
	rec.ts = Time_Stamp::get_ts();
	rec.core_count = get_core_count();

	return diag;
}
/**
 * Gets the monitoring record
 *
 * @param rec
 * @return
 */
Diag Cpu_Acquisitor::get(ev_cpu_mon & rec){
	Diag diag = Diag::OK;

	if(!is_initialized()){
		if( (diag = init()) != Diag::OK ){
			LOG(ERROR) << "Issues with acquisitor initialization. ATS";
			return diag;
		}
	}

	if( (diag = init_caps()) != Diag::OK){
		LOG(ERROR) << "Issues with the initialization of the caps";
		return diag;
	}

	// now everything should be initialized, so go get the mon data

	if ( (diag = sample_procfs()) != Diag::OK ){
		LOG(ERROR) << "Errors with sampling the procfs";
		return diag;
	}

	if ( (diag = compute_usage()) != Diag::OK ){
		LOG(ERROR) << "The usage computation";
		return diag;
	}

	rec = mon;

	return diag;
}
/**
 * Initializes the capabilities
 * @return
 */
Diag Cpu_Acquisitor::init_caps(){
	Diag diag = Diag::OK;

	if (are_caps_initialized()){
		return diag;
	}
	caps.core_count = get_core_count();
	caps.ts = Time_Stamp::get_ts();

	usage.resize(caps.core_count + 1);

	curr.resize(caps.core_count + 1);
	prev.resize(caps.core_count + 1);
	mon.cpu_and_core_usage = new float[caps.core_count + 1];
	mon.core_plus_one_count = caps.core_count + 1;

	LOG(DEBUG) << "Vectors resized to " <<  usage.size();


	return diag;
}
/**
 * tests if the caps have been initialized or not
 * @return true yes
 *         false no
 */
bool Cpu_Acquisitor::are_caps_initialized(){
	return (usage.size() > 0);
}
/**
 * says if the monitoring has been initialized
 * @return true yes
 *         false no
 */
bool Cpu_Acquisitor::is_mon_initialized(){
	return are_caps_initialized();
}
/**
 * returns the total number of cores
 * @return the number of cores detected on the system
 */
int Cpu_Acquisitor::get_core_count(void) {
	return sysconf(_SC_NPROCESSORS_ONLN);
}

/**
 * Samples procs to get the usage of all interfaces
 * @return
 */
Diag Cpu_Acquisitor::sample_procfs() {
	Diag diag = Diag::OK;

	// refresh the /proc/stat
	proc_stream.seekg(0);

	// first line is the cpu as an entire cpu
	// subsequent lines are cores: cpu0, cpu1, cpu2, ...
	string line;

	// this is for skipping the name of the core
	string dummy_str;

	memset(&curr[0], 0, curr.size() * sizeof(curr[0]));

	ts_curr = Time_Stamp::get_ts();

	for (auto & cpu : curr) {
		if (!getline(proc_stream, line)) {
			LOG(ERROR)<< "Problem with reading from /proc/stat";
			diag = Diag::ERR;
			break;
		}

		istringstream iss(line);
		if( !(iss >> dummy_str >> cpu.user >> cpu.nice >> cpu.system >>
						cpu.idle >> cpu.iowait >> cpu.irq >> cpu.softirq >> cpu.steal)) {
			LOG(ERROR) << "Problem with reading an input file";
			diag = Diag::ERR;
			break;
		}
	}

	return diag;
}
/**
 * computes the cpu count usage
 *
 * count the usage for an entire cpu and for each core
 * the idle time is a combined time idle and iowait (maybe this assumption
 * is wrong but the cpu is waiting for io data during iowait, right?
 *
 * @param usage_vec
 * @return Diag::OK if everything was ok, otherwise != Diag::OK
 */
Diag Cpu_Acquisitor::compute_usage(){
	Diag diag = Diag::OK;

	long delta_total_jiffs = 0;
	long delta_busy_jiffs = 0;

	// count busy jiffs between two readings
	for (int i = 0; i < caps.core_count + 1; ++i) {
		delta_busy_jiffs = curr[i].user - prev[i].user + curr[i].nice
				- prev[i].nice + curr[i].system - prev[i].system;

		delta_total_jiffs = delta_busy_jiffs + curr[i].idle - prev[i].idle
				+ curr[i].iowait - prev[i].iowait + curr[i].irq - prev[i].irq
				+ curr[i].softirq - prev[i].softirq + curr[i].steal
				- prev[i].steal;

		// count the usage as a percentage
		if (delta_total_jiffs) {
			usage[i] = ((static_cast<float>(delta_busy_jiffs) * 100.0)
					/ (delta_total_jiffs));
		} else {
			usage[i] = 0.0;
		}
		if (isnan(usage[i]) != 0) {
			usage[i] = CW_Runtime::IN_CASE_NAN;
			LOG(WARNING)<< "A Not A Number identified: delta_busy_jiffs =" <<
			delta_busy_jiffs << ", delta_total_jiffs= " <<
			delta_total_jiffs << " (total shouldn't be a zero). Setting default value "
			<< usage[i];
			diag = Diag::NAN_ERR;
		}
		// the current becomes the past
		prev[i] = curr[i];
		// update usage
		mon.cpu_and_core_usage[i] = usage[i];
	}
	// update the timestamp
	ts_prev = ts_curr;
	// update the mon record timestamp
	mon.ts = ts_curr;


	return diag;
}
// ----------------------------------------------------------------------
// Spy
// ---------------------------------------------------------------------
Cpu_Spy::~Cpu_Spy(){
	delete [] mon.cpu_and_core_usage;
	mon.cpu_and_core_usage = nullptr;
}
/**
 * Does nothing special apart from getting the identity that is stored in
 * Spy::id
 * @return
 */
Diag Cpu_Spy::setup_for_read(){
	Diag diag = Diag::OK;

	if( (diag = Spy::setup_for_read()) != Diag::OK ){
		return diag;
	}

	if( (diag = acq.get(caps)) != Diag::OK){
		LOG(ERROR) << "Can't read the capabilities";
		return diag;
	}

	caps.id = id;

	// set the cpu_and_core_usage; this should be freed in the the destructor
	mon.cpu_and_core_usage = new float[caps.core_count + 1];

	return diag;
}
/**
 * Reads and sends the monitoring data
 *
 * @return Diag::OK if everything went ok
 */
Diag Cpu_Spy::read_mon(){
	Diag diag = Diag::OK;

	mon.core_plus_one_count = caps.core_count + 1;
	assert(mon.cpu_and_core_usage);

	if ( ( diag = acq.get(mon)) != Diag::OK){
		LOG(ERROR) << "Can't read the monitoring";
		return diag;
	}
	// the id is not set by acq::get_mon()
	mon.id = caps.id.id;

	return diag;
}
/**
 * getter for caps
 * @return
 */
ev_cpu_cap * Cpu_Spy::get_caps(){
	return &caps;
}
/**
 * getter for mon
 * @return
 */
ev_cpu_mon * Cpu_Spy::get_mon(){
	return &mon;
}
/**
 * Nothing to clean after read
 * @return
 */
Diag Cpu_Spy::clean_after_read() {
	Diag diag = Diag::OK;
	return diag;
}

Diag Cpu_Spy::setup_for_process() {
	Diag diag = Diag::OK;

	// get the identity
	if( (diag = Spy::setup_for_process()) != Diag::OK ){
		return diag;
	}

	return diag;
}
/**
 * Process the record when you receive it
 * @param rec
 * @return
 */
Diag Cpu_Spy::process(const ev_cpu_cap & rec) {
	Diag diag = Diag::OK;

	cout << "CPU:" << rec << "\n";

	return diag;
}

/**
 * Process the record when you receive it
 * @param rec
 * @return
 */
Diag Cpu_Spy::process(const ev_cpu_mon & rec) {
	Diag diag = Diag::OK;

	cout << "CPU:" << rec << "\n";

	return diag;
}

Diag Cpu_Spy::clean_after_process() {
	Diag diag = Diag::OK;

	return diag;
}
/**
 * Sets the ts for the capability record
 * @param ts
 * @return
 */
Diag Cpu_Spy::set_caps_ts(long long int ts) {
	caps.ts = ts;
}
/**
 * sets ts for the monitoring record
 * @param ts
 * @return
 */
Diag Cpu_Spy::set_mon_ts(long long int ts) {
	mon.ts = ts;
}
// -----------------------------------------------
// Ev_Cpu_Cap_Msg
// -----------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Cpu_Cap_Msg * Ev_Cpu_Cap_Msg::p_msg = nullptr;


/**
 * Registers all necessary stuff related to creating appropriate data
 * for ev_xxx_msg
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Cpu_Cap_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = cpu_cap_field_list;
	format_list = cpu_cap_format_list;

	//CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	// TODO I suspect this is not required for the EVpath submit events
	// cause EVcreate_submit_handle registers
	//format = CMregister_format(cm, &format_list_old[0]);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Cpu_Cap_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Cpu_Cap_Msg *Ev_Cpu_Cap_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Cpu_Cap_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

// -----------------------------------------------
// Ev_Cpu_Mon_Msg
// -----------------------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Cpu_Mon_Msg * Ev_Cpu_Mon_Msg::p_msg = nullptr;


/**
 * Registers all necessary stuff related to creating appropriate data
 * for ev_xxx_msg
 *
 * This method should be called otherwise it is of no use
 *
 * @return Diag::OK if everything went great - currently always
 */
Diag Ev_Cpu_Mon_Msg::reg(){
	Diag diag = Diag::OK;

	field_list = cpu_mon_field_list;

/*	field_list = new FMField[5];
	add_field_list_elem(0, "id", "integer", sizeof(int), FMOffset(ev_cpu_mon *, id));
	add_field_list_elem(1, "ts", "integer", sizeof(long long int), FMOffset(ev_cpu_mon *, ts));
	add_field_list_elem(2, "core_plus_one_count", "integer", sizeof(int), FMOffset(ev_cpu_mon *, core_plus_one_count));
	add_field_list_elem(3, "cpu_and_core_usage", "float[core_plus_one_count]", sizeof(float), FMOffset(struct ev_cpu_mon *, cpu_and_core_usage));
	add_field_list_elem(4);

	format_list = new FMStructDescRec[2];
	add_format_list_elem(0, "ev_cpu_mon", field_list, sizeof(ev_cpu_mon), nullptr);
	add_format_list_elem(1); */
	format_list = cpu_mon_format_list;

	//CManager cm = Ev_Topo::get_CM_now();

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	//format = CMregister_format(cm, &format_list_old[0]);

	return diag;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Cpu_Mon_Msg::exists(){
	return (nullptr != p_msg);
}

/**
 * Creates the singleton or
 * @return
 */
Ev_Cpu_Mon_Msg *Ev_Cpu_Mon_Msg::get_instance(){
	if (!exists()){
		p_msg = new Ev_Cpu_Mon_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}


} /* namespace cw */
