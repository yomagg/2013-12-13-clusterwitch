/**
 *  @file   mem_spy.cpp
 *
 *  @date   Created on: Feb 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <cassert>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cmath>

#include "easylogging++.h"

#include "mem_spy.h"
#include "spy.h"
#include "mem_spy_events_desc.h"
#include "misc.h"
#include "cw_runtime.h"


namespace cw {

using namespace std;

// -------------------------
// ev_mem_cap
// -------------------------
std::ostream & operator<<(std::ostream & os, const ev_mem_cap& rec){
	os << "TS:ID:HOST:PID:MEM_TOT_KB:SWAP_TOT_KB " << rec.ts << ":"
	   << rec.id.id << ":" << rec.id.hostname << ":" << rec.id.pid << ":"
	   << rec.mem_total_kB << ":" << rec.swap_total_kB;

	return os;
}

ev_mem_cap::ev_mem_cap(): id(identity()), ts(0),  mem_total_kB(0), swap_total_kB(0) {
}

ev_mem_cap::ev_mem_cap(const ev_mem_cap & other) : id(other.id), ts(other.ts),
		mem_total_kB(other.mem_total_kB), swap_total_kB(other.swap_total_kB){
}

ev_mem_cap & ev_mem_cap::operator=(const ev_mem_cap & other){
	ts = other.ts;
	id = other.id;
	mem_total_kB = other.mem_total_kB;
	swap_total_kB = other.swap_total_kB;

	return *this;
}
// -------------------------
// ev_mem_mon
// -------------------------
std::ostream & operator<<(std::ostream & os, const ev_mem_mon& rec){
	os << "TS:ID:MEM_UTIL_P:MEM_BUF_UTIL_P:MEM_CACHED_UTIL_P:MEM_ACTIVE_UTIL_P" <<
			":MEM_INACTIVE_UTIL_P:SLAB_UTIL_P:MAPPED_P:SWAP_CACHED_P:SWAP_UTIL_P:"
			<< rec.ts << ":" << rec.id << ":" << rec.mem_util_perc << ":" << rec.mem_buffers_util_perc
			<< ":" << rec.mem_cached_util_perc << ":" << rec.mem_active_util_perc
			<< ":" << rec.mem_inactive_util_perc << ":" << rec.slab_util_perc
			<< ":" << rec.mapped_perc << ":" << rec.swap_cached_perc << ":" << rec.swap_util_perc;
	return os;
}


// -------------------------
// Mem_Acquisitor
// -------------------------
Mem_Acquisitor::Mem_Acquisitor() : mem_total(0), swap_total(0){
	stream_name = "/proc/meminfo";
}

/**
 * Gets the cap. The caps.id is not set in this member function.
 * Sets the mem_total and swap_total class members
 * @param caps
 * @return Diag::OK everything went fine
 *         != Diag::OK some errors
 */
Diag Mem_Acquisitor::get_caps(ev_mem_cap & caps){
	Diag diag = Diag::OK;

	if(!is_initialized()){
		if( (diag = init()) != Diag::OK ){
			LOG(ERROR) << "Issues with opening acquisitor initialization. ATS";
			return diag;
		}
	}

	// get timestamp for this reading
	caps.ts = Time_Stamp::get_ts();

	// refresh /proc/meminfo
	proc_stream.seekg(0);

	string line;

	while(std::getline(proc_stream, line)){
		istringstream iss(line);
		string name;
		long bytes = 0L;
		//string dummy;  // for "kB"

		iss >> name;
		iss >> bytes;

		if(name.compare("MemTotal:") == 0){
			// this is the first thing we are looking for
			// TODO in general not recommended because it relies on
			// the order of fields; should be counter or something
			caps.mem_total_kB = bytes;
			continue;
		}
		if(name.compare("SwapTotal:") == 0){
			// this is the first thing we are looking for
			// TODO in general not recommended because it relies on
			// the order of fields; should be counter or something
			caps.swap_total_kB = bytes;
			break;
		}
	}

	// update the class member fields
	mem_total = caps.mem_total_kB;
	swap_total = caps.swap_total_kB;

	return diag;
}

/**
 * gets the monitoring record; @see Mem_Acquisitor::get_caps()
 * for the comment about /proc/meminfo
 *
 * The rec.id is not set.
 *
 * It is required to call get_caps prior to get_mon, as
 * get_mon uses the values of mem_total and swap_total to compute
 * utilization
 *
 * @param rec This record will be changed
 * @return Diag::OK everything went fine
 */
Diag Mem_Acquisitor::get_mon(ev_mem_mon & rec){
	Diag diag = Diag::OK;

	// TODO this check might be skipped for performance reasons
	// but I am leaving it for now
	if(!is_initialized()){
		if( (diag = init()) != Diag::OK ){
			LOG(ERROR) << "Issues with opening acquisitor initialization. ATS";
			return diag;
		}
	}

	// get timestamp for this reading
	rec.ts = Time_Stamp::get_ts();

	// refresh /proc/meminfo
	proc_stream.seekg(0);

	string line;

	while(std::getline(proc_stream, line)){
		istringstream iss(line);
		string name;
		long bytes = 0L;
		//string dummy;  // for "kB"

		iss >> name;
		iss >> bytes;

		// this is the first thing we are looking for
		// TODO in general not recommended because it relies on
		// the order of fields; should be counter or something
		if(name.compare("MemFree:") == 0){
			rec.mem_util_perc = Characteristics::utilization(mem_total, (float) (mem_total-bytes));
			continue;
		}
		if(name.compare("Buffers:") == 0){
			rec.mem_buffers_util_perc = Characteristics::utilization(mem_total, (float) (bytes));
			continue;
		}
		if(name.compare("Cached:") == 0){
			rec.mem_cached_util_perc = Characteristics::utilization(mem_total, (float) (bytes));
			continue;
		}
		if(name.compare("SwapCached:") == 0){
			rec.swap_cached_perc = Characteristics::utilization(swap_total, (float) (bytes));
			continue;
		}
		if(name.compare("Active:") == 0){
			rec.mem_active_util_perc = Characteristics::utilization(mem_total, (float) (bytes));
			continue;
		}
		if(name.compare("Inactive:") == 0){
			rec.mem_inactive_util_perc = Characteristics::utilization(mem_total, (float) (bytes));
			continue;
		}
		if(name.compare("SwapFree:") == 0){
			rec.swap_util_perc = Characteristics::utilization(swap_total, (float) (swap_total-bytes));
			continue;
		}
		if(name.compare("Mapped:") == 0){
			rec.mapped_perc = Characteristics::utilization(mem_total, (float) (bytes));
			continue;
		}
		if(name.compare("Slab:") == 0){
			rec.slab_util_perc = Characteristics::utilization(mem_total, (float) (bytes));
			break;
		}
	}

	return diag;
}
// -------------------------------
// Mem_Spy
// -------------------------------
/**
 * it initializes the capabilities and calls the original
 * Spy::setup_for_read() member function
 *
 * @return Diag::OK if everything went ok
 * 		   != Diag::OK if something went wrong
 */
Diag Mem_Spy::setup_for_read(){
	Diag diag = Diag::OK;

	// initialize the capabilities
	diag = init_caps();
	if( Diag::OK != diag){
		return diag;
	}

	diag = Spy::setup_for_read();
	if( Diag::OK != diag){
		return diag;
	} else {
		// copy the id
		caps.id = id;
	}

	return diag;
}


Diag Mem_Spy::read_mon() {
	Diag diag = Diag::OK;

	diag = acq.get_mon(mon);
	mon.id = caps.id.id;

	return diag;
}

ev_mem_cap * Mem_Spy::get_caps(){
	return &caps;
}

ev_mem_mon * Mem_Spy::get_mon(){
	return &mon;
}

Diag Mem_Spy::clean_after_read() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Mem_Spy::setup_for_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Mem_Spy::process(const ev_mem_cap& rec) {
	Diag diag = Diag::OK;

	cout << "MEM:" << rec << "\n";

	return diag;
}

Diag Mem_Spy::process(const ev_mem_mon& rec) {
	Diag diag = Diag::OK;

	cout << "MEM:" << rec << "\n";

	return diag;
}

Diag Mem_Spy::clean_after_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Mem_Spy::set_caps_ts(long long int ts) {
	caps.ts = ts;
	return Diag::OK;
}

Diag Mem_Spy::set_mon_ts(long long int ts) {
	mon.ts = ts;
	return Diag::OK;
}

/**
 * Initializes the mem capability record
 * @return
 */
Diag Mem_Spy::init_caps(){
	Diag diag = Diag::OK;

	// this should get the capabilities initialized if they are not
	diag = acq.get_caps(caps);

	return diag;
}
// --------------------------------
// Ev_Mem_Cap_Msg
// --------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Mem_Cap_Msg * Ev_Mem_Cap_Msg::p_msg = nullptr;

/**
 * singleton
 *
 * @return
 */
Ev_Mem_Cap_Msg* Ev_Mem_Cap_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Mem_Cap_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}
/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Mem_Cap_Msg::exists() {
	return (nullptr != p_msg);
}

/**
 * Registers formats
 *
 * @return
 */
Diag Ev_Mem_Cap_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = mem_cap_field_list;
	format_list = mem_cap_format_list;

	return diag;
}

// --------------------------------
// Ev_Mem_Mon_Msg
// --------------------------------
//! global initialization to facilitate a singleton design pattern
Ev_Mem_Mon_Msg * Ev_Mem_Mon_Msg::p_msg = nullptr;

/**
 * singleton
 * @return
 */
Ev_Mem_Mon_Msg* Ev_Mem_Mon_Msg::get_instance() {
	if (!exists()){
		p_msg = new Ev_Mem_Mon_Msg();
		assert(nullptr != p_msg);
		// register the format with EVPATH
		p_msg->reg();
	}
	return p_msg;
}

/**
 * Checks if the object has been initiated
 *
 * @return true the object has been initiated
 *         false the object has not been initiated
 */
bool Ev_Mem_Mon_Msg::exists() {
	return (nullptr != p_msg);
}

Diag Ev_Mem_Mon_Msg::reg() {
	Diag diag = Diag::OK;

	field_list = mem_mon_field_list;
	format_list = mem_mon_format_list;

	return diag;
}


}// namespace cw

