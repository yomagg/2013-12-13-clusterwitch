/**
 * @file  super_transport.cpp
 * @date  Sep 4, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <cassert>

#include "evpath.h"
#include "easylogging++.h"
#include "super_transport.h"

#include "cpu_spy.h"
#include "cpu_transport.h"
#include "mem_spy.h"
#include "mem_transport.h"
#include "net_spy.h"
#include "net_transport.h"
#ifdef NVML_PRESENT
#include "nvml_spy.h"
#include "nvml_transport.h"
#endif

namespace cw {

// ---------------------------------
// Ev_Dfg_Super_White_Atom
// ---------------------------------
/**
 * This is where I send the monitoring data to dfg
 * @param cm
 * @param client_data the pointer to the Ev_Dfg_Super_White_Atom
 */
extern "C" void dfg_super_white_probe(CManager cm, void *client_data){
	Ev_Dfg_Super_White_Atom *p_atom = static_cast<Ev_Dfg_Super_White_Atom *>(client_data);

	assert(p_atom);

	if( p_atom->submit() != Diag::OK){
		return;
	}

    return;
}

/**
 * Setups the super spy for reading and registers the stones
 * in DFG. In the implementation remember that registration order
 * of the stones should compliant with the submit()
 *
 * @param node_name the name of the node hosting the stones
 * @param names the name of the stones if NVML_PRESENT is not
 *        set the stone_names.nvml_xxx may not be set
 * @return Diag::OK everything went fine
 *         != Diag::OK some issues
 */
Diag Ev_Dfg_Super_White_Atom::reg(string node_name, stone_names& names) {
	Diag diag = Diag::OK;

	// sets the spy
	spy.setup_for_read();

	// the order of registration is important as it should be
	// compliant with submit(); not nice but it is as it is
	auto func = [] (Diag  status, Diag &my_diag, string & node_name, string &stone_name) {
		string str = "node_name:stone_name " + node_name + ":" + "stone_name";
		if (status != Diag::OK ) {
			LOG(ERROR) << str << " was NOT REGISTERED";
			if (my_diag == Diag::OK && status != Diag::OK){
				my_diag = status;
			}
		} else {
			LOG(DEBUG) << str << " REGISTRATION ... SUCCESS";
		}
	};

	func(reg_spy<Ev_Cpu_Mon_Msg>(node_name, names.cpu_stone_name), diag, node_name, names.cpu_stone_name);
	func(reg_spy<Ev_Mem_Mon_Msg>(node_name, names.mem_stone_name), diag, node_name, names.mem_stone_name);
	func(reg_spy<Ev_Net_Mon_Msg>(node_name, names.net_stone_name), diag, node_name, names.net_stone_name);

#ifdef NVML_PRESENT
	func(reg_spy<Ev_Nvml_Mon_Msg>(node_name, names.nvml_stone_name), diag, node_name, names.nvml_stone_name);
#endif

	return diag;
}

/**
 * This reads the events simultaneously and submits the
 * events through the dfg structure
 * @return Diag::OK if everything went fine
 *         != Diag::OK otherwise
 */
Diag Ev_Dfg_Super_White_Atom::submit() {

	// TODO need to do something with reading the mon events
	// and checking the diagnostics; this will return Diag::ERR
	// for nvml spy if one of the features is not supported,
	// For now, I am ignoring the output diagnostic value and assuming
	// that I have anyway the default value in my records so
	// instead of
	// if( spy.read_mon() != Diag::OK ){
	//	return Diag::ERR;
	// }
	spy.read_mon();    // TODO not checking the diagnostic value

	int i = 0;

	for( auto & el: src_vec){
		if( EVclient_source_active(el)){
			Spy * p_concrete_spy = spy.get_spies_vec().at(i);
			void * rec = nullptr;

			if( dynamic_cast<Cpu_Spy*>(p_concrete_spy) ) {
				rec = static_cast<void*>(dynamic_cast<Cpu_Spy*>(p_concrete_spy)->get_mon());
			} else if ( dynamic_cast<Mem_Spy*>(p_concrete_spy)  ){
				rec = static_cast<void*>(dynamic_cast<Mem_Spy*>(p_concrete_spy)->get_mon());
			} else if ( dynamic_cast<Net_Spy*>(p_concrete_spy) ){
				rec = static_cast<void*>(dynamic_cast<Net_Spy*>(p_concrete_spy)->get_mon());
			}
#ifdef NVML_PRESENT
			else if ( dynamic_cast<Nvml_Spy*>(p_concrete_spy) ){
				rec = static_cast<void*>(dynamic_cast<Nvml_Spy*>(p_concrete_spy)->get_mon());
			}
#endif

			if(rec){
				EVsubmit(el, rec, nullptr);
				// send the same event to the splitter hook in case
				// the external entity is interested in this event
				EVsubmit(get_splitter_hook().at(i++)->get_src(), rec, nullptr);
			} else {
				LOG(WARNING) << "Spy typed not identified. Nothing sent nowhere";
			}
		}
	}

	return Diag::OK;
}
/**
 * sets up the initial sampling rate for the super white atom
 * @return
 */
Diag Ev_Dfg_Super_White_Atom::run() {
	Diag diag = Diag::OK;

	CManager cm = Ev_Topo::get_CM_now();
	CMadd_periodic_task(cm, Cfg_Dfg::get_monitoring_sampling_sec_rate(),
			Cfg_Dfg::get_monitoring_sampling_usec_rate(), dfg_super_white_probe, this);

	return diag;
}
// --------------------------------------
// Ev_Dfg_Super_Black_Atom
// --------------------------------------

// this is a work around that
// template<....> does not work with extern "C"

// These are the handlers for handling events for different
// spies in the super spy handler
extern "C" int dfg_super_cpu_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_black_handler<ev_cpu_mon, Cpu_Spy, Cpu_Sqlite3_Reader>(
				cm, vevent, client_data, attrs);
	return 1;
}

extern "C" int dfg_super_mem_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_black_handler<ev_mem_mon, Mem_Spy, Mem_Sqlite3_Reader>(
				cm, vevent, client_data, attrs);
	return 1;
}

extern "C" int dfg_super_net_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_black_handler<ev_net_mon, Net_Spy, Net_Sqlite3_Reader>(
			cm, vevent, client_data, attrs);
	return 1;
}
#ifdef NVML_PRESENT
extern "C" int dfg_super_nvml_black_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_black_handler<ev_nvml_mon, Nvml_Spy, Nvml_Sqlite3_Reader>(
				cm, vevent, client_data, attrs);
	return 1;
}
#endif

/**
 * This is the registration of the super black atom, it creates
 * the necessary db for each event and joins the dfg
 * @param node_name
 * @param names
 * @return
 */
Diag Ev_Dfg_Super_Black_Atom::reg(string node_name, stone_names& names) {
	Diag diag = Diag::OK;

	if( (diag = get_sqlite3_reader()->open_db()) != Diag::OK){
		return diag;
	}

	// TODO add diagnostics
	reg_spy<Ev_Cpu_Mon_Msg>(node_name, names.cpu_stone_name, dfg_super_cpu_black_handler);
	reg_spy<Ev_Mem_Mon_Msg>(node_name, names.mem_stone_name, dfg_super_mem_black_handler);
	reg_spy<Ev_Net_Mon_Msg>(node_name, names.net_stone_name, dfg_super_net_black_handler);
#ifdef NVML_PRESENT
	reg_spy<Ev_Nvml_Mon_Msg>(node_name, names.nvml_stone_name, dfg_super_nvml_black_handler);
#endif

	return diag;
}

// -------------------------------------------
// Ev_Dfg_Super_Gray_Atom
// -------------------------------------------

// the handlers for each spy of the super spy
extern "C" int dfg_super_cpu_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_gray_handler<ev_cpu_mon, Cpu_Spy>(cm, vevent, client_data, attrs);
	return 1;
}

extern "C" int dfg_super_mem_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_gray_handler<ev_mem_mon, Mem_Spy>(cm, vevent, client_data, attrs);
	return 1;
}

extern "C" int dfg_super_net_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_gray_handler<ev_net_mon, Net_Spy>(cm, vevent, client_data, attrs);
	return 1;
}
#ifdef NVML_PRESENT
extern "C" int dfg_super_nvml_gray_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	dfg_super_gray_handler<ev_nvml_mon, Nvml_Spy>(cm, vevent, client_data, attrs);
	return 1;
}
#endif
/**
 * Registers the stones in the DFG
 *
 * @param node_name the DFG node name
 * @param src_names names of the sources
 * @param sink_names names of sinks
 * @return
 */
Diag Ev_Dfg_Super_Gray_Atom::reg(string node_name, stone_names& src_names,
	stone_names& sink_names) {
	Diag diag = Diag::OK;

	if( (diag = reg_spy<Ev_Cpu_Mon_Msg>(node_name, src_names.cpu_stone_name,
			sink_names.cpu_stone_name, dfg_super_cpu_gray_handler)) != Diag::OK){
		return diag;
	}

	if ( (diag = reg_spy<Ev_Mem_Mon_Msg>(node_name, src_names.mem_stone_name,
				sink_names.mem_stone_name, dfg_super_mem_gray_handler)) != Diag::OK){
		return diag;
	}

	if ( (diag = reg_spy<Ev_Net_Mon_Msg>(node_name, src_names.net_stone_name,
				sink_names.net_stone_name, dfg_super_net_gray_handler)) != Diag::OK){
		return diag;
	}

#ifdef NVML_PRESENT
	if ( (diag = reg_spy<Ev_Nvml_Mon_Msg>(node_name, src_names.nvml_stone_name,
				sink_names.nvml_stone_name, dfg_super_nvml_gray_handler)) != Diag::OK){
		return diag;
	}
#endif

	return diag;
}

/**
 * Should nicely quit the topology
 * @return
 */
Diag Ev_Dfg_Super_Gray_Atom::quit_topo() {
	LOG(WARNING) << "Not implemented";
	return Diag::OK;
}

} // cw namespace

