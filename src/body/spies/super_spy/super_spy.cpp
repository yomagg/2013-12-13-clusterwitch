/**
 * @file  super_spy.cpp
 * @date  Sep 4, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#include "super_spy.h"

#include "cpu_spy.h"
#include "mem_spy.h"
#include "net_spy.h"
#ifdef NVML_PRESENT
#include "nvml_spy.h"
#endif

namespace cw {

/**
 * fills the spies_vec with available spies. New spies should be
 * added here
 */
Super_Spy::Super_Spy(){
	// add all spies that will be run by the super spy and setup them for read
	spies_vec.emplace_back(new Cpu_Spy());
	spies_vec.emplace_back(new Mem_Spy());
	spies_vec.emplace_back(new Net_Spy());
#ifdef NVML_PRESENT
	spies_vec.emplace_back(new Nvml_Spy());
#endif
}

/**
 * frees the allocated memory hold in the spies vec
 */
Super_Spy::~Super_Spy(){
	for( auto &el : spies_vec){
		delete el;
	}
}

Diag Super_Spy::setup_for_read(){
	Diag diag = Diag::OK;

	if( (diag = Spy::setup_for_read()) != Diag::OK ){
		return diag;
	}

	auto ts = Time_Stamp::get_ts();
	for( auto &el : spies_vec){
		if ( (diag = el->setup_for_read()) != Diag::OK ){
			return diag;
		}
		// all will have the same identity as the super spy
		// TODO not sure if this is a good idea
		el->set_identity(id);
		el->set_caps_ts(ts);
	}
	return diag;
}

Diag Super_Spy::read_mon() {
	Diag diag = Diag::OK;

	auto ts = Time_Stamp::get_ts();

	for( auto &el : spies_vec){
		if( (diag = el->read_mon()) != Diag::OK){
			return diag;
		}
		el->set_mon_ts(ts);
	}
	return diag;
}

Diag Super_Spy::clean_after_read() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Super_Spy::setup_for_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Super_Spy::clean_after_process() {
	Diag diag = Diag::OK;

	return diag;
}

Diag Super_Spy::set_caps_ts(long long int ts) {
	for( auto &el : spies_vec){
		el->set_caps_ts(ts);
	}
	return Diag::OK;
}

Diag Super_Spy::set_mon_ts(long long int ts) {
	for( auto &el : spies_vec){
		el->set_mon_ts(ts);
	}
	return Diag::OK;
}

std::vector<Spy*>& Super_Spy::get_spies_vec() {
	return spies_vec;
}

} // end namespace cw


