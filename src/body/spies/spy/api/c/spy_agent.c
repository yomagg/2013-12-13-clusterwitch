/**
 *  @file   spy_agent.c
 *
 *  @date   Created on: Feb 25, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <assert.h>
#include <string.h>

#include "spy_agent.h"

static diag_t _nondfg_get_mon_events(struct cw_mon_event *p_event, const char * contact_file, FMStructDescRec *p_format_list);
static diag_t _dfg_get_mon_events(struct cw_mon_event *p_event, const char * contact_file, FMStructDescRec *p_format_list);
static diag_t _dfg_create_contact_key(struct cw_mon_event *p_event, const char *key_buf, int key_buf_size);

//! the number of elements in the vector that can be registered in CW
#define RAGENT_COUNT 1000

// -------------------------------------------
// global variables
// -------------------------------------------
//! the connection manager
CManager cw_cm = NULL;
//! it remembers the registered elements; currently it works as a bag
//! I am not doing any removal from this array
static struct ragent_info cw_ragents_arr[RAGENT_COUNT];
//! the index of the first available slot in cw_ragents
static int cw_ragents_idx = 0;



// PUBLIC API
/**
 * Initializes CW for watching for events e.g. cm manager
 */
diag_t cw_init(){
	assert( 0 == cw_ragents_idx );
	if( !cw_cm ){
		cw_cm = CManager_create();
		if( !cw_cm ){
			p_error("Can't create the CManager()\n");
			return DIAG_CW_EVPATH_ERR;
		}
		CMlisten(cw_cm);
	}
	return DIAG_CW_OK;
}

/**
 * Gets the monitoring events by interfacing to CW for a non-dfg topology
 *
 * @param p_event The description of the event to be monitored
 */
diag_t cw_nondfg_get_mon_events(struct cw_mon_event *p_event){
	diag_t diag = DIAG_CW_OK;

	switch(p_event->id){
	case cw_MON_EVENT_CPU:
		diag = _nondfg_get_mon_events(p_event,
				Ev_Cpu_Monitoring_Atom_MON_ATOM_CONTACT_FILE, cpu_mon_format_list);
		break;
	case cw_MON_EVENT_MEM: diag = _nondfg_get_mon_events(p_event,
			Ev_Mem_Monitoring_Atom_MON_ATOM_CONTACT_FILE, mem_mon_format_list);
		break;
	case cw_MON_EVENT_NET: diag = _nondfg_get_mon_events(p_event,
			Ev_Net_Monitoring_Atom_MON_ATOM_CONTACT_FILE, net_mon_format_list);
		break;
#ifdef NVML_PRESENT
	case cw_MON_EVENT_NVML: diag = _nondfg_get_mon_events(p_event,
				Ev_Nvml_Monitoring_Atom_MON_ATOM_CONTACT_FILE, nvml_mon_format_list);
		break;
#endif
	default:
		p_error("Unsupported monitoring event.");
		diag = DIAG_CW_ERR;
		break;
	}
	return diag;
}

/**
 * Gets the monitoring events by interfacing to CW for the dfg topology
 *
 * @param p_event The description of the event to be monitored
 */
diag_t cw_get_mon_events(struct cw_mon_event *p_event){
	diag_t diag = DIAG_CW_OK;

	switch(p_event->id){
	case cw_MON_EVENT_CPU:
		diag = _dfg_get_mon_events(p_event,
				DFG_MON_SPLITTER_CONTACT_FILE, cpu_mon_format_list);
		break;
	case cw_MON_EVENT_MEM: diag = _dfg_get_mon_events(p_event,
			DFG_MON_SPLITTER_CONTACT_FILE, mem_mon_format_list);
		break;
	case cw_MON_EVENT_NET: diag = _dfg_get_mon_events(p_event,
			DFG_MON_SPLITTER_CONTACT_FILE, net_mon_format_list);
		break;
#ifdef NVML_PRESENT
	case cw_MON_EVENT_NVML: diag = _dfg_get_mon_events(p_event,
				DFG_MON_SPLITTER_CONTACT_FILE, nvml_mon_format_list);
		break;
#endif
	default:
		p_error("Unsupported monitoring event.");
		diag = DIAG_CW_ERR;
		break;
	}
	return diag;
}


/**
 * starts monitoring
 * @param secs the duration of the monitoring; 0 value - monitor forever;
 * 				>0 value - monitor  for the specified duration
 */
diag_t cw_monitor(int secs){
	if(!cw_cm){
		p_error("The Cw has been not initialized. ATS\n");
		return DIAG_CW_UNINITIALIZED;
	}
	// start servicing the network
	if( 0 == secs){
		// run forever
		CMrun_network(cw_cm);
	} else {
		CMsleep(cw_cm, secs);
	}
	return DIAG_CW_OK;
}

/**
 * Closes the CW. Currently if there are errors during the destruction
 * of stones, this error is reported but it is ignored.
 *
 * TODO This function should probably take care of unregistering handlers
 *      from the global var cw_regents_arr. But right now, it doesn't
 *      do this
 *
 * @return DIAG_CW_OK if everything went fine
 *         != DIAG_CW_OK if anything went wrong
 */
diag_t cw_close(){
	diag_t diag = DIAG_CW_OK;

	// clean output stones
	for(int i = 0; i < cw_ragents_idx; ++i){
		if( destroy_output_stone(&cw_ragents_arr[i]) != DIAG_CW_OK ){
			p_error("Issues with destroy_output_stone(). Ignoring ...\n");
			diag = DIAG_CW_ERR;
		}
	}
	// TODO not sure if the network needs to be serviced to be sure
	// if destroy needs to work
	p_debug("About to exit ...\n");
	//CMsleep(cw_cm, 10);

	CManager_close(cw_cm);
	//forget about all stored ragents
	cw_ragents_idx = 0;

	return diag;
}
// PRIVATE API
/**
 * Connects to the monitoring splitter stone, i.e.,
 * reads the splitter contact information, creates the
 * output stone and registers the provided handler
 *
 * @param p_event The descriptor of the monitoring event
 * @param contact_file The name of the contact file
 * @param p_format_list The name of the formats related to the event type
 * @return DIAG_CW_OK if everything went great
 *         != DIAG_CW_OK if any errors
 */
static diag_t _nondfg_get_mon_events(struct cw_mon_event *p_event, const char * contact_file,
		FMStructDescRec *p_format_list){

	assert(p_event->handler);

	// check if the contact file to the local monitor exists and get
	// the contact information
	diag_t diag = DIAG_CW_OK;

	struct ragent_info ragent_info;

	if( (diag = read_splitter_contact(contact_file, &ragent_info)) != DIAG_CW_OK){
		p_error("Got errors from read_splitter_contact: (ec=%d)\n", diag);
		return diag;
	}

	// create the output stone with REVpath

	struct ev_handler_info handler_info;
	handler_info.p_formats = p_format_list;
	handler_info.p_client_data = p_event->client_data;
	handler_info.p_handler = p_event->handler;

	if( (diag = create_output_stone(&ragent_info, &handler_info)) != DIAG_CW_OK){
		p_error("Got errors from read_splitter_contact: (ec=%d)\n", diag);
		return diag;
	}

	if( (diag = register_ragent(cw_ragents_arr, RAGENT_COUNT, &cw_ragents_idx, &ragent_info)) != DIAG_CW_OK){
		p_error("Got errors from register_ragent: (ec=%d)\n", diag);
		return diag;
	}

	return diag;
}
/**
 * Connects to the monitoring splitter stone, i.e.,
 * reads the splitter contact information, creates the
 * output stone and registers the provided handler
 *
 * @param p_event The descriptor of the monitoring event
 * @param contact_file The name of the contact file
 * @param p_format_list The name of the formats related to the event type
 * @return DIAG_CW_OK if everything went great
 *         != DIAG_CW_OK if any errors
 */
static diag_t _dfg_get_mon_events(struct cw_mon_event *p_event, const char * contact_file,
		FMStructDescRec *p_format_list){

	assert(p_event->handler);

	// check if the contact file to the local monitor exists and get
	// the contact information
	diag_t diag = DIAG_CW_OK;

	struct ragent_info ragent_info;

	if( (diag = _dfg_create_contact_key(p_event, ragent_info.key_contact,
			sizeof(ragent_info.key_contact))) != DIAG_CW_OK){
		p_error("Got errors _dfg_create_contact_key(): (ec = %d). Returning error \n", diag);
		return diag;
	}

	if( (diag = dfg_read_splitter_contact(contact_file, &ragent_info)) != DIAG_CW_OK){
		p_error("Got errors from read_splitter_contact: (ec=%d). Returning an error\n", diag);
		return diag;
	}

	// create the output stone with REVpath

	struct ev_handler_info handler_info;
	handler_info.p_formats = p_format_list;
	handler_info.p_client_data = p_event->client_data;
	handler_info.p_handler = p_event->handler;

	if( (diag = create_output_stone(&ragent_info, &handler_info)) != DIAG_CW_OK){
		p_error("Got errors from read_splitter_contact: (ec=%d)\n", diag);
		return diag;
	}

	if( (diag = register_ragent(cw_ragents_arr, RAGENT_COUNT, &cw_ragents_idx, &ragent_info)) != DIAG_CW_OK){
		p_error("Got errors from register_ragent: (ec=%d)\n", diag);
		return diag;
	}

	return diag;
}
/**
 * Creates a contact key. It should correspond to the way how the key is
 * created: Ev_Splitter_Hook::create_splitter_hook(). Right now
 * it is created by a concatenation of the contact_node and contact_stone
 * and the concatenation is ' ' (space)
 *
 * @param p_event (IN) I assume that the contact_node and contact_stone names
 *                     are not empty
 * @param key_buf (OUT) The resultant contact key; assumed it is not null and
 *                     there is enough space to hold the concatenation
 *                     of strlen(p_event->contact_node) + strlen(p_event->contact_stone)+2
 *                     (space and string end)
 * @param key_buf_size (IN) the size of the key_buf (to check if I have enough
 *                     space for the concatenation
 * @return DIAG_CW_OK if everything went ok
 *         != DIAG_CW_OK if something went wrong
 */
static diag_t _dfg_create_contact_key(struct cw_mon_event *p_event, const char *key_buf, int key_buf_size){
	assert(p_event);
	// check if names are not empty
	assert(strlen(p_event->contact_node));
	assert(strlen(p_event->contact_stone));
	assert(key_buf);

	// include the space
	int chars_copied = strlen(p_event->contact_node)+strlen(p_event->contact_stone)+1;

	assert(key_buf_size >= chars_copied);

	if (snprintf(key_buf, chars_copied, "%s_%s", p_event->contact_node, p_event->contact_stone)
			!= chars_copied){
		p_error("Problem with concatenation, returning an error\n");
		return DIAG_CW_ERR;
	}

	return DIAG_CW_OK;
}
