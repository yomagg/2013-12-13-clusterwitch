/**
 *  @file   spy_agent_utils.c
 *
 *  @date   Created on: Feb 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "evpath.h"
#include "revpath.h"

#include "spy_agent.h"
#include "debug.h"


// --------------------------------------------
// PUBLIC utility functions
// --------------------------------------------

/**
 * reads the splitter contact from the file
 *
 * @param file_name (IN) The file name that I want to read from
 * @param p_ragent_info (OUT) Here the read contact information will
 *                        be stored (contact, rsplit_stone and rsplit_action
 * @return DIAG_CW_OK if everything ok
 *         DIAG_CW_ERR if any errors
 */
diag_t read_splitter_contact(const  char * file_name, struct ragent_info *p_ragent_info){
	diag_t diag = DIAG_CW_OK;

	FILE * fp = NULL;

	assert(NULL != file_name);
	assert(NULL != p_ragent_info);

	fp = fopen (file_name, "r");
	if( !fp ){
		p_error("Can't open the file to get the splitter contact: %s\n", file_name);
		return DIAG_CW_ERR;
	}

	// parse the cpntact info
	const int LINE_SIZE = 128;
	char line[LINE_SIZE];

	// assuming that there is only one line; probably this will be wrong
	// as more spies will created
	/*while (fgets(line, LINE_SIZE, fp) != NULL) {
		p_debug("%s\n", line);
	}*/
	// this is the first line
	if( fgets(line,LINE_SIZE,fp) ){
		sscanf(line, "%s %d %d", &p_ragent_info->contact[0], &p_ragent_info->rsplit_stone, &p_ragent_info->rsplit_action);
		p_debug("Read (contact:split_stone:split_action) %s:%d:%d\n", p_ragent_info->contact,
				p_ragent_info->rsplit_stone, p_ragent_info->rsplit_action);
	} else {
		p_error("Can't read the line from the file: %s\n", file_name);
		return DIAG_CW_ERR;
	}

	fclose(fp);

	return diag;
}
/**
 * Simple and not very effective parser. Attemps to find the first
 * matching key.
 * reads the splitter contact from the file for the dfg flavor. The splitter
 * contact file is one per node where all splitter stones write their contacts
 * See Ev_Splitter_Hook::create_splitter_hook in order to see how the
 * contact is written.
 * Here is the example format
 * of the splitter contact file: a key that is a concatenation of
 * the node name and the stone name the concatenation is '_' followed
 * by '=' and a concatenation of contact,splitter_stone_id, splitter_action_id
 * and concatenation is done with ':'.
 *
 * [splitters]
 * a_a_nvml_src=AAIAAJTJ8o3HZQAAATkCmGcAqMA=:3:0
 * a_a_mem_src=AAIAAJTJ8o3HZQAAATkCmGcAqMA=:1:0
 * b_b_mem_src=AAIAAJTJ8o2rZQAAATkCmGcAqMA=:1:0
 * b_b_net_src=AAIAAJTJ8o2rZQAAATkCmGcAqMA=:2:0
 * b_b_nvml_src=AAIAAJTJ8o2rZQAAATkCmGcAqMA=:3:0
 * b_b_cpu_src=AAIAAJTJ8o2rZQAAATkCmGcAqMA=:0:0
 * a_a_cpu_src=AAIAAJTJ8o3HZQAAATkCmGcAqMA=:0:0
 * a_a_net_src=AAIAAJTJ8o3HZQAAATkCmGcAqMA=:2:0
 *
 * @param file_name (IN) The file name that I want to read from
 * @param p_ragent_info (OUT) Here the read contact information will
 *                        be stored (contact, rsplit_stone and rsplit_action
 *                      (IN) the key_contact; the dfg atom writes the contact
 *                      in one file on a node, and that file contains key-value
 *                      pairs: the key is the nodename_stonename and the
 *                      value is the contact. The read should correspond to
 *                      write see e.g. Ev_Dfg_White_Atom::join_dfg_node
 * @return DIAG_CW_OK if everything ok
 *         DIAG_CW_ERR if any errors e.g. can't read the file
 *         DIAG_CW_CONTACT_KEY_NOT_FOUND when the key was not found
 */
diag_t dfg_read_splitter_contact(const  char * file_name, struct ragent_info *p_ragent_info){
	diag_t diag = DIAG_CW_OK;

	FILE * fp = NULL;

	assert(NULL != file_name);
	assert(NULL != p_ragent_info);

	fp = fopen (file_name, "rt");
	if( !fp ){
		p_error("Can't open the file to get the splitter contact: %s\n", file_name);
		return DIAG_CW_ERR;
	}

	// parse the contact info
	const int LINE_SIZE = 300;
	char line[LINE_SIZE];

	// the first line should be ignored; it is the section name
	if( !fgets(line, LINE_SIZE, fp) ){
		p_error("Can't read the line from the file: %s. Returning an error.\n", file_name);
		return DIAG_CW_ERR;
	}

	struct ragent_info ragent;
	// sentinel telling if the key has been found
	int key_found = 0;

	while( fgets(line, sizeof(line), fp) != NULL){
		int result = 0;
		char rsplit_stone_buf[10];
		// a trick with scanf to have different delimiters
		// a_a_nvml_src=AAIAAJTJ8o3fZQAAATkCmGcAqMA=:3:0
		if( (result=sscanf(line, "%[^'=']=%[^':']:%[^':']:%d",
				&ragent.key_contact[0], &ragent.contact[0],
				rsplit_stone_buf, &ragent.rsplit_action)) != 4) {
			p_error("Sscanf returned an error. Read %d tokens. Returning error\n", result);
			diag = DIAG_CW_ERR;
			break;
		}
		// check if this is the key I am looking for
		if(strncmp(ragent.key_contact, p_ragent_info->key_contact, strlen(p_ragent_info->key_contact)) == 0){
			// we found the key
			key_found = 1;
			p_debug("The key has been found (key contact split_stone split_action) %s %s %s %d\n",
					ragent.key_contact, ragent.contact,
					rsplit_stone_buf, ragent.rsplit_action);

			p_ragent_info->rsplit_stone = atoi(rsplit_stone_buf);
			p_ragent_info->rsplit_action = ragent.rsplit_action;
			// just to be sure that it ends with \0
			memset(p_ragent_info->contact, 0, sizeof(p_ragent_info->contact));
			strncpy(p_ragent_info->contact, ragent.contact, sizeof(ragent.contact));

			break;
		}
	}

	// perform copy if the fields
	if( !key_found ){
		p_warn("The key %s has not been found\n", p_ragent_info->key_contact);
		// it might be that something returned an error from the while loop
		if( DIAG_CW_ERR != diag ){
			diag = DIAG_CW_CONTACT_KEY_NOT_FOUND;
		}
	}

	fclose(fp);

	return diag;
}

/**
 * Setups all necessary structure on the remote splitter, i.e., creates
 * the output stone on the remote splitter. It doesn't not run the
 * thread to service the network, so still CMsleep(cm, xx) or CMrun_network()
 * needs to be run.
 *
 * @param p_ragent_info (INOUT) The information that will be provided by this
 *                           function when creating a part of the infrastructure
 * @param p_handler_info (IN) The handler information
 * @return DIAG_OK if everything went great
 *         != DIAG_OK otherwise
 */
diag_t create_output_stone(
		struct ragent_info *p_ragent_info,
		const struct ev_handler_info *p_handler_info){
	assert(p_ragent_info);
	assert(p_handler_info);

	diag_t diag = DIAG_CW_OK;

	// TODO probably I need to store this information somewhere
	if( !cw_cm ){
		p_error("CW not initialized\n");
		return DIAG_CW_UNINITIALIZED;
	}

	// remember connection manager
	p_ragent_info->cm = cw_cm;

	attr_list sender_contact_list = attr_list_from_string(p_ragent_info->contact);

	// initiate the connection with the sender; I need this for
	// creating of stones remotely
	CMConnection conn = CMinitiate_conn(p_ragent_info->cm, sender_contact_list);

	if( NULL == conn){
		p_error("CMinitiate_conn returned NULL. ATS ...\n");
		diag = DIAG_CW_EVPATH_ERR;
		free_attr_list(sender_contact_list);
		return diag;
	}
	free_attr_list(sender_contact_list);
	// remember the connection
	p_ragent_info->conn = conn;

	// now do the magic
	// * create another remote output stone on a sender
	// * add this stone as a splitter target

	// create an output stone remotely
	p_ragent_info->rbridge_stone = REValloc_stone(conn);

	// create local stone that will be connected with the remote stone
	assert(p_handler_info->p_formats);
	assert(p_handler_info->p_handler);
	attr_list my_contact = CMget_contact_list(p_ragent_info->cm);
	p_ragent_info->lterminal_stone = EValloc_stone(p_ragent_info->cm);
	EVassoc_terminal_action(p_ragent_info->cm, p_ragent_info->lterminal_stone,
			p_handler_info->p_formats, p_handler_info->p_handler,
			p_handler_info->p_client_data );

	// connect routput stone with the local stone
	REVassoc_bridge_action(conn, p_ragent_info->rbridge_stone, my_contact,
			p_ragent_info->lterminal_stone);
	free_attr_list(my_contact);
	p_debug("Created and bridged remote output stone: remote-bridge-stone-id:local-terminal-stone-id = %d:%d\n",
			p_ragent_info->rbridge_stone, p_ragent_info->lterminal_stone);

	// connect the new output stone to the splitter stone; it is recommended
	// that adding the split target is performed after all preparatory steps
	// have been accomplished such as creation of the remote-output and local-terminal
	// stones and their bridging, otherwise there might be situations
	// that evpath might complain that there is nothing that can consume events
	// that are being produced
	REVaction_add_split_target(conn, p_ragent_info->rsplit_stone,
			p_ragent_info->rsplit_action, p_ragent_info->rbridge_stone);
	p_debug("Added a new target on a remote splitter\n");

	return diag;
}

/**
 * The information about the remote agent
 * @param p_ragent_info
 * @return DIAG_OK if everything went great
 *         != DIAG
 */
diag_t destroy_output_stone(struct ragent_info *p_ragent_info){
	diag_t diag = DIAG_CW_OK;

	REVaction_remove_split_target(p_ragent_info->conn, p_ragent_info->rsplit_stone,
			p_ragent_info->rsplit_action,
			p_ragent_info->rbridge_stone);

	if(1 != REVdestroy_stone(p_ragent_info->conn, p_ragent_info->rbridge_stone)){
		p_error("Issues with destroying the remote output stone\n");
		return DIAG_CW_EVPATH_ERR;
	} else {
		p_debug("Remote split target removed. The remote output stone %d DESTROYED\n", p_ragent_info->rbridge_stone);
	}

	EVdestroy_stone(p_ragent_info->cm, p_ragent_info->lterminal_stone);

	return diag;
}

/**
 * @param arr Where to register the info about the ragent
 * @param arr_len The size of the array
 * @param (INOUT) p_arr_slot The index where to put the p_ragent_info
 * @param p_ragent_info the information that will be registered
 * @return DIAG_CW_OK if everything went ok
 *         != DIAG_CW_OK
 */
diag_t register_ragent(struct ragent_info * arr, int arr_len, int * p_arr_slot,
		struct ragent_info *p_ragent_info){
	diag_t diag = DIAG_CW_OK;
	if( (*p_arr_slot) >= arr_len ){
		return DIAG_CW_RAGENT_COUNT_EXCEEDED;
	}
	// ok, I can insert to the array
	// I think the default copy should work
	arr[*p_arr_slot] = *p_ragent_info;
	// update current index
	++(*p_arr_slot);

	return diag;
}
