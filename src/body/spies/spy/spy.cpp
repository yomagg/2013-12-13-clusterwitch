/**
 *  @file   spy.cpp
 *
 *  @date   Created on: Dec 18, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <ctime>
#include <unistd.h>

#include "cw_runtime.h"
#include "spy.h"
#include "misc.h"

#include "easylogging++.h"

namespace cw {
// ---------------------------
// identity
// ---------------------------
std::ostream& operator<<(std::ostream& os, const identity& obj){
	// write obj to stream
	os <<  obj.id << ":" << obj.hostname << ":" << obj.pid;
	return os;
}
identity::identity() :id(0), hostname(nullptr), pid(0)  {
}

identity::identity(const identity & other) : hostname(nullptr){
	copy_identity(other);
}

identity & identity::operator=(const identity & other){
	copy_identity(other);

	return *this;
}
/**
 * copies the content of the other into this object
 *
 * @param other what will be copied
 * @return Diag::OK if everything went great
 *         != Diag::OK if strdup went wrong
 */
Diag identity::copy_identity(const identity & other){
	Diag diag = Diag::OK;

	id = other.id;
	pid = other.pid;
	free(hostname);
	hostname = nullptr;
	if( other.hostname ){
		hostname = strdup(other.hostname);
		if( !hostname ){
			LOG(ERROR) << "Issues with strdup. hostname assigned nullptr ...";
			diag = Diag::ERR;
		}
	}

	return diag;
}
/**
 * allocates memory for the hostname; should be free at some point
 *
 * @param an_id
 * @param p_hostname
 * @param a_pid
 */
identity::identity(int an_id, const char* p_hostname, pid_t a_pid) :
	id(an_id), pid(a_pid){
	// leave nullptr if empty
	if(p_hostname){
		hostname = strdup(p_hostname);
		// check for an error
		if (!hostname){
				LOG(ERROR) << "Can't allocate memory for hostname. Ignoring ...";
		}
	} else {
		hostname = nullptr;
	}
}
/**
 * frees the memory allocated for the hostname
 */
identity::~identity(){
	free(hostname);
	hostname = nullptr;
}
// -------------------------------------------------------------------
// Identity
// -------------------------------------------------------------------
/**
 * Initialized to 0
 */
Identity::Identity() : id(0), hostname(""), pid(0) {
}


Diag Identity::determine() {
	Diag diag = Diag::OK;
	// fill the identity structure
	time_t 		t;

	srand48(getpid() + time(&t));
	id = lrand48();
	pid = getpid();
	char h[256];
	if( gethostname(h, sizeof(h)) != 0 ){
		LOG(ERROR) << "Problems with getting the host name\n";
		diag = Diag::ERR;
	} else {
		hostname = h;
	}

	LOG(DEBUG) << "Identity (id,hostname,pid) " << id << " " << hostname <<
			" " << pid;

	return diag;
}

Identity::operator identity(){
	if(strcmp(hostname.c_str(), "") == 0){
		return identity(id, nullptr, pid);
	}
	return identity(id, hostname.c_str(), pid);
}

/**
 * @return getter for id
 */
int Identity::get_id(){
	return id;
}
/**
 * @return getter for pid
 */
pid_t Identity::get_pid(){
	return pid;
}

/**
 * @return getter for the hostname
 */
const std::string & Identity::get_hostname(){
	return hostname;
}

// -----------------------------------------------------
// Source_Caps
// -----------------------------------------------------
/**
 * Determines the identity
 *
 * @param id This will be loaded with the correct values obtained from the
 *           identity
 * @return Diag::OK if everything went ok
 *         != Diag::OK otherwise
 */
/*Diag Source_Caps::determine_id(identity & id){
	Identity my_id;

	if ( my_id.determine() != Diag::OK ){
		LOG(ERROR) << "Issues with determining the id";
		return Diag::ERR;
	} else {
		// I am good; continue
		// TODO something is wrong with this code
		// here the automatic conversion Identity->identity
		// should work
		//id = my_id;
		id.id = my_id.get_id();
		id.pid = my_id.get_pid();
		id.hostname = strdup(my_id.get_hostname().c_str());
	}

	return Diag::OK;
}*/
// -----------------------------------------------------
// Data
// -----------------------------------------------------
/*Data::Data() : ts(0L){

}*/
/**
 * Updates the timestamp
 * @return
 */
/*Diag Data::update() {
	Diag diag = Diag::OK;
	ts = Time_Stamp::get_ts();

	return diag;
}*/

/**
 * Returns the current timestamp
 * @return
 */
/*long long int Data::get_ts(){
	return ts;
}*/

// -----------------------------------------------------
// Data_Source
// -----------------------------------------------------
/*Data_Source::Data_Source() {
}

Data_Source::~Data_Source() {
}*/
// --------------------------------------------------------------------
// Spy
// --------------------------------------------------------------------

/**
 * Initializes the spy to be disabled
 */
Spy::Spy() : enabled(true){
}


/**
 * Returns the status of the spy if this is enabled
 * @return true the spy is enabled
 *         false the spy is disabled
 */
bool Spy::is_enabled() const {
	return enabled;
}

/**
 * Enables the spy.
 * @return DIAG_OK everything went fine
 */
Diag Spy::enable() {
	enabled = true;

	return Diag::OK;
}

/**
 * Disables the spy
 * @return Diag::OK everything went fine
 */
Diag Spy::disable() {
	enabled = false;

	return Diag::OK;
}

/**
 * setups the spy for read; it determines the identity
 *
 * @return Diag::OK if everything went ok
 *         != Diag::OK if something went wrong
 */
Diag Spy::setup_for_read(){
	Diag diag = Diag::OK;

	// get the identity
	if( (diag = determine_identity()) != Diag::OK){
		LOG(ERROR) << "Got an error from determining identity. Returning ...";
		return diag;
	}
	return diag;
}

/**
 * Setups the spy for the processing; it determines the identity
 *
 * @return Diag::OK if everything went ok
 *         != Diag::OK if something went wrong
 */
Diag Spy::setup_for_process(){
	Diag diag = Diag::OK;

	// get the identity
	if( (diag = determine_identity()) != Diag::OK){
		LOG(ERROR) << "Got an error from determining identity. Returning ...";
		return diag;
	}
	return diag;
}

/**
 * Determines the identity of the spy
 *
 * @return Diag::OK if everything went well
 */
Diag Spy::determine_identity() {
	Diag diag = Diag::OK;

	if ((diag = id.determine()) != Diag::OK) {
		LOG(ERROR) << "Can't get the identity. Returning ...";
		return diag;
	}

	return diag;
}

/**
 * @return getter for the identity
 */
Identity Spy::get_identity(){
	return id;
}
/**
 * setter for the identity
 * @param new_id
 * @return
 */
Diag Spy::set_identity(const Identity & new_id){
	id = new_id;

	return Diag::OK;
}
// -------------------------------------------------------------------
// Proc_Acquisitor
// -------------------------------------------------------------------
/**
 * Closes the stream
 */
Proc_Acquisitor::~Proc_Acquisitor(){
	proc_stream.close();
}
/**
 * @return Diag::OK if everything went great
 *         != Diag::OK if the stream can't be open
 */
Diag Proc_Acquisitor::init(){
	Diag diag = Diag::OK;

	proc_stream.open(stream_name.c_str(), std::ifstream::in);
	if( !proc_stream.is_open() ){
		LOG(ERROR) << "Issues with opening " << stream_name << " ATS";
		return Diag::ERR;
	}

	return diag;
}
/**
 * checks if the acquisitor has been initialized
 *
 * @return true initialized
 *         false not initialized
 */
bool Proc_Acquisitor::is_initialized(){
	return proc_stream.is_open();
}

} /* namespace cw */

