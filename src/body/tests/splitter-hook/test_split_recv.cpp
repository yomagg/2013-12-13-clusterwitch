/**
 * test_split_recv.cpp
 *
 *  Created on: Aug 8, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>

#include "easylogging++.h"

#include "evpath.h"

#include "data.h"

_INITIALIZE_EASYLOGGINGPP

using namespace std;
//using namespace cw;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	// TODO what exactly should this handler return? 0 (original test) or 1
	// (see e.g. the evpath test)
    return 0;
}

int main(int argc, char **argv){

	if (2 != argc){
		cout << "Usage: " << argv[0] << " how-long-to-run\n";
		cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
		cout << "Eg. " << argv[0] << " 600\n";
		return 0;
	}

	int status = 0;;
	CManager cm = CManager_create();
	CMlisten(cm);

	// prepare for receiving complex_rec
	EVstone complex_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, complex_stone, complex_format_list, complex_handler, nullptr);

	char *contact = attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "Run: CMSelfFormats=1 ./test_split_send " << complex_stone << ":" << contact << "\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));

	CManager_close(cm);

	return status;

}
