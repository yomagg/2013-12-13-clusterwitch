# @file readme.txt
# @date Created on: Aug 8, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

PURPOSE
=======
Tests if Ev_Splitter_Hook works.

DESCRIPTION
============
The sender creates a source that connects to the splitter.
Then based on the input parameters it creates as many
output stones as parameters and connects the output
stones to the splitter. Eventually it the source submits
the complex_rec event. It should be received by all active
receivers.

The sender also outputs its contact info, i.e., the
contact info of the splitter stone to a file. This file 
should be removed upon completion.

The receivers just output their contact information
and wait for the incoming events.

BUILD
=====
It is build with all tests

RUN
====
First run and it will tell you how to run the ./test_split_send

$ ./test_split_recv

You can run as many receivers as you want.