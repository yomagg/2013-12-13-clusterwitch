/**
 * test_split_send.cpp
 *
 *  Created on: Aug 8, 2014
 *      Author: magg
 */
#include <iostream>
#include <vector>
#include <sstream>

#include "easylogging++.h"

#include "evpath.h"

#include "data.h"
#include "transport.h"

_INITIALIZE_EASYLOGGINGPP

using namespace std;

int main(int argc, char *argv[]){
	CManager cm = cw::Ev_Topo::get_CM_now();

//	CManager cm = CManager_create();
//	CMfork_comm_thread(cm);
//	CMlisten(cm);

	if (argc < 2) {
		cout << "USAGE:\n";
		cout << argv[0] << " remote-stone-id1:contact-string remote-stone-id2:contact-string ...\n";
		cout << "EX. " << argv[0] << " 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA= 1:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";

		return 0;
	}

	const string CONTACT_FILE="my_hook.txt";
	cw::Ev_Splitter_Hook hook(CONTACT_FILE.c_str());


	if( hook.create_splitter_hook(complex_format_list, string("key")) != cw::Diag::OK ){
		cout << "ERROR: problems with creating the splitter hook\n";
		return 1;
	}

	attr_list contact_list = nullptr;
	vector<EVstone> output_stone_ids;

	for(int i = 1; i < argc; ++i){
		string contact_info = argv[i];
		stringstream strm(contact_info);
		EVstone remote_stone_id;
		char delim;
		strm >> remote_stone_id >> delim;
		stringstream remote_stone_contact;

		strm.get(* remote_stone_contact.rdbuf());

		// create a local output stone and connect this stone with the
		// remote stone
		EVstone output_stone_id = EValloc_stone(cm);
		contact_list = attr_list_from_string(remote_stone_contact.str().c_str());

		// remember the output stone id
		output_stone_ids.push_back(output_stone_id);

		EVassoc_bridge_action(cm, output_stone_id, contact_list, remote_stone_id);
		// the below call adds the output stone to the list of stones
		// to which the split stone will replicate data. The call requires
		// the stone to which the split action was registered and the
		// split action that was returned
		EVaction_add_split_target(cm, hook.get_split_stone(), hook.get_split_action(), output_stone_id);

		cout << "Output stone id: " << output_stone_id << ", created and added as a split target\n";
		free_attr_list(contact_list);
	}

	struct complex_rec rec = {15.1, 16.3};
	cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

	// submit an event to be received by a remote stone
	EVsubmit(hook.get_src(), &rec, nullptr);

	remove(CONTACT_FILE.c_str());

	return 0;
}
