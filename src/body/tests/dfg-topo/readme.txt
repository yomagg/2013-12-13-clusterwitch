# @file readme.txt
# @date Created on Apr 10, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

GENERAL
========
Running the example:

# run the master - the creator of the DFG structure
# see for options
./dfg_master

# see for options
./dfg_client

RUNNING
=======
# requires configuration files present in the same directory as executables

* a-b have config files for the topo a->b
* a-b-c a->b->c
* adbf for c->b;c->b; d->b; d->e

links.txt - the links between nodes
nodes.txt - the name of the DFG nodes
stones.txt - the name of the DFG stones that will reside in DFG nodes

# first run the master
./dfg_master

# run the dfg_client (start with the sink, follow by source/sink, end with the src)
./dfg_client options
./dfg_client options

Indicators of the termination of the description:

src_end  
sink_end

NOTES
=======
# this is the asynchronous client (each spy runs at will, their time stamps
# are not synchronized in contrast to dfg_client)
./dfg_aclient

# EOF
