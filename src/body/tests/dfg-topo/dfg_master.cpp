/**
 *  @file   dfg_master.cpp
 *
 *  @date   Created on: Apr 2, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>

#include "easylogging++.h"
#include "evpath.h"

#include "misc.h"
#include "cw_runtime.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

int main(int argc, char *argv[]){
	if( argc != 1){
		cout << "USAGE\n" << argv[0] << "\n";
		cout << "The execution time duration is read from the config file: ";
		cout << Cfg::CFG_DEFAULT_NAME << " or default values are assigned if ";
		cout << " the config file can't be find\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << "\n";
		return 1;
	}

	#include "easylogging_default_cfg.h"
	Ev_Dfg_CW_Master_Runtime *rt = Ev_Dfg_CW_Master_Runtime::get_instance();

	Diag diag = Diag::OK;

	if( (diag = rt->create_dfg()) != Diag::OK) {
		LOG(ERROR) << "Can't create the dfg";
	} else {
		LOG(DEBUG) << "The dfg created";
	}


	int secs = Cfg_Dfg::get_dfg_master_running_time_sec();
	LOG(DEBUG) << argv[0] << ": Waiting for sinks and sources to join\n";

	if (Diag::OK != Ev_Topo::run(secs)){
		LOG(ERROR) << "Can't run the network";
		return 1;
	}

	LOG(DEBUG) << "About to exit ...\n";

	return 0;
}

