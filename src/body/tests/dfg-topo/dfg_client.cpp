/**
 * @file  dfg_client.cpp
 * @date  Sep 12, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>

#include "easylogging++.h"
#include "evpath.h"

#include "misc.h"
#include "cw_runtime.h"
#include "super_transport.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;


int main(int argc, char*argv[]){

	if( argc != 3){
		cout << "USAGE\n" << argv[0] << " type node-name\n";
		cout << "  type The type of the client: w- white (source), b- black (sink), g- gray (source and sink)\n";
		cout << "  node-name The name of the node\n";
		cout << "\n";
		cout << "The sink, src, and appropriate spies are concatenated\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " w node_a\n";
		cout << argv[0] << " g node_b\n";
		cout << argv[0] << " b node_c\n";

		return 1;
	}

	#include "easylogging_default_cfg.h"

	string cmd_line = argv[1];

	for(auto i=2; i < argc; ++i){
		cmd_line += " ";
		cmd_line += argv[i];
	}

	stringstream ss(cmd_line);

	string type;
	string node_name;

	ss >> type >> node_name;

	if (ss.fail()){
		LOG(ERROR) << "Can't read the input parameter";
		return 1;
	}

	Diag diag = Diag::OK;

	// it is important that atoms stay in the correct visibility region
	// they can't be in the local scope because the destructors are called
	Ev_Dfg_CW_Client_Runtime *rt = Ev_Dfg_CW_Client_Runtime::get_instance();

	Ev_Dfg_Super_White_Atom w_atom;
	Ev_Dfg_Super_Black_Atom b_atom;
	Ev_Dfg_Super_Gray_Atom g_atom;

	stone_names src_names;
	stone_names sink_names;

	src_names.cpu_stone_name = node_name + "_cpu_src";
	src_names.mem_stone_name = node_name + "_mem_src";
	src_names.net_stone_name = node_name + "_net_src";
	src_names.nvml_stone_name = node_name + "_nvml_src";

	sink_names.cpu_stone_name = node_name + "_cpu_sink";
	sink_names.mem_stone_name = node_name + "_mem_sink";
	sink_names.net_stone_name = node_name + "_net_sink";
	sink_names.nvml_stone_name = node_name + "_nvml_sink";

	if( type.compare("w") == 0){
		// white means source

		// register stones
		if ( (diag = w_atom.reg(node_name, src_names)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name << "can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		// one of the white atoms needs to join the node to DFG (each of
		// atoms needs to register individually)
		if( (diag = rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name <<
					" can't join the DFG. Reporting an error";
			return 1;
		}

		if ( (diag = w_atom.run()) != Diag::OK){
			LOG(ERROR) << "super can't run DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
	} else if ( type.compare("b") == 0){

		if ( (diag = b_atom.reg(node_name, sink_names)) != Diag::OK){
			LOG(ERROR) << "Can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		if ( (diag=rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name << " can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}
	} else if ( type.compare("g") == 0){

		if ( (diag = g_atom.reg(node_name, sink_names, src_names)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name << " can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}

		if ( (diag = rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name << " can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}
	}

	int secs = Cfg_Dfg::get_dfg_client_running_time_sec();

	//! run to give chance the master send you a command
	Ev_Topo::run(secs);
	LOG(DEBUG) << "Quitting ...";

	return 0;
}

