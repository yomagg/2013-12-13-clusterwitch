/**
 *  @file   dfg_aclient.cpp
 *
 *  @date   Created on: Apr 9, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


#include <iostream>

#include "easylogging++.h"
#include "evpath.h"

#include "misc.h"
#include "cw_runtime.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;


int main(int argc, char*argv[]){

	if( argc != 3){
		cout << "USAGE\n" << argv[0] << " type node-name\n";
		cout << "  type The type of the client: w- white (source), b- black (sink), g- gray (source and sink)\n";
		cout << "  node-name The name of the node\n";
		cout << "\n";
		cout << "The sink, src, and appropriate spies are concatenated\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " w node_a\n";
		cout << argv[0] << " g node_b\n";
		cout << argv[0] << " b node_c\n";

		return 1;
	}

	#include "easylogging_default_cfg.h"

	string cmd_line = argv[1];

	for(auto i=2; i < argc; ++i){
		cmd_line += " ";
		cmd_line += argv[i];
	}

	stringstream ss(cmd_line);

	string type;
	string node_name;

	ss >> type >> node_name;

	if (ss.fail()){
		LOG(ERROR) << "Can't read the input parameter";
		return 1;
	}

	Diag diag = Diag::OK;

	// it is important that atoms stay in the correct visibility region
	// they can't be in the local scope because the destructors are called
	Ev_Dfg_CW_Client_Runtime *rt = Ev_Dfg_CW_Client_Runtime::get_instance();

	Ev_Dfg_Cpu_White_Atom cpu_white_atom;
	Ev_Dfg_Mem_White_Atom mem_white_atom;
	Ev_Dfg_Net_White_Atom net_white_atom;
#ifdef NVML_PRESENT
	Ev_Dfg_Nvml_White_Atom nvml_white_atom;
#endif
	Ev_Dfg_Cpu_Black_Atom cpu_black_atom;
	Ev_Dfg_Mem_Black_Atom mem_black_atom;
	Ev_Dfg_Net_Black_Atom net_black_atom;
#ifdef NVML_PRESENT
	Ev_Dfg_Nvml_Black_Atom nvml_black_atom;
#endif

	Ev_Dfg_Cpu_Gray_Atom cpu_gray_atom;
	Ev_Dfg_Mem_Gray_Atom mem_gray_atom;
	Ev_Dfg_Net_Gray_Atom net_gray_atom;
#ifdef NVML_PRESENT
	Ev_Dfg_Nvml_Gray_Atom nvml_gray_atom;
#endif

	string stone_name;
	string sink_name;
	string src_name;

	if( type.compare("w") == 0){
		// white means source

		// register stones
		stone_name = node_name + "_cpu_src";
		LOG(DEBUG) << stone_name;

		if ( (diag=cpu_white_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "Cpu can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
		stone_name = node_name + "_mem_src";
		LOG(DEBUG) << stone_name;

		if ( (diag=mem_white_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "mem can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		stone_name = node_name + "_net_src";
		LOG(DEBUG) << stone_name;
		if ( (diag = net_white_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "net can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

#ifdef NVML_PRESENT
		stone_name = node_name + "_nvml_src";
		LOG(DEBUG) << stone_name;
		if ( (diag = nvml_white_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "nvml can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
#endif

		// one of the white atoms needs to join the node to DFG (each of
		// atoms needs to register individually)
		if( (diag = rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "Node: " << node_name <<
					" can't join the DFG. Reporting an error";
			return 1;
		}

		// each of the atoms needs to run individually
		if ( (diag=cpu_white_atom.run()) != Diag::OK){
			LOG(ERROR) << "mem can't running DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		if ( (diag=mem_white_atom.run()) != Diag::OK){
			LOG(ERROR) << "mem can't running DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		if ( (diag=net_white_atom.run()) != Diag::OK){
			LOG(ERROR) << "net can't running DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
#ifdef NVML_PRESENT
		if ( (diag=nvml_white_atom.run()) != Diag::OK){
			LOG(ERROR) << "nvml can't running DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
#endif
	} else if ( type.compare("b") == 0){
		stone_name = node_name + "_cpu_sink";
		LOG(DEBUG) << stone_name;
		if ( (diag=cpu_black_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "Cpu can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		stone_name = node_name + "_mem_sink";
		LOG(DEBUG) << stone_name;
		if ( (diag=mem_black_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "Mem can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}

		stone_name = node_name + "_net_sink";
		LOG(DEBUG) << stone_name;
		if ( (diag=net_black_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "net can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
#ifdef NVML_PRESENT
		stone_name = node_name + "_nvml_sink";
		LOG(DEBUG) << stone_name;
		if ( (diag=nvml_black_atom.reg(node_name, stone_name)) != Diag::OK){
			LOG(ERROR) << "nvml can't register in DFG: ec=" << static_cast<int>(diag);
			return 1;
		}
#endif

		if ( (diag=rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "nvml can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}
	} else if ( type.compare("g") == 0){

		 sink_name = node_name + "_cpu_sink";
		 src_name = node_name + "_cpu_src";
		 LOG(DEBUG) << sink_name << " " << src_name;

		 if ( (diag=cpu_gray_atom.reg(node_name, sink_name, src_name)) != Diag::OK){
			LOG(ERROR) << "Cpu can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}

		sink_name = node_name + "_mem_sink";
		src_name = node_name + "_mem_src";
		LOG(DEBUG) << sink_name << " " << src_name;

		if ( (diag=mem_gray_atom.reg(node_name, sink_name, src_name)) != Diag::OK){
			LOG(ERROR) << "mem can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}

		sink_name = node_name + "_net_sink";
		src_name = node_name + "_net_src";
		LOG(DEBUG) << sink_name << " " << src_name;

		if ( (diag=net_gray_atom.reg(node_name, sink_name, src_name)) != Diag::OK){
			LOG(ERROR) << "net can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}

#ifdef NVML_PRESENT
		sink_name = node_name + "_nvml_sink";
		src_name = node_name + "_nvml_src";
		LOG(DEBUG) << sink_name << " " << src_name;

		if ( (diag=nvml_gray_atom.reg(node_name, sink_name, src_name)) != Diag::OK){
			LOG(ERROR) << "nvml can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}
#endif
        // one atom needs to join the node to dfg
		if ( (diag=rt->run_dfg(node_name)) != Diag::OK){
			LOG(ERROR) << "nvml can't join the topo: ec=" << static_cast<int>(diag);
			return 1;
		}
	}

	int secs = Cfg_Dfg::get_dfg_client_running_time_sec();

	//! run to give chance the master send you a command
	Ev_Topo::run(secs);
	LOG(DEBUG) << "Quitting ...";

	return 0;
}

