# @file readme.txt
# @date Created on Jan 6, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

DESCRIPTION
===========

The EVPath libraries and libcw need to be visible to test files
(set LD_LIBRARY_PATH accordingly)

* test_main - a bunch of test cases; a good reference to see how to use CW
* basic-master-worker - testing basic connectivity
* ctrl-messaging-master-worker -test sending control message
* mon-agg - tests the class Ev_Monitoring_Atom and Ev_Aggregator_Atom
* c_api - testing the C api (for dfg and non-dfg)
* dfg-topo - tests for playing with the DFG topology
* splitter-hook - tests for checking if the splitter hook works correctly

TEST CHECKLIST
=============
As of 2014-09-15: 

* ./test_main
* ./dfg_master ; ./dfg_client
* ./test_split_recv; ./test_split_send
* ./test_c_api_spy_agent_utils
* ./test_c_api_dfg (read the c_api/test_c_api_dfg.c how to run the test; 
   requires dfg_master and dfg_client running)


# EOF
