/**
 *  @file   test_mon.cpp
 *
 *  @date   Created on: Jan 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


// classes that need to be tested
#include <iostream>

#include "easylogging++.h"

#include "evpath.h"

#include "misc.h"
#include "cw_runtime.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

int main(int argc, char*argv[]){

	if( argc < 3){
		cout << "USAGE\n" << argv[0] << " ini-file-name how-long-in-sec\n";
		cout << "  ini-file-name The name of the ini file\n";
		cout << "  how-long-in-sec How long in seconds it should run\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " my-test.ini 30\n";
		return 1;
	}

#include "easylogging_default_cfg.h"
	Diag diag = Diag::OK;
	Ev_CW_Mon_Runtime *rt = Ev_CW_Mon_Runtime::get_instance();


	string ini = argv[1];

	if ( (diag=rt->read_aggregator_contact_list(ini)) != Diag::OK ){
		LOG(ERROR) << "Can't get the contact aggreg contact list: ec=" << static_cast<int>(diag);
		return 1;
	}

	Ev_Cpu_Monitoring_Atom cpu_mon_atom;
	if ( (diag=cpu_mon_atom.join_topo()) != Diag::OK){
		LOG(ERROR) << "Cpu can't join the topo: ec=" << static_cast<int>(diag);
				return 1;
	}

	Ev_Mem_Monitoring_Atom mem_mon_atom;
	if ( (diag=mem_mon_atom.join_topo()) != Diag::OK){
		LOG(ERROR) << "Mem can't join the topo: ec=" << static_cast<int>(diag);
		return 1;
	}

	Ev_Net_Monitoring_Atom net_mon_atom;
	if ( (diag=net_mon_atom.join_topo()) != Diag::OK){
		LOG(ERROR) << "Net can't join the topo: ec=" << static_cast<int>(diag);
		return 1;
	}

#ifdef NVML_PRESENT
	Ev_Nvml_Monitoring_Atom nvml_mon_atom;
	if ( (diag=nvml_mon_atom.join_topo()) != Diag::OK){
		LOG(ERROR) << "Nvml can't join the topo: ec=" << static_cast<int>(diag);
		return 1;
	}
#endif // NVML_PRESENT

	stringstream ss(argv[2]);
	int secs = 0;
	ss >> secs;
	if (ss.fail()){
		LOG(ERROR) << "Can't read the input parameter";
		return 1;
	}

	//! run to give chance the master send you a command
	Ev_Topo::run(secs);
	LOG(DEBUG) << "Quitting ...";

	return 0;
}
