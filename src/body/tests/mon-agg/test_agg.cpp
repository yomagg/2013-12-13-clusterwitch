/**
 *  @file   test_agg.cpp
 *
 *  @date   Created on: Jan 14, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


// classes that need to be tested
#include <iostream>

#include "easylogging++.h"
#include "evpath.h"

#include "misc.h"
#include "cw_runtime.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

int main(int argc, char*argv[]){

	if( argc < 3){
		cout << "USAGE\n" << argv[0] << " ini-file-name how-long-in-sec\n";
		cout << "  ini-file-name The name of the ini file\n";
		cout << "  how-long-in-sec How long in seconds it should run\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " my-test.ini 30\n";
		return 1;
	}

#include "easylogging_default_cfg.h"

	string ini = argv[1];

	string my_contact;
	if( Ev_Utils::get_my_contact(my_contact) != Diag::OK){
		LOG(ERROR) << "Can't get my contact";
		return 1;
	}

	if( Cfg::wrt_val(ini, my_contact,Cfg::CFG_EV_MASTER_SEC, Cfg::CFG_EV_MASTER_SEC_CONTACT_KEY) != Diag::OK){
		LOG(ERROR) << "Can't write my contact";
		return 1;
	}

	Ev_CW_Agg_Runtime *rt = Ev_CW_Agg_Runtime::get_instance();


	stringstream ss(argv[2]);
	int secs = 0;
	ss >> secs;
	if (ss.fail()){
		LOG(ERROR) << "Can't read the input parameter";
		return 1;
	}

	cout << "Waiting for alive messages from the monitoring atoms\n";

	if (Diag::OK != Ev_Topo::run(secs)){
		LOG(ERROR) << "Can't run the network";
		return 1;
	}

	LOG(DEBUG) << "About to exit ...\n";

	if (remove(ini.c_str()) != 0 ){
		LOG(ERROR) << "Issues with removing the config file";
		return 1;
	}

	return 0;
}
