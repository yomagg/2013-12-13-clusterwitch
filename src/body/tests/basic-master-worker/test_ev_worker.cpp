/**
 *  @file   test_ev_worker.cpp
 *
 *  @date   Created on: Jan 6, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


#include "evpath.h"

// classes that need to be tested
#include <iostream>

#include "misc.h"
#include "cpu_spy.h"
#include "transport.h"

#include "easylogging++.h"

#include "evpath.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

//extern int master_cmd_handler(CManager cm, void *vevent, void *client_data, attr_list attrs);

int main(int argc, char*argv[]){

	if( argc < 2){
		cout << "USAGE\n" << argv[0] << " ini-file-name\n";
		cout << "  ini-file-name The name of the ini file\n\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " my-test.ini\n";
		return 1;
	}

#include "easylogging_default_cfg.h"

	string ini = argv[1];

	Ev_Worker_Atom atom(ini);
	Diag diag= Diag::OK;

	if( (diag=atom.run()) != Diag::OK){
			LOG(ERROR) << "Can't run the atom: ec=" << static_cast<int>(diag);
			return 1;
	}

	string my_contact;
	if( atom.get_my_contact(my_contact) != Diag::OK){
		LOG(ERROR) << "Errors with get_my_contact";
		return 1;
	} else {
		LOG(DEBUG) << "Contact: " << my_contact;
	}
	//! run forever
	Ev_Topo::run(20);
	LOG(DEBUG) << "Quitting ...";
	return 0;
}

