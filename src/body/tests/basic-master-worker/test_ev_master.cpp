/**
 *  @file   test_ev_master.cpp
 *
 *  @date   Created on: Jan 6, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include "evpath.h"

// classes that need to be tested
#include <iostream>

#include "misc.h"
#include "cpu_spy.h"
#include "transport.h"

#include "easylogging++.h"

#include "evpath.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

int main(int argc, char*argv[]){

	if( argc < 3){
		cout << "USAGE\n" << argv[0] << " ini-file-name how-long-in-sec\n";
		cout << "  ini-file-name The name of the ini file\n";
		cout << "  how-long-in-sec How long in seconds it should run\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << " my-test.ini 30\n";
		return 1;
	}

	// configuration for the logging
#include "easylogging_default_cfg.h"

	string ini = argv[1];

	Ev_Master_Atom m_atom;

	if(m_atom.run(ini) != Diag::OK){
		LOG(ERROR) << "Errors with running the network";
		return 1;
	}

	stringstream ss(argv[2]);
	int secs = 0;
	ss >> secs;
	if (ss.fail()){
		LOG(ERROR) << "Cant read the input parameter";
		return 1;
	}

	cout << "Waiting for workers\n";
	if (Diag::OK != Ev_Topo::run(secs)){
		LOG(ERROR) << "Can't run the network";
		return 1;
	}

	LOG(DEBUG) << "About to exit ...\n";

	if (remove(ini.c_str()) != 0 ){
		LOG(ERROR) << "Issues with removing the config file";
		return 1;
	}
	return 0;
}
