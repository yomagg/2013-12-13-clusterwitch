/**
 *  @file   test_main.cpp
 *
 *  @date   Created on: Dec 19, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include "test_main.h"
#include "gtest/gtest.h"

// classes that need to be tested
#include "misc.h"

#include "cpu_spy.h"
#include "mem_spy.h"
#include "net_spy.h"
#ifdef NVML_PRESENT
#include "nvml_spy.h"
#endif

#include "easylogging++.h"
_INITIALIZE_EASYLOGGINGPP

// comment to disable testing the evpath stuff; sometimes
// it has some issues because it is not properly stopped
// or multiple CM managers might be created in the same process;
// uncommenting this might be useful especially during debugging
#define ENABLE_EVPATH_STUFF

namespace cw {

// ------------------------------------------
// complex
// -------------------------------------------
struct complex_rec {
    double r;
    double i;
};

static FMField complex_field_list[] = {
    {"r", "double", sizeof(double), FMOffset(struct complex_rec*, r)},
    {"i", "double", sizeof(double), FMOffset(struct complex_rec*, i)},
    {nullptr, nullptr, 0, 0}
};

static FMStructDescRec complex_format_list[] = {
		{const_cast<char*>("complex_rec"), complex_field_list, sizeof(struct complex_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};



Test_Main::Test_Main() {
	// You can do set-up work for each test here.
}

Test_Main::~Test_Main() {
	// You can do clean-up work that doesn't throw exceptions here.
}

void Test_Main::SetUp() {
		// Code here will be called immediately after the constructor (right
	    // before each test).
}

void Test_Main::TearDown() {
		// Code here will be called immediately after each test (right
	    // before the destructor).
}

int get_core_count(){
	return sysconf(_SC_NPROCESSORS_ONLN);
}

// --------------------- Time_Stamp
TEST(Time_Stamp, Non_zero_timestamp){
	long long int ts = Time_Stamp::get_ts();

	EXPECT_LT(0,ts);   // 0 < ts
}
// ------------------- Procfs
/*TEST(Procfs_cpu, update_cpu_usage){
	Procfs_cpu procfs;
	std::vector<float> usage_vec;
	long long int ts;	// timestep
	int core_count = get_core_count();
	usage_vec.resize(core_count + 1);

	EXPECT_EQ(Diag::OK, procfs.update_cpu_usage(usage_vec, &ts));
	sleep(1);
	EXPECT_EQ(Diag::OK, procfs.update_cpu_usage(usage_vec, &ts));
	sleep(1);
	EXPECT_EQ(Diag::OK, procfs.update_cpu_usage(usage_vec, &ts));
}*/

// -------------------- struct identity

TEST(identity, strdup){
	//char *str1 = strdup(nullptr); // this causes segfault
	char *str1 = strdup("");
	EXPECT_STREQ("", str1);

	free(str1);
}
TEST(identity, default_constrcutor){
	identity id;

	EXPECT_EQ(0, id.id);
	EXPECT_EQ(0, id.pid);
	EXPECT_EQ(nullptr, id.hostname);
}
TEST(identity, non_default_constructor_nullhostname){
	identity id(10, nullptr, 13);
	EXPECT_EQ(10, id.id);
	EXPECT_EQ(nullptr, id.hostname);
	EXPECT_EQ(13, id.pid);
}
TEST(identity, non_default_constructor_emptyhostname){
	identity id(10, "", 13);
	EXPECT_EQ(10, id.id);
	EXPECT_STREQ("", id.hostname);
	EXPECT_EQ(13, id.pid);
}

TEST(identity, non_default_constructor_regular){
	identity id(10, "heffalin", 13);
	EXPECT_EQ(10, id.id);
	EXPECT_STREQ("heffalin", id.hostname);
	EXPECT_EQ(13, id.pid);
}

TEST(identity, copy_constructor){
	identity id1(12, "heffalin", 14);
	identity id2(id1);

	EXPECT_EQ(12, id2.id);
	EXPECT_STREQ("heffalin", id2.hostname);
	EXPECT_EQ(14, id2.pid);
}

TEST(identity, assignment_operator){
	identity id1(12, "heffalin", 14);
	identity id2;

	// initial values of id2
	EXPECT_EQ(0, id2.id);
	EXPECT_EQ(nullptr, id2.hostname);
	EXPECT_EQ(0, id2.pid);

	// now do an assignment
	id2 = id1;

	EXPECT_EQ(12, id2.id);
	EXPECT_STREQ("heffalin", id2.hostname);
	EXPECT_EQ(14, id2.pid);
}
// -------------------- ev_cpu_mon
TEST(ev_cpu_mon, dflt_constructor){
	ev_cpu_mon rec;

	EXPECT_EQ(0, rec.id);
	EXPECT_EQ(0, rec.ts);
	EXPECT_EQ(0, rec.core_plus_one_count);
	EXPECT_EQ(nullptr, rec.cpu_and_core_usage);
}

TEST(ev_cpu_mon, copy_constr){
	ev_cpu_mon rec;
	rec.id = 12;
	rec.ts = 13;
	rec.core_plus_one_count = 3;
	float arr[3];
	rec.cpu_and_core_usage = arr;


	ev_cpu_mon rec2(rec);
	EXPECT_EQ(rec.id, rec2.id);
	EXPECT_EQ(rec.ts, rec2.ts);
	EXPECT_EQ(rec.core_plus_one_count, rec2.core_plus_one_count);
	for(int i = 0; i < rec.core_plus_one_count; ++i){
		EXPECT_EQ(rec.cpu_and_core_usage[i], rec2.cpu_and_core_usage[i]);
	}
	// since it was allocated
	delete []rec2.cpu_and_core_usage;
}

TEST(ev_cpu_mon, assignement_op){
	ev_cpu_mon rec;
	rec.id = 12;
	rec.ts = 13;
	rec.core_plus_one_count = 3;
	float arr[3] = {1,2,3};
	rec.cpu_and_core_usage = arr;

	ev_cpu_mon rec2;

	float arr2[3];
	rec2.cpu_and_core_usage = arr2;
	rec2 = rec;

	EXPECT_EQ(rec.id, rec2.id);
	EXPECT_EQ(rec.ts, rec2.ts);
	EXPECT_EQ(rec.core_plus_one_count, rec2.core_plus_one_count);
	for(int i = 0; i < rec.core_plus_one_count; ++i){
		EXPECT_EQ(rec.cpu_and_core_usage[i], rec2.cpu_and_core_usage[i]);
	}
}

// ------------- Cpu_Acquisitor
TEST(Cpu_Acquisitor, get_caps){
	Cpu_Acquisitor acq;
	ev_cpu_cap rec;

	EXPECT_TRUE(acq.get(rec) == Diag::OK);
	EXPECT_GT(rec.core_count, 0.0);

	EXPECT_GT(rec.ts, 0);

	// no identity is expected to be set
	EXPECT_EQ(rec.id.id, 0);
	EXPECT_EQ(rec.id.pid, 0);
	EXPECT_EQ(rec.id.hostname, nullptr);
}

TEST(Cpu_Acquisitor, get_mon){
	Cpu_Acquisitor acq;
	ev_cpu_cap rec;

	EXPECT_TRUE(acq.get(rec) == Diag::OK);
	EXPECT_GT(rec.core_count, 0.0);

	EXPECT_GT(rec.ts, 0);

	// no identity is expected to be set
	EXPECT_EQ(rec.id.id, 0);
	EXPECT_EQ(rec.id.pid, 0);
	EXPECT_EQ(rec.id.hostname, nullptr);

	float arr[100];
	ev_cpu_mon mon;
	mon.cpu_and_core_usage = arr;
	EXPECT_TRUE(acq.get(mon) == Diag::OK);

	EXPECT_EQ(get_core_count()+1, mon.core_plus_one_count);
	EXPECT_GT(mon.ts, 0);
	// no id expected to be set
	EXPECT_EQ(mon.id, 0);

	for(int i = 0; i < mon.core_plus_one_count; ++i){
		EXPECT_GE(mon.cpu_and_core_usage[i], 0.0);
		EXPECT_LE(mon.cpu_and_core_usage[i], 100.0);
	}
}

// ------------------------ Contact_File
TEST(Contact_File, wrt_read){

	// 1. write and read
	struct Ev_Contact ct;
	ct.contact = "AAIAAJTJ8o3vZQAAATkCmGQyPYA=";
	ct.split_action = 13;
	ct.split_stone = 15;

	string contact_file="contact_file.txt";
	ASSERT_TRUE(Contact_File::wrt_to(contact_file, ct) == Diag::OK);

	// now read
	struct Ev_Contact rd_ct = {"", 0, 0};  // zero everything
	ASSERT_TRUE(Contact_File::read_from(contact_file, rd_ct) == Diag::OK);

	EXPECT_EQ(ct.split_action, rd_ct.split_action);
	EXPECT_EQ(ct.split_stone, rd_ct.split_stone);
	EXPECT_STREQ(ct.contact.c_str(), rd_ct.contact.c_str());

	EXPECT_TRUE(remove(contact_file.c_str()) == 0);
}

TEST(Contact_File, read_from_no_file){
	string contact_file="contact_file.txt";
	// check that the file does not exists
	EXPECT_TRUE(remove(contact_file.c_str()) != 0);

	struct Ev_Contact rd_ct = {"", 0, 0};  // zero everything
	EXPECT_TRUE(Contact_File::read_from(contact_file, rd_ct) != Diag::OK);
}

TEST(Contact_File, rewrite_file){
	struct Ev_Contact ct;
	ct.contact = "AAIAAJTJ8o3vZQAAATkCmGQyPYA=";
	ct.split_action = 13;
	ct.split_stone = 15;

	string contact_file="contact_file.txt";
	ASSERT_TRUE(Contact_File::wrt_to(contact_file, ct) == Diag::OK);

	// now read
	struct Ev_Contact rd_ct = {"", 0, 0};  // zero everything
	ASSERT_TRUE(Contact_File::read_from(contact_file, rd_ct) == Diag::OK);

	ASSERT_EQ(ct.split_action, rd_ct.split_action);
	ASSERT_EQ(ct.split_stone, rd_ct.split_stone);
	ASSERT_STREQ(ct.contact.c_str(), rd_ct.contact.c_str());

	// now write again
	ct.contact = "BBIAAJTJ8o3vZQAAATkCmGQyPYA=";
	ct.split_action = 20;
	ct.split_stone = 32;
	ASSERT_TRUE(Contact_File::wrt_to(contact_file, ct) == Diag::OK);

	// and read again
	ASSERT_TRUE(Contact_File::read_from(contact_file, rd_ct) == Diag::OK);

	ASSERT_EQ(ct.split_action, rd_ct.split_action);
	ASSERT_EQ(ct.split_stone, rd_ct.split_stone);
	ASSERT_STREQ(ct.contact.c_str(), rd_ct.contact.c_str());

	EXPECT_TRUE(remove(contact_file.c_str()) == 0);
}

// ------------- Mem_Acquisitor
TEST(Mem_Acquisitor, get_caps){
	Mem_Acquisitor acq;

	ev_mem_cap cap;
	EXPECT_EQ(cap.mem_total_kB, 0);
	EXPECT_EQ(cap.swap_total_kB, 0);
	EXPECT_EQ(cap.ts, 0);
	EXPECT_EQ(cap.id.id, 0);
	EXPECT_EQ(cap.id.pid, 0);
	EXPECT_EQ(cap.id.hostname, nullptr);

	EXPECT_TRUE( acq.get_caps(cap) == Diag::OK);
	EXPECT_NE(cap.mem_total_kB, 0);
	// some disks might have 0 swap (e.g. my laptop)
	EXPECT_TRUE(cap.swap_total_kB >= 0);
	EXPECT_NE(cap.ts, 0);
	// no identity is expected to be set
	EXPECT_EQ(cap.id.id, 0);
	EXPECT_EQ(cap.id.pid, 0);
	EXPECT_EQ(cap.id.hostname, nullptr);
}
TEST(Mem_Acquisitor, get_mon){
	Mem_Acquisitor acq;
	ev_mem_cap cap;

	// get caps should be invoked before get_mon
	EXPECT_TRUE( acq.get_caps(cap) == Diag::OK);
	EXPECT_NE(cap.mem_total_kB, 0);
	// some disks might have 0 swap (e.g. my laptop)
	EXPECT_TRUE(cap.swap_total_kB >= 0);

	ev_mem_mon mon;
	EXPECT_TRUE( acq.get_mon(mon) == Diag::OK);

	EXPECT_GE(mon.mem_util_perc, 0.0);
	EXPECT_LE(mon.mem_util_perc, 100.0);

	EXPECT_GE(mon.mem_buffers_util_perc, 0.0);
	EXPECT_LE(mon.mem_buffers_util_perc, 100.0);

	EXPECT_GE(mon.mem_cached_util_perc, 0.0);
	EXPECT_LE(mon.mem_cached_util_perc, 100.0);

	EXPECT_GE(mon.swap_cached_perc, 0.0);
	EXPECT_LE(mon.swap_cached_perc,100.0);

	EXPECT_GE(mon.mem_active_util_perc, 0.0);
	EXPECT_LE(mon.mem_active_util_perc, 100.0);

	EXPECT_GE(mon.mem_inactive_util_perc, 0.0);
	EXPECT_LE(mon.mem_inactive_util_perc, 100.0);
	EXPECT_GE(mon.swap_util_perc, 0.0);
	EXPECT_LE(mon.swap_util_perc, 100.0);
	EXPECT_GE(mon.mapped_perc, 0.0);
	EXPECT_LE(mon.mapped_perc, 100.0);
	EXPECT_GE(mon.slab_util_perc,0.0);
	EXPECT_LE(mon.slab_util_perc,100.0);
}
// ------------------- Processing_Functions
TEST(Processing_Functions, percentage){
	EXPECT_EQ(10.0, Processing_Functions::percentage(10, 1));
	EXPECT_EQ(-10.0, Processing_Functions::percentage(10, -1));
	EXPECT_EQ(10.0, Processing_Functions::percentage(-10, -1));
	EXPECT_EQ(-20.0, Processing_Functions::percentage(-10, 2));

}
// ------------------- Characteristics
TEST(Characteristics, percentage){
	EXPECT_EQ(10.0, Characteristics::utilization(10, 1));
	EXPECT_EQ(0.0, Characteristics::utilization(10, -1));
	EXPECT_EQ(0.0, Characteristics::utilization(-10, -1));
	EXPECT_EQ(0.0, Characteristics::utilization(-10, 3.0));

	EXPECT_EQ( 400.0 , Characteristics::utilization(100.0, 400.0));
}
// ------------ ev_nic_usage
TEST(ev_nic_usage, assignment_op){
	ev_nic_usage nic;
	string iface_name = "eth0";
	nic.name = strdup(iface_name.c_str());
	nic.combined_usage = 13.0;
	nic.received = 12.0;
	nic.transmitted = 11.0;

	ev_nic_usage nic2;
	nic2 = nic;
	EXPECT_STREQ(iface_name.c_str(), nic2.name);
	EXPECT_STREQ(nic.name, nic2.name);
	EXPECT_EQ(nic.combined_usage, nic2.combined_usage);
	EXPECT_EQ(nic.received, nic2.received);
	EXPECT_EQ(nic.transmitted, nic2.transmitted);

}

// ------------Net_Acquisitor::procfs_net_fields
TEST(procfs_net_fields_common, deflt_constructor){
	Net_Acquisitor::procfs_net_fields_common r;

	EXPECT_EQ(0, r.bytes);
	EXPECT_EQ(0, r.packets);
	EXPECT_EQ(0, r.errs);
	EXPECT_EQ(0, r.drop);
	EXPECT_EQ(0, r.fifo);
	EXPECT_EQ(0, r.compressed);
}
// ------------Net_Acquisitor::procfs_net
TEST(procfs_net, deflt_constructor){
	Net_Acquisitor::procfs_net r;

	EXPECT_EQ(0, r.ts);
}
TEST(procfs_net, assignment_op){
	Net_Acquisitor::procfs_net r1, r2;

	r1.receive.bytes = 1;
	r1.transmit.bytes = 3;
	r1.ts = 134;

	r2 = r1;

	EXPECT_EQ(r1.receive.bytes, r2.receive.bytes);
	EXPECT_EQ(r1.transmit.bytes, r2.transmit.bytes);
	EXPECT_EQ(r1.ts, r2.ts);
}
// ------------- Net_Acquisitor
TEST(Net_Acquisitor, get_caps){
	Net_Acquisitor acq;

	ev_net_cap cap;
	EXPECT_EQ(cap.nics_count, 0);
	EXPECT_EQ(cap.nics_names, nullptr);
	EXPECT_EQ(cap.ts, 0);
	EXPECT_EQ(cap.id.id, 0);
	EXPECT_EQ(cap.id.pid, 0);
	EXPECT_EQ(cap.id.hostname, nullptr);
	// make sure that the caps will be set the get_caps function
	cap.nics_count = -1;

	EXPECT_TRUE( acq.get_caps(cap) == Diag::OK);
	// some systems might not have the ib or eth
	EXPECT_GE(cap.nics_count, 0);
	EXPECT_NE(nullptr, cap.nics_names);

	EXPECT_NE(cap.ts, 0);
	// no identity is expected to be set
	EXPECT_EQ(cap.id.id, 0);
	EXPECT_EQ(cap.id.pid, 0);
	EXPECT_EQ(cap.id.hostname, nullptr);
	//LOG(DEBUG) << cap;
	cout << "Nics count: " << cap.nics_count << "\n";
	cout << "Nics names:";
	for(int i = 0 ; i < cap.nics_count ; ++i){
		cout << " " << cap.nics_names[i];
	}
	cout << "\n";
}
TEST(Net_Acquisitor, get_mon){
	Net_Acquisitor acq;

	ev_net_cap caps;
	ev_net_mon mon;

	EXPECT_EQ(Diag::OK, acq.get_caps(caps));
	EXPECT_EQ(0, mon.id);
	EXPECT_EQ(0, mon.ts);
	EXPECT_EQ(0, mon.nics_count);
	EXPECT_EQ(nullptr, mon.nics_usage_arr);

	mon.nics_count = caps.nics_count;
	mon.nics_usage_arr = new ev_nic_usage [mon.nics_count];
	EXPECT_EQ(Diag::OK, acq.get_mon(mon));
	// no changed in id
	EXPECT_EQ(0, mon.id);
	EXPECT_GT(mon.ts, 0);
	long long int ts = mon.ts;

	cout << "First mon:\n" <<  mon << "\n";
	sleep(1);

	EXPECT_EQ(Diag::OK, acq.get_mon(mon));
	EXPECT_EQ(0, mon.id);
	EXPECT_GT(mon.ts, ts);
	cout << "Second mon:\n" <<  mon << "\n";
}

#ifdef NVML_PRESENT
// ------------------ struct ev_nvml_dev_cap
TEST(ev_nvml_dev_cap, default_constructor){
	ev_nvml_dev_cap cap;

	EXPECT_EQ(nullptr, cap.product_name);
	EXPECT_EQ(nullptr, cap.serial_number);
	EXPECT_EQ(-1, cap.compute_mode);
	EXPECT_EQ(0, cap.id);
	EXPECT_EQ(0, cap.mem_total_bytes);
	EXPECT_EQ(0, cap.power_limit_mW);
	EXPECT_EQ(0, cap.max_graphics_clock_MHz);
	EXPECT_EQ(0, cap.max_SM_clock_MHz);
	EXPECT_EQ(0, cap.max_memory_clock_MHz);
}
TEST(ev_nvml_dev_cap, copy_constructor){
	ev_nvml_dev_cap c1;
	c1.product_name = c1.copy_str("PRODUCT");
	c1.serial_number = c1.copy_str("SERIAL");
	c1.compute_mode = 1;
	c1.id = 1234;
	c1.mem_total_bytes = 1123;
	c1.power_limit_mW = 1000;
	c1.max_graphics_clock_MHz = 150;
	c1.max_SM_clock_MHz = 150;
	c1.max_memory_clock_MHz = 150;

	ev_nvml_dev_cap c2(c1);
	EXPECT_STREQ(c2.product_name, "PRODUCT");
	EXPECT_STREQ(c2.serial_number, c1.serial_number);
	EXPECT_EQ(c2.compute_mode, c1.compute_mode);
	EXPECT_EQ(c2.id, c1.id);
	EXPECT_EQ(c2.mem_total_bytes, c1.mem_total_bytes);
	EXPECT_EQ(c2.power_limit_mW, c1.power_limit_mW);
	EXPECT_EQ(c2.max_graphics_clock_MHz, c1.max_graphics_clock_MHz);
	EXPECT_EQ(c2.max_SM_clock_MHz, c1.max_SM_clock_MHz);
	EXPECT_EQ(c2.max_memory_clock_MHz, c1.max_memory_clock_MHz);
	
	c1.free_str(c1.product_name);
	c1.free_str(c1.serial_number);
	c2.free_str(c2.product_name);
	c2.free_str(c2.serial_number);
}
TEST(ev_nvml_dev_cap, assigment_op){
	ev_nvml_dev_cap c1;
	
	c1.product_name = c1.copy_str("PRODUCT");
	c1.serial_number = c1.copy_str("SERIAL");
	c1.compute_mode = 1;
	c1.id = 1234;
	c1.mem_total_bytes = 1123;
	c1.power_limit_mW = 1000;
	c1.max_graphics_clock_MHz = 150;
	c1.max_SM_clock_MHz = 150;
	c1.max_memory_clock_MHz = 150;

	ev_nvml_dev_cap c2;

	c2 = c1;

	EXPECT_STREQ(c2.product_name, "PRODUCT");
	EXPECT_STREQ(c2.serial_number, c1.serial_number);
	EXPECT_EQ(c2.compute_mode, c1.compute_mode);
	EXPECT_EQ(c2.id, c1.id);
	EXPECT_EQ(c2.mem_total_bytes, c1.mem_total_bytes);
	EXPECT_EQ(c2.power_limit_mW, c1.power_limit_mW);
	EXPECT_EQ(c2.max_graphics_clock_MHz, c1.max_graphics_clock_MHz);
	EXPECT_EQ(c2.max_SM_clock_MHz, c1.max_SM_clock_MHz);
	EXPECT_EQ(c2.max_memory_clock_MHz, c1.max_memory_clock_MHz);
	
	c1.free_str(c1.product_name);
	c1.free_str(c1.serial_number);
	c2.free_str(c2.product_name);
	c2.free_str(c2.serial_number);
}
// --------------- ev_nvml_cap
TEST(ev_nvml_cap, default_constructor){
	ev_nvml_cap c;
	EXPECT_EQ(c.ts, 0);
	EXPECT_EQ(c.driver_ver, nullptr);
	EXPECT_EQ(0, c.gpu_count);
	EXPECT_EQ(nullptr, c.devs);
	EXPECT_EQ(0, c.id.id);
	EXPECT_EQ(nullptr, c.id.hostname);
	EXPECT_EQ(0, c.id.pid);
}
TEST(ev_nvml_cap, copy_constructur){

	ev_nvml_cap c1;
	
	c1.ts = 10;
	c1.driver_ver = strdup("329.18");
	c1.gpu_count = 1;
	c1.id.id = 10;
	c1.id.pid = 12;
	
	ev_nvml_cap c2(c1);

	EXPECT_EQ(c1.ts, c2.ts);
	EXPECT_STREQ(c1.driver_ver, c2.driver_ver);
	EXPECT_EQ(c1.gpu_count, c2.gpu_count);
	EXPECT_EQ(nullptr, c2.devs);
	EXPECT_EQ(c1.id.id, c2.id.id);
	EXPECT_EQ(nullptr, c2.id.hostname);
	EXPECT_EQ(c1.id.pid, c2.id.pid);
}
TEST(ev_nvml_cap, assignment_operator){

	ev_nvml_cap c1;
	
	c1.ts = 10;
	c1.driver_ver = strdup("329.18");
	c1.gpu_count = 1;
	c1.id.id = 10;
	c1.id.pid = 12;
	
	ev_nvml_cap c2;

	c2 = c1;

	EXPECT_EQ(c1.ts, c2.ts);
	EXPECT_STREQ(c1.driver_ver, c2.driver_ver);
	EXPECT_EQ(c1.gpu_count, c2.gpu_count);
	EXPECT_EQ(nullptr, c2.devs);
	EXPECT_EQ(c1.id.id, c2.id.id);
	EXPECT_EQ(nullptr, c2.id.hostname);
	EXPECT_EQ(c1.id.pid, c2.id.pid);
}
#endif

// ------------------------- Cfg
TEST(Cfg, wrt_read_string){
	string file_name = "cfg-ini.ini";
	string value = "value";
	string section = "section";
	string key = "key1";
	string read_val;

	// clear the file if it exists
	remove(file_name.c_str());

	EXPECT_EQ(Cfg::wrt_val(file_name, value, section,  key), Diag::OK);

	EXPECT_EQ(Cfg::read_val(file_name, read_val, section,  key), Diag::OK);
	EXPECT_EQ(read_val, value);

	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

TEST(Cfg, wrt_read_int){
	string file_name = "cfg-ini.ini";
	int value = 13;
	string section = "section";
	string key = "key2";
	int read_value = 0;

	EXPECT_EQ(Cfg::wrt_val(file_name, value, section,  key), Diag::OK);

	EXPECT_EQ(Cfg::read_val(file_name, &read_value, section,  key), Diag::OK);
	EXPECT_EQ(read_value, value);

	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

TEST(Cfg, combined_same_section){
	string file_name = "cfg-ini.ini";

	string section1 = "section";
	string key1 = "key1";
	int value1 = 13;

	string key2 = "key2";
	string value2 = "value";

	EXPECT_EQ(Cfg::wrt_val(file_name, value1, section1,  key1), Diag::OK);
	EXPECT_EQ(Cfg::wrt_val(file_name, value2, section1,  key2), Diag::OK);

	// now read it

	int read_value1 = 0;
	EXPECT_EQ(Cfg::read_val(file_name, &read_value1, section1,  key1), Diag::OK);
	EXPECT_EQ(read_value1, value1);

	string read_value2;

	EXPECT_EQ(Cfg::read_val(file_name, read_value2, section1,  key2), Diag::OK);
	EXPECT_EQ(read_value2, value2);
	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

TEST(Cfg, combined_different_sections){
	string file_name = "cfg-ini.ini";

	string section1 = "section1";
	string key1 = "key1";
	int value1 = 13;

	string section2 = "section2";
	string key2 = "key2";
	string value2 = "value";

	EXPECT_EQ(Cfg::wrt_val(file_name, value1, section1,  key1), Diag::OK);
	EXPECT_EQ(Cfg::wrt_val(file_name, value2, section2,  key2), Diag::OK);

	// now read it

	int read_value1 = 0;
	EXPECT_EQ(Cfg::read_val(file_name, &read_value1, section1,  key1), Diag::OK);
	EXPECT_EQ(read_value1, value1);

	string read_value2;

	EXPECT_EQ(Cfg::read_val(file_name, read_value2, section2,  key2), Diag::OK);
	EXPECT_EQ(read_value2, value2);
	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

// --------------------- Cfg_Dfg
TEST(Cfg_Dfg, cfg_creation){
	string file_name = Cfg::CFG_DEFAULT_NAME;

	remove(file_name.c_str());
	EXPECT_EQ(Cfg_Dfg::create_cfg_file(), Diag::OK);

	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

TEST(Cfg_Dfg, reading_from_created_cfg){
	string file_name = Cfg::CFG_DEFAULT_NAME;

	remove(file_name.c_str());
	EXPECT_EQ(Cfg_Dfg::create_cfg_file(), Diag::OK);

	EXPECT_EQ(Cfg_Dfg::get_monitoring_sampling_sec_rate(), Cfg::CFG_EV_MONITOR_PERIOD);
	EXPECT_EQ(Cfg_Dfg::get_monitoring_sampling_usec_rate(), Cfg::CFG_EV_MONITOR_PERIOD_USEC);
	string output_dir = Cfg_Dfg::get_sqlite3_reader_output_dir();
	EXPECT_EQ(Cfg::CFG_SQLITE3_READER_OUTPUT_DIR, output_dir);
	EXPECT_EQ(Cfg_Dfg::get_dfg_master_running_time_sec(), Cfg_Dfg::CFG_EV_DFG_MASTER_RUNNING_SEC);
	EXPECT_EQ(Cfg_Dfg::get_dfg_client_running_time_sec(), Cfg_Dfg::CFG_EV_DFG_CLIENT_RUNNING_SEC);


	// check that the file exists and remove it
	EXPECT_TRUE(remove(file_name.c_str()) == 0);
}

TEST(Cfg_Dfg, getting_from_no_cfg){
	string file_name = Cfg::CFG_DEFAULT_NAME;

	remove(file_name.c_str());

	EXPECT_EQ(Cfg_Dfg::get_monitoring_sampling_sec_rate(), Cfg::CFG_EV_MONITOR_PERIOD);
	EXPECT_EQ(Cfg_Dfg::get_monitoring_sampling_usec_rate(), Cfg::CFG_EV_MONITOR_PERIOD_USEC);


	string output_dir = Cfg_Dfg::get_sqlite3_reader_output_dir();
	EXPECT_EQ(Cfg::CFG_SQLITE3_READER_OUTPUT_DIR, output_dir);

	EXPECT_EQ(Cfg_Dfg::get_dfg_master_running_time_sec(), Cfg_Dfg::CFG_EV_DFG_MASTER_RUNNING_SEC);
	EXPECT_EQ(Cfg_Dfg::get_dfg_client_running_time_sec(), Cfg_Dfg::CFG_EV_DFG_CLIENT_RUNNING_SEC);
}


// it is recommended that the EVPATH stuff
// is tested at the end of; otherwise I am
// getting some errors probably because handling
// creating the CM is not appropriate
#ifdef ENABLE_EVPATH_STUFF

// ------------------------- struct ev_alive_mon_msg
TEST(ev_alive_mon_msg, default_constructor){
	ev_alive_mon_msg msg1;
	EXPECT_EQ(nullptr, msg1.contact);
	EXPECT_EQ(nullptr, msg1.stone_ids);
	EXPECT_EQ(0, msg1.stone_ids_count);
	EXPECT_EQ(Atom_Type::UNKNOWN, msg1.atom_type);
}
TEST(ev_alive_mon_msg, stones_count_constructor){
	ev_alive_mon_msg msg1(1);
	EXPECT_EQ(nullptr, msg1.contact);
	EXPECT_TRUE(nullptr != msg1.stone_ids);
	EXPECT_EQ(1, msg1.stone_ids_count);
	EXPECT_EQ(Atom_Type::UNKNOWN, msg1.atom_type);
}

// ------------------------- Ev_Topo
TEST(Ev_Topo, exists){
	EXPECT_FALSE(Ev_Topo::exists());
	Ev_Topo *p_topo = Ev_Topo::get_instance();
	EXPECT_TRUE(Ev_Topo::exists());
	ASSERT_TRUE( nullptr != p_topo);
}

TEST(Ev_Topo, singleton){
	// because previous test
	ASSERT_TRUE(Ev_Topo::exists());
	Ev_Topo *p_topo1 = Ev_Topo::get_instance();
	EXPECT_TRUE(nullptr != p_topo1);
	EXPECT_EQ(p_topo1, Ev_Topo::get_instance());
}

TEST(Ev_Topo, get_cm){
	ASSERT_TRUE(Ev_Topo::exists());
	Ev_Topo *p_topo = Ev_Topo::get_instance();
	ASSERT_TRUE(p_topo);
	ASSERT_TRUE(p_topo->get_CM());
}

// --------------------- Ev_Splitter_Hook
TEST(Ev_Splitter_Hook, Ev_Splitter_Hook){
	string contact_file = "my_hook.txt";
	Ev_Splitter_Hook hook(contact_file);

	EXPECT_TRUE(hook.get_split_stone() == Ev_Atom::INVALID_STONE_ID);
	EXPECT_TRUE(hook.get_split_action() == Ev_Atom::INVALID_ACTION_ID);
	EXPECT_TRUE(hook.get_src() == nullptr);
}

TEST(Ev_Splitter_Hook, get_contact_file_name){
	string contact_file = "my_hook.txt";

	Ev_Splitter_Hook hook(contact_file);

	EXPECT_EQ(hook.get_contact_file_name(), contact_file);
}

TEST(Ev_Splitter_Hook, create_splitter_hook){
	string contact_file = "my_hook.txt";

	// check that the file does not exist
	EXPECT_TRUE(remove(contact_file.c_str()) != 0);

	Ev_Splitter_Hook hook(contact_file);
	EXPECT_EQ(hook.create_splitter_hook(complex_format_list), Diag::OK);

	// check that the file does not exist
	EXPECT_TRUE(remove(contact_file.c_str()) == 0);
}

TEST(Ev_Splitter_Hook, create_splitter_hook_if_exists){
	string contact_file = "my_hook.txt";

	// check that the file does not exist
	EXPECT_TRUE(remove(contact_file.c_str()) != 0);

	Ev_Splitter_Hook hook(contact_file);
	EXPECT_EQ(hook.create_splitter_hook(complex_format_list), Diag::OK);

	EXPECT_EQ(hook.create_splitter_hook(complex_format_list), Diag::WARN);

	// check that the file does not exist
	EXPECT_TRUE(remove(contact_file.c_str()) == 0);
}

TEST(Ev_Splitter_Hook, is_hook_created){
	string contact_file = "my_hook.txt";

	// check that the file does not exist
	EXPECT_TRUE(remove(contact_file.c_str()) != 0);

	Ev_Splitter_Hook hook(contact_file);

	EXPECT_FALSE(hook.is_hook_created());

	EXPECT_EQ(hook.create_splitter_hook(complex_format_list), Diag::OK);

	EXPECT_TRUE(hook.is_hook_created());
	EXPECT_TRUE(hook.is_hook_created());

	// clean the contact file
	EXPECT_TRUE(remove(contact_file.c_str()) == 0);
}

// ------------------------Ev_Ctrl_Msg
// call them after topo tests (I am getting an instance to a topo here
// so if it doesn't exist it will be created; the first of topo
// assumes that the topo was not instantiated never
TEST(Ev_Ctrl_Msg, exists){
	EXPECT_FALSE(Ev_Ctrl_Msg::exists());
	Ev_Ctrl_Msg *p_msg = Ev_Ctrl_Msg::get_instance();
	ASSERT_TRUE(nullptr != p_msg);
	EXPECT_TRUE(Ev_Ctrl_Msg::exists());
}

TEST(Ev_Ctrl_Msg, singleton){
	ASSERT_TRUE(Ev_Ctrl_Msg::exists()); // because the previous test instantiated it
	Ev_Ctrl_Msg *p_msg = Ev_Ctrl_Msg::get_instance();
	EXPECT_TRUE(nullptr != p_msg);
	EXPECT_EQ(p_msg, Ev_Ctrl_Msg::get_instance());
}

// ------------------------- Ev_Master_Atom
TEST(Ev_Master_Atom, get_my_contact){
	Ev_Master_Atom atom;
	string my_contact;

	EXPECT_TRUE(Diag::OK == atom.get_my_contact(my_contact));
	ASSERT_TRUE(0 < my_contact.size());
	cout << "My contact: " << my_contact << "\n";
}

TEST(Ev_Master_Atom, wrt_and_read_my_contact){
	Ev_Master_Atom atom;
	string my_contact;

	EXPECT_TRUE(Diag::OK == atom.get_my_contact(my_contact));
	ASSERT_TRUE(0 < my_contact.size());
	string ini_file_name = "my_test.ini";
	atom.wrt_my_contact(ini_file_name, my_contact);
	cout << "Master contact: " << my_contact << "\n";

	// check if the file was created
	ifstream ini(ini_file_name);
	EXPECT_TRUE(ini.good());
	ini.close();

	Ev_Worker_Atom worker_atom;

	EXPECT_TRUE(Diag::OK == worker_atom.read_master_contact(ini_file_name));
	string read_contact(worker_atom.get_master_contact());
	cout << "Read contact: " << read_contact << "\n";
	EXPECT_STREQ(my_contact.c_str(), read_contact.c_str());

	// remove the ini file
	EXPECT_TRUE( remove(ini_file_name.c_str()) == 0);
}

TEST(Ev_Master_Atom, join_topo){
	Ev_Master_Atom m_atom;
	string my_contact;
	EXPECT_TRUE(Diag::OK == m_atom.get_my_contact(my_contact));
	string ini_file_name = "my_test.ini";

	EXPECT_TRUE(Diag::OK == m_atom.wrt_my_contact(ini_file_name, my_contact));
	cout << "Master contact: " << my_contact << "\n";

	// start the topology
	EXPECT_TRUE(Diag::OK == m_atom.join_topo());

	// now start the worker
	Ev_Worker_Atom w_atom(ini_file_name);
	string w_contact;
	EXPECT_TRUE(Diag::OK == w_atom.get_my_contact(w_contact));
	cout << "Worker contact: " << w_contact << "\n";
	EXPECT_EQ(Diag::OK, w_atom.join_topo());

	// clean the ini
	EXPECT_TRUE(remove(ini_file_name.c_str()) == 0);

}

#endif // #ifdef DISABLE_EVPATH_STUFF

} /* namespace cw */

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);

  el::Configurations defaultConf;
  defaultConf.setToDefault();
  defaultConf.setGlobally(el::ConfigurationType::Format, "%level %func:%loc %msg");
  el::Loggers::reconfigureLogger("default", defaultConf);

  return RUN_ALL_TESTS();
}
