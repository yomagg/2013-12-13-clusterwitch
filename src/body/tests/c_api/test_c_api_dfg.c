/**
 * test_c_api_dfg.c
 *
 *  Created on: Aug 12, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <stdio.h>

#include "spy_agent.h"

// this is the code that implements the handlers
#include "handlers.c"

/**
 * The scenario is that I have the a->b->c DFG graph deployed
 * and on
 * a : a_cpu_src, a_mem_src, a_net_src, a_nvml_src
 * b : b_cpu_sink, b_mem_sink, b_net_sink, b_nvml_sink
 *     b_cpu_src, b_mem_src, b_net_src, b_nvml_src
 * c : c_cpu_sink, c_mem_sink, c_net_sink, c_nvml_sink
 *
 * However, I want to get cpu and mem events from the node a
 * and I want to get net and nvml events from the node b
 *
 * To run the test:
 *
 * 1. First run the dfg topology with the above DFG configuration
 * 2. Then run this test. You should receive events cpu and mem events
 * from the a node and net and nvml events from the b node.
 *
 * @return 0 if everything went fine
 *         1 if something went wrong
 */
int main(){

	struct cw_mon_event ev;

	diag_t diag = DIAG_CW_OK;

	// first init cw
	if( (diag = cw_init()) != DIAG_CW_OK ){
		p_error("Issues with CW initialization\n");
		return 1;
	}

	// next specify what events are you interested in

	// get cpu events
	ev.attrs = NULL;
	ev.client_data = NULL;
	ev.handler = cpu_mon_handler;
	ev.id = cw_MON_EVENT_CPU;
	// on node "a" and from source "a_cpu_src"
	strcpy(ev.contact_node, "a");
	strcpy(ev.contact_stone, "a_cpu_src");

	if( cw_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

	// get men events
	ev.id = cw_MON_EVENT_MEM;
	ev.handler = mem_mon_handler;
	// on node "a" and from source "a_mem_src"
	strcpy(ev.contact_stone, "a_mem_src");

	if( cw_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

	// get net events
	ev.id = cw_MON_EVENT_NET;
	ev.handler = net_mon_handler;
	// on node "b" and from source "b_net_src"
	strcpy(ev.contact_node, "b");
	strcpy(ev.contact_stone, "b_net_src");

	if( cw_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

#ifdef NVML_PRESENT
	// get nvml events
	ev.id = cw_MON_EVENT_NVML;
	ev.handler = nvml_mon_handler;
	// on node "b" and from source "b_nvml_src"
	strcpy(ev.contact_stone, "b_nvml_src");

	if( cw_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}
#endif

	// next enable montioring
	if( cw_monitor(30) != DIAG_CW_OK ){
		p_error("Issues with enabling monitoring\n");
		return 1;
	}

	// close CW
	if( cw_close() != DIAG_CW_OK ){
		p_error("Issues with closing CW\n");
		return 1;
	}

	return 0;
}


