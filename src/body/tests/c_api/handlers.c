/**
 * handlers.c
 *
 *  Created on: Aug 12, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

/*
 * This file is a convenience file to concile the
 * non-redundancy requirement and keeping the relative simplicity.
 *
 * It should be included with the #include directive with the
 * test files that require those handlers.
 *
 */

static
int cpu_mon_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_cpu_mon *event =  (struct ev_cpu_mon *) vevent;
	// "TS:ID:CORE_COUNT:UTIL_AGG:UTIL_0:UTIL_1
	printf("CPU: %lld:%d:%d:UTIL_AGG:UTIL_0:UTIL_1:... ",
			event->ts, event->id, event->core_plus_one_count - 1);

	for(int i = 0; i < event->core_plus_one_count; ++i){
		printf(":%.2f", event->cpu_and_core_usage[i]);
	}
	printf("\n");

    return 1;
}

static
int mem_mon_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_mem_mon *event =  (struct ev_mem_mon *) vevent;
	// TS:ID:MEM_UTIL_P:MEM_BUF_UTIL_P:MEM_CACHED_UTIL_P:MEM_ACTIVE_UTIL_P:MEM_INACTIVE_UTIL_P:SLAB_UTIL_P:MAPPED_P:SWAP_CACHED_P:SWAP_UTIL_P
	printf("MEM: %lld:%d: UTIL:BUF:CACHED:ACTIVE:INACTIVE:SLAB:MAPPED:SWAP_CACHED:SWAP:"
			"%.2f:%.2f:%.2f:%.2f:%.2f:%.2f:%.2f:%.2f:%.2f\n",
			event->ts, event->id, event->mem_util_perc,
			event->mem_buffers_util_perc,
			event->mem_cached_util_perc,
			event->mem_active_util_perc,
			event->mem_inactive_util_perc,
			event->slab_util_perc,
			event->mapped_perc,
			event->swap_cached_perc,
			event->swap_util_perc);

    return 1;
}

static
int net_mon_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_net_mon *event =  (struct ev_net_mon *) vevent;
	// TS:ID:eth0:COMBINED:TSMITTED:RECV:1393443219462614:1953627505:0.00:0.00:0.00

	for(int i = 0; i < event->nics_count; ++i){
		printf("NET:%lld:%d:%s:%d:COMBINED:TSMITTED:RECV:%.2f:%.2f:%.2f\n",
					event->ts, event->id, event->nics_usage_arr[i].name,
					i,
					event->nics_usage_arr[i].combined_usage,
					event->nics_usage_arr[i].transmitted,
					event->nics_usage_arr[i].received);
	}

    return 1;
}

#ifdef NVML_PRESENT
static
int nvml_mon_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct ev_nvml_mon *event =  (struct ev_nvml_mon *) vevent;
	// TS:ID:GPU_COUNT 1394733587140426:02:PERF_STATE:MEM_USED_BYTES:NVML_UTIL_GPU_PERC:NVML_UTIL_MEM_PERC:MEM_UTIL:POWR_DRAW:GRAPHICS_CL:SM_CL:MEM_CL 1:6893568:0:0:0:0:0:0:200
	for(int i = 0; i < (int)event->gpu_count; ++i){
		printf("NVML:%lld:%d:%d:PERF_ST:MEM_USED_BYTES:NVML_UTIL_GPU_PERC:NVML_UTIL_MEM_PERC:UTIL_MEM_PERC:POWR_DRAW_PERC:G_CL_PERC:SM_CL_PERC:MEM_CL_PERC:"
				"%d:%lld:%d:%d:%.2f:%.2f:%.2f:%.2f:%.2f\n",
					event->ts, event->id, i,
					event->gpu_arr[i].performance_state,
					event->gpu_arr[i].mem_used_bytes,
					event->gpu_arr[i].nvml_util_gpu,
					event->gpu_arr[i].nvml_util_mem,
					event->gpu_arr[i].util_mem,
					event->gpu_arr[i].power_draw,
					event->gpu_arr[i].graphics_clock,
					event->gpu_arr[i].sm_clock,
					event->gpu_arr[i].mem_clock);
	}

    return 1;
}

#endif

