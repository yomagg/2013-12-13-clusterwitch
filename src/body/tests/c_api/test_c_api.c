/**
 *  @file   test_c_api.c
 *
 *  @date   Created on: Feb 7, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <stdio.h>

#include "spy_agent.h"

// this is the code that implements the handlers
#include "handlers.c"

int main(){
	struct cw_mon_event ev;

	diag_t diag = DIAG_CW_OK;

	// first init cw
	if( (diag = cw_init()) != DIAG_CW_OK ){
		p_error("Issues with CW initialization\n");
		return 1;
	}

	// next specify what events are you interested in

	// get cpu events
	ev.attrs = NULL;
	ev.client_data = NULL;
	ev.handler = cpu_mon_handler;
	ev.id = cw_MON_EVENT_CPU;

	if( cw_nondfg_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

	// get men events
	ev.id = cw_MON_EVENT_MEM;
	ev.handler = mem_mon_handler;

	if( cw_nondfg_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

	// get net events
	ev.id = cw_MON_EVENT_NET;
	ev.handler = net_mon_handler;

	if( cw_nondfg_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}

#ifdef NVML_PRESENT
	// get nvml events
	ev.id = cw_MON_EVENT_NVML;
	ev.handler = nvml_mon_handler;

	if( cw_nondfg_get_mon_events(&ev) != DIAG_CW_OK){
		p_error("Can't get monitoring events for event.id = %d\n", ev.id);
		return 1;
	}
#endif

	// next enable montioring
	if( cw_monitor(30) != DIAG_CW_OK ){
		p_error("Issues with enabling monitoring\n");
		return 1;
	}

	// close CW
	if( cw_close() != DIAG_CW_OK ){
		p_error("Issues with closing CW\n");
		return 1;
	}

	return 0;
}
