/**
 * test_c_api_spy_agent_utils.c
 *
 *  Created on: Aug 12, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */


#include <stdio.h>
#include <string.h>

#include "spy_agent.h"

int total_test_count = 0;

#define TEST_PASSED 0
#define TEST_FAILED 1

/**
 * Attempt to open an none existing file
 * @return
 */
int test_dfg_read_splitter_contact_wrong_file(){
	const char * SPLITTER_FILE="test1-cw-splitter.txt";
	struct ragent_info ragent;

	total_test_count++;

	strcpy(ragent.key_contact, "anode_bstone");

	if( dfg_read_splitter_contact(SPLITTER_FILE, &ragent) != DIAG_CW_ERR){
		p_error("TEST FAILED\n");
		return TEST_FAILED;
	}
	return TEST_PASSED;
}

/**
 * existing file, none existing key
 * @return
 */
int test_dfg_read_splitter_contact_non_existant_key(){
	const char * SPLITTER_FILE="test-cw-splitters.txt";
	struct ragent_info ragent;

	total_test_count++;
	strcpy(ragent.key_contact, "anode_bstone");

	if( dfg_read_splitter_contact(SPLITTER_FILE, &ragent) != DIAG_CW_CONTACT_KEY_NOT_FOUND){
		p_error("TEST FAILED\n");
		return TEST_FAILED;
	}
	return TEST_PASSED;
}

/**
 * existing file, existing key
 * @return
 */
int test_dfg_read_splitter_contact_existant_key(){
	const char * SPLITTER_FILE="test-cw-splitters.txt";
	struct ragent_info ragent;

	total_test_count++;
	// first one
	strcpy(ragent.key_contact, "a_a_nvml_src");

	if( dfg_read_splitter_contact(SPLITTER_FILE, &ragent) != DIAG_CW_OK){
		p_error("TEST FAILED\n");
		return TEST_FAILED;
	}

	// in the middle
	strcpy(ragent.key_contact, "b_b_cpu_src");

	if( dfg_read_splitter_contact(SPLITTER_FILE, &ragent) != DIAG_CW_OK){
		p_error("TEST FAILED\n");
		return TEST_FAILED;
	}

	// the last one
	strcpy(ragent.key_contact, "a_a_net_src");

	if( dfg_read_splitter_contact(SPLITTER_FILE, &ragent) != DIAG_CW_OK){
		p_error("TEST FAILED\n");
		return TEST_FAILED;
	}

	return TEST_PASSED;
}


int main(){
	int failed_tests_count = 0;
	failed_tests_count += test_dfg_read_splitter_contact_wrong_file();
	failed_tests_count += test_dfg_read_splitter_contact_non_existant_key();
	failed_tests_count += test_dfg_read_splitter_contact_existant_key();

	printf("=========================\n");
	printf("TEST SUMMARY\n");
	printf("Number of total tests executed:         %d\n", total_test_count);
	printf("Number of tests with the result FAILED: %d\n", failed_tests_count);
	printf("Number of tests witht he result PASSED: %d\n", total_test_count - failed_tests_count);
	return 0;
}

