/**
 * dfg_ini_create.cpp
 *
 *  Created on: Aug 19, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */



#include <sys/types.h>
#include <sys/time.h>

#include <cassert>
#include <iostream>
#include <fstream>

#include "easylogging++.h"
using namespace std;

#include "INI.h"

#include "misc.h"
#include "transport.h"

_INITIALIZE_EASYLOGGINGPP

using namespace cw;
using namespace std;

Diag usage(string exec_name){
	cout << "Usage: " << exec_name << "\n";
	cout << "Description: The tool creates the ini file and initializes it with"
			" the default parameters\n";
	cout << "Ex.: " << exec_name << " 1\n";
	return Diag::OK;
}

int main(int argc, char *argv[]){

#include "easylogging_default_cfg.h"

	if(  1 != argc ){
		usage(argv[0]);
		return 2;
	}

	if( Cfg_Dfg::create_cfg_file( ) != Diag::OK ){
		LOG(ERROR) << "Issues with creating the configuration file. Returning an error";
		return 1;
	} else {
		LOG(DEBUG) << "File successfully created.";
	}

	return 0;
}
