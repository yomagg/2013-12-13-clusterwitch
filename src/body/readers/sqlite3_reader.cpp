/**
 *  @file   sqlite3_reader.cpp
 *
 *  @date   Created on: Feb 12, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <string>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <cassert>

#include "easylogging++.h"

#include "sqlite3_reader.h"

namespace cw {

using namespace std;

// -------------------------------
// Sqlite3_Reader
// -------------------------------

/**
 * This is for some reason required by sqlite3
 * @param NotUsed
 * @param argc
 * @param argv
 * @param azColName
 * @return
 */
static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	int i;
	for(i=0; i<argc; i++){
		LOG(DEBUG) << azColName[i] << "=" << (argv[i] ? argv[i] : "NULL") << "\n";
	}
	return 0;
}

Sqlite3_Reader::Sqlite3_Reader(): db(nullptr){
}

Sqlite3_Reader::~Sqlite3_Reader(){
	close_db();
}

/**
 * Opens the database with an automatically created name
 *
 * spy_name-kid096.nics.utk.edu-2014-02-12-10_00_23.db
 *
 * The file is written to Cfg::get_sqlite3_reader_output_dir()
 *
 * @return Diag::OK if everything ok
 *
 */
Diag Sqlite3_Reader::open_db(){
	Diag diag = Diag::OK;
	string part_db_file_name;
	if ( (diag = gen_db_file_name(part_db_file_name)) != Diag::OK ){
		LOG(ERROR) << "Got error from gen_db_file_name(). Quitting ";
	}
	string file_path = Cfg_Dfg::get_sqlite3_reader_output_dir() + "/" +
			DB_PREFIX + "-" + part_db_file_name + ".db";

	int rc = 0;

	// open the database
	rc = sqlite3_open(file_path.c_str(), &db);
	if( rc ){
		LOG(ERROR) << "Can't open database " << sqlite3_errmsg(db) << "\n";
		sqlite3_close(db);
		return Diag::ERR;
	} else {
		LOG(DEBUG) << "Database created: " << file_path;
	}

	// now create cap and mon tabs
	if( (diag = create_cap_tab()) != Diag::OK ){
		LOG(ERROR) << "Issues with creation of the cap tab";
		return diag;
	}

	if( (diag = create_mon_tab()) != Diag::OK ){
		LOG(ERROR) << "Issues with creation of the mon tab";
		return diag;
	}

	return diag;
}

/**
 * Closes the database
 *
 * @return Diag::OK always
 */
Diag Sqlite3_Reader::close_db(){
	Diag diag = Diag::OK;

	sqlite3_close(db);
	db = nullptr;

	return diag;
}
/**
 * It generates the name
 * hostname-date-hours.db
 * kid096.nics.utk.edu-2014-02-12-10_00_23.db
 * @param file_name this will hold the file name
 * @return Diag::OK if everything went fine
 *        != Diag::OK if something went wrong
 */
Diag Sqlite3_Reader::gen_db_file_name(string & file_name){
	Diag diag = Diag::OK;
	char host_buf[200];

	// get hostname
	if( gethostname(host_buf, sizeof(host_buf)) != 0 ){
		LOG(ERROR) << "Can't get the name of the host";
		return Diag::ERR;
	}

	// get current date
	time_t     now = time(0);
	struct tm  tstruct;
	char       time_buf[80];
	tstruct = *localtime(&now);
	strftime(time_buf, sizeof(time_buf), "%Y-%m-%d-%H_%M_%S", &tstruct);

	file_name = string(host_buf) + "-" + string(time_buf);

	return diag;
}

/**
 * executes an sql statement
 * @param sql The statement that will be executed; this is
 *
 * @return Diag::OK if everything ok
 *         != Diag::OK if anything went wrong
 */
Diag Sqlite3_Reader::exec_sql(const string & sql){
	Diag diag = Diag::OK;
	assert(nullptr != db);
	assert(sql.length() > 0);

	int rc = 0;
	char *zErrMsg = nullptr;

	rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
	if( rc != SQLITE_OK ){
		LOG(ERROR) << "SQL error " << zErrMsg;
		sqlite3_free(zErrMsg);
		return Diag::SQL_ERR;
	}

	return diag;
}

// --------------------------------------
// Cpu_Sqlite3_Reader
// --------------------------------------
Cpu_Sqlite3_Reader::Cpu_Sqlite3_Reader() {
		CAP_TAB = "CPU_CAP_TAB";
		MON_TAB = "CPU_MON_TAB";
		DB_PREFIX = "cpu";
}
/**
 * Creates the cap tab for the cpu_sqlite3_reader
 * @return
 */
Diag Cpu_Sqlite3_Reader::create_cap_tab(){
	Diag diag = Diag::OK;

	string tab_name = CAP_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
	           tab_name +
		       " (SPY_ID INTEGER," \
		       "TS INTEGER," \
		       "HOSTNAME TEXT," \
		       "PID INTEGER," \
		       "CORE_COUNT INTEGER);";

	diag = exec_sql(sql);

	return diag;
}
/**
 * creates a mon tab for the cpu_sqlite3_reader
 * @return
 */
Diag Cpu_Sqlite3_Reader::create_mon_tab(){
	Diag diag = Diag::OK;

	string tab_name = MON_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
			tab_name +
				" (SPY_ID INT NOT NULL," \
				"TS INT NOT NULL," \
				"CORE_ID INT NOT NULL, "\
				"CORE_UTIL REAL NOT NULL);";

	diag = exec_sql(sql);

	return diag;
}

/**
 * Assumes that the db is not NULL (doesn't check this)
 * @param p_rec The record that will be inserted
 * @return
 */
Diag Cpu_Sqlite3_Reader::insert_cap_rec(const ev_cpu_cap *p_rec){
	Diag diag = Diag::OK;
	stringstream ss;
	string tab_name = CAP_TAB;
	string hostname = p_rec->id.hostname;

	ss << "INSERT INTO " << tab_name << " (SPY_ID,TS,HOSTNAME,PID,CORE_COUNT) "
	   << "VALUES (" << p_rec->id.id << "," << p_rec->ts << ",\""
	   << hostname.c_str()
	   << "\"," << p_rec->id.pid << "," << p_rec->core_count << "); ";

	LOG(DEBUG) << ss.str();

	diag = exec_sql(ss.str());

	return diag;
}

/**
 * ASsumes that the db != nullptr (doesn't check this)
 * @param p_rec The rec that will be inserted
 * @return
 */
Diag Cpu_Sqlite3_Reader::insert_mon_rec(const ev_cpu_mon *p_rec){
	Diag diag = Diag::OK;

	stringstream ss;
	string tab_name = MON_TAB;

	for( int i = 0; i < p_rec->core_plus_one_count; ++i){
		ss << "INSERT INTO " << tab_name << " (SPY_ID,TS,CORE_ID,CORE_UTIL) "
		   << "VALUES (" << p_rec->id << "," << p_rec->ts << ","
		   << i << "," << p_rec->cpu_and_core_usage[i] << "); ";
	}

	diag = exec_sql(ss.str());

	return diag;
}

// --------------------------------------
// Mem_Sqlite3_Reader
// --------------------------------------
Mem_Sqlite3_Reader::Mem_Sqlite3_Reader() {
		CAP_TAB = "MEM_CAP_TAB";
		MON_TAB = "MEM_MON_TAB";
		DB_PREFIX = "mem";
}
/**
 * Creates the cap tab for the cpu_sqlite3_reader
 * @return
 */
Diag Mem_Sqlite3_Reader::create_cap_tab(){
	Diag diag = Diag::OK;

	string tab_name = CAP_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
	           tab_name +
		       " (SPY_ID INTEGER," \
		       "TS INTEGER," \
		       "HOSTNAME TEXT," \
		       "PID INTEGER," \
		       "TOTAL_MEM_KB INTEGER,"\
		       "TOTAL_SWAP_KB INTEGER);";

	diag = exec_sql(sql);

	return diag;
}
/**
 * creates a mon tab for the cpu_sqlite3_reader
 * @return
 */
Diag Mem_Sqlite3_Reader::create_mon_tab(){
	Diag diag = Diag::OK;

	string tab_name = MON_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
			tab_name +
				" (SPY_ID INTEGER NOT NULL," \
				"TS INTEGER NOT NULL," \
				"MEM_UTIL REAL, "\
				"MEM_BUFFERED REAL, "\
				"MEM_CACHED REAL, "\
				"MEM_ACTIVE REAL, "\
				"MEM_INACTIVE REAL, "\
				"SLAB REAL, " \
				"MAPPED REAL, " \
				"SWAP_UTIL REAL, "\
				"SWAP_CACHED REAL);";

	diag = exec_sql(sql);

	return diag;
}

/**
 * Assumes that the db is not NULL (doesn't check this)
 * @param p_rec The record that will be inserted
 * @return
 */
Diag Mem_Sqlite3_Reader::insert_cap_rec(const ev_mem_cap *p_rec){
	Diag diag = Diag::OK;
	stringstream ss;
	string tab_name = CAP_TAB;
	string hostname = p_rec->id.hostname;

	ss << "INSERT INTO " << tab_name << " (SPY_ID,TS,HOSTNAME,PID,TOTAL_MEM_KB,TOTAL_SWAP_KB) "
	   << "VALUES (" << p_rec->id.id << "," << p_rec->ts << ",\""
	   << hostname.c_str()
	   << "\"," << p_rec->id.pid << "," << p_rec->mem_total_kB << "," << p_rec->swap_total_kB << "); ";

	LOG(DEBUG) << ss.str();

	diag = exec_sql(ss.str());

	return diag;
}

/**
 * ASsumes that the db != nullptr (doesn't check this)
 * @param p_rec The rec that will be inserted
 * @return
 */
Diag Mem_Sqlite3_Reader::insert_mon_rec(const ev_mem_mon *p_rec){
	Diag diag = Diag::OK;

	stringstream ss;
	string tab_name = MON_TAB;

	ss << "INSERT INTO " << tab_name
	   << " (SPY_ID,TS,MEM_UTIL,MEM_BUFFERED,MEM_CACHED,MEM_ACTIVE,MEM_INACTIVE,SLAB,MAPPED,SWAP_UTIL,SWAP_CACHED) "
		   << "VALUES (" << p_rec->id << "," << p_rec->ts << ","
		   << p_rec->mem_util_perc << ","
		   << p_rec->mem_buffers_util_perc << ","
		   << p_rec->mem_cached_util_perc << ","
		   << p_rec->mem_active_util_perc << ","
		   << p_rec->mem_inactive_util_perc << ","
		   << p_rec->slab_util_perc << ","
		   << p_rec->mapped_perc << ","
		   << p_rec->swap_util_perc << ","
		   << p_rec->swap_cached_perc << "); ";

	// ss should contain a combined values with inserting things
	diag = exec_sql(ss.str());

	return diag;
}

// --------------------------------------
// Net_Sqlite3_Reader
// --------------------------------------
Net_Sqlite3_Reader::Net_Sqlite3_Reader() {
		CAP_TAB = "NET_CAP_TAB";
		MON_TAB = "NET_MON_TAB";
		DB_PREFIX = "net";
}
/**
 * Creates the cap tab for the sqlite3_reader
 * @return
 */
Diag Net_Sqlite3_Reader::create_cap_tab(){
	Diag diag = Diag::OK;

	string tab_name = CAP_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
	           tab_name +
		       " (SPY_ID INTEGER," \
		       "TS INTEGER," \
		       "HOSTNAME TEXT," \
		       "PID INTEGER," \
		       "NIC_ID INTEGER,"\
		       "NIC_NAME TEXT);";

	if ( ( diag = exec_sql(sql)) != Diag::OK){
		return diag;
	}

	return diag;
}
/**
 * creates a mon tab for the cpu_sqlite3_reader
 * @return
 */
Diag Net_Sqlite3_Reader::create_mon_tab(){
	Diag diag = Diag::OK;

	string tab_name = MON_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
			tab_name +
				" (SPY_ID INTEGER NOT NULL," \
				"TS INTEGER NOT NULL," \
				"NIC_ID INTEGER NOT NULL," \
				"NIC_COMBINED REAL,"\
				"NIC_TRANS REAL,"\
				"NIC_RECV REAL);";

	diag = exec_sql(sql);

	return diag;
}

/**
 * Assumes that the db is not NULL (doesn't check this)
 * @param p_rec The record that will be inserted
 * @return
 */
Diag Net_Sqlite3_Reader::insert_cap_rec(const ev_net_cap *p_rec){
	Diag diag = Diag::OK;
	stringstream ss;
	string tab_name = CAP_TAB;
	string hostname = p_rec->id.hostname;

	for( int i = 0; i < p_rec->nics_count; ++i){
		ss << "INSERT INTO " << tab_name << " (SPY_ID,TS,HOSTNAME,PID,NIC_ID,NIC_NAME) "
		  << "VALUES (" << p_rec->id.id << "," << p_rec->ts
		  << ",\""<< hostname.c_str()<< "\"," << p_rec->id.pid
		  << "," << i << ",\"" << p_rec->nics_names[i] << "\");" ;
	}

	LOG(DEBUG) << ss.str();

	diag = exec_sql(ss.str());

	return diag;
}

/**
 * ASsumes that the db != nullptr (doesn't check this)
 * @param p_rec The rec that will be inserted
 * @return
 */
Diag Net_Sqlite3_Reader::insert_mon_rec(const ev_net_mon *p_rec){
	Diag diag = Diag::OK;

	stringstream ss;
	string tab_name = MON_TAB;

	for(int i = 0 ; i < p_rec->nics_count; ++i){
		ss << "INSERT INTO " << tab_name
		   << " (SPY_ID,TS,NIC_ID,NIC_COMBINED, NIC_TRANS, NIC_RECV) "
		   << "VALUES (" << p_rec->id << "," << p_rec->ts
		   << ", " << i << "," << p_rec->nics_usage_arr[i].combined_usage
		   << ", " << p_rec->nics_usage_arr[i].transmitted
		   << ", " << p_rec->nics_usage_arr[i].received << "); ";
	}

	// ss should contain a combined values with inserting things
	diag = exec_sql(ss.str());

	return diag;
}

// --------------------------------------
// Nvml_Sqlite3_Reader
// --------------------------------------
Nvml_Sqlite3_Reader::Nvml_Sqlite3_Reader() {
		CAP_TAB = "NVML_CAP_TAB";
		DEV_CAP_TAB = "NVML_GPU_CAP_TAB";
		MON_TAB = "NVML_MON_TAB";
		DB_PREFIX = "nvml";
}

/**
 * Creates the cap tab for the sqlite3_reader
 * @return
 */
Diag Nvml_Sqlite3_Reader::create_cap_tab(){
	Diag diag = Diag::OK;

	string tab_name = CAP_TAB;
	string dev_tab_name = DEV_CAP_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
	           tab_name +
		       " (SPY_ID INTEGER," \
		       "TS INTEGER," \
		       "HOSTNAME TEXT," \
		       "PID INTEGER," \
		       "DRIVER_VER TEXT,"\
		       "GPU_COUNT INTEGER);"\
		       "CREATE TABLE IF NOT EXISTS "  +
		       dev_tab_name +
		       " (SPY_ID INTEGER, "\
		       "  DEV_ID INTEGER, " \
		       "  PRODUCT_NAME TEXT, "\
		       "  SERIAL_NUMBER TEXT, "\
		       "  COMPUTE_MODE INTEGER, "\
		       "  MEM_TOTAL_BYTES INTEGER,"\
		       "  POWER_LIMIT_mW INTEGER,"\
		       "  MAX_GRAPHICS_CLOCK_MHz INTEGER," \
		       "  MAX_SM_CLOCK_MHz INTEGER,"\
		       "  MAX_MEMORY_CLOCK_MHz INTEGER);"
		       ;

	if ( ( diag = exec_sql(sql)) != Diag::OK){
		return diag;
	}

	return diag;
}

/**
 * creates a mon tab for the cpu_sqlite3_reader
 * @return
 */
Diag Nvml_Sqlite3_Reader::create_mon_tab(){
	Diag diag = Diag::OK;

	string tab_name = MON_TAB;

	string sql = "CREATE TABLE IF NOT EXISTS " +
			tab_name +
				" (SPY_ID INTEGER NOT NULL," \
				"TS INTEGER NOT NULL," \
				"DEV_ID INTEGER NOT NULL," \
				"PERFORMANCE_STATE INTEGER,"\
				"MEM_USED_BYTES INTEGER,"\
				"NVML_UTIL_GPU INTEGER,"\
				"NVML_UTIL_MEM INTEGER,"\
				"UTIL_MEM REAL,"\
				"POWER_DRAW REAL,"\
				"GRAPHICS_CLOCK REAL,"\
				"SM_CLOCK REAL,"\
				"MEM_CLOCK REAL);";

	diag = exec_sql(sql);

	return diag;
}

/**
 * Assumes that the db is not NULL (doesn't check this)
 * @param p_rec The record that will be inserted
 * @return
 */
Diag Nvml_Sqlite3_Reader::insert_cap_rec(const ev_nvml_cap *p_rec){
	Diag diag = Diag::OK;
	stringstream ss;
	string tab_name = CAP_TAB;
	string hostname = p_rec->id.hostname;

	string dev_tab_name = DEV_CAP_TAB;

	// these are the main general cap record
	ss << "INSERT INTO " << tab_name << " (SPY_ID,TS,HOSTNAME,PID,DRIVER_VER,GPU_COUNT) "
	<< "VALUES (" << p_rec->id.id << "," << p_rec->ts
	<< ",\""<< hostname.c_str()<< "\"," << p_rec->id.pid << "," << p_rec->driver_ver
	<< ", " << p_rec->gpu_count << ");";

	for( unsigned int i = 0; i < p_rec->gpu_count; ++i){
		string product_name = p_rec->devs[i].product_name;
		string serial_no = p_rec->devs[i].serial_number;

		ss << "INSERT INTO " << dev_tab_name
		   << " (SPY_ID,DEV_ID,PRODUCT_NAME,SERIAL_NUMBER,COMPUTE_MODE,MEM_TOTAL_BYTES,"
		   << " POWER_LIMIT_mW, MAX_GRAPHICS_CLOCK_MHz, MAX_SM_CLOCK_MHz, MAX_MEMORY_CLOCK_MHz) "
		   << "VALUES (" << p_rec->id.id << "," <<  i
		   << ",\""<< product_name.c_str()<< "\","
		   << "\""<< serial_no.c_str()<< "\","
		   << p_rec->devs[i].compute_mode << ", " << p_rec->devs[i].mem_total_bytes
		   << ", " << p_rec->devs[i].power_limit_mW << ", "
		   << p_rec->devs[i].max_graphics_clock_MHz << ", "
		   << p_rec->devs[i].max_SM_clock_MHz << ", "
		   << p_rec->devs[i].max_memory_clock_MHz <<");" ;
	}

	LOG(DEBUG) << ss.str();

	diag = exec_sql(ss.str());

	return diag;
}

/**
 * Assumes that the db != nullptr (doesn't check this)
 * @param p_rec The rec that will be inserted
 * @return
 */
Diag Nvml_Sqlite3_Reader::insert_mon_rec(const ev_nvml_mon *p_rec){
	Diag diag = Diag::OK;

	stringstream ss;
	string tab_name = MON_TAB;

	for(unsigned int i = 0 ; i < p_rec->gpu_count; ++i){
		ss << "INSERT INTO " << tab_name
		   << " (SPY_ID,TS, DEV_ID, PERFORMANCE_STATE, MEM_USED_BYTES,NVML_UTIL_GPU,"
		   << " NVML_UTIL_MEM, UTIL_MEM, POWER_DRAW,GRAPHICS_CLOCK,SM_CLOCK,MEM_CLOCK) "
		   << "VALUES (" << p_rec->id << "," << p_rec->ts
		   << ", " << i << "," << p_rec->gpu_arr[i].performance_state  << ", "
		   << p_rec->gpu_arr[i].mem_used_bytes << ", " << p_rec->gpu_arr[i].nvml_util_gpu
		   << ", " << p_rec->gpu_arr[i].nvml_util_mem << ", "
		   << p_rec->gpu_arr[i].util_mem << ", " << p_rec->gpu_arr[i].power_draw
		   << ", " << p_rec->gpu_arr[i].graphics_clock << ", "
		   << p_rec->gpu_arr[i].sm_clock << ", " << p_rec->gpu_arr[i].mem_clock
		   << "); ";
	}

	// ss should contain a combined values with inserting things
	diag = exec_sql(ss.str());

	return diag;
}
// -----------------------------------------
// Super_Sqlite3_Reader
// -----------------------------------------
/**
 * Allocates all readers, i.e., fills the vector with new reader objects
 * and opens their databases
 *
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong (something with opening databases)
 */
Diag Super_Sqlite3_Reader::open_db(){
	Diag diag = Diag::OK;
	// create all readers
	readers_vec.emplace_back(new Cpu_Sqlite3_Reader);
	readers_vec.emplace_back(new Mem_Sqlite3_Reader);
	readers_vec.emplace_back(new Net_Sqlite3_Reader);
	readers_vec.emplace_back(new Nvml_Sqlite3_Reader);

	// open databases for each of them
	for(auto & el : readers_vec){
		if( (diag = el->open_db()) != Diag::OK ){
			return diag;
		}
	}

	return diag;
}
/**
 * Complementary to open_db() - closes the db and deallocates the reader
 * objects
 *
 * @return Diag::OK if everything went fine
 *         != Diag::OK if anything went wrong (something with closing database)
 */
Diag Super_Sqlite3_Reader::close_db() {
	Diag diag = Diag::OK;

	// close databases for each of the reader
	for( auto & el : readers_vec){
		if( (diag = el->close_db()) != Diag::OK ){
			return diag;
		}
	}

	// deallocate readers
	for( auto & el : readers_vec){
		delete el;
	}

	return diag;
}

/**
 * As far as I can tell this have no special meaning in Super reader
 * as the readers know how to create their tables and I override the open_db()
 *
 * @return Diag::OK always
 */
Diag Super_Sqlite3_Reader::create_cap_tab() {
	return Diag::OK;
}

/**
 * It is used by open_db(), and my open_db does not use this so just a
 * dummy implementation
 *
 * @return Diag::OK always
 */
Diag Super_Sqlite3_Reader::create_mon_tab() {
	return Diag::OK;
}

/**
 * getter for readers_vec
 *
 * @return readers_vec
 */
std::vector<Sqlite3_Reader*>& Super_Sqlite3_Reader::get_sqlite3_readers() {
	return readers_vec;
}

} // namespace cw
