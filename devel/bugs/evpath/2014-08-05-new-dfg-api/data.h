/*
 * data.h
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

#ifndef DATA_H_
#define DATA_H_


#include <string>

// for defining the names used in the program
const std::string MASTER_SOURCE_NAME="master_source";
const std::string HANDLER_NAME="complex_handler";

// ------------------------------------------
// complex
// -------------------------------------------
struct complex_rec {
    double r;
    double i;
};

static FMField complex_field_list[] = {
    {"r", "double", sizeof(double), FMOffset(struct complex_rec*, r)},
    {"i", "double", sizeof(double), FMOffset(struct complex_rec*, i)},
    {nullptr, nullptr, 0, 0}
};

static FMStructDescRec complex_format_list[] = {
		{"complex_rec", complex_field_list, sizeof(struct complex_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};

// ------------------------------------------
// alive message
// -----------------------------------------
struct alive_msg {
	// the contact information
	char *contact;
	// the
	int splitter_stone_id;
	int splitter_action_id;
};

FMField alive_msg_field_list[] = {
  {"contact", "string", sizeof(char*), FMOffset(struct alive_msg *, contact)},
  {nullptr, nullptr, 0, 0}
};

FMStructDescRec alive_msg_format_list[] = {
  { "alive_msg", alive_msg_field_list, sizeof(struct alive_msg), nullptr },
  { nullptr, nullptr, 0, nullptr}
};


#endif /* DATA_H_ */
