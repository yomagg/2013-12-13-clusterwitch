# @file readme.txt
# @date Created on: Aug 5, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

2014-08-05
I have sent the issue to Greg, and Greg confirmed that it was a bug as one
variable was uninitialized that caused segfault during the freeing allocated
resources. The bug was fixed.

ATTENTION
=========
Please pay attention which DFG api you are using - the old one or the new one 
and load appropriate modules.

PURPOSE
==========
Learning the new DFG API. Be sure to use the new API.

ERROR DESCRIPTION
=================
I am getting the segfault when executing the master

(gdb) r
Starting program: /opt/synchrobox/proj/w-ecl/2013-12-13-git-clusterwitch/devel/bugs/evpath/2014-08-05-new-dfg-api/master 
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

Program received signal SIGSEGV, Segmentation fault.
free_attr_list (list=list@entry=0x7ffff7b81f98)
    at /opt/synchrobox/proj/w-ecl/rew-evpath/evpath-build/atl/source/attr.c:1454
1454	    list->ref_count--;
(gdb) where
#0  free_attr_list (list=list@entry=0x7ffff7b81f98)
    at /opt/synchrobox/proj/w-ecl/rew-evpath/evpath-build/atl/source/attr.c:1454
#1  0x00007ffff7bbe852 in dfg_assoc_client (cm=cm@entry=0x603070, 
    node_name=node_name@entry=0x4017bf "a", 
    master_contact=master_contact@entry=0x0, master=master@entry=0x609cb0)
    at /opt/synchrobox/proj/w-ecl/rew-evpath/evpath-build/evpath/source/ev_dfg.c:1252
#2  0x00007ffff7bbeada in INT_EVdfg_assoc_client_local (cm=cm@entry=0x603070, 
    node_name=node_name@entry=0x4017bf "a", master=master@entry=0x609cb0)
    at /opt/synchrobox/proj/w-ecl/rew-evpath/evpath-build/evpath/source/ev_dfg.c:1259
#3  0x00007ffff7ba584f in EVdfg_assoc_client_local (cm=0x603070, 
    node_name=0x4017bf "a", master=0x609cb0)
    at /opt/synchrobox/proj/w-ecl/rew-evpath/evpath-build/evpath/marulin/cm_interface.c:914
#4  0x0000000000401461 in main (argc=1, argv=0x7fffffffd778) at master.cpp:88
(gdb) 



BUILD
=====

> scons

RUNNING
=======
First run the master, and master will print the
instruction how to run a worker

> ./master 

# EOF
