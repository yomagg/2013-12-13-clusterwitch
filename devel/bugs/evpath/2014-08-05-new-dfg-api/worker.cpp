/*
 * dfg_client.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

/**
 *  @file   evtest_worker.cpp
 *
 *  @date   Created on: Dec 16, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVdfg_client dfg_client = nullptr;

// EVdfg dfg = nullptr;  // old API


int main(int argc, char *argv[]) {

	CManager cm = CManager_create();

	if (argc != 3) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact\n";
		cout << "EX. " << argv[0] << " b AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	// create dfg and join the dfg client
	dfg_client = EVdfg_assoc_client(cm, argv[1], argv[2]);
	//dfg = EVdfg_create(cm); // old API

	// TODO not sure what -1 means (-1 indicates a stone id to which the data
	// will be submitted
	EVsource src_id = EVcreate_submit_handle(cm, -1, complex_format_list);

	EVdfg_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), src_id);
//	EVdfg_register_sink_handler(cm, const_cast<char*>(MASTER_SOURCE_NAME.c_str()), complex_format_list,
//			(EVSimpleHandlerFunc) complex_handler);

	// EVdfg_join_dfg(dfg, argv[1], argv[2]); // old API
	cout << argv[1] << " joining master\n";

	EVdfg_ready_wait (dfg_client);
	//EVdfg_ready_wait (dfg); // old API

	if (EVdfg_active_sink_count(dfg_client) == 0) {
		EVdfg_ready_for_shutdown(dfg_client);
	}
	/*  // old API
	  if (EVdfg_active_sink_count(dfg) == 0) {
		EVdfg_ready_for_shutdown(dfg);
	}*/

	if (EVdfg_source_active(src_id)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src_id, &rec, nullptr);
	}
	EVfree_source(src_id);

	return EVdfg_wait_for_shutdown(dfg_client);
	//return EVdfg_wait_for_shutdown(dfg);  // old API
}
