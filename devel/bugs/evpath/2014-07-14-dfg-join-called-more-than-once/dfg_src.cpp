/**
 *  @file   dfg_src.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//#include "config.h"
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"

using namespace std;

// include the format definitions
#include "evfmts.h"

int main(int argc, char **argv){

	if( argc != 2){
		cout << "USAGE\n" << argv[0] <<  "\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << "blahblah\n";
		return 1;
	}

	CManager cm;

    EVsource src;
    cout << "Child created\n";

    int status;
    //EVdfg test_dfg;

    char dfg_contact[200];
    cm = CManager_create();



    test_dfg = EVdfg_create(cm);

    source_join_info src_info;
    src_info.node_name="node2";
    src_info.stone_name="b";
    src_info.simple_format_list=simple_format_list;
    src_info.node_dfg=test_dfg;
    src_info.src=&src;

    EVsource src1 = EVcreate_submit_handle(cm, -1, simple_format_list);
    EVdfg_register_source("A", src1);

    EVsource src2 = EVcreate_submit_handle(cm, -1, simple_format_list2);
    EVdfg_register_source("A", src2);

    // join DFG
    EVdfg_join_dfg(test_dfg,"A",mastercontact);

    dfg_source_join(cm,&src_info);
    cout << "\n joined\n";


    EVdfg_ready_wait(test_dfg);

    if(src_info.src==NULL){
    	cout << "\nevsource is null\n";
    	return 1;
    }

    if (EVdfg_source_active(*(src_info.src))){
        cout << "Submitting record\n";
        simple_rec rec;
        //generate_simple_record(&rec);
        /* submit would be quietly ignored if source is not active */
        rec.integer_field=55;
        EVsubmit(*(src_info.src), &rec, NULL);
    }


    if( dfg_slave_shutdown(test_dfg) == 0){
    	cout << "dfg_slave_shutdown: return 0\n";
    } else {
    	cout << "dfg_slave_shutdown: return !=0\n";
    }

    return 0;
}
