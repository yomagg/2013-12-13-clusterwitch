/**
 *  @file   dfg_sink.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//#include "config.h"
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"

// include the formats
#include "evfmts.h"

int main(int argc, char **argv){
	CManager cm;

    cout << "child created\n";
    cm = CManager_create();
    if (argc != 1) {
    	cout << "Child usage" <<   argv[0] << "\n";
    	return 1;
    }
    //EVdfg test_dfg;
    test_dfg = EVdfg_create(cm);

    sink_join_info sink_info;
    sink_info.node_name="node1";
    sink_info.stone_name="a";
    sink_info.simple_format_list=simple_format_list;
    sink_info.handler_func=(EVSimpleHandlerFunc) simple_handler;
    sink_info.node_dfg=test_dfg;

    dfg_sink_join(cm,&sink_info);
    cout << "sink joined\n" ;

    EVdfg_ready_wait(test_dfg);

    if( dfg_slave_shutdown(test_dfg) == 0){
    	cout << "dfg_slave_shutdown: return 0\n";
    } else {
    	cout << "dfg_slave_shutdown: return !=0\n";
    }

    return 0;
}

