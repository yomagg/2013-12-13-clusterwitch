/**
 *  @file   dfg_master.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"


int main(int argc, char **argv){
	if( argc != 1){
		cout << "USAGE\n" << argv[0] << "\n";
		return 1;
	}

	CManager cm;

	cm = CManager_create();
	CMlisten(cm);

	EVdfg dfg = EVdfg_create(cm);
	char * dfg_contact = EVdfg_get_contact_list(dfg);

	printf("DFG contact string: %s\n", dfg_contact);
	char ** node_list = { "A", "B"};

	EVdfg_register_node_list(dfg, node_list);

	// create dfg
	EVdfg_stone src1 = EVdfg_create_source_stone(dfg, "src1");
	EVdfg_assign_node(src1, "A");
	printf("Assigned stone src1 to node A\n");


	EVdfg_stone src2 = EVdfg_create_source_stone(dfg, "src2");
	EVdfg_assign_node(src2, "A");
	printf("Assigned stone src2 to node A\n");

	EVdfg_stone sink1 = EVdfg_create_sink_stone(dft->dfg,"sink1");
	EVdfg_assign_node(sink1, "B");
	printf("Assigned stone sink1 to node B\n");

	EVdfg_stone sink2 = EVdfg_create_sink_stone(dft->dfg,"sink2");
	EVdfg_assign_node(sink2, "B");
	printf("Assigned stone sink2 to node B\n");

	// do the linking
	EVdfg_link_port(src1, 0, sink1);
	EVdfg_link_port(src2, 0, sink2);

	EVdfg_realize(dfg);

}
