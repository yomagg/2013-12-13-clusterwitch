/**
 *  @file   evfmts.h
 *
 *  @date   Created on: Apr 1, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef EVFMTS_H_
#define EVFMTS_H_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//#include "config.h"
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"

static EVdfg test_dfg;

typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event =(simple_rec_ptr) vevent;
    cout << "handler called: " <<  event->integer_field << "\n";
    EVdfg_shutdown(test_dfg, 0);
    return 0;
}

// for simple_handler 2
typedef struct _simple_rec2 {
    float float_field;
} simple_rec2, *simple_rec_ptr2;

static FMField simple_field_list2[] =
{
    {"float_field", "float", sizeof(float), FMOffset(simple_rec_ptr2, float_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list2[] =
{
    {"simple2", simple_field_list2, sizeof(simple_rec2), NULL},
    {NULL, NULL}
};

int
simple_handler2(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr2 event =(simple_rec_ptr2) vevent;
    cout << "handler called: " <<  event->float_field << "\n";
    EVdfg_shutdown(test_dfg, 0);
    return 0;
}




#endif /* EVFMTS_H_ */
