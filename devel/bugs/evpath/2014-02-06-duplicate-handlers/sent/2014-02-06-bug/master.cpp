/**
 * @file master.cpp
 *
 * @date Created on: Dec 10, 2013
 * @author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>
#include "evpath.h"

#include "data.h"

using namespace std;

static int complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	complex_rec * event = static_cast<complex_rec*> (vevent);

	cout << "complex_handler: (" << event->r << ", " << event->i << ")\n";
    return 1;
}

extern "C" void alive_handler(CManager cm, CMConnection conn, void *vevent,
			void *client_data, attr_list attrs){
	alive_msg *event = static_cast<alive_msg * > (vevent);
	EVstone *p_complex_stone = static_cast<EVstone *>(client_data);

	cout << "alive_handler:  (contact:stone_id)" << event->contact << ":" << event->output_stone_id
		<< "\n";

	EVstone output_stone = EValloc_stone(cm);
	attr_list worker_atom_contact_list = attr_list_from_string(event->contact);
	EVassoc_bridge_action(cm, output_stone, worker_atom_contact_list, event->output_stone_id);
	free_attr_list(worker_atom_contact_list);

	alive_msg rec;
	rec.contact = attr_list_to_string(CMget_contact_list(cm));
	rec.output_stone_id = *p_complex_stone;

	cout << "Contact for complex_handler (contact:stone) " << rec.contact << ":"
			<< rec.output_stone_id << "\n";

	EVsource source = EVcreate_submit_handle(cm, output_stone, alive_msg_format_list);
	EVsubmit(source, &rec, nullptr);

	return;
}

/*static int alive_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	alive_msg *event = static_cast<alive_msg * > (vevent);
	cout << "alive_handler:  (contact:)" << event->contact << ":" << event->

	return 1;
}*/

int main(int argc, char *argv[]){

	if (2 != argc){
		cout << "Usage: " << argv[0] << " how-long-to-run\n";
		cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
		cout << "Eg. " << argv[0] << " 600\n";
		return 0;
	}
	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);

	cout << "Forked a comm thread\n";
	CMlisten(cm);

	EVstone complex_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, complex_stone, complex_format_list, complex_handler, nullptr);

	// register formats
	// TODO not sure what CMregister_format returns so don't checking it
	CMFormat format = CMregister_format(cm, alive_msg_format_list);
	CMregister_handler(format, alive_handler, &complex_stone);

	char *string_list = attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "Contact list (my-contact-info) " <<  string_list << "\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));
	CManager_close(cm);
	//CMrun_network(cm);

	return 0;
}


