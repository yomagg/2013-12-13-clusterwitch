# @file readme.txt
# @date Created on Feb 6, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

PURPOSE
============
Trying to figure out what is causing a message like this

CM - No handler for incoming data of this version of format "ev_alive_mon_msg"

This happens because of two reasons:

1. I must somehow register twice the same handler so in a list (or array) 
   cm->in_formats I have the same handler registered twice, although
   the second registrations causes that cm_format->handler is set to
   NULL
2. Because the lack of the "break" in the folling loop: 

Breakpoint 6, CMact_on_data (conn=conn@entry=0x7ffff0010cd0,
    cm_buffer=cm_buffer@entry=0x7ffff0000ab0,
    buffer=buffer@entry=0x7ffff0000b40 "\251DMCL", length=84)
    at /rock/synchrobox/proj/w-ecl/2013-02-06-evpath/evpath-build/evpath/source/cm.c:2325
2325        if ((cm_format == NULL) || (cm_format->handler == NULL)) {


The loop lines 2315 - 2319; this part of code was not touched for 15 years or so.
(according to Greg E.)

   for (i=0; i< cm->in_format_count; i++) {
	if (cm->in_formats[i].format == format) {
	    cm_format = &cm->in_formats[i];
	}
    }

if cm->in_formats has two registered handlers (which is not checked by EVPath
as of today, i.e., 2014-02-06); the second one will be overwritten - since there is no a break 
statement. So I am getting the cm_format->handler equals to NULL. And
that's why I can see the message reporting about the issue with
no handler found. The question is how I manage to register twice
the same handler. This snippet attempts to reproduce this error.

DESCRIPTION 
===============

* master - outputs its contact information and waits for alive_msg from workers
           and for complex_rec from workers as well. 
* worker - the goal is to send the complex_rec to a master; to do that 
           it needs to ask the master the contact stone for handling
           complex_rec (EVsubmit). So it takes the contact list of the master 
           and prepares the alive_msg that contains workers contact list
           and a contact stone. The master upon receive the worker's alive
           message uses its information to send to the worker the contact
           stone for master's complex_handler. The worker receives the alive_msg
           back from master and uses its information to send the master
           complex_rec.
           
HOW TO REPRODUCE THE ERROR
===========================

Run without arguments programs to see their options:

> ./master
> ./worker

First run master
> ./master 500
Forked a comm thread
Contact list (my-contact-info) AAIAAJTJ8o3XZQAAATkCmEoBqMA=
....

In the second terminal run worker

> ./worker 15 AAIAAJTJ8o3XZQAAATkCmEoBqMA=
alive_handler:  (contact:)AAIAAJTJ8o3XZQAAATkCmEoBqMA=:0
Sending ...
* * * * * * * * * * * * * *

And when it finishes run the worker again
> ./worker 15 AAIAAJTJ8o3XZQAAATkCmEoBqMA=
 
And this is the output I can see from the worker

SERVER READ FAILED, ERRNO = 0
SERVER READ FAILED, ERRNO = 0
SERVER READ FAILED, ERRNO = 0

And the master output after running those two consecutive workers:

Forked a comm thread
Contact list (my-contact-info) AAIAAJTJ8o3XZQAAATkCmEoBqMA=
alive_handler:  (contact:stone_id)AAIAAJTJ8o3DZQAAATkCmEoBqMA=:0
Contact for complex_handler (contact:stone) AAIAAJTJ8o3XZQAAATkCmEoBqMA=:0
complex_handler: (13.3, 12.3)
complex_handler: (14.3, 13.3)
complex_handler: (15.3, 14.3)
complex_handler: (16.3, 15.3)
complex_handler: (17.3, 16.3)
complex_handler: (18.3, 17.3)
complex_handler: (19.3, 18.3)
complex_handler: (20.3, 19.3)
complex_handler: (21.3, 20.3)
complex_handler: (22.3, 21.3)
complex_handler: (23.3, 22.3)
complex_handler: (24.3, 23.3)
complex_handler: (25.3, 24.3)
complex_handler: (26.3, 25.3)
CM - No handler for incoming data of this version of format "alive_msg"


# EOF
