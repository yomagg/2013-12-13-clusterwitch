/**
 *  master.cpp
 *
 *  Created on: Dec 10, 2013
 *  Author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>
#include <sstream>

#include "evpath.h"


#include "data.h"

using namespace std;

static int alive_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	alive_msg *event = static_cast<alive_msg * > (vevent);
	// this is passed from main and this is what I care fore
	EVstone *p_remote_complex_stone = static_cast< EVstone * >(client_data);

	*p_remote_complex_stone = event->output_stone_id;

	cout << "alive_handler:  (contact:)" << event->contact << ":"
			<< event->output_stone_id << "\n";

	return 1;
}

int main(int argc, char * argv[]){

	if (argc < 2){
		cout << "USAGE\n";
		cout << argv[0] << " how-long-to-run remote-contact-string\n";
		cout << "  how-long-to-run tells how long the " << argv[0] << " will run in seconds\n";
		cout << "  remote-contact-string contact information output by the master\n";
		cout << "EXAMPLE\n" << argv[0] << " 30 AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	attr_list  remote_contact_list = attr_list_from_string(argv[2]);

	CMConnection conn = CMinitiate_conn(cm, remote_contact_list);
	if (conn == nullptr){
		cout << "Can't initiate a connection. ATS ...\n";
		return 1;
	}

	CMFormat format = CMregister_format(cm, alive_msg_format_list);

	// the stone to send the complex events
	EVstone remote_complex_stone = -1;

	// now send an alive message
	EVstone alive_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, alive_stone, alive_msg_format_list, alive_handler, &remote_complex_stone);


	char *my_contact_list = attr_list_to_string(CMget_contact_list(cm));
	alive_msg rec;
	rec.contact = attr_list_to_string(CMget_contact_list(cm));
	rec.output_stone_id = alive_stone;

	if (1 != CMwrite(conn, format, &rec )){
		cout << "Issues with writing to the master. ATS ...\n";
		return 1;
	}

	// now wait until you got the response from the master
	while(-1 == remote_complex_stone){
		sleep(1);
	}

	// ok, so now I can submit complex events
	EVstone output_stone_id = EValloc_stone(cm);
	EVassoc_bridge_action(cm, output_stone_id, remote_contact_list, remote_complex_stone);
	EVsource source = EVcreate_submit_handle(cm, output_stone_id, complex_format_list);

	struct complex_rec data;
	data.r = 13.3;
	data.i = 12.3;

	cout << "Sending ...\n";

	for(int i = 1; i < atoi(argv[1]); ++i){
		cout << "* ";
		// The order EVsubmit,CMsleep or CMsleep,EVsubmit is somewhat important
		// in CMsleep,EVsubmit the last message will probably be not delivered
		EVsubmit(source, &data, nullptr);
		CMsleep(cm, 1);
		data.r++;
		data.i++;
	}

	cout << "\n";
	CManager_close(cm);

	// don't bother with cleaning, I am exiting

	return 0;
}

