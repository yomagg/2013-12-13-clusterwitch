/*
 * data.h
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

#ifndef DATA_H_
#define DATA_H_

#include <string>


// nodes of this DFG
const std::string NODE_A = "A";
const std::string NODE_B = "B";

// src and sinks
const std::string SRC_A = "a";

const std::string SINK_B1 = "b1";
const std::string SINK_B2 = "b2";


// ------------------------------------------
// complex
// -------------------------------------------
struct complex_rec {
    double r;
    double i;
};

static FMField complex_field_list[] = {
    {"r", "double", sizeof(double), FMOffset(struct complex_rec*, r)},
    {"i", "double", sizeof(double), FMOffset(struct complex_rec*, i)},
    {nullptr, nullptr, 0, 0}
};

static FMStructDescRec complex_format_list[] = {
		{"complex_rec", complex_field_list, sizeof(struct complex_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};


#endif /* DATA_H_ */
