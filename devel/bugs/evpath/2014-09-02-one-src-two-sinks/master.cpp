/**
 * master.cpp
 *
 *  Created on: Sep 2, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */


#include <iostream>
#include <string>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

int main(int argc, char **argv){

	int status = 0;;

	CManager cm = CManager_create();
	CMlisten(cm);

	EVmaster master = EVmaster_create(cm);
	char *contact = EVmaster_get_contact_list(master);

	// the name of nodes that the master expect to join the dfg
	char *nodes[] = {const_cast<char*>(NODE_A.c_str()), const_cast<char*>(NODE_B.c_str()), nullptr};
	EVmaster_register_node_list(master, nodes);

	// create a structure of the DFG
	EVdfg dfg = EVdfg_create(master);

	EVdfg_stone src = EVdfg_create_source_stone(dfg, const_cast<char*>(SRC_A.c_str()));
	EVdfg_assign_node(src, nodes[0]);

	EVdfg_stone sink1 = EVdfg_create_sink_stone(dfg, const_cast<char*>(SINK_B1.c_str()));
	EVdfg_assign_node(sink1, nodes[1]);

	EVdfg_stone sink2 = EVdfg_create_sink_stone(dfg, const_cast<char*>(SINK_B2.c_str()));
	EVdfg_assign_node(sink2, nodes[1]);

	// connect sources with sink
	EVdfg_link_dest(src, sink1);
	EVdfg_link_dest(src, sink2);

	// deploy EVdfg
	EVdfg_realize(dfg);

	cout << "Run ./sink " << SINK_B1 << " " << contact << "\n";
	cout << "Run ./sink " << SINK_B2 << " " << contact << "\n";
	cout << "Run ./src " << contact << "\n";

	cout << "NOTE: in case of EVPATH connection errors proceed with CMSelfFormats=1\n";
	cout << "Waiting for nodes to join ... \n";

	free(contact);
	contact = nullptr;

	CMrun_network(cm);

	return status;
}



