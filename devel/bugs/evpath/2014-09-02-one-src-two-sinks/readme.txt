# @file readme.txt
# @date Created on: Sep 2, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

HISTORY
=======
2014-09-04 

I was trying to do something different and Greg pointed this
out. I want to send the event to sinks on different nodes from a source 
on another node.

MANIFESTATION
=============
When running b2 sink, EVPath outputs the message that it can't find
the handler for sink:b1

....
node:sink B:b2 JOINED master.
Failed to find handler func "b1"
No action found for event 873420 submitted to 
local stone number 0 (global 80000001)
Dump stone ID 0, local addr 871de0, default action -1
       Target Stones:  proto_action_count 0:
  proto_action_count 0:
  response_cache_count 2:
Response cache item 0, reference format 0x871b50 (complex_rec:02000018d3325123972100b9)
stage 0, action_type Action_NoAction, proto_action_id -1914990652, requires_decoded 0
Response cache item 1, reference format 0x871b50 (complex_rec:02000018d3325123972100b9)
stage 1, action_type Action_NoAction, proto_action_id -1925064776, requires_decoded 0
    Unhandled incoming event format was "complex_rec:02000018d3325123972100b9"

	** use "format_info <format_name>" to get full format information ** 

Receiver got: (15.1, 16.3)



PURPOSE
===========
Check how DFG deals with one source and multiple sinks.
The snippet tests a scenario:

Nodes: A, B
A: a sources
B: b1, b2 sinks

BUILD
==========

make

RUN
===========
Run 

./master

it will tell you how to run the rest.