/**
 * @file  src.cpp
 * @date  Sep 2, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */


#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

int main(int argc, char *argv[]){
	if (argc != 2) {
		cout << "USAGE:\n";
		cout << argv[0] << " mastercontact\n";
		cout << "EX. " << argv[0] << " AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	CManager cm = CManager_create();
	char* contact = argv[1];

	EVsource src = EVcreate_submit_handle(cm, DFG_SOURCE, complex_format_list);
	EVclient_sources source_caps =
			EVclient_register_source(const_cast<char*>(SRC_A.c_str()), src);

	// create dfg and join the dfg client
	EVclient client =  EVclient_assoc(cm, const_cast<char*>(NODE_A.c_str()),
			contact, source_caps, nullptr);

	cout << "node:src " << NODE_A << ":" << SRC_A << " JOINED master\n";

	EVclient_ready_wait (client);

	if (EVclient_active_sink_count(client) == 0) {
		EVclient_ready_for_shutdown(client);
	}

	if (EVclient_source_active(src)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src, &rec, nullptr);
		//EVclient_ready_for_shutdown(client);
	}

	int status = EVclient_wait_for_shutdown(client);
	EVfree_source(src);

	CManager_close(cm);

	return status;
}


