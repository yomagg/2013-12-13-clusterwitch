# @file readme.txt
# @date Created on: Sep 3, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

HISTORY
=========
Bug confirmed and fixed on 2014-09-04

MANIFESTATION
=============

Seems that the difference how sink and source stones are created matters, 
as if first the sink is created followed by source creation as in the master_strange
the strange output is created. In case when first the source is created
and then sink (as in master) everything is fine.

$ ./master

$ ./master_strange 
Warning, stone 0 (assigned to node b) has no outputs connected to other stones
    This stones particulars are:
stone 0x1defaf0, node 1, stone_id 80000000  (current condition Undeployed)
 out_count 0 : 
 action_count 1, action = "sink:b_sink"

bridge_target ffffffff

BUILD
=======

make

RUN
====

# should just quietly exit
./master

# produces the warning message
./master_strange