# @file readme.txt
# @date Created on: Aug 26, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

STATUS
2014-08-27 confirm by Greg and fixed.

ERROR MANIFESTATION
===================
2014-08-26
ev_dfg.c: perform_deployment(): dfg->client == 0x0 

Breakpoint 2, check_all_nodes_registered (master=0x611780)
    at /opt/synchrobox/proj/w-ecl/evpath-dev/evpath-build/evpath/source/ev_dfg.c:2596
2596		perform_deployment(dfg);
(gdb) s
perform_deployment (dfg=0x6126b0)
    at /opt/synchrobox/proj/w-ecl/evpath-dev/evpath-build/evpath/source/ev_dfg.c:2309
2309	    EVmaster master = dfg->master;
(gdb) n
2311	    if (dfg->master->sig_reconfig_bool == 0) {
(gdb) 
2312		assert(master->state == DFG_Joining);
(gdb) 
2314		CMtrace_out(master->cm, EVdfgVerbose, "EVDFG check all nodes registered -  master DFG state is %s\n", str_state[master->state]);
(gdb) 
2313		master->state = DFG_Starting;
(gdb) 
2314		CMtrace_out(master->cm, EVdfgVerbose, "EVDFG check all nodes registered -  master DFG state is %s\n", str_state[master->state]);
(gdb) 
2315		add_bridge_stones(dfg, dfg->working_state);
(gdb) 
2317		if (dfg->deploy_ack_condition == -1) {
(gdb) p dfg->deploy_ack_condition
$7 = -1
(gdb) n
2316		dfg->deploy_ack_count = 1;  /* we are number 1 */
(gdb) n
2317		if (dfg->deploy_ack_condition == -1) {
(gdb) 
2318		    dfg->deploy_ack_condition = INT_CMCondition_get(dfg->client->cm, NULL);
(gdb) p dfg->client

$8 = (struct _EVclient *) 0x0
(gdb) p dfg->client
Program received signal SIGSEGV, Segmentation fault.
0x00007ffff7bbda29 in perform_deployment (dfg=0x6126b0)
    at /opt/synchrobox/proj/w-ecl/evpath-dev/evpath-build/evpath/source/ev_dfg.c:2318
2318		    dfg->deploy_ack_condition = INT_CMCondition_get(dfg->client->cm, NULL);
(gdb) 

BUILD
=======

make

RUN
=====

./master   # it will tell you how to run ./sink and ./src

