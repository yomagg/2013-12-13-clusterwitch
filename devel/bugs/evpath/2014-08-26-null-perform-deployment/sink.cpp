/**
 * sink.cpp
 *
 *  Created on: Aug 26, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */


#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

EVclient client;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	//EVclient client = static_cast<EVclient>(client);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	EVclient_ready_for_shutdown(client);
	EVclient_shutdown(client, 0);

	return 0;
}

int main(int argc, char *argv[]){
	if (argc != 3) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact\n";
		cout << "EX. " << argv[0] << " a AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	CManager cm = CManager_create();
	//EVclient client;
	EVclient_sinks sink_caps = EVclient_register_sink_handler(cm,
			const_cast<char*>(SINK_NAME.c_str()), complex_format_list,
				(EVSimpleHandlerFunc) complex_handler, &client);


	// create dfg and join the dfg client
	client =  EVclient_assoc(cm, argv[1], argv[2], nullptr, sink_caps);

	cout << "Node " << argv[1] << " joined master\n";

	EVclient_ready_wait (client);

	int status = EVclient_wait_for_shutdown(client);

	CManager_close(cm);

	return status;
}

