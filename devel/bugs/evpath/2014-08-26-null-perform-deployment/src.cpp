/**
 * src.cpp
 *
 *  Created on: Aug 26, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

int main(int argc, char *argv[]){
	if (argc != 3) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact\n";
		cout << "EX. " << argv[0] << " a AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	CManager cm = CManager_create();

	EVsource src = EVcreate_submit_handle(cm, DFG_SOURCE, complex_format_list);
	EVclient_sources source_caps =
			EVclient_register_source(const_cast<char*>(SRC_NAME.c_str()), src);

	// create dfg and join the dfg client
	EVclient client =  EVclient_assoc(cm, argv[1], argv[2], source_caps, nullptr);

	cout << "Node " << argv[1] << " joined master\n";

	EVclient_ready_wait (client);

	if (EVclient_active_sink_count(client) == 0) {
		EVclient_ready_for_shutdown(client);
	}

	if (EVclient_source_active(src)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src, &rec, nullptr);
		//EVclient_ready_for_shutdown(client);
	}

	EVfree_source(src);

	int status = EVclient_wait_for_shutdown(client);

	CManager_close(cm);

	return status;
}


