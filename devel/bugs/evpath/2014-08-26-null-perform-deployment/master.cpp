/**
 * master.cpp
 *
 *  Created on: Aug 26, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

int main(int argc, char **argv){
	CManager cm = CManager_create();
	CMlisten(cm);

	EVmaster master = EVmaster_create(cm);
	char *contact = EVmaster_get_contact_list(master);

	char *nodes[] = { "a", "b", nullptr};
	EVmaster_register_node_list(master, nodes);

	EVdfg dfg = EVdfg_create(master);

	EVdfg_stone src = EVdfg_create_source_stone(dfg, const_cast<char*>( SRC_NAME.c_str()));
	EVdfg_assign_node(src, nodes[0]);

	EVdfg_stone sink = EVdfg_create_sink_stone(dfg, const_cast<char*>(SINK_NAME.c_str()));
	EVdfg_assign_node(sink, nodes[1]);

	// connect source with sink
	EVdfg_link_dest(src, sink);

	// deploy EVdfg
	EVdfg_realize(dfg);

	cout << "Run ./src " << nodes[0] << " " << contact << "\n";
	cout << "Run ./sink " << nodes[1] << " " << contact << "\n";

	free(contact);
	contact = nullptr;

	CMrun_network(cm);

	return 0;
}


