/**
 *  @file   equal_char.cpp
 *
 *  @date   Created on: Jan 3, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <string>

#include "INI.h" // it was with INI 1.31

using namespace std;

int main(){
	string file_name = "my_test.ini";

	INI <string, string, string> ini(file_name, true);  // parse by default

	ini.select("ev_master");
	string key = "contact";
	string value = ini.get(key);

	cout << "Value: " << value << "\n";

	return 0;
}
