# @file readme.txt
# @date Created on Jan 3, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

Steps to reproduce problem:

I want to read the ini file in which the value contains the character '='

[ev_master]
contact=AAIAAJTJ8o28ZQAAATkCmDo8PYA=

I am able to write such an file, however when I try to read the file, 

Value of: read_contact.c_str()
  Actual: "AAIAAJTJ8o28ZQAAATkCmDo8PYA"
Expected: my_contact.c_str()
Which is: "AAIAAJTJ8o28ZQAAATkCmDo8PYA="

The expected outcome is:

I would expect to be able to read the entire string after the first occurrence of '=', including '='

My compiler and OS is:

gcc (Ubuntu/Linaro 4.8.1-10ubuntu9) 4.8.1
Ubuntu 13.10


Additional information:

Apart from that I like your approach. Thanks!
# EOF
