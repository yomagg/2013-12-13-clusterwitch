/**
 *  @file   cpu_cap_format.h
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef CPU_CAP_FORMAT_H_
#define CPU_CAP_FORMAT_H_

vector<FMField> field_list1;
vector<FMStructDescRec> format_list1;

vector<FMField> field_list2;
vector<FMStructDescRec> format_list2;


// identity
FMField tmp_field_list1[] = {
		{"id", "integer", sizeof(int), FMOffset(struct identity *, id)},
		{"hostname", "string", sizeof(char*), FMOffset(struct identity *, hostname)},
		{"pid", "integer", sizeof(pid_t), FMOffset(struct identity *, pid)},
		{ nullptr, nullptr, 0, 0}
	};

for(auto el : tmp_field_list1){
	field_list1.emplace_back(el);
}

FMStructDescRec tmp_format_list1[] = {
	{ "struct identity", &field_list1[0], sizeof(identity), nullptr },
	{ nullptr, nullptr, 0, nullptr}
};

for(auto el : tmp_format_list1){
	format_list1.emplace_back(el);
}

// ev_cpu_cap
FMField tmp_field_list2[] = {
			{ "ts", "integer", sizeof(long long int), FMOffset(ev_cpu_cap *, ts) },
			{ "id", "struct identity", sizeof(identity), FMOffset(ev_cpu_cap *, id) },
			{ "core_count", "integer", sizeof(int), FMOffset(ev_cpu_cap *, core_count) },
			{ nullptr, nullptr, 0, 0 }
};

for(auto el : tmp_field_list2){
	field_list2.emplace_back(el);
}

FMStructDescRec tmp_format_list2[] = {
		{ "struct ev_cpu_cap", &field_list2[0], sizeof(ev_cpu_cap), nullptr },
		{ "struct identity", &field_list1[0], sizeof(identity), nullptr },
		{ nullptr, nullptr, 0, nullptr }
};

for(auto el : tmp_format_list2){
	format_list2.emplace_back(el);
}


#endif /* CPU_CAP_FORMAT_H_ */
