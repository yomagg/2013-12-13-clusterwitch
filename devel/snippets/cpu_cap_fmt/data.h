/**
 *  @file   data.h
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef DATA_H_
#define DATA_H_

#include <unistd.h>

/**
 * the structure that helps describe identity of the
 * such as a unique id, etc
 */
struct identity {
	int 		id;			      //! for storing unique id
	char* 		hostname;    	  //! for storing the name of the host
	pid_t		pid;			  //! the pid of the process
};

/**
 * The structure that describes the cpu capabilities
 * This structure will be sent only once
 */
struct ev_cpu_cap {
	//! the timestamp when we started measurement
	long long int ts;
	//! the identity of the cpu_scout
	identity id;
	//! the cpu capabilities
	int 	core_count;
};

#endif /* DATA_H_ */
