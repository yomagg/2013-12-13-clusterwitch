/**
 *  @file   recv.cpp
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <vector>

#include "evpath.h"

#include "data.h"

using namespace std;

static int
my_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	ev_cpu_cap *event = static_cast<ev_cpu_cap*> (vevent);
	cout << "Got: ";
	cout <<  event->id.hostname << ":" << event->id.id << ":" <<
				event->id.pid << ":" << event->core_count << ":" << event->ts << "\n";

	return 1;
}

int main(int argc, char *argv[]){
#include "cpu_cap_format.h"

	if (2 != argc){
			cout << "Usage: " << argv[0] << " how-long-to-run\n";
			cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
			cout << "Eg. " << argv[0] << " 30\n";
			return 0;
	}


	CManager cm = CManager_create();
	CMfork_comm_thread(cm);

	cout << "Forked a comm thread\n";
	CMlisten(cm);

	EVstone stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, stone, &format_list2[0], my_handler, nullptr);

	char *string_list = attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "Contact list (stone-id:my-contact-info) \"" << stone << ":"<< string_list << "\"\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));
	CManager_close(cm);
	//CMrun_network(cm);

	return 0;
}

