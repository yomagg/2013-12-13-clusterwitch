/**
 *  @file   sender.cpp
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <cstring>

#include "evpath.h"

#include "data.h"
using namespace std;


int main(int argc, char *argv[]){

#include "cpu_cap_format.h"

	CManager cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	if (argc < 2){
		cout << "\nUSAGE\n";
		cout << argv[0] << " remote-stone-id1:contact-string how-long-to-run-sec\n";
		cout << "  remote-stone-id1:contact-string contact information output by the receiver 1\n";
		cout << "  how-long-to-run tells how long the " << argv[0] << " will run in seconds before removing of stones begins\n";
		cout << "\nDESCRIPTION\n";
		cout << "The program sends teh cap_ message";
		cout << "\nEXAMPLE\n" << argv[0] << " 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA= 30\n";
		return 0;
	}

	string contact_info = argv[1];
	stringstream strm(contact_info);
	EVstone remote_stone_id;
	char delim;
	strm >> remote_stone_id >> delim;
	stringstream remote_stone_contact;
	strm.get(* remote_stone_contact.rdbuf());

	// create a local output stone and connect this stone with the
	// remote stone
	EVstone output_stone_id = EValloc_stone(cm);

	attr_list remote_contact_list = attr_list_from_string(remote_stone_contact.str().c_str());

	EVassoc_bridge_action(cm, output_stone_id, remote_contact_list, remote_stone_id);
	free_attr_list(remote_contact_list);

	EVsource source = EVcreate_submit_handle(cm, output_stone_id, &format_list2[0]);

	ev_cpu_cap caps;
	caps.id.hostname = strdup("superhostname");
	caps.id.id = 2012;
	caps.id.pid = 394;
	caps.core_count = 10;
	caps.ts = 129334;

	cout << "Sending: " << caps.id.hostname << ":" << caps.id.id << ":" <<
			caps.id.pid << ":" << caps.core_count << ":" << caps.ts << "\n";

	EVsubmit(source, &caps, nullptr);
	CMsleep(cm, atoi(argv[2]));

	CManager_close(cm);
	free(caps.id.hostname);

	return 0;
}
