# @file readme.txt
# @date Created on Jan 21, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#
The example demonstrates sending the cpu cap message.

BUILD

# run scons
$ ./scons

scons --help

RUN

# first run the receiver
$ ./recv
# next run the sender
$ ./sender

# EOF
