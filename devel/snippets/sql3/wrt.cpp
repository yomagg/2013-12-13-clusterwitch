/**
 *  @file   wrt.cpp
 *
 *  @date   Created on: Feb 11, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <sqlite3.h>

using namespace std;

const int CORE_COUNT = 4;

struct cpu_mon {
	long ts;
	long spy_id;
	int core_count;
	float usage[CORE_COUNT];
};

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
	int i;
	for(i=0; i<argc; i++){
		cout << azColName[i] << "=" << (argv[i] ? argv[i] : "NULL") << "\n";
	}
	cout << "\n";
	return 0;
}

const string DB_NAME="testdb.db";

/**
 * Creates a database and inserts a few records to the database
 *
 * @param argc
 * @param argv
 * @return
 */

int main(int argc, char **argv){
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;

    if( argc == 1){
    	cout << "Usage: " << argv[0] << "1\n";
    	cout << " 1 is a dummy parameter\n";
    	return 1;
    }

    // open the database
    rc = sqlite3_open(DB_NAME.c_str(), &db);
    if( rc ){
    	cout << "Can't open database: " << sqlite3_errmsg(db) << "\n";
    	sqlite3_close(db);
    	return 1;
    }

    // Create SQL statement
    string sql = "CREATE TABLE IF NOT EXISTS CPU_MON_TAB("  \
    		"SPY_ID INT NOT NULL," \
    		"TS INT NOT NULL," \
    		"CORE_ID INT NOT NULL, "\
    		"CORE_UTIL REAL NOT NULL);";

    rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
    if( rc != SQLITE_OK ){
    	cout << "SQL error: " << zErrMsg << "\n";
    	sqlite3_free(zErrMsg);
    }

    // insert
    sql = "INSERT INTO CPU_MON_TAB (SPY_ID,TS,CORE_ID,CORE_UTIL) "  \
            "VALUES (100, 1234, 0, 99.00 ); " \
          "INSERT INTO CPU_MON_TAB (SPY_ID,TS,CORE_ID,CORE_UTIL) "  \
            "VALUES (100, 1234, 1, 49.00 ); "
          "INSERT INTO CPU_MON_TAB (SPY_ID,TS,CORE_ID,CORE_UTIL) "  \
                      "VALUES (100, 1234, 2, 59.00 ); "
          "INSERT INTO CPU_MON_TAB (SPY_ID,TS,CORE_ID,CORE_UTIL) "  \
                      "VALUES (100, 1234, 3, 13.00 ); "
          "INSERT INTO CPU_MON_TAB (SPY_ID,TS,CORE_ID,CORE_UTIL) "  \
                                "VALUES (100, 1234, -1, 56.24 ); "
          ;

   rc = sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);
   if( rc != SQLITE_OK ){
	   cout << "SQL error: " << zErrMsg << "\n";
	   sqlite3_free(zErrMsg);
   }

    sqlite3_close(db);
    return 0;
}
