/**
 *  @file   sender.cpp
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>
#include <sstream>
#include <cstring>

#include "evpath.h"

#include "data.h"
using namespace std;


int main(int argc, char *argv[]){


	CManager cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	if (argc < 2){
		cout << "\nUSAGE\n";
		cout << argv[0] << " remote-stone-id1:contact-string how-long-to-run-sec\n";
		cout << "  remote-stone-id1:contact-string contact information output by the receiver 1\n";
		cout << "  how-long-to-run tells how long the " << argv[0] << " will run in seconds before removing of stones begins\n";
		cout << "\nDESCRIPTION\n";
		cout << "The program sends the type1_ and type2_ message";
		cout << "\nEXAMPLE\n" << argv[0] << " 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA= 30\n";
		return 0;
	}

	string contact_info = argv[1];
	stringstream strm(contact_info);
	EVstone remote_stone_id;
	char delim;
	strm >> remote_stone_id >> delim;
	stringstream remote_stone_contact;
	strm.get(* remote_stone_contact.rdbuf());

	// create a local output stone and connect this stone with the
	// remote stone

	attr_list remote_contact_list = attr_list_from_string(remote_stone_contact.str().c_str());

	// source type1
	EVstone output_type1_stone = EValloc_stone(cm);
	EVassoc_bridge_action(cm, output_type1_stone, remote_contact_list, remote_stone_id);
	EVsource type1_src = EVcreate_submit_handle(cm, output_type1_stone, &type1_format_list[0]);

	// source for type2
	EVstone output_type2_stone = EValloc_stone(cm);
	EVassoc_bridge_action(cm, output_type2_stone, remote_contact_list, remote_stone_id);
	EVsource type2_src = EVcreate_submit_handle(cm, output_type2_stone, &type2_format_list[0]);

	free_attr_list(remote_contact_list);

	// load events
	type1_rec type1 = {1.00};
	type2_rec type2 = {10.0, 10.00};



	for(int i = 0; i < atoi(argv[2]); ++i ){
		type1.f1 += 1.0;
		type2.f1 += 10.0;
		type2.f2 += 10.0;
		EVsubmit(type1_src, &type1, nullptr);
	//	EVsubmit(type2_src, &type2, nullptr);

		cout << "Sending package no. " << i << ": type1 " << type1.f1 << " ";
		cout << "type2 " << type2.f1 << ", " << type2.f2 << "\n";
		CMsleep(cm, 1);
	}

	CMsleep(cm, 2);

	CManager_close(cm);

	return 0;
}
