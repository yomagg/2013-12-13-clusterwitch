/**
 *  @file   data.h
 *
 *  @date   Created on: Apr 11, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef DATA_H_
#define DATA_H_


// ------------------------------------------
// types
// -------------------------------------------
struct type1_rec {
	double f1;
};

struct type2_rec {
	double f1;
	double f2;
};


static FMField type1_field_list[] = {
    {"f1", "double", sizeof(double), FMOffset(struct type1_rec*, f1)},
    {nullptr, nullptr, 0, 0}
};

static FMStructDescRec type1_format_list[] = {
		{"type1_rec", type1_field_list, sizeof(struct type1_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};

static FMField type2_field_list[] = {
    {"f1", "double", sizeof(double), FMOffset(struct type2_rec*, f1)},
    {"f2", "double", sizeof(double), FMOffset(struct type2_rec*, f2)},
    {nullptr, nullptr, 0, 0}
};

static FMStructDescRec type2_format_list[] = {
		{"type2_rec", type2_field_list, sizeof(struct type2_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};


#endif /* DATA_H_ */
