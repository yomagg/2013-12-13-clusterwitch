/**
 *  @file   recv.cpp
 *
 *  @date   Created on: Jan 21, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <vector>

#include "evpath.h"

#include "data.h"

using namespace std;

static FMStructDescList queue_list[] = {type1_format_list, type2_format_list, nullptr};

// a transformation in COD that passes the events
//static char *trans = "{\n}\0\0";

// discards
static char *trans = "{\n\
    int found_t1 = 0;\n\
    type1_rec *t1;\n\
	type2_rec *t2;\n\
	type1_rec r;\n\
    if (EVpresent(type1_rec_ID, 0)) {\n\
		t1 = EVdata_type1_rec(0);\n\
        ++found_t1;\n\
    }\n\
	if (EVpresent(type2_rec_ID, 0)) {\n\
		t2 = EVdata_type2_rec(0); ++found_t1;\n\
	}\n\
	if (found_t1 == 2) {\n\
		r.f1 = t1.f1 + t2.f1;\n\
		EVdiscard_type1_rec(0);\n\
		EVdiscard_type2_rec(0);\n\
		EVsubmit_type1_rec(0, r);\n\
	} else {\n\
		printf(\"Not found 2\\n\");\n\
	}\n\
}\0\0";


static int
my_type1_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	type1_rec *event = static_cast<type1_rec*>(vevent);

	cout << "Got: type1_rec: " << event->f1 << "\n";
	return 1;
}

static int
my_type2_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	type2_rec *event = static_cast<type2_rec*>(vevent);

	cout << "Got: type2_rec: " << event->f1 << "," << event->f2 << "\n";
	return 1;
}



int main(int argc, char *argv[]){

	if (2 != argc){
			cout << "Usage: " << argv[0] << " how-long-to-run\n";
			cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
			cout << "Eg. " << argv[0] << " 30\n";
			return 0;
	}


	CManager cm = CManager_create();
	CMfork_comm_thread(cm);

	cout << "Forked a comm thread\n";
	CMlisten(cm);

	EVstone stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, stone, &type1_format_list[0], my_type1_handler, nullptr);
	EVassoc_terminal_action(cm, stone, &type2_format_list[0], my_type2_handler, nullptr);

	char *filter = nullptr;
	filter = create_multityped_action_spec(queue_list, trans);

	EVaction action = 0;
	action = EVassoc_multi_action(cm, stone, filter, nullptr);
	EVaction_set_output(cm, stone, action, 0, stone);

	char *string_list = attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "Contact list (stone-id:my-contact-info) \"" << stone << ":"<< string_list << "\"\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));
	CManager_close(cm);
	//CMrun_network(cm);

	return 0;
}

