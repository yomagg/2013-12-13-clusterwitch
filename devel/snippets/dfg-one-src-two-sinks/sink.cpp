/**
 * @file  sink.cpp
 * @date  Sep 2, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>
#include <string>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVclient client = nullptr;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	EVclient_shutdown(client, 0);
    return 0;
}

int main(int argc, char *argv[]) {

	if (argc != 4) {
		cout << "USAGE:\n";
		cout << "Run ./master. Master will tell you how to run me\n";

		return 0;
	}

	char * node_name = argv[1];
	char * sink_name = argv[2];
	char * master_contact = argv[3];

	CManager cm = CManager_create();

	EVclient_sinks sink_caps = EVclient_register_sink_handler(cm, sink_name, complex_format_list,
			(EVSimpleHandlerFunc) complex_handler, nullptr);

	// associate this process with the DFG master
	client = EVclient_assoc(cm, node_name, master_contact, nullptr, sink_caps);

	cout << "node:sink " << node_name << ":" << sink_name << " JOINED master.\n";

	EVclient_ready_wait (client);

	if (EVclient_active_sink_count(client) == 0) {
		EVclient_ready_for_shutdown(client);
	}

	int status = EVclient_wait_for_shutdown(client);

	CManager_close(cm);
	return status;
}
