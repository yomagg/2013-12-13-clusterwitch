# @file readme.txt
# @date Created on: Sep 2, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

PURPOSE
===========
Check how DFG deals with one source and multiple sinks.
The snippet tests a scenario:

Nodes: A, B, C
A: a source
B: b sink
C: c sink

BUILD
==========

scons
scons --help

RUN
===========
Run 

./master

it will tell you how to run the rest.