/**
 * recv.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>
#include <vector>

#include "evpath.h"

#include "data.h"

using namespace std;

static int
my_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	complex_rec *event = static_cast<complex_rec*> (vevent);
	cout << "Got complex(r,i): " << event->r << ", " << event->i << "\n";

	return 1;
}

int main(int argc, char *argv[]){
	if (2 != argc){
		cout << "Usage: " << argv[0] << " how-long-to-run\n";
		cout << "how-long-to-run specify how long the network will be serviced in seconds\n";
		cout << "Eg. " << argv[0] << " 60\n";
		return 0;
	}

	CManager cm = CManager_create();
	CMfork_comm_thread(cm);

	CMlisten(cm);

	// create receiver stone
	EVstone stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, stone, complex_format_list, my_handler, nullptr);


	char *string_list = attr_list_to_string(CMget_contact_list(cm));
	cout << "Run: ./sender " << stone << ":" << string_list << "\n";

	// how long the program should run
	string period = argv[1];
	CMsleep(cm, atoi(period.c_str()));
	CManager_close(cm);

	return 0;
}

