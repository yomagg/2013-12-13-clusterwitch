/**
 * sender.cpp
 *
 *  Created on: Aug 20, 2014
 *      Author: Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
//#include <vector>
#include <sstream>
#include <cstring>

#include "evpath.h"

#include "data.h"
using namespace std;

extern "C" void sender_handler(CManager cm, void *client_data){
	EVsource src = static_cast<EVsource>(client_data);

	struct complex_rec rec;
	rec.i = 13.2;
	rec.r = 26.3;

	cout << "Submitting a complex: (" << rec.r << ", " << rec.i << ")\n";
	EVsubmit(src, &rec, nullptr);

	return;
}


int main(int argc, char *argv[]){

	if (argc < 2){
		cout << "\nUSAGE\n";
		cout << argv[0] << " remote-stone-id1:contact-string\n";
		cout << "  remote-stone-id1:contact-string contact information output by the receiver 1\n";
		cout << "\nDESCRIPTION\n";
		cout << "The program sends the complex_rec event and tests adding and removing periodic events";
		cout << "\nEXAMPLE\n" << argv[0] << " 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	// get the remote stone contact info
	string contact_info = argv[1];
	stringstream strm(contact_info);
	// receiver stone id
	EVstone rstone;
	char delim;
	strm >> rstone >> delim;
	stringstream rstone_contact;
	strm.get(*rstone_contact.rdbuf());


	// create a local output stone and connect this stone with the rstone
	EVstone ostone = EValloc_stone(cm);
	attr_list rcontact_list = attr_list_from_string(rstone_contact.str().c_str());
	EVassoc_bridge_action(cm, ostone, rcontact_list, rstone);
	free_attr_list(rcontact_list);

	EVsource source = EVcreate_submit_handle(cm, ostone, complex_format_list);

	cout << "Add periodic 3-sec periodic task\n";
	CMTaskHandle task_handle = CMadd_periodic_task(cm, 3, 0, sender_handler, source);
	CMsleep(cm, 10);

	// I guess CMremove_periodic() is complementary to CMadd_periodic(), and
	// in the comment CMadd_periodic is deprecated; by the rule of elimination
	// I assume that CMremove_task is the correct one
	cout << "Remove periodic periodic task and sleep\n";
	CMremove_task(task_handle);

	for(int i = 0; i < 10; ++i){
		CMsleep(cm, 1);
		cout << ".";
	}
	cout << "\n";

	cout << "Add periodic 1-sec periodic task\n";
	task_handle = CMadd_periodic_task(cm, 1, 0, sender_handler, source);
	CMsleep(cm, 10);

	CManager_close(cm);

	return 0;
}
