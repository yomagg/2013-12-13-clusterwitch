/*
 * dfg_client.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

/**
 *  @file   dfg_client.cpp
 *
 *  @date   Created on: Dec 16, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVclient dfg_client = nullptr;


int main(int argc, char *argv[]) {

	if (argc != 3) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact\n";
		cout << "EX. " << argv[0] << " b AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	CManager cm = CManager_create();

	EVsource src_id = EVcreate_submit_handle(cm, DFG_SOURCE, complex_format_list);
	EVclient_sources source_caps =
			EVclient_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), src_id);

	// create dfg and join the dfg client
	dfg_client = EVclient_assoc(cm, argv[1], argv[2], source_caps, nullptr);

	cout << argv[1] << " joining master\n";

	EVclient_ready_wait (dfg_client);

	if (EVclient_active_sink_count(dfg_client) == 0) {
		EVclient_ready_for_shutdown(dfg_client);
	}

	if (EVclient_source_active(src_id)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src_id, &rec, nullptr);
	}
	EVfree_source(src_id);

	int status = EVclient_wait_for_shutdown(dfg_client);

	CManager_close(cm);
	return status;
}
