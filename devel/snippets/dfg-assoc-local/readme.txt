# @file readme.txt
# @date Created on: Aug 5, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

ATTENTION
=========
Please pay attention which DFG api you are using - the old one or the new one 
and load appropriate modules.

BUILD
=====

> scons
> scons --help

RUNNING
=======
First run the master, and master will print the
instruction how to run a worker

> ./dfg_master 

# EOF
