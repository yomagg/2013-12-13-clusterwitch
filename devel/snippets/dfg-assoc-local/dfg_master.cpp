/**
 * @file dfg_master.cpp
 *
 * @date Aug 5, 2014
 * @author Magdalena S. aka Magic Magg magg dot gatech at gmail.com
 */

#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// the pointer to the dfg structure
static EVdfg dfg = nullptr;
// this is the handle with node lists, configuration and reconfiguration
// management
static EVmaster dfg_master = nullptr;
// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVclient dfg_client = nullptr;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	// the 0 argument is from ev_dfg.c -- STATUS_SUCCESS; this
	// influence the dfg->nodes[xxx].shutdown_status_contribution
	// new API
	EVclient_shutdown(dfg_client, 0);
	// EVdfg_shutdown(dfg, 0); // old API

	// TODO what exactly should this handler return? 0 (original test) or 1
	// (see e.g. the evpath test)
    return 0;
}

int main(int argc, char **argv){

	int status = 0;;
	CManager cm = CManager_create();
	CMlisten(cm);

	// local DFG support

	// DFG creation
	// the new-dfg API
	dfg_master = EVmaster_create(cm);
	// the contact list to talk to the master
	char *str_contact = EVmaster_get_contact_list(dfg_master);

	// the name of nodes that the master expect to join the dfg
	char *nodes[] = {"a", "b", nullptr};
	EVmaster_register_node_list(dfg_master, nodes);

	dfg = EVdfg_create(dfg_master);

	EVdfg_stone src_id = EVdfg_create_source_stone(dfg, const_cast<char*>( MASTER_SOURCE_NAME.c_str()));
	EVdfg_assign_node(src_id, "b");

	EVdfg_stone sink_id = EVdfg_create_sink_stone(dfg, const_cast<char*>(HANDLER_NAME.c_str()));
	EVdfg_assign_node(sink_id, "a");

	// connect source with sink
	EVdfg_link_port(src_id, 0, sink_id);

	// deploy EVdfg
	EVdfg_realize(dfg);

	// I  guess we want to be a sink
	// the new API
	EVclient_sinks sink_caps = EVclient_register_sink_handler(cm, const_cast<char*>(HANDLER_NAME.c_str()), complex_format_list,
			(EVSimpleHandlerFunc) complex_handler, nullptr);

	dfg_client = EVclient_assoc_local(cm, nodes[0], dfg_master, nullptr, sink_caps);

	cout << "Dfg joined: " << nodes[0] << "\n";
	cout << "Dfg waiting for node " << nodes[1] << " to join\n";

	cout << "Run ./dfg_client " << nodes[1] << " " << str_contact << "\n";
	cout << "or\n";
	cout << "CMSelfFormats=1 ./dfg_client " << nodes[1] << " " << str_contact << "\n";
	free(str_contact);
	str_contact = nullptr;

	// wait for the appearance of the source
	// seems as it always returns 1, anyway
	if ( EVclient_ready_wait(dfg_client) != 1 ){
		cerr << "ERROR: dfg initialization failed\n";
	}
	// EVdfg_ready_wait(dfg);  // old API

	// new API
	if (0 == EVclient_active_sink_count(dfg_client)){
		EVclient_ready_for_shutdown(dfg_client);
	}

	// new API
	status = EVclient_wait_for_shutdown(dfg_client);
	// status = EVdfg_wait_for_shutdown(dfg); // old API

	//EVfree_source(source_handle);
	CManager_close(cm);

	return status;
}

