/*
 * dfg_master.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// the pointer to the dfg structure
static EVdfg dfg = nullptr;
// this is the handle with node lists, configuration and reconfiguration
// management
static EVdfg_master dfg_master = nullptr;
// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVdfg_client dfg_client = nullptr;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	// the 0 argument is from ev_dfg.c -- STATUS_SUCCESS; this
	// influence the dfg->nodes[xxx].shutdown_status_contribution
	// new API
	//EVdfg_shutdown(dfg_client, 0);

	// TODO what exactly should this handler return? 0 (original test) or 1
	// (see e.g. the evpath test)
    return 0;
}

int main(int argc, char **argv){

	int status = 0;;
	CManager cm = CManager_create();
	CMlisten(cm);

	// prepare the output stone for the receiving events from a splitter
	// independently from the dfg
	EVstone stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, stone, complex_format_list, complex_handler, nullptr);
	char *string_list= attr_list_to_string(CMget_contact_list(cm));
	// print the stone id and the stringified contact information
	cout << "INFO: Contact list (stone-id:my-contact-info) " << stone << ":"<< string_list << "\n";



	// local DFG support
	// what does it mean -1 for the parameter for the stone (EVstone)
	//EVsource source_handle = EVcreate_submit_handle(cm, -1, complex_format_list);
	//EVdfg_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), source_handle);
	EVdfg_register_sink_handler(cm, const_cast<char*>(HANDLER_NAME.c_str()), complex_format_list,
			(EVSimpleHandlerFunc) complex_handler, nullptr);

	// DFG creation
	// the new-dfg API
	dfg_master = EVdfg_create_master(cm);
	// the contact list to talk to the master
	char *str_contact = EVdfg_get_contact_list(dfg_master);
	// the name of nodes that the master expect to join the dfg
	char *nodes[] = {"a", "b", nullptr};
	EVdfg_register_node_list(dfg_master, nodes);
	dfg = EVdfg_create(dfg_master);

	EVdfg_stone src_id = EVdfg_create_source_stone(dfg, const_cast<char*>( MASTER_SOURCE_NAME.c_str()));
	EVdfg_assign_node(src_id, "a");

	EVdfg_stone sink_id = EVdfg_create_sink_stone(dfg, const_cast<char*>(HANDLER_NAME.c_str()));
	EVdfg_assign_node(sink_id, "b");

	// connect source with sink
	EVdfg_link_port(src_id, 0, sink_id);

	// deploy EVdfg
	EVdfg_realize(dfg);

	// I  guess we want to be a sink
	dfg_client = EVdfg_assoc_client_local(cm, nodes[1], dfg_master);

	cout << "Dfg joined: " << nodes[1] << "\n";
	cout << "Dfg waiting for node " << nodes[0] << " to join\n";

	cout << "Run ./dfg_client " << nodes[0] << " " << str_contact << " " << stone << ":"<< string_list << "\n";
	cout << "or\n";
	cout << "CMSelfFormats=1 ./dfg_client " << nodes[0] << " " << str_contact << " " << stone << ":"<< string_list << "\n";
	free(str_contact);
	str_contact = nullptr;

	// wait for the appearance of the source
	// seems as it always returns 1, anyway
	EVdfg_ready_wait(dfg_client);
	// EVdfg_ready_wait(dfg);  // old API

/*	if (EVdfg_source_active(source_handle)){
		struct complex_rec rec = { 23.04, 12.01 };
		EVsubmit(source_handle, &rec, nullptr);
	} */

	// new API
	if (0 == EVdfg_active_sink_count(dfg_client)){
		EVdfg_ready_for_shutdown(dfg_client);
	}

	// new API
	status = EVdfg_wait_for_shutdown(dfg_client);

	//EVfree_source(source_handle);
	CManager_close(cm);

	return status;
}

