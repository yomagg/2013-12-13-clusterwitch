/*
 * dfg_client.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: magg
 */

/**
 *  @file   evtest_worker.cpp
 *
 *  @date   Created on: Dec 16, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>
#include <vector>
#include <sstream>


#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// associated with waiting for ready (really waiting for the first deployment
// and shutdown); used for all participants
static EVdfg_client dfg_client = nullptr;


int main(int argc, char *argv[]) {

	CManager cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	if (argc != 4) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact remote-stone-id1:contact-string\n";
		cout << "EX. " << argv[0] << " b AAIAAJTJ8o3CZQAAATkCmEoBqMA= 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";

		return 0;
	}

	// >>>>>> -------------------- the splitter part

	// create a split stone - allocate a stone and associate a split
	// action with the stone
	EVstone split_stone = EValloc_stone(cm);
	EVaction split_action = EVassoc_split_action(cm, split_stone, nullptr);

	// get a contact list
	attr_list contact_list = nullptr;
	contact_list =  CMget_contact_list(cm);

	char *contact_list_str=nullptr;
	if (contact_list) {
		contact_list_str = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
		cout << "My contact split_stone_id:split_action_id:contact: "
				<< split_stone << ":" << split_action << ":"
				<< contact_list_str << "\n";
	} else {
		cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return 1;
	}

	vector<EVstone> output_stone_ids;

	for(int i = 3; i < argc; ++i){
		string contact_info = argv[i];
		stringstream strm(contact_info);
		EVstone remote_stone_id;
		char delim;
		strm >> remote_stone_id >> delim;
		stringstream remote_stone_contact;

		strm.get(* remote_stone_contact.rdbuf());

		// create a local output stone and connect this stone with the
		// remote stone
		EVstone output_stone_id = EValloc_stone(cm);
		contact_list = attr_list_from_string(remote_stone_contact.str().c_str());

		// remember the output stone id
		output_stone_ids.push_back(output_stone_id);

		EVassoc_bridge_action(cm, output_stone_id, contact_list, remote_stone_id);
		// the below call adds the output stone to the list of stones
		// to which the split stone will replicate data. The call requires
		// the stone to which the split action was registered and the
		// split action that was returned
		EVaction_add_split_target(cm, split_stone, split_action, output_stone_id);

		cout << "Output stone id: " << output_stone_id << ", created and added as a split target\n";
		free_attr_list(contact_list);
	}


	// ------------------------- the splitter part >>>>>>>>>>>>>


	// create dfg and join the dfg client
	dfg_client = EVdfg_assoc_client(cm, argv[1], argv[2]);

	// TODO not sure what -1 means (-1 indicates a stone id to which the data
	// will be submitted
	EVsource src_id = EVcreate_submit_handle(cm, -1, complex_format_list);
	EVsource src_for_splitter = EVcreate_submit_handle(cm, split_stone, complex_format_list);

	EVdfg_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), src_id);
//	EVdfg_register_sink_handler(cm, const_cast<char*>(MASTER_SOURCE_NAME.c_str()), complex_format_list,
//			(EVSimpleHandlerFunc) complex_handler);

	cout << argv[1] << " joining master\n";

	EVdfg_ready_wait (dfg_client);

	if (EVdfg_active_sink_count(dfg_client) == 0) {
		EVdfg_ready_for_shutdown(dfg_client);
	}

	if (EVdfg_source_active(src_id)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src_id, &rec, nullptr);
		// submit an event to be received by a remote stone
		EVsubmit(src_for_splitter, &rec, nullptr);
	}
	EVfree_source(src_id);

	return EVdfg_wait_for_shutdown(dfg_client);
}
