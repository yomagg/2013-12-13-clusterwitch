# @file readme.txt
# @date Created on: Aug 5, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com

ATTENTION
=========
Please pay attention which DFG api you are using - the old one or the new one 
and load appropriate modules.

PURPOSE
=======
Understand how to add a splitter to a dfg. 

DESCRIPTION
===========
dfg_master deploys DFG and registers as a sink. In addition it creates an 
output stone to receive independently an event from dfg_client that comes
through an independent source.

dfg_client registers as a source in DFG, and in addition it creates a splitter
stone, and an independent source. It also connects the independent source to a
remote output stone created in dfg_master (it gets that information through the 
input parameters provided to dfg_client). And sends the same event, however,
twice - once to the source connectect to the DFG and once to the source
connected to the splitter stone.

I tried to configure the src connected to DFG to connect directly to the
splitter stone it didn't work (tried to change the -1 parameter to splitter_stone).

BUILD
=====

> scons
> scons --help

RUNNING
=======
First run the master, and master will print the
instruction how to run a worker

> ./dfg_master 

# EOF
