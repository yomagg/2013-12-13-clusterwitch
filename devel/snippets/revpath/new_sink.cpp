/**
 * @file new_sink.cpp
 *
 * @date Created on: Dec 10, 2013
 * @author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>

#include "evpath.h"
#include "revpath.h"

#include "data.h"

using namespace std;

static int complex_handler(CManager cm,  void *vevent, void *client_data, attr_list attrs){
	complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "New sink got: (" << event->r << ", " << event->i << ")\n";

	return 1;
}

/**
 * splits the string
 * @param in_str The input string
 * @param delimiter The
 * @param output
 * @return
 */
int split(const char* in_str, const char* delimiter, vector<string> & output){
	string s = in_str;
	string del = delimiter;

	size_t pos = 0;
	string token;
	while ((pos = s.find(del)) != string::npos) {
		token = s.substr(0, pos);
		output.push_back(token);
		s.erase(0, pos + del.length());
	}
	output.push_back(s);

	return 0;
}

int main(int argc, char * argv[]){

	if (3 != argc){
		cout << "Usage: " << argv[0] << "how-long-to-run remote-splitter-stone-id:remote-splitter-action-id:contact-string\n";
		cout << "  how-long-to-run tells how long the " << argv[0] << " will run in seconds\n";
		cout << "  remote-splitter-stone-id:remote-splitter-action-id:contact-string contact info output by sender\n";
		cout << "Eg. " << argv[0] << "300 0:0:AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm;

	cm = CManager_create();
	//CMfork_comm_thread(cm);
	CMlisten(cm);

	// assuming that the format is splitter-stone-id:splitter-action-id:contact-string

	vector<string> sender_contact;
	split(argv[2], ":", sender_contact);
	attr_list sender_contact_list = attr_list_from_string(sender_contact[2].c_str());

	// initiate the connection with the sender; I need this for
	// creating of stones remotely
	CMConnection conn = CMinitiate_conn(cm, sender_contact_list);

	if (nullptr == conn){
		cout << "ERROR: initiate connection returned nullptr. Quitting ...\n";
		CMsleep(cm, 1);
		CManager_close(cm);
		return 1;
	}

	free_attr_list(sender_contact_list);
	// now do the magic
	// * create another remote output stone remotely (on sender)
	// * add the this stone as a splitter target

	// create an output stone remotely
	EVstone routput_stone_id = REValloc_stone(conn);
	EVaction rsplit_action_id = atoi(sender_contact[1].c_str());
	EVstone rsplit_stone_id = atoi(sender_contact[0].c_str());

	// create local stone that will be connected with the remote stone
	EVstone lterminal_stone_id = EValloc_stone(cm);
	EVassoc_terminal_action(cm, lterminal_stone_id, complex_format_list, complex_handler, nullptr);

	// connect routput_stone with the local_stone
	attr_list tmp_list = CMget_contact_list(cm);
	REVassoc_bridge_action(conn, routput_stone_id, tmp_list, lterminal_stone_id);
	free_attr_list(tmp_list);

	cout << "Created and bridged remote output stone: (remote-output-stone-id:local-terminal-stone-id) "
				<< routput_stone_id << ":" << lterminal_stone_id << "\n";

	// connect the new output stone to the splitter stone; it is recommended
	// that adding the split target is performed after all preparatory steps
	// have been accomplished such as creation of the remote-output and local-terminal
	// stones and their bridging, otherwise there might be situations
	// that evpath might complain that there is nothing that can consume events
	// that are being produced
	REVaction_add_split_target(conn, rsplit_stone_id, rsplit_action_id, routput_stone_id);
	cout << "Added a new splitter target\n";

	// specify how long the network will be serviced
	CMsleep(cm, atoi(argv[1]));

	// now try to clean things up on the remote side, i.e., remove
	// a splitter target and destroy the output stone (free stone does not
	// drain the event queue)
	//
	// This combination CMsleep(); REVaction_blah(); CMsleep() makes
	// sense because REVaction_blah returns void; actually REVdestroy_stone
	// returns int, so I don't know (TODO)
	REVaction_remove_split_target(conn, rsplit_stone_id, rsplit_action_id, routput_stone_id);

	if(1 != REVdestroy_stone(conn, routput_stone_id)){
		cout << "ERROR: Issues with destroying the remote output stone\n";
	} else {
		cout << "Remote split target removed. The remote output stone destroyed. Done.\n";
	}

	// service network for some time to let perform cleaning of the remote
	// stones
	CMsleep(cm, 10);

	CManager_close(cm);

	return 0;
}
