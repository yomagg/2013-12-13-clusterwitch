/**
 * @file  vec.cpp
 * @date  Sep 4, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */

#include <iostream>
#include <vector>

using namespace std;

const int MY_INT = 7;
const double MY_DOUBLE = 0.123;

struct s1 {
	int field1;
};

struct s2 {
	double field1;
};

class Base {
public:
	Base() = default;
	virtual ~Base() = default;

	virtual void print(){
		cout << "This is Base class\n";
	}

	virtual void read() = 0;

};

class A : public Base {
public:
	A() = default;
	~A()  = default;

	void print(){
		cout << "This is A class\n";
	}

	void read(){
		p.field1 = MY_INT;
	}

	s1* get_p() {return &p; } ;

protected:
	struct s1 p;
};

class B : public Base {
public:
	B() = default;
	~B() = default;

	void print(){
		cout << "This is B class\n";
	}

	void read(){
		p.field1 = MY_DOUBLE;
	}

	s2* get_p() {return &p; } ;

protected:
	s2 p;
};


int main(){
	vector<Base*> v;

	s1 * my_s1;
	s2 * my_s2;
	A a;
	B b;

	v.emplace_back(&a);
	v.emplace_back(&b);

	for(auto &i : v){
		i->print();
		i->read();
	}

	for(auto &i : v){
		A * p_a = dynamic_cast<A*>(i);
		if(p_a){
			cout << p_a->get_p()->field1 << " ";
		} else {
			B * p_b = dynamic_cast<B*>(i);
			cout << p_b->get_p()->field1 << " ";
		}
	}
	cout << "\n";


	cout << "Expected result:\n";
	a.print();
	b.print();

	cout << MY_INT << " " << MY_DOUBLE << "\n";

	return 0;
}
