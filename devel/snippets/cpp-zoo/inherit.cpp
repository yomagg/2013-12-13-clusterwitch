/**
 * @file  inherit.cpp
 * @date  Sep 5, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */


#include <iostream>

using namespace std;

const int MY_INT_A = 7;
const int MY_INT_B = 8;
const int MY_INT_BASE = 3;

class Base {
public:
	Base() : field(MY_INT_BASE) { cout << "Base const\n";};
	virtual ~Base() = default;

	virtual void print(){
		cout << "This is Base class: field=" << field << "\n";
	}

protected:
	int field;
};

class A : public  Base {
public:
	A()  { field = MY_INT_A; cout << "A const\n"; };
	~A()  = default;

	void print(){
		cout << "This is A class: field=" << field << "\n";
	}
};

class B : public  Base {
public:
	B()   { field = MY_INT_B; cout << "B const\n";};
	~B() = default;

	void print(){
		cout << "This is B class: field=" << field << "\n";
	}
};

class C : public A, public B {
public:
	C() { cout << "C const\n";};
	~C() = default;
	void print(){
		cout << "This is C::A class: field=" << A::field << "\n";
		cout << "This is C::B class: field=" << B::field << "\n";
	}
};

int main(){

	A a;
	B b;
	C c;

	cout << "\n";

	a.print();
	b.print();
	c.print();

	cout << "Expected result:\n";
	cout << "field=" << MY_INT_A << "\n";
	cout << "field=" << MY_INT_B << "\n";


	return 0;
}


