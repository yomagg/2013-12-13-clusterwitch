/**
 * @file  template_member.cpp
 * @date  Sep 9, 2014
 * @author Magdalena Slawinska magg dot gatech at gmail.com
 */
#include <iostream>

class A {
public:
	A() = default;
	~A() = default;

	template<class SPY_TYPE>
	void get_spy(SPY_TYPE *index);
};

template<class SPY_TYPE>
void A::get_spy(SPY_TYPE *index){
	*index = 10;
	std::cout << index << "\n";
}



int main(){

	A a;
	double index;

	a.get_spy<double>(&index);

	return 0;
}
