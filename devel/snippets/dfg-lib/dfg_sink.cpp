/**
 *  @file   dfg_sink.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

// include the formats and headers
#include "evfmts.h"

int main(int argc, char **argv) {

	if (argc != 1) {
		cout << "Usage: " << argv[0] << "\n";
		return 1;
	}

	CManager cm;

	cm = CManager_create();

	sink_join_info sink_info;
	sink_info.node_name = const_cast<char*>("node1");
	sink_info.stone_name = const_cast<char*>("a_si");
	sink_info.simple_format_list = simple_format_list;
	sink_info.handler_func = (EVSimpleHandlerFunc) simple_handler;

	dfg_reg_sink(cm, &sink_info);

	cout << "sink joined\n";

	join_info info;
	info.node_name = const_cast<char*>(sink_info.node_name);
	info.master = nullptr;
	if (dfg_join(cm, &info, nullptr, sink_info.sink_caps) != DIAG_OK) {
		cout << "ERROR: issues with dfg_join_graph\n";
		return 1;
	}
	client = info.client;
	cout << "DEBUG: Node " << info.node_name << " joined the graph ... SUCCESS\n";

	int status = EVclient_wait_for_shutdown(client);
	CManager_close(cm);

	return status;
}

