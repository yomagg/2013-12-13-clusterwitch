/**
 *  @file   dfg_master.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "evpath.h"
#include "ev_dfg.h"

#include "dfg.h"

int main(int argc, char **argv){
	if( argc != 1){
		cout << "USAGE\n" << argv[0] << "\n";
		return 1;
	}

	CManager cm;
	cm = CManager_create();
	CMlisten(cm);

	DFG *dfg = dfg_create(cm);

	CMrun_network(cm);

	return 0;
}
