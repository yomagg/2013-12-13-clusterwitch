/**
 *  @file   evfmts.h
 *
 *  @date   Created on: Apr 1, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#ifndef EVFMTS_H_
#define EVFMTS_H_

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"

static EVclient client;

typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] = {
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {nullptr, nullptr, 0, 0}
};
static FMStructDescRec simple_format_list[] = {
    {"simple", simple_field_list, sizeof(simple_rec), nullptr},
    {nullptr, nullptr, 0, nullptr}
};

int simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
    simple_rec_ptr event = static_cast<simple_rec_ptr>( vevent);
    cout << "Received: " <<  event->integer_field << "\n";
    dfg_shutdown(client);
    return 0;
}


#endif /* EVFMTS_H_ */
