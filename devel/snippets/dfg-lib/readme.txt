# @file readm.txt
# @date Created on Apr 2, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

DESCRIPTION
===========
The main source is the dfg_master.cpp
This creates the dfg and prints the contact string.

./dfg_master # creates a dfg (should be run first)
./dfg_src    # source
./dfg_sink   # sink 

The contact information is passed via file

I would recommend running slave2 before slave

nodes.txt : List of Nodes
stones.txt : List of stones for each node
links.txt : Linkages between stones (Format : Sink followed by sources)

BUILD
=====

# with the default EVPath
$ scons

# look for available options
$ scons --help

# EOF
