/**
 *  @file   dfg_src.cpp
 *
 *  @date   Created on: Mar 28, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */


#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"

using namespace std;

// include the format definitions
#include "evfmts.h"

int main(int argc, char **argv){

	if( argc != 1){
		cout << "USAGE\n" << argv[0] <<  "\n";
		cout << "EXAMPLE\n";
		cout << argv[0] << "\n";
		return 1;
	}

    source_join_info src_info;
    src_info.node_name = const_cast<char*>("node2");
    src_info.stone_name = const_cast<char*>("b_so");
    src_info.simple_format_list = simple_format_list;

    CManager cm;
    cm = CManager_create();
    if ( dfg_reg_source(cm, &src_info) != DIAG_OK ){
    	cout << "ERROR: issues with source registration\n";
    	return 1;
    }

    join_info info;

    info.node_name = src_info.node_name;
    info.master = nullptr;

    if ( dfg_join(cm, &info, src_info.source_caps, nullptr) != DIAG_OK){
    	cout << "ERROR: issues with joining DFG\n";
    	return 1;
    }

    client = info.client;

    if (EVclient_source_active(src_info.src)){
        simple_rec rec;
        rec.integer_field = 55;
        EVsubmit(src_info.src, &rec, nullptr);
        cout << "Submitted: " << rec.integer_field << "\n";

        if( dfg_shutdown(info.client) != DIAG_OK){
        	cout << "ERROR: issues with shutting down the client\n";
        } else {
        	cout << "DEBUG: shutdown ... SUCCESS\n";
        	CManager_close(cm);
        }
    }


    return 0;
}
