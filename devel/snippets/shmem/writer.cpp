/**
 *  @file   writer.cpp
 *
 *  @date   Created on: Jan 9, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>

using namespace std;

const int SHMSZ = 27;


int main(int argc, char* argv[]){

	char c;
	int shmid;
	key_t key;	// this type seems to be an int
	char *shm, *s;

	/*
	 * We'll name our shared memory segment
	 * "5678".
	 */
	key = 5678;

	/*
	 * Create the segment.
	 */
	if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*
	 * Now we attach the segment to our data space.
	 */
	if ((shm = static_cast<char*>(shmat(shmid, NULL, 0))) == reinterpret_cast<char *>( -1)) {
		perror("shmat");
		return 1;
	}

	/*
	 * Now put some things into the memory for the
	 * other process to read.
	 */
	s = shm;

	for (c = 'a'; c <= 'z'; c++)
		*s++ = c;
	*s = '\0';

	/*
	 * Finally, we wait until the other process
	 * changes the first character of our memory
	 * to '*', indicating that it has read what
	 * we put there.
	 */
	while (*shm != '*'){
		sleep(1);
	}

	return 0;
}
