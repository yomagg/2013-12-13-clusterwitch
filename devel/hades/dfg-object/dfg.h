/**
 *  @file   dfg.h
 *
 *  @date   Created on: Mar 27, 2014
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 *
 *  Reused Aditi's file
 */
#ifndef DFG_H_
#define DFG_H_

#include <string.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <map>
#include <vector>

#include "evpath.h"
#include "ev_dfg.h"

using namespace std;

struct join_info{
	char* node_name;
	char* stone_name;
	FMStructDescList simple_format_list;
	//! needed for EVclient_assoc(_local), can be null
	EVclient_sinks sinks_caps;
	//! needed for EVclient_assoc(_local), can be null
	EVclient_sources source_caps;

	EVdfg node_dfg;
	EVmaster master;
	EVclient client;
};
typedef struct join_info join_info;

struct sink_join_info : join_info{
	EVSimpleHandlerFunc handler_func;
	void * client_data;
};
typedef struct sink_join_info sink_join_info;

struct source_join_info:join_info{
	EVsource *src;
};
typedef struct source_join_info source_join_info;

struct node_info
{
	char* name;
	int num_src;
	int num_sink;
	std::map<string,EVdfg_stone> source_list;
	std::map<string,EVdfg_stone> sink_list;

};
typedef struct node_info node_info;

struct stone_info
{
	EVdfg_stone stone_id;
	int src_count;
};
typedef struct stone_info stone_info;

struct DFG
{
	//! holds the handle for the managing the virtual stones
	EVdfg dfg;
	//! master of the dfg
	EVmaster master;
	//! dfg client
	EVclient client;

	char **nodes;
	//! the contact to the master dfg; the general contact point
	//! for this DFG
	char* dfg_contact;
	const char* master_node;
	std::map<string,node_info> name_node_map;
	std::map<string,stone_info> all_stone_map;
	std::map<string,string>stone_to_node_map;
};
typedef struct DFG DFG;

struct DFG_config_info
{
	char node_list[200];
	char stone_list[200];
	char link_list[200];
};

typedef struct DFG_config_info DFG_config_info;
//int dfg_source_join(CManager cm,source_join_info *source_info);
int dfg_register_source(CManager cm,source_join_info *src_info);
//int dfg_sink_join(CManager cm,sink_join_info *sink_info);
int dfg_register_sink_handler(CManager cm,sink_join_info *sink_info);

int dfg_join_graph(CManager cm, join_info *info, bool local);
//int dfg_associate_another_action(char *sink_name,char *action_name);
int get_num_file_lines( char *filename);
void dfg_assign_names(DFG *dfg,char *filename);
void dfg_create_stones(DFG *dfg,char *file);
void dfg_link_stones(DFG *dfg, char *file);
int dfg_master_shutdown(DFG *dfg);
int dfg_node_shutdown(EVdfg dfg);
int dfg_slave_shutdown(EVdfg dfg);
DFG* dfg_create(CManager cm,DFG_config_info config_info);

/**
 * I am not sure if I need this object but maybe yes,
 * so as a common base for the Dfg objects.
 */
class Dfg_Object {
public:

	Dfg_Object() = default;
	~Dfg_Object() = default;
};
/**
 * Wrapper primarily for operation on EVmaster. I was told
 * by Greg E. that it can be many EVmasters within a single process
 * as long as there is one per CM. Since I assume one CM per process
 * I also assume Dfg_Manager is a singleton object.
 */
class Dfg_Manager : public Dfg_Object {
public:
	static Dfg_Manager * get_instance();
	static bool init_mgr();
	static bool create_mgr();

	static EVmaster get_EVmaster(CManager cm);
//	static string get_master_contact_list(CManager cm);

	static EVdfg get_EVdfg();

protected:
	//! the master of the DFG; responsible for the organizing the DFG
	EVmaster master;
	//! contact string to the master
	string contact_list;
	//! a connection manager within which the master is created
	CManager cm;
	//! responsible wiring and connecting the user DFG with
	//! its instantiation
	EVdfg mgr;

	//! this will store my manager instance
	static Dfg_Manager *p_dfg_mgr;
	//! this is the name of the master contact file
	static string MASTER_CONTACT_FILE;

	//! the name of the file containing node names
	static string NODE_FILE;
	//! the name of the file containing links
	static string LINK_FILE;
	//! the name of the file containing stones per node
	static string STONE_FILE;

	Dfg_Manager();
	virtual ~Dfg_Manager() = default;

	//! to satisfy the singleton object requirements
	Dfg_Manager(const Dfg_Manager &) = delete;
	Dfg_Manager & operator=(const Dfg_Manager &) = delete;

private:
	struct v_stone {
		EVdfg_stone id;
		int out_degree;
		v_stone() : out_degree(0){}
	};

	static bool get_dfg_nodes(vector<char*> & nodes);
	static bool get_dfg_stones(map<string, v_stone> & v_stone_map);
	static bool get_dfg_links(map<string, v_stone> & v_stone_map);


};

#endif /* DFG_H_ */
