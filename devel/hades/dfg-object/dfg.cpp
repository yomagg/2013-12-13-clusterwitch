#include <iostream>
#include <fstream>
#include <sstream>
#include <assert>


#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <map>
#include <string>
//#include <assert.h>
//#include "config.h"
#include "evpath.h"
#include "ev_dfg.h"
#include "dfg.h"
#include <unistd.h>

#define MASTER_CONTACT "master-contact.txt"
//! diagnostic information
#define DIAG_OK  0
#define DIAG_ERR 1

using namespace std;

 
#include "easylogging++.h"

//int count=0;

/**
 * gets the master contact from the MASTER_CONTACT file
 *
 * @param buf where to store the master contact string, must be big enough
 *            and preallocated
 * @return DIAG_OK everything went fine
 *         DIAG_ERR some errors
 */
static int get_master_contact( char * buf){
	FILE *fp = fopen(MASTER_CONTACT,"r");

    if(fp) {
    	fscanf(fp,"%s", buf);
    	fclose(fp);
    } else {
    	printf("\nERROR: Master contact file not found. Returning an error code\n");
    	return DIAG_ERR;
    }
    return DIAG_OK;
}


static int get_num_file_lines( char *filename)
{
	FILE *fp=fopen(filename,"r");
	int count=0;
	int c;
	while((c=fgetc(fp))!=EOF)
	{
		if(c=='\n')
		count++;

	}
		
	//printf("\n count%d\n",count);
	return count;
}


/**
 * reads the names of the nodes from the file
 * @param dfg
 * @param filename
 */
void dfg_assign_names(DFG *dfg,char* filename)
{
	DFG *dfg_1=dfg;
	int i=0;
	
	FILE *fp=fopen(filename,"r");
	char buf[200];
	if(fp == NULL)
	{
		printf("\n File to assign Node names not found");
		exit(1);
	}
	while(fscanf(fp,"%s",buf)!=EOF)
	{
		dfg_1->nodes[i]=strdup(buf);
		node_info node_info_struct;
		node_info_struct.name=strdup(dfg_1->nodes[i]);
		char* node_name=strdup(dfg_1->nodes[i]);
		printf(" \ninserting %s of size strlen %d \n",dfg_1->nodes[i],strlen(dfg_1->nodes[i]));
		string str(node_name);
		dfg_1->name_node_map[str]=node_info_struct;
		
		i++;
	}
	
	
	dfg_1->nodes[i]=NULL;

}

/**
 * Create source and sink stones
 *
 * @param dfg_1
 * @param file
 */
void dfg_create_stones(DFG *dfg_1,char * file)
{
//DFG *dfg_1=dfg;
FILE *fp;
char node_name[200];
char stone_name[200];
char buf[20];
fp= fopen(file,"r");
if(fp == NULL)
{
	printf("\n File to create stones not found \n");
	exit(1);
}


	while(fscanf(fp,"%s",buf)!=EOF)
	{
		//printf("\n here1");
		if(strcmp(buf,"*")==0)
		{
			//printf("\n here2");
			fscanf(fp,"%s",node_name);
			std::string node_name_str(node_name);
			fscanf(fp,"%s",stone_name);
			while(strcmp(stone_name,"src_end")!=0)
			{
				//fscanf(fp,"%s",stone_name);
				std::string stone_name_str(stone_name);
				printf("\n Creating source %s \n",stone_name);
				EVdfg_stone src=EVdfg_create_source_stone(dfg_1->dfg,stone_name);
				printf("\n Assigning stone %s to node %s  \n",stone_name,node_name);
				EVdfg_assign_node(src,node_name);
				//printf("\n Inserting stone %s : %d to all-stone-map\n",stone_name,src);
				stone_info stone;
				stone.stone_id=src;
				stone.src_count=0;
				dfg_1->all_stone_map[stone_name_str]=stone;
				printf("\n Inserting stone %s : %d to all-stone-map\n",stone_name,dfg_1->all_stone_map[stone_name_str].stone_id);
				printf("\n COUNT : %d\n",dfg_1->all_stone_map[stone_name_str].src_count);
				//insert_node_source_list(dfg_1,node_name_str,stone_name_str,src);
				fscanf(fp,"%s",stone_name);

			}
			 fscanf(fp,"%s",stone_name);
			 while(strcmp(stone_name,"sink_end")!=0)
			 {

			 	std::string stone_name_str(stone_name);
				printf("\n Creating sink %s \n",stone_name);
				EVdfg_stone sink=EVdfg_create_sink_stone(dfg_1->dfg,stone_name);
				printf("\n Assigning stone %s to node %s  \n",stone_name,node_name);
				EVdfg_assign_node(sink,node_name);
				//printf("\n Inserting stone %s : %d to all-stone-map\n",stone_name,sink);
				stone_info stone;
				stone.stone_id=sink;
				stone.src_count=0;
				dfg_1->all_stone_map[stone_name_str]=stone;
				printf("\n Inserting stone %s : %d to all-stone-map\n",stone_name,dfg_1->all_stone_map[stone_name_str].stone_id);
				//insert_node_sink_list(dfg_1,node_name_str,stone_name_str,sink);
				fscanf(fp,"%s",stone_name);
			 }
		}
	}


}



void dfg_link_stones(DFG *dfg, char *file)
	{
		DFG *dfg_1=dfg;
		FILE *fp = fopen(file,"r");
		char buf[200];
		string sink;
		EVdfg_stone sink_id;
		if( fp == NULL)
		{
			printf("\n File to link stones not found \n");
			exit(1);
		}
			
		while(fscanf(fp,"%s",buf)!=EOF)
		{
			if(strcmp(buf,"*")==0)
			{
				fscanf(fp,"%s",buf);
				sink=strdup(buf);
				sink_id = dfg_1->all_stone_map[sink].stone_id;
				printf("\n SINK:%s ID : %d \n",sink.c_str(),sink_id);

			}
			else
			{
				// all these are sources to the previous sink
				std::string source_str(buf);
				int out_degree=dfg_1->all_stone_map[source_str].src_count;
				EVdfg_stone src_id = dfg_1->all_stone_map[source_str].stone_id;
				assert(src_id);
				printf("\n Linking stone %s:%d and %s:%d with port-count %d\n",source_str.c_str(),src_id,sink.c_str(),sink_id,out_degree);
				EVdfg_link_port(src_id,out_degree,sink_id);
				dfg_1->all_stone_map[source_str].src_count++;
				printf("\n New count is %d \n",dfg_1->all_stone_map[source_str].src_count);


			}
		}
		// look at file,find each stone and make the links
	}


DFG* dfg_create(CManager cm,DFG_config_info dfg_info)
{
	DFG *dfg_1 = new DFG();
	FILE *fp;
	
	dfg_1->master=EVmaster_create(cm);
	dfg_1->dfg_contact = EVmaster_get_contact_list(dfg_1->master);
	int num_participating_nodes=get_num_file_lines(dfg_info.node_list);
	
	dfg_1->nodes=(char**) malloc(sizeof(char*)*num_participating_nodes); // this 4 needs to be reconfigurable
	
	// reads the name
	dfg_assign_names(dfg_1,dfg_info.node_list);

	// write DFG contact string to the file
	printf("\n DFG Contact String %s\n",dfg_1->dfg_contact);
	fp=fopen(MASTER_CONTACT,"w+");
	fprintf(fp,"%s",dfg_1->dfg_contact);
	fclose(fp);

	EVdfg_register_node_list(dfg_1->master, dfg_1->nodes);
	
	dfg_1->dfg = EVdfg_create(dfg_1->master);
	
	dfg_create_stones(dfg_1,dfg_info.stone_list);
	dfg_link_stones(dfg_1,dfg_info.link_list);
	EVdfg_realize(dfg_1->dfg);

	
   return dfg_1;
   
}

int dfg_register_source(CManager cm,source_join_info *src_info)
{
	assert(cm!=NULL);
	assert(src_info!=NULL);
	EVsource src = EVcreate_submit_handle(cm, DFG_SOURCE, (src_info->simple_format_list));
	if(src == NULL)
		return 0;
	*(src_info->src)=src;
	// I can ignore source_caps because EVdfg keeps track of them
	// and later I can just call assoc function with a NULL parameter for source
	// capabilities
	EVclient_sources source_caps = EVclient_register_source(src_info->stone_name, *(src_info->src) );


 	return 1;
}
/**
 * @not ported, don't use it
 */
/*int dfg_source_join(CManager cm,source_join_info *src_info)
{
	char mastercontact[200];
	int result = 0;
	assert(cm!=NULL);
	assert(src_info!=NULL);
	dfg_register_source(cm,src_info);
	FILE *fp = fopen(MASTER_CONTACT,"r");
    if(fp!=NULL){
    	fscanf(fp,"%s",mastercontact);
    	fclose(fp);
    } else {
    	printf("\n Master contact file not found\n");
    	result = -1;
    }
    if((result != -1) && mastercontact){
        printf("\nmastercontact:%s\n",mastercontact);
    	EVdfg_join_dfg(src_info->node_dfg,src_info->node_name,mastercontact);
	}

	return result;

}*/
/**
 * Pure join the dfg, no registration of handlers. I needed that function
 * in CW, otherwise it was difficult to implement a generic Sink and Source
 * atoms.
 *
 * The node joins the DFG
 *
 * @author Magdalena Slawinska
 * @param cm
 * @param info needs the info.node_name and node_dfg; the rest don't care;
 *             if local true the info->master is required
 * @param true local if this is local association (within master process)
 *        false otherwise, i.e., non-master client
 * @return DIAG_OK If everything went fine
 *         DIAG_ERR Some errors, e.g. the master file contact has been not found
 *
 */
int dfg_join_graph(CManager cm, join_info *info, bool local){
	char mastercontact[200];
	assert(cm);
	assert(info);

	if( get_master_contact(mastercontact) != DIAG_OK) {
		cout << "ERROR: got error from get_master_contact(). Returning error\n";
		return DIAG_ERR;
	}

    if(mastercontact){
        cout << "mastercontact: " << mastercontact <<"\n";
        if(local){
        	info->client = EVclient_assoc_local(cm, info->node_name, info->master, info->source_caps, info->sinks_caps);
        } else {
        	info->client = EVclient_assoc(cm, info->node_name, mastercontact, info->source_caps, info->sinks_caps);
        }
	} else {
		printf("ERROR: The master contact has been read incorrectly from "
				"the master file contact\n");
		return DIAG_ERR;
	}

	return DIAG_OK;
}

int dfg_register_sink_handler(CManager cm,sink_join_info *sink_info){
	assert(cm!=NULL);
	assert(sink_info!=NULL);
	sink_info->sink_caps = EVclient_register_sink_handler(cm,sink_info->stone_name,sink_info->simple_format_list,sink_info->handler_func, sink_info->client_data);

	return 1;
}
/*
 * not ported
 *
int dfg_sink_join(CManager cm,sink_join_info *sink_info)
{
	char mastercontact[200];

	assert(cm!=NULL);
	assert(sink_info!=NULL);
	dfg_register_sink_handler(cm,sink_info);
	

	FILE *fp = fopen(MASTER_CONTACT,"r");
	if( fp == NULL)
	{
		printf("\n Master Contact file not found\n");
		exit(1);
	}
    if(fp!=NULL)
    fscanf(fp,"%s",mastercontact);
    fclose(fp);
    if(mastercontact)
    {

        printf("mastercontact:%s",mastercontact);
        printf("\n joining dfg");
    	EVdfg_join_dfg(sink_info->node_dfg,sink_info->node_name,mastercontact);
	}
	return 1;
}
*/
/**
 * Pure join the dfg, no registration of handlers. I needed that function
 * in CW, otherwise it was difficult to implement a generic Sink and Source
 * atoms.
 * @author Magdalena Slawinska
 * @param cm
 * @param info
 * @return 0 If everything went fine
 *         1 Some errors, e.g. the master file contact has been not found
 */
int dfg_sink_join_only(CManager cm, sink_join_info *info){
	char mastercontact[200];
	int diag_ok = 0;
	int diag_err = 1;

	assert(cm);
	assert(info);

	FILE *fp = fopen(MASTER_CONTACT,"r");
	if( fp ){
		fscanf(fp,"%s",mastercontact);
		fclose(fp);
	} else {
		printf("\nERROR: Master Contact file not found\n");
		return diag_err;
	}

    if(mastercontact){
        printf("mastercontact:%s joining dfg",mastercontact);
    	EVdfg_join_dfg(info->node_dfg,info->node_name,mastercontact);
    	printf("Sink named: %s joined the DFG (mastercontact: %s)\n",
    			info->node_name, mastercontact);
	} else {
		printf("ERROR: the mastercontact string not read correctly from the "
				"mastercontact file. Returning error.\n");
		return diag_err;
	}
	return diag_ok;
}


int dfg_master_shutdown(DFG *dfg_1)
{
	int status;

status = EVdfg_wait_for_shutdown(dfg_1->dfg);
unlink(MASTER_CONTACT);
free(dfg_1->nodes);
delete dfg_1;
return status;
}

int dfg_node_shutdown(EVdfg test_dfg)
{
	EVdfg_wait_for_shutdown(test_dfg);
}

int dfg_slave_shutdown(EVdfg dfg){
	if (EVdfg_active_sink_count(dfg) == 0) {
	   EVdfg_ready_for_shutdown(dfg);
	}

	return EVdfg_wait_for_shutdown(dfg);
}


// ---------------------------------
// Dfg_Manager
// ---------------------------------
//! global initialization to facilitate a singleton design pattern
Dfg_Manager * Dfg_Manager::p_dfg_mgr = nullptr;

//! the name of the contact file for the master
string Dfg_Manager::MASTER_CONTACT_FILE = "master-contact.txt";

string Dfg_Manager::NODE_FILE = "nodes.txt";
string Dfg_Manager::LINK_FILE = "links.txt";
string Dfg_Manager::STONE_FILE = "stones.txt";

/**
 * Initializes the singleton object
 */
Dfg_Manager::Dfg_Manager() : master(nullptr), cm(nullptr), mgr(nullptr) {
}

/**
 * This will get the instance of the Dfg_Manager
 *
 * @return The instance of this object
 */
Dfg_Manager *Dfg_Manager::get_instance(){
	if( !p_mgr){
		p_mgr = new Dfg_Manager();
	}
	assert(p_mgr);
	return p_mgr;
}

/**
 * Initializes the Dfg Manager by create EVmaster and dumping
 * its contact information to the file. It attempts to detect if it was
 * already initialized and returns with a true value
 *
 * @param cm
 * @return true always
 */
bool Dfg_Manager::init_mgr(CManager cm){
	if(master){
		LOG(WARNING) << "The Dfg manager has been already initialized. Returning";
		return true;
	}
	master = EVmaster_create(cm);
	contact_list = EVmaster_get_contact_list(master);

	// dump the contact list to the file
	ofstream contact_file;
	contact_file.open(Dfg_Manager::MASTER_CONTACT_FILE);
	contact_file << contact_list;
	contact_file.close();

	// create the manager of the DFG
	mgr = EVdfg_create(master);
	assert(mgr);

	return true;
}

/**
 * Registers node names, creates a DFG based on LINK, STONE, and NODE files.
 *
 * @return true always
 */
bool Dfg_Manager::create_mgr(){
	assert(master);
	assert(!mgr);

	// read nodes from the file

	// register nodes list
	vector<char*> nodes;
	get_dfg_nodes(nodes);
	// EVmaster takes care of copying the values
	EVmaster_register_node_list(master, &nodes[0]);

	// get stones
	map<string, EVdfg_stone> v_map_stones;
	get_dfg_stones(v_map_stones);

	// get links
	get_dfg_links(v_map_stones);


	return true;
}

/**
 * Get the dfg nodes' names and put it into nodes vector. Currently
 * the node names are read from a file
 *
 * @param nodes the vector containing the null as the last element
 *              of names
 * @return true everything went fine
 */
bool Dfg_Manager::get_dfg_nodes(vector<char*> & nodes){
	// get the node file
	ifstream f;
	f.open(Dfg_Manager::NODE_FILE);

	string node_name;
	while(f >> node_name){
		nodes.emplace_back(node_name.c_str());
	}
	nodes.emplace_back(nullptr);

	f.close();

	return true;
}

/**
 * Reads the stones from the file Dfg_Manager::STONE_FILE,
 * creates virtual stones to represent sources and sinks,
 * assigns stones to nodes in DFG
 *
 * @param v_stone_map (OUT)	the mapping between the stone name
 *                      (either source or sink) and the virtual stone
 *
 * @return true;
 */
bool Dfg_Manager::get_dfg_stones(map<string, v_stone> & v_stone_map){
	EVdfg my_mgr = Dfg_Manager::get_EVdfg();

	ifstream f;
	f.open(Dfg_Manager::STONE_FILE);

	string line;

	// current node name
	string node;

	while( getline(f, line)){
		string delim = " ";
		size_t pos = 0;
		string token;

		pos = line.find(delim);

		token = line.substr(0, pos);
		if( token == "*"){
			// this is a new node
			line.erase(0, pos + delim.length());
			line.find(delim);
			node = line.substr(0, pos);

			// read source line
			getline(f, line);

			while( (pos = line.find(delim)) != string::npos ){
				token = line.substr(0, pos);
				if( token != "src_end"){
					// token has the name of the source
					v_stone stone;
					stone.id = EVdfg_create_source_stone(my_mgr, const_cast<char*>( token.c_str() ));
					EVdfg_assign_node(stone.id, const_cast<char*>(node.c_str());
					v_stone_map[token] = stone;
					s.erase(0, pos + delim.length());
				}
			}

			getline(f, line);

			// now read sink line
			while( (pos = line.find(delim)) != string::npos ){
				token = line.substr(0, pos);
				if( token != "sink_end"){
					// token has the name of the sink
					v_stone stone;
					stone.id = EVdfg_create_sink_stone(my_mgr, const_cast<char*>( token.c_str() ));
					EVdfg_assign_node(stone.id, const_cast<char*>(node.c_str());
					v_stone_map[token] = stone;
					s.erase(0, pos + delim.length());
				}
			}
		}
	}

	f.close();

	return true;
}

/**
 * Read links from the file Dfg_Manager::LINK_FILE and do the linking
 * of sources and sinks.
 *
 * @param v_stone_map (INOUT)
 * @return true always
 */
bool Dfg_Manager::get_dfg_links(map<string, v_stone> & v_stone_map){
	EVdfg my_mgr = Dfg_Manager::get_EVdfg();

	ifstream f;
	f.open(Dfg_Manager::LINK_FILE);

	string line;

	while( getline(f, line)){
		string delim = " ";
		size_t pos = 0;
		string token;
		string sink;

		pos = line.find(delim);

		token = line.substr(0, pos);
		if( token == "*"){
			line.erase(0, pos + delim.length());
			line.find(delim);
			sink = line.substr(0, pos);
			line.erase(0, pos + delim.length());

			// all these are sources to the previous sink
			while( (pos = line.find(delim)) != string::npos ){
				string stone_name = line.substr(0, pos);
				int out_degree = v_stone_map[src].out_degree;
				EVdfg_stone id = v_stone_map[src].id;
				assert(id);
				LOG(DEBUG) << "Linking stone " << stone_name << ":" << id
						<< " with " << sink << ":" << v_stone_map[sink].id <<
						" with port-count " << out_degree;
				EVdfg_link_port(id, out_degree, v_stone_map[sink].id);
				++v_stone_map[src].out_degree;
			}
		}
	}

	f.close();

	return true;
}

/**
 * getter for EVdfg
 * @return
 */
EVdfg Dfg_Manager::get_EVdfg(){
	return mgr;
}

/**
 * gets the EVmaster reference, initializing it if necessary
 *
 * @param cm The connection manager that is associated with this EVmaster
 *           or will be associated
 * @return EVmaster the EVmaster
 */
EVmaster Dfg_Manager::get_EVmaster(CManager cm){
	Dfg_Manager * mgr = Dfg_Manager::get_instance();
	assert(mgr);

	if( !master ){
		master = EVmaster_create(cm);
		contact_list = EVmaster_get_contact_list(master);
		this->cm = cm;
	}

	assert(master);
	if (this->cm != cm){
		LOG(ERROR) << "Detected different CManagers: " << this->cm << " and " << cm;
	}

	return master;
}
